<?php

return [
    'fallback' => [
        'theme' => 'classic',
        'name' => '',
        'email' => '',
        'phone' => '',
        'phone__hiding_part' => 0,
        'phone__show_mobile_in_sidebar' => 0,
        'phone__bottom_text' => 'Отдел продаж',
        'city' => '',
        'address' => '',
        'schedule' => 'C 8:00 до 20:00, без выходных',
        'advert__percent' => 0,
        'credit_percent' => 7.9,
        'credit_max_month' => 84,
        'credit_amount_from' => 90000,
        'initial_fee' => 10,
        'yandex__map' => '',
        'footer__text' => file_get_contents(storage_path('app/setting_default/footer__text.txt')),
        'footer__text_hide' => file_get_contents(storage_path('app/setting_default/footer__text_hide.txt')),
        'footer__contact_text' => '',

        'yandex__id' => '',
        'yandex__id2' => '',
        'yandex__use' => 1,
        'google__id' => '',
        'mail__id' => '',

        'yandex__goals__all' => '',
        'yandex__goals__credit' => '',
        'yandex__goals__credit_modal' => '',
        'yandex__goals__trade_in' => '',
        'yandex__goals__trade_in_exchange' => '',
        'yandex__goals__buy' => '',
        'yandex__goals__promo' => '',
        'yandex__goals__contact' => '',
        'yandex__goals__subscribe' => '',
        'yandex__goals__phone_click' => '',

        'google__goals__all' => '',
        'google__goals__credit' => '',
        'google__goals__credit_modal' => '',
        'google__goals__trade_in' => '',
        'google__goals__trade_in_exchange' => '',
        'google__goals__promo' => '',
        'google__goals__contact' => '',
        'google__goals__subscribe' => '',
        'google__goals__phone_click' => 'cliсktell',

        'mail__goals__all' => '',

        'logo__head' => '',
        'logo__foot' => '',
        'favicon' => '',

        'page__credit_enable' => 1,
        'page__trade_in_enable' => 1,
        'page__buy_enable' => 1,
        'page__promo_enable' => 1,
        'page__carservice_enable' => 0,
        'page__about_enable' => 0,
        'page__catalog_autoload' => 0,
        'page__promo_header_text_enable' => 0,
        'page__promo_header_text' => 'Выгодные акции при покупке автомобиля в автосалоне [[name]] в [[city]]. Автокредитование от [[credit_percent]]% в год, комлект зимней резины, 300 литров бензина и прочие акции в нашем автосалоне',
        'page__contact_subscribe_show' => 0,

        'title' => 'Авто с пробегом',
        'page__description' => 'Продажа поддержанных авто от дилера',
        'page__main_title' => 'Главная страница | [[title]]',
        'page__main_description' => 'Продажа поддержанных авто от дилера | [[title]]',
        'page__credit_title' => 'Автомобили с пробегом в кредит от [[credit_percent]]% годовых | [[title]]',
        'page__credit_description' => 'Выгодные условия автокредитования от [[credit_percent]]% годовых | [[title]]',
        'page__trade_in_title' => 'Трейд-ин (Trade-in): Обмен автомобилей с пробегом',
        'page__trade_in_description' => 'Быстрый обмен старого автомобиля на новый.',
        'page__buy_title' => 'Выкуп автомобилей с пробегом. Дорого. Быстро. Надежно.',
        'page__buy_description' => 'Выкуп автомобилей Б/У с проблемами и без бюджетного, среднего и премиального класса, за максимальную цену. Бесплатная оценка транспортного средства на ходу и нет. Доставка эвакуатором',
        'page__promo_title' => 'Акции, скидки, подарки',
        'page__promo_description' => 'Выгодные акции для клиентов автосалона [[name]].',
        'page__promo_item_title' => 'Получи [[promo_name]]',
        'page__promo_item_description' => '[[full_description]]',
        'page__contact_title' => 'Контакты автосалона [[name]]',
        'page__contact_description' => 'Телефон: [[phone]], почта: [[email]], наш адрес: [[address]], график работы: [[schedule]]',
        'page__catalog_title' => 'Купить автомобиль с пробегом в Тюмени, Б/У автомобили в автосалоне [[name]]',
        'page__catalog_description' => 'Продажа автомобилей с пробегом в кредит или Trade-in. Большое выбор б/у автомобилей известных марок с фото и характеристиками в [[city]].',
        'page__catalog_brand_title' => 'Купить [[brand]] с пробегом в [[city]], Б/У автомобили в автосалоне [[name]]',
        'page__catalog_brand_description' => 'Продажа [[brand]] с пробегом в кредит или Trade-in. Большое выбор б/у автомобилей известных марок с фото и характеристиками в [[city]].',
        'page__catalog_brand_model_title' => 'Купить [[brand]] [[model]] с пробегом в [[city]], Б/У автомобили в автосалоне [[name]]',
        'page__catalog_brand_model_description' => 'Продажа [[brand]] [[model]] с пробегом в кредит или Trade-in. Большое выбор б/у автомобилей известных марок с фото и характеристиками в [[city]].',
        'page__advert_title' => 'Купить [[brand]] [[model]] [[year_of_issue]] г.в. [[mileage]] [[transmission]] [[engine]]/[[volume]] см3 за [[price_by_action]] руб. скидка [[discount]] руб. в Кредит от [[credit_percent]]% годовых.',
        'page__advert_description' => 'Б/у автомобиль [[brand]] [[model]] [[year_of_issue]] года за [[price_by_action]] руб. Купить автомобиль с пробегом в автосалоне [[name]]',

        'yandex__feed' => null,

        'api_token' => '-',
        'marquizru__id' => '',
        'calltouch__id' => '',
        'pixel__enable' => '',
        'head_scripts' => '',
        'footer_scripts' => '',
    ],






    /*
    |--------------------------------------------------------------------------
    | Enable / Disable auto save
    |--------------------------------------------------------------------------
    |
    | Auto-save every time the application shuts down
    |
    */
    'auto_save'         => false,

    /*
    |--------------------------------------------------------------------------
    | Cache
    |--------------------------------------------------------------------------
    |
    | Options for caching. Set whether to enable cache, its key, time to live
    | in seconds and whether to auto clear after save.
    |
    */
    'cache' => [
        'enabled'       => false,
        'key'           => 'setting',
        'ttl'           => 3600,
        'auto_clear'    => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Setting driver
    |--------------------------------------------------------------------------
    |
    | Select where to store the settings.
    |
    | Supported: "database", "json", "memory"
    |
    */
    'driver'            => 'database',

    /*
    |--------------------------------------------------------------------------
    | Database driver
    |--------------------------------------------------------------------------
    |
    | Options for database driver. Enter which connection to use, null means
    | the default connection. Set the table and column names.
    |
    */
    'database' => [
        'connection'    => null,
        'table'         => 'settings',
        'key'           => 'key',
        'value'         => 'value',
    ],

    /*
    |--------------------------------------------------------------------------
    | JSON driver
    |--------------------------------------------------------------------------
    |
    | Options for json driver. Enter the full path to the .json file.
    |
    */
    'json' => [
        'path'          => storage_path() . '/settings.json',
    ],

    /*
    |--------------------------------------------------------------------------
    | Override application config values
    |--------------------------------------------------------------------------
    |
    | If defined, settings package will override these config values.
    |
    | Sample:
    |   "app.locale" => "settings.locale",
    |
    */
    'override' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Required Extra Columns
    |--------------------------------------------------------------------------
    |
    | The list of columns required to be set up
    |
    | Sample:
    |   "user_id",
    |   "tenant_id",
    |
    */
    'required_extra_columns' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | Encryption
    |--------------------------------------------------------------------------
    |
    | Define the keys which should be crypt automatically.
    |
    | Sample:
    |   "payment.key"
    |
    */
   'encrypted_keys' => [

   ],

];
