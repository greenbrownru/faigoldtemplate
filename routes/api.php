<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('subscribes', App\Http\Controllers\Api\SubscribesController::class.'@index');
Route::get('v2/subscribes', App\Http\Controllers\Api\SubscribesController::class.'@index2');

Route::get('TargetFeed', App\Http\Controllers\Api\TargetFeedController::class.'@index');
