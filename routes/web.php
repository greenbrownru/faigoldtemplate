<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', 'App\Http\Controllers\AboutController@index');
Route::get('/about/personal_data', 'App\Http\Controllers\AboutController@personalData');
Route::get('/contacts', 'App\Http\Controllers\MainController@contacts');
Route::post('/contacts/request-add', 'App\Http\Controllers\MainController@contactsRequestAdd')->withoutMiddleware('auth')->name('contacts.request_add');
Route::post('/promo/request-add', 'App\Http\Controllers\PromoController@requestAdd')->withoutMiddleware('auth')->name('promo.request_add');
Route::get('/', 'App\Http\Controllers\MainController@index');
Route::get('/promo', 'App\Http\Controllers\PromoController@index');
Route::get('/promo/{promo}', 'App\Http\Controllers\PromoController@show');
//Route::get('/credit', 'App\Http\Controllers\CreditController@index');

Route::get('/subscribes', 'App\Http\Controllers\SubscribesController@index');
Route::get('/subscribes/approve', 'App\Http\Controllers\SubscribesController@approve');
Route::post('/subscribes/subscribe', 'App\Http\Controllers\SubscribesController@subscribe');
Route::get('/subscribes/unsubscribe', 'App\Http\Controllers\SubscribesController@unsubscribe');
Route::post('/subscribes/unsubscribe', 'App\Http\Controllers\SubscribesController@unsubscribe');
Route::get('/subscribes/unsubscribe-approve', 'App\Http\Controllers\SubscribesController@unsubscribeApprove');

Route::get('/adverts/{advert}', 'App\Http\Controllers\AdvertsController@advert')->name('advert');
Route::get('/catalog/{advert}', 'App\Http\Controllers\AdvertsController@advert');
Route::get('/adverts/{advert}/edit', 'App\Http\Controllers\AdvertsController@editAjax');
Route::post('/adverts/{advert}/edit', 'App\Http\Controllers\AdvertsController@editAjax');

Route::get('/credit/{advert?}', 'App\Http\Controllers\CreditController@index');
Route::post('/credit/ajax-models', 'App\Http\Controllers\CreditController@ajaxModels');
Route::post('/credit/ajax-models-filter', 'App\Http\Controllers\CreditController@ajaxModelsFilter');
Route::post('/credit/ajax-generations-filter', 'App\Http\Controllers\CreditController@ajaxGenerationsFilter');
Route::post('/credit/ajax-adverts', 'App\Http\Controllers\CreditController@ajaxAdverts');
Route::post('/credit/ajax-advert-image', 'App\Http\Controllers\CreditController@ajaxAdvertImage');

Route::get('/catalog', 'App\Http\Controllers\CatalogController@index');
Route::get('/catalog/{brand}', 'App\Http\Controllers\CatalogController@brand')->name('catalog.brand');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/home/get-models', 'App\Http\Controllers\HomeController@getModels')->withoutMiddleware('auth');
Route::post('/credit/request-add', 'App\Http\Controllers\CreditController@requestAdd')->withoutMiddleware('auth')->name('credit.request_add');
Route::get('/credit-modal/{item}', 'App\Http\Controllers\CreditController@modal')->withoutMiddleware('auth')->name('credit.modal');
Route::get('/promo/modal/{item}', 'App\Http\Controllers\PromoController@modal')->withoutMiddleware('auth')->name('promo.modal');
Route::get('/reviews/video/{item}', 'App\Http\Controllers\AdvertsController@reviewVideo')->withoutMiddleware('auth')->name('review.modal_video');


Route::get('/trade-in', 'App\Http\Controllers\TradeInController@index');
Route::get('/tradein', 'App\Http\Controllers\TradeInController@index');
Route::post('/trade-in/ajax-client-model', 'App\Http\Controllers\TradeInController@ajaxclientmodel');
Route::post('/trade-in/request-add', 'App\Http\Controllers\TradeInController@requestAdd')->withoutMiddleware('auth')->name('tradein.request_add');
Route::post('/trade-in/request-add-exchange', 'App\Http\Controllers\TradeInController@requestAddExchange')->withoutMiddleware('auth')->name('tradein.requestAddExchange');

Route::get('/buy', 'App\Http\Controllers\BuyController@index');
Route::post('/buy/request-add', 'App\Http\Controllers\BuyController@requestAdd')->withoutMiddleware('auth')->name('buy.request_add');
Route::get('/privacy', 'App\Http\Controllers\MainController@privacy')->name('privacy');
Route::get('/car_service', 'App\Http\Controllers\MainController@car_service');
Route::post('/car_service/request-add', 'App\Http\Controllers\MainController@carServiceRequestAdd');


//Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'admin'], function () {
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::resource('request-all', 'RequestAllController');

    Route::resource('reviews', 'ReviewsController');
    Route::resource('promo', 'PromoController');
    Route::resource('equipment-groups', 'EquipmentGroupsController');
    Route::resource('request-on-credit', 'RequestOnCreditController');
    Route::resource('request-on-trade-in', 'RequestOnTradeInController');
    Route::resource('request-on-promo', 'RequestOnPromoController');

    Route::match(['GET', 'POST'],'adverts/import', 'AdvertsController@import');
    Route::resource('adverts', 'AdvertsController');

    Route::resource('equipments', 'EquipmentsController');
    Route::resource('banners', 'BannersController');
    Route::resource('request-on-buy', 'RequestOnBuyController');
    Route::resource('contacts', 'ContactsController');

    Route::get('/', 'AdminController@index');
    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('users', 'UsersController');
    Route::resource('pages', 'PagesController');
    Route::resource('activitylogs', 'ActivityLogsController')->only([
        'index', 'show', 'destroy'
    ]);
    Route::resource('banner-on-main', 'BannerOnMainController');

    Route::resource('request-in-contacts', 'RequestInContactsController');
    Route::get('settings', 'SettingsController@index')->name('settings.index');
    Route::post('settings', 'SettingsController@update')->name('settings.update');
    Route::post('settings/yandex_feed/{name}', 'SettingsController@runYandexFeed')->name('settings.yandex_feed');
    Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
    Route::resource('subscribes', 'SubscribesController');

    Route::get('recovers', 'AdvertsController@recovers');
    Route::get('recovers/show/{importId}', 'AdvertsController@recoversShow');
    Route::post('adverts/set-current', 'AdvertsController@setCurrent');

});


Route::get('/get-equipments', function () {
   //$content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-equipment-groups');
   $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-equipment');
   $content_array = json_decode($content, true);

   foreach ($content_array['data']['vehicleEquipment'] as $data) {
       \App\Models\Equipment::updateOrCreate(
           ['name' => $data['name'], 'group_id' => $data['groupId'], 'sort_order' => $data['sortOrder'], 'id' => $data['id']],
           ['id' => $data['id']]
       );
   }
   dd($content);
});

Route::get('/get-equipment-groups', function () {
    $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-equipment-groups');
    $content_array = json_decode($content, true);

    foreach ($content_array['data']['vehicleEquipmentGroups'] as $data) {
        \App\Models\EquipmentGroup::updateOrCreate(
            ['name' => $data['name'], 'sort_order' => $data['sortOrder'], 'id' => $data['id']],
            ['id' => $data['id']]
        );
    }
    dd($content);
});

Route::get('/get-maxposter-brands', function () {
    $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-brands');
    $content_array = json_decode($content, true);

    foreach ($content_array['data']['vehicleBrands'] as $data) {
        \App\Models\Brand::updateOrCreate(
            ['name' => $data['name'], 'id' => $data['id']],
            ['id' => $data['id']]
        );
    }
    //dd($content);
});

Route::get('/passsss', function () {
    $password = Hash::make('87654321');
    dd($password);
});

Route::get('/get-maxposter-models', function () {
    $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-models');
    $content_array = json_decode($content, true);

    foreach ($content_array['data']['vehicleModels'] as $data) {

        /**
        id": 1,
        "name": "378 GT Zagato",
        "brandId": 1,
        "categoryId": 1,
        "yearFrom": 2012,
        "yearTo": 2012,
        "isActive": true
         */
        \App\Models\AModel::updateOrCreate(
            [
                'name' => $data['name'],
                'id' => $data['id'],
                'brand_id' => $data['brandId'],
                'category_id' => $data['categoryId'],
                'year_from' => $data['yearFrom'],
                'year_to' => $data['yearTo'],
                'is_active' => $data['isActive'],

            ],
            ['id' => $data['id']]
        );
    }
    //dd($content);
});


Route::get('/get-maxposter-generations', function () {
    $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-generations');
    $content_array = json_decode($content, true);

    foreach ($content_array['data']['vehicleGenerations'] as $data) {

        /**
        {"id":2283,"name":"I",
         * "brandId":2,
         * "categoryId":1,
         * "modelId":8,
         * "autoruId": "5139648",
         * "yearFrom":1997,
         * "yearTo":2000,
         * "isActive":true
         * },
         */
        \App\Models\Generation::updateOrCreate(
            [
                'name' => $data['name'],
                'id' => $data['id'],
                'brand_id' => $data['brandId'],
                'model_id' => $data['modelId'],
                'autoru_id' => $data['autoruId'],
                'category_id' => $data['categoryId'],
                'year_from' => $data['yearFrom'],
                'year_to' => $data['yearTo'],
                'is_active' => $data['isActive'],
            ],
            ['id' => $data['id']]
        );
    }
    //dd($content);
});

Route::get('/get-maxposter-modifications', function () {
    $content = file_get_contents('https://api.maxposter.ru/partners-api/directories/vehicle-modifications');
    $content_array = json_decode($content, true);

    foreach ($content_array['data']['vehicleModifications'] as $data) {

        /**
        {"id":2283,"name":"I",
         * "brandId":2,
         * "categoryId":1,
         * "modelId":8,
         * "autoruId": "5139648",
         * "yearFrom":1997,
         * "yearTo":2000,
         * "isActive":true
         * },
         */
        \App\Models\Modification::updateOrCreate(
            [
                'name' => $data['name'],
                'id' => $data['id'],
                'brand_id' => $data['brandId'],
                'model_id' => $data['modelId'],
                'autoru_id' => $data['autoruId'],
                'category_id' => $data['categoryId'],
                'year_from' => $data['yearFrom'],
                'year_to' => $data['yearTo'],
                'is_active' => $data['isActive'],
            ],
            ['id' => $data['id']]
        );
    }
    //dd($content);
});
Route::get('{brand}', 'App\Http\Controllers\CatalogController@brand')->name('main.brand');
Route::get('{brand}/{model}', 'App\Http\Controllers\CatalogController@brandModel')->name('main.brand_model');
