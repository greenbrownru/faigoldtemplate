@extends('layouts.auto')
@section('content')
    <div class="product">
        <!-- breadcrumbs-->
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="{{url('/')}}">Главная</a></li>
                    <li><a href="/catalog">Каталог</a></li>
                    <li><a href="/{{strtolower($item->brand->name)}}">{{$item->brand->name}}</a></li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs-->
        <div class="container">
            <div class="product__row">
                <div class="product__left">
                    <!-- product-card-->
                    <div class="product-card">
                        <h1 class="product-card__name">{{$item->brand->name}} {{$item->amodel->name}}</h1>
                        @if($item->is_current)
                            <div class="product-card__price">
                                @if(env('APP_SITE_REGION') == 23 && in_array(env('SITE_SUFFIX'), ['', 'adb', 'prius', 'autoclub']))
                                    @if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                    @if(intval($item->priceByAction()) < $item->price) <span class="old"
                                                                                             style="color: #5e5e5e">{{number_format($item->price, 0, ',', ' ')}} &#8381;</span> @endif
                                    @if(intval($item->priceByAction()) == $item->price) <span class="old"
                                                                                              style="color: #5e5e5e; text-decoration: none">{{number_format($item->price, 0, ',', ' ')}} &#8381;</span> @endif
                                    <div class="product-card__sub-block">
                                        <p class="product-card__sub-price">@if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif</p>
                                        <ul class="product-card__sub-list">
                                            <li class="product-card__sub-list-item">
                                                до
                                                <i>{{number_format($item->kaskoPrice(), 0, ',', ' ')}} &#8381;</i> при
                                                оформлении КАСКО
                                            </li>
                                            <li class="product-card__sub-list-item">
                                                до
                                                <i>{{number_format($item->tradeinDiscount(), 0, ',', ' ')}} &#8381;</i> при
                                                сдаче
                                                своего а/м по Trade-in
                                            </li>
                                            <li class="product-card__sub-list-item">
                                                до <i>{{number_format($item->creditPrice(), 0, ',', ' ')}} &#8381;</i> при
                                                покупке а/м в кредит
                                            </li>
                                        </ul>
                                    </div>
                                @else
                                    <div class="tooltip">
                                        @if(intval($item->priceByAction()) < $item->price)<span
                                            class="product-card__main-price">{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                        <div class="tooltiptext">@if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                            <p style="margin-top: auto">до
                                                <i>{{number_format($item->insurance_discount, 0, ',', ' ')}} &#8381;</i> при
                                                оформлении КАСКО</p>
                                            <p style="text-align-last: right;">до
                                                <i>{{number_format($item->tradein_discount, 0, ',', ' ')}} &#8381;</i> при
                                                сдаче
                                                своего а/м по Trade-in</p>
                                            <p>до <i>{{number_format($item->credit_discount, 0, ',', ' ')}} &#8381;</i> при
                                                покупке а/м в кредит</p>
                                        </div>
                                    </div>
                                    @if(intval($item->priceByAction()) < $item->price) <span class="old"
                                                                                             style="color: #5e5e5e">{{number_format($item->price, 0, ',', ' ')}} &#8381;</span> @endif
                                    @if(intval($item->priceByAction()) == $item->price) <span class="old"
                                                                                              style="color: #5e5e5e; text-decoration: none; font-size: 26px;">{{number_format($item->price, 0, ',', ' ')}} &#8381;</span> @endif
                                @endif
                            </div>
                        @else
                            <div class="product-card__price">
                                <span>Автомобиль продан</span>
                            </div>
                        @endif
                        <div class="product-card__img">
                            <!-- gallery-->
                            <div class="gallery">
                                <div class="gallery__labels">
                                    @include('advert._card_greed_labels', ['item' => $item])
                                </div>
                                <div class="gallery__img">
                                    @if($item->photos->count() > 1)
                                        <button class="prev" type="button"></button>
                                        <button class="next" type="button"></button>
                                    @endif
                                    @if($item->photos->count() > 0)
                                        @foreach($item->photos as $photo)
                                            <a class="gallery__item" href="{{$photo->imagepath ?? ''}}"
                                               data-fancybox="porsche">
                                                <img src="{{$photo->imagepath ?? ''}}" alt=""></a>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="product_carousel owl-carousel">
                                    @if($item->photos->count() > 0)
                                        @foreach($item->photos as $photo)
                                            <div class="item">
                                                <a class="gallery__item" href="{{$photo->imagepath ?? ''}}"
                                                   data-fancybox="porsche">
                                                    <img src="{{$photo->imagepath ?? ''}}" alt="">
                                                </a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <!-- gallery-thumbs-->
                                <div class="gallery-thumbs">
                                    @if($item->photos->count() > 0)
                                        @foreach($item->photos as $photo)
                                            <a class="gallery-thumbs__item" href="{{$photo->imagepath ?? ''}}"
                                               data-fancybox="porsche-thumbs"
                                               @if($loop->index < 6) data-index="{{$loop->index}}" @endif>
                                                <img src="{{$photo->imagepath ?? ''}}" alt="">
                                                @if($loop->index == 5) <span>еще <br> {{$item->photos->count() - 6}} фото</span> @endif
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                                <!-- end gallery-thumbs-->
                            </div>
                            <!-- end gallery-->
                        </div>
                        <div class="product-card__tooltip">
                            <div class="tooltiptext">@if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                <p style="margin-top: auto">до
                                    <i>{{number_format($item->insurance_discount, 0, ',', ' ')}} &#8381;</i> при
                                    оформлении КАСКО</p>
                                <p style="text-align-last: right;">до
                                    <i>{{number_format($item->tradein_discount, 0, ',', ' ')}} &#8381;</i> при сдаче
                                    своего а/м по
                                    системе Trade-in</p>
                                <p>до <i>{{number_format($item->credit_discount, 0, ',', ' ')}} &#8381;</i> при покупке
                                    а/м в кредит</p>
                            </div>
                        </div>
                        <div class="product-card__info">
                            <p class="product-card__credit">от&nbsp;<span>{{$item->CreditPayByMonth}} р/мес.</span>&nbsp;от {{\App\Services\Region::getCreditPercent()}}% годовых</p>

                            <a class="product-card__btn" data-fancybox data-type="ajax" data-touch="false"
                                   data-src="{{url('/credit-modal', ['item' => $item->id])}}" href="javascript:;">
                                @if(in_array(env('SITE_SUFFIX'), ['probeg_spb']))
                                    Узнать подробнее
                                @else
                                    Купить в кредит
                                @endif
                            </a>

                            <!-- car-info-->
                            <ul class="car-info">
                                <li><span>Год выпуска</span><span>{{$item->year_of_issue}}</span></li>
                                <li><span>Пробег</span><span>{{number_format($item->mileage, 0, ',', ' ')}} км</span>
                                </li>
                                <li><span>Кузов</span><span>{{$item->carbodyLabel}}</span></li>
                                <li><span>КПП</span><span>{{$item->transmissionLabel}}</span></li>
                                <li><span>Двигатель</span><span>{{$item->engineLabel}} / {{$item->volume}} см3</span>
                                </li>
                                <li><span>Руль</span><span>{{$item->steeringwheelLabel}}</span></li>
                                <li><span>Цвет</span><span>{{__($item->color)}}</span></li>
                                <li><span>Привод</span><span>{{$item->cardriveLabel}}</span></li>
                                <li><span>Мощность</span><span>{{$item->power}} л. с.</span></li>
                                <li><span>Владельцев</span><span>{{$item->number_of_owners}} </span></li>
                            </ul>

                            <a class="advert_exchange"
                               @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onclick="ga('send', 'event', 'tradeinmodel', 'Submit');" @endif

                               data-fancybox data-animation-duration="500"
                               data-src="#changeCarModal" href="javascript:;"><img src="/img/exchange.svg" style="width: 20px"/> Обмен на мой авто </a>

                            <!-- end car-info-->


                        </div>
                    </div>
                    <div class="product-desc__left">
                        <h2 class="section-title">Описание<span class="grey"><span class="blue"></span></span></h2>

                        <div class="product-desc__row">
                            <?php $i = 0 ?>
                            @if(env('APP_SITE_CITY_LOCATION') && env('APP_SITE_CITY_LOCATION') == 'chel')
                                <?php $cnt = $item->equipments->count() / 2 ?>
                                @if($item->equipments()->count() > 1)

                                    <div class="product-desc__col">
                                        <div class="product-desc__section" style="margin-left: 20px">
                                            <ul>
                                                @foreach($item->equipments->take($cnt) as $equipment)
                                                    <li>{{$equipment->text}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-desc__col">
                                        <div class="product-desc__section" style="margin-left: 20px">
                                            <ul>
                                                @foreach($item->equipments->skip($cnt)->all() as $equipment)
                                                    <li>{{$equipment->text}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                @else
                                    <div class="product-desc__section" style="margin-left: 20px">
                                        <ul>
                                            @foreach($item->equipments as $equipment)
                                                <li>{{$equipment->text}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            @else
                                @foreach($equipmentGroups as $group)
                                    <div class="product-desc__col @if($i > 5)hidden @endif">
                                        <div class="product-desc__section">
                                            <p>{{$group->name}} </p>
                                            <ul>
                                                @foreach($group->equipments as $equipment)
                                                    @if(in_array($equipment->id, $selectedEquipments))
                                                        <li>{{$equipment->name}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <?php $i++ ?>
                                @endforeach
                                <a href="#" class="more_info_btn" style="background-color: white">Подробнее</a>
                            @endif

                        </div>
                    </div>
                    <!-- end product-card-->
                </div>
                <div class="product__right">
                    @if($item->region_id == 66 || $item->region_id == 63)
                        <div class="catalog-grid__item catalog-grid__item--third catalog_my_car_class"
                             style="width: 100%; margin: 0 0 30px;">
                            <b style="font-size: 20px">Наши контакты</b>
                            <ul>
                                <li>
                                    Адрес: @if($item->region_id == 66) г. Екатеринбург, Базовый переулок, 38 @else г.
                                    Самара, Южное шоссе, 20 @endif
                                </li>
                                <li>
                                    Телефон: {{showPhone()}}
                                </li>
                            </ul>
                        </div>
                    @endif
                    @include('_banners')
                    <div class="product-desc__right">
                        <!-- legal-->
                        <div class="legal">
                            <div class="legal__vin" style="{{$item->starredvin ? '' : 'display: none;'}}"><span> {{$item->starredvin}}</span></div>
                            <p>Юридическая чистота</p>
                            <ul>
                                <li>Сведения о нахождении в залоге: не обнаружены</li>
                                <li>Сведения о нахождении в розыске: не обнаружены</li>
                                <li>Ограничения на регистрационные действия: не обнаружены</li>
                            </ul>
                            <p>Владельцы по ПТС</p>
                            <ul>
                                <li>Количество владельцев: {{$item->number_of_owners}}</li>
                            </ul>
                        </div>
                        <!-- end legal-->
                    </div>
                </div>
            </div>
        </div>
        <!-- product-desc-->
        <section class="product-desc" id="js-desc">
            <div class="container">
                <div class="product-desc__row">
                    <div class="product-desc__right">
                        <!-- legal-->
                        <div class="legal">
<!--                            <div class="legal__vin"><span> X7LBSRB1*AH****88</span></div>-->
                            <p>Юридическая чистота</p>
                            <ul>
                                <li>Сведения о нахождении в залоге: не обнаружены</li>
                                <li>Сведения о нахождении в розыске: не обнаружены</li>
                                <li>Ограничения на регистрационные действия: не обнаружены</li>
                            </ul>
                            <p>Владельцы по ПТС</p>
                            <ul>
                                <li style="padding-bottom: 5px">Количество владельцев: {{$item->number_of_owners}}</li>
                            </ul>
                        </div>
                        <!-- end legal-->
                    </div>
                </div>
            </div>
        </section>
        <!-- end product-desc-->
        @if($similar->count() > 0)
            @include('parts.similar', ['items' => $similar])
        @endif
    </div>
    <!-- end product-->
    <div style="display: none;" id="changeCarModal" class="animated-modal">
        <div class="modal_top">
            <div class="modal_content_center">
                @if(in_array(env('APP_SITE_REGION'), ['', 66, 63]))
                    <b>Обменять свой автомобиль на этот</b>
                @else
                    <h4>Обменять свой автомобиль на этот</h4>
                @endif
                <div class="change_car_flex">
                    <div class="change_car_block">
                        <div class="your_car">
                            <img src="/img/auto_icon.png" alt="">
                        </div>
                        @if(in_array(env('APP_SITE_REGION'), ['', 66, 63]))
                            <b>Ваш автомобиль</b>
                        @else
                            <h6>Ваш автомобиль</h6>
                        @endif

                        <span>Дилер уточнит у вас его параметры</span>
                    </div>
                    <div class="change_car_block">
                        <div class="changing_car">
                            <img src="{{$item->photos->first()->imagepath ?? ''}}" alt="">
                        </div>
                        <div class="car_name">{{$item->brand->name}} {{$item->amodel->name}}</div>
                        <div class="car_price">
                            @if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                            <span class="old"
                                  @if(intval($item->priceByAction()) == $item->price) style="color: #5e5e5e; text-decoration: none" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal_bottom">
            <div class="modal_content_center">
                @if(in_array(env('APP_SITE_REGION'), ['', 66, 63]))
                    <b>Укажите свои данные</b>
                @else
                    <h6>Укажите свои данные</h6>
                @endif

                <form action="{{route('tradein.requestAddExchange')}}" method="post" id="tradein-exchange-form"
                      @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','creditmodel'); ga('send', 'event', 'creditmodel', 'Submit');" @endif
                      @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','tradeinmodel'); return true;" @endif
                >
                    @csrf
                    <div class="modal_form_top">
                        <div class="modal_form_block">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                            </div>
                        </div>
                        <input type="hidden" name="advert_id" value="{{$item->id}}">
                        <div class="modal_form_block">
                            <div class="form-group">
                                <input class="phone" type="tel" name="phone" required autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="modal_form_bottom">
                        <div class="modal_form_block">
                            <button class="feedback__btn">
                                Перезвоните мне
                            </button>
                            <div class="mt-2">
                                <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')

    <!-- Rating@Mail.ru counter dynamic remarketing appendix -->
    <script type="text/javascript">
        var _tmr = _tmr || [];
        _tmr.push({
            type: 'itemView',
            productid: {!! $item->client_id !!},
            pagetype: 'product',
            list: 2,
            totalvalue: {!! $item->priceByAction() !!}
        });
    </script>
    <script>
        $(document).on('submit', '#tradein-exchange-form', function (e) {
            var form = $(this);
            var button = $(form).find('button');
            $.ajax({
                beforeSend: function () {
                    beforeSendMakes(form);

                },
                complete: function () {
                    $(button).removeClass('progress-bar-striped');
                    $(button).removeClass('progress-bar-animated');
                    $(form).trigger("reset");
                },
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.serialize(),
                success: function (data) {

					if(data == 0) {
						$('form#tradein-exchange-form input.phone').css({border: '1px solid red'})
					}else{
						$('[data-fancybox-close]').click();
						$('body').addClass('thanksPopupBody');
						$('.thanks_modal').addClass('active');
                    }

                },
                error: function (error) {
                    //console.log(error);
					$('[data-fancybox-close]').click();
					$('body').addClass('thanksPopupBody');
					$('.thanks_modal').addClass('active');
                }
            });
            e.preventDefault();
        });
    </script>
@endsection
@php
    $_catalogTemplateVars = [
        'title',
        'credit_percent',
        'name',
        'city',
        'brand' => $item->brand->name,
        'model' => $item->AModel->name,
        'year_of_issue' => $item->year_of_issue,
        'mileage' => $item->mileage,
        'transmission' => $item->transmissionLabel,
        'engine' => $item->engineLabel,
        'volume' => $item->volume,
        'price' => $item->price,
        'price_by_action' => $item->priceByAction(),
        'discount' => $item->price - $item->priceByAction(),
    ];
@endphp

@section('title'){{ setTemplateVars($_catalogTemplateVars, setting('page__advert_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars($_catalogTemplateVars, setting('page__advert_description') ?: setting('page__description')) }}@endsection


@section('title')
    @if(env('APP_SITE_REGION') == '')Купить {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}}г.в. {{$item->mileage}} {{$item->transmissionLabel}} {{$item->engineLabel}}/{{$item->volume}} см3 за {{$item->price_by_action}}&#8381; скидка {{$item->price - $item->price_by_action}}&#8381; в Кредит от  {{\App\Services\Region::getCreditPercent()}}% годовых.@endif
    @if(env('APP_SITE_REGION') == 63){{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}}г.в. в Самаре {{$item->price_by_action}} руб от 6.4% годовых | Автосалон АвтоДромиум @endif
    @if(env('APP_SITE_REGION') == 66)Купить {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}}г в Екатеринбурге от {{$item->CreditPayByMonth}}р/мес под 6.4% годовых. | Автосалон Автодромиум @endif
    @if(env('APP_SITE_REGION') == 74)Купить {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}}г.в. {{$item->mileage}} {{$item->transmissionLabel}} {{$item->engineLabel}}/{{$item->volume}} см3  в Челябинске за {{$item->price_by_action}}&#8381; скидка {{$item->price - $item->price_by_action}}&#8381; в Кредит от  {{\App\Services\Region::getCreditPercent()}}% годовых.@endif
    @if(env('APP_SITE_REGION') == 72)Купить {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}} года с пробегом в Тюмени, Б/У автомобили в АвтоФулл@endif
    @if(env('APP_SITE_REGION') == 18)Купить Б/У {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}} г в Ижевске | УдмуртАвтоПробег @endif
@endsection
@section('meta_description')
    @if(env('APP_SITE_REGION') == 72)
        <meta name="description"
              content="Продажа Б/У автомобиля {{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}} года с пробегом {{$item->mileage}} км. Фото, цены на подержанные автомобили в Автосалоне АвтоФулл в Тюмени.">
    @elseif(env('APP_SITE_REGION') == 18)
        <meta name="description"
              content="{{$item->brand->name}} {{$item->AModel->name}} {{$item->year_of_issue}} года с пробегом за {{$item->CreditPayByMonth}} р/месяц в кредит или Trade-in. Большой выбор б/у автомобилей известных марок с фото и характеристиками в Ижевске.">
    @endif
    @if(env('APP_SITE_REGION') == 63)
        <meta name="description"
              content="Автомобиль {{$item->brand->name}} {{$item->amodel->name}} с пробегом {{$item->year_of_issue}}г в Самаре. Акции, скидки, выгодные предложения, выгодное автокредитование на покупку б/у автомобилей от автосалона АвтоДромиум"/>
    @endif
    @if(env('APP_SITE_REGION') == 66)
        <meta name="description"
              content=" Б/у автомобиль {{$item->brand->name}} {{$item->amodel->name}} {{$item->year_of_issue}} года за @if(intval($item->priceByAction()) < $item->price){{number_format($item->priceByAction(), 0, ',', ' ')}} @endif руб. Купить автомобиль с пробегом в
            Екатеринбурге на Базовом переулке в автосалоне Автодромиум"/>
    @endif
    @if(env('APP_SITE_REGION') == 123)
        <meta name="description" content="{{$item->brand->name}} {{$item->amodel->name}} {{$item->year_of_issue}} года за @if(intval($item->priceByAction()) < $item->price){{number_format($item->priceByAction(), 0, ',', ' ')}}@endif рублей. Успей купить {{$item->brand->name}} {{$item->amodel->name}} в кредит от {{$item->CreditPayByMonth}} рублей в месяц. Участвуй в акциях от автосалона Олимп Трейд в Краснодаре на Ростовском шоссе."/>
    @endif
@endsection
