@extends('layouts.auto')
@section('content')
    <section class="page" id="js-page">
        <div class="container">
            <h1 class="section-title">{{$item->nameFormat}}<span class="grey"><span class="blue"></span></span></h1>
            <div class="page__row">
                <div class="page__left">
                    <!-- article-->
                    <article class="article" style="margin-bottom: 12px">
                        <div class="article__img">
                            <img src="/storage/photos/{{$item->photo}}" alt="">
                        </div>
                        <div class="article__text">
                            {!!  $item->fullDescriptionFormat !!}
                            @if($item->with_form === true)
                                @if(in_array(env('SITE_SUFFIX'), ['probeg_v_spb']))
                                    @include('promos._modal_form')
                                @else
                                    <a class="article__btn" data-fancybox="" @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onclick="ym(61299526,'reachGoal','akcii'); ga('send', 'event', 'akcii', 'Submit');" @endif data-type="ajax" href="javascript:;" data-src="/promo/modal/{{$item->id}}">Участвовать в акции </a>
                                @endif
                            @endif
                        </div>

                    </article>
                    <!-- end article-->
                </div>
                <div class="page__right">
                    <div class="promo__row">
                        @if($otherPromos->count() > 0)
                            @foreach($otherPromos as $promo)
                                <a class="promo__item promo__item--full" href="{{url('/promo', ['promo' => $promo->id])}}">
                                    <img src="/storage/photos/{{$promo->photo}}" alt="">
                                    <div class="promo__desc">
                                        <b class="promo__title">{{$promo->nameFormat}}</b>
                                        <p>{{$promo->shortDescriptionFormat}}</p>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city', 'promo_name' => $item->nameFormat], setting('page__promo_item_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city', 'promo_name' => $item->nameFormat, 'full_description' => strip_tags($item->fullDescriptionFormat)], setting('page__promo_item_description') ?: setting('page__description')) }}@endsection