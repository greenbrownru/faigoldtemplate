<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="feedback__title">Заявка на участие</h4>
            </div>
            <div class="modal-body" style="background-color: #f2f5f7">
                <form action="/promo/request-add" method="post"
                      @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','akcii'); ga('send', 'event', 'akcii', 'Submit');" @endif
                      @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','akcii');return true;" @endif
                >
                    @csrf
                    <div class="feedback__section">
                        <h4 class="feedback__subtitle">Укажите свои данные</h4>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Ваше имя" required>
                        </div>
                        <div class="form-group">
                            <input class="phone" type="tel" name="phone" required>
                        </div>

                        <input type="hidden" name="promo_id" value="{{$item->id}}">
                        <button class="feedback__btn" type="submit">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
