@extends('layouts.auto')
@section('content')
    <!-- promo-->
    <section class="promo" id="js-promo">
        <div class="container">
            @if(setting('page__promo_header_text_enable'))
                <div class="article__text">
                    {!! setTemplateVars(['name', 'city', 'credit_percent'], setting('page__promo_header_text')) !!}
                </div>
            @endif
            <h1 class="section-title">Акции<span class="grey"><span class="blue"></span></span></h1>
            <div class="promo__row">
                @foreach($items as $item)
                <a class="promo__item" href="{{url('/promo', ['promo' => $item->id])}}"><img src="/storage/photos/{{$item->photo}}" alt="">
                    <div class="promo__desc">
                        @if(env('APP_SITE_REGION') == 123)
                            <h2 class="promo__title">{{$item->nameFormat}}</h2>
                        @else
                            <h3 class="promo__title">{{$item->nameFormat}}</h3>
                        @endif
                        <p>{{$item->shortDescriptionFormat}}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </section>
    <!-- end promo-->
@endsection
@section('javascript')
    <script>
        $(function() {
            $(window).on('resize', function() {
                $('main').css('height', window.innerHeight > $('.wrap').height() ? ($('main').height() + window.innerHeight - $('.wrap').height() - 21)+'px' : '')
            }).trigger('resize');
        });
    </script>
@endsection

@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__promo_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__promo_description') ?: setting('page__description')) }}@endsection
