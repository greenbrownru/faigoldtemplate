<style>
    .modal-flex {
        background: #f2f5f7 !important;
        justify-content: center;
    }
    @if(in_array(env('SITE_SUFFIX'), ['probeg_v_spb']))
    .modal-flex {
        background: #eee !important;
        justify-content: flex-start;
    }
    @endif
</style>

<div class="modal-flex">

    <form id="promo-modal" action="/promo/request-add" method="post"
          @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','akcii'); ga('send', 'event', 'akcii', 'Submit');" @endif
          @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','akcii'); return true;" @endif
    >
        @csrf
        <div class="feedback__section">
            <h4 class="feedback__subtitle">{{ in_array(env('SITE_SUFFIX'), ['probeg_v_spb']) ? 'Участвовать в акции' : 'Укажите свои данные' }}</h4>
            <div class="form-group">
                <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
            </div>
            <div class="form-group">
                <input class="phone" type="tel" name="phone" required autocomplete="off">
            </div>

            <input type="hidden" name="promo_id" value="{{$item->id}}">
            <button class="feedback__btn" type="submit">Отправить</button>

            <div class="mt-2">
                <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
            </div>
        </div>
    </form>

</div>
<script src="/js/libs.min.js"></script>
<script src="/js/scripts.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script>
    $(document).on('submit', '#promo-modal', function (e) {
        var form = $(this);
        var formData = new FormData(this);
        $.ajax({
            beforeSend: function (xhr, opts) {
                beforeSendMakes(form, xhr);
                $('#promo-modal').addClass('send');

            },
            complete: function () {
                $('.feedback__btn').removeClass('progress-bar-striped');
                $('.feedback__btn').removeClass('progress-bar-animated');
                $(form).trigger("reset");
                $('#promo-modal').removeClass('send');
            },
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            dataType: 'html',
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {

                $('.fancybox-close-small').click();
                $('[data-fancybox-close]').click();
                $('body').addClass('thanksPopupBody');
                $('.thanks_modal').addClass('active');


            }
        });
        //return false;
        e.preventDefault();

    });
</script>
