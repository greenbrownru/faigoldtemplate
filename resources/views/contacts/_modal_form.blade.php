<style>
    .contact__modal {
        padding: 0 !important;
    }
</style>
<div class="fancybox-content contact__modal">
    <section class="feedback">
        <h3 class="feedback__title">Укажите свои данные</h3>
        <form action="/contacts/request-add" method="post" id="contact-form"
              @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','contacts'); ga('send', 'event', 'contacts', 'Submit');" @endif
              @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','contacts'); return true;" @endif >
            @csrf
            <div class="form-group">
                <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
            </div>
            <div class="form-group">
                <input class="phone" type="tel" name="phone" required autocomplete="off">
            </div>
            <button class="feedback__btn" type="submit">Отправить</button>
            <div class="mt-2">
                <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
            </div>
        </form>
    </section>
</div>

<script>
    var phoneFocusTrigger = true;
    $('.phone')
        //======fix for autocomplete
        .attr('readonly',true)
        .on('click focus', function() {
            $(this).attr('readonly', false);
            if (phoneFocusTrigger) {
                phoneFocusTrigger = false;
                document.querySelector('.name').focus();
                this.focus();
            }
        })
        //======./fix for autocomplete

        .intlTelInput({
            defaultCountry: 'ru',
            preferredCountries: ['ru'],
            separateDialCode: true,
        });

    $(document).on('keyup', '.name', function(e){
        $(this).val($(this).val().replace(/[^\+а-яА-Яa-zA-Z ]/g, ''));
    });

    $('#contact-form').on('submit', function (e) {
        var form = $(this);

        $.ajax({
            beforeSend: function (xhr, opts) {
                beforeSendMakes(form, xhr);
            },
            complete: function () {
                $('.feedback__btn').removeClass('progress-bar-striped');
                $('.feedback__btn').removeClass('progress-bar-animated');
                $(form).trigger("reset");
            },
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            success: function (data) {

                if(data == 0){
                    $('form#contact-form input.phone').css({border: '1px solid red'})
                }else{
                    $('[data-fancybox-close]').click();
                    $('body').addClass('thanksPopupBody');
                    $('.thanks_modal').addClass('active');
                }


            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });
</script>
