@extends('layouts.auto')
@section('content')
    <section class="page" id="js-page">
        <div class="container">
            <h2 class="section-title">Политика в отношении обработки персональных данных<span class="grey"><span class="blue"></span></span></h2>
            <div class="page__row">
                <article>
                    {!! str_replace('[HOST_NAME]', request()->getHttpHost(), html_entity_decode(\App\Models\Page::whereTitle('privacy')->first()->content ?? '')) !!}
                </article>
            </div>
        </div>
    </section>
@endsection
