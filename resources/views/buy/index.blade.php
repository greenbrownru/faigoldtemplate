@extends('layouts.auto')
@section('content')
    <!-- page-->
    <section class="page" id="js-page">
        <div class="container">
            <h1 class="section-title">Выкуп авто
                @if(env('APP_SITE_REGION') == 63) в Самаре @endif
                @if(env('APP_SITE_REGION') == 66) в Екатеринбурге @endif
                @if(env('APP_SITE_REGION') == 52) в Нижнем Новгороде @endif
                <span class="grey"><span class="blue"></span></span>
            </h1>
            <div class="page__row">
                <div class="page__left">
                    <!-- article-->
                    <article class="article">
                        {!! html_entity_decode(\App\Models\Page::whereTitle('buy')->first()->content ?? '') !!}
                    </article>
                    <!-- end article-->
                </div>
                <div class="page__right">
                    <!-- feedback-->
                    <section class="feedback">
                        <h2 class="feedback__title">Форма заявки</h2>
                        <form action="/buy/request-add" method="post" id="buy-form"
                              @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','vikup'); ga('send', 'event', 'vikup', 'Submit');" @endif
                              @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','vikup');return true;" @endif
                        >
                            @csrf
                            <div class="feedback__section">
                                <b class="feedback__subtitle">Укажите свой автомобиль</b>
                                <div class="form-group">
                                    <div class="select" id="select_brand">
                                        <div class="select__label"> Марка</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="client_brand" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Марка</li>
                                            @if($brandsClient)
                                                @foreach($brandsClient as $brand)
                                                        <li class="li-client-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_buy_model">
                                        <div class="select__label"> Модель</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="client_model" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Модель</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="feedback__section">
                                <b class="feedback__subtitle">Укажите свои данные</b>
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input class="phone" type="tel" name="phone" required autocomplete="off">
                                </div>
                                <button class="feedback__btn" type="submit">Отправить</button>
                                <div class="mt-2">
                                    <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                                </div>
                            </div>
                        </form>
                    </section>
                    <!-- end feedback-->
                </div>
            </div>
        </div>
    </section>
    <!-- end page-->
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.li-client-brand', function (e) {
                let elementModel = $('input[name="client_model"]').parent().find('ul');
                $(elementModel).html('<li data-value="">Модель</li>');
                let brand = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/trade-in/ajax-client-model',
                    type: 'post',
                    data: {"brand_id": brand},
                    dataType: 'json',
                    success: function (data) {
                        let content = '<li class="li-client-model" data-value="">Модель</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-client-model" data-value="'+value.id+'">'+value.name+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });
        });

        $(document).on('submit', '#buy-form', function (e) {
            var form = $(this);
            $.ajax({
                beforeSend: function (xhr, opts) {
                    beforeSendMakes(form, xhr);

                },
                complete: function () {
                    $('.feedback__btn').removeClass('progress-bar-striped');
                    $('.feedback__btn').removeClass('progress-bar-animated');
                    $(form).trigger("reset");
                },
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.serialize(),
                success: function (data) {


					if(data == 0){
						$('form#buy-form input.phone').css({border: '1px solid red'})
					}else{
						$('[data-fancybox-close]').click();
						$('body').addClass('thanksPopupBody');
						$('.thanks_modal').addClass('active');
					}
                },
                error: function (error) {
					$('[data-fancybox-close]').click();
					$('body').addClass('thanksPopupBody');
					$('.thanks_modal').addClass('active');
                }
            });
            e.preventDefault();
        });
    </script>
@endsection
@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__buy_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__buy_description') ?: setting('page__description')) }}@endsection

