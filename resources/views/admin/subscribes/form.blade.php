<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('brand') ? 'has-error' : ''}}">
    {!! Form::label('brand', 'Brand', ['class' => 'control-label']) !!}
    {!! Form::number('brand', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('brand', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('year_from') ? 'has-error' : ''}}">
    {!! Form::label('year_from', 'Year From', ['class' => 'control-label']) !!}
    {!! Form::number('year_from', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('year_from', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('year_to') ? 'has-error' : ''}}">
    {!! Form::label('year_to', 'Year To', ['class' => 'control-label']) !!}
    {!! Form::number('year_to', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('year_to', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price_from') ? 'has-error' : ''}}">
    {!! Form::label('price_from', 'Price From', ['class' => 'control-label']) !!}
    {!! Form::number('price_from', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price_from', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price_to') ? 'has-error' : ''}}">
    {!! Form::label('price_to', 'Price To', ['class' => 'control-label']) !!}
    {!! Form::number('price_to', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price_to', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
