<div class="form-group{{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Тип', ['class' => 'control-label']) !!}
    {!! Form::select('type', [\App\Models\Banner::BANNER_TYPE_MAIN => 'Основной', \App\Models\Banner::BANNER_TYPE_ADDITIONAL => 'Дополнительный'], null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('filename') ? 'has-error' : ''}}">
    {!! Form::label('filename', 'Фото', ['class' => 'control-label']) !!}
    {!! Form::file('filename', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('filename', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Название', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('short_text') ? 'has-error' : ''}}">
    {!! Form::label('short_text', 'Текст', ['class' => 'control-label']) !!}
    {!! Form::text('short_text', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('short_text', '<p class="help-block">:message</p>') !!}
</div>
<div class="mb-3">
    Переменные:
    <br><b>[[name]]</b> - Название салона
    <br><b>[[credit_percent]]</b> - Процентная ставка
    <br><b>[[city]]</b> - Город
</div>


<div class="form-group{{ $errors->has('link') ? 'has-error' : ''}}">
    {!! Form::label('link', 'Ссылка', ['class' => 'control-label']) !!}
    {!! Form::text('link', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('is_big') ? 'has-error' : ''}}">
    {!! Form::label('is_big', 'Большой', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('is_big', '1') !!} Да</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('is_big', '0', true) !!} Нет</label>
</div>
    {!! $errors->first('is_big', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('display_on_main') ? 'has-error' : ''}}">
    {!! Form::label('display_on_main', 'Выводить на главной', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('display_on_main', '1') !!} Да</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('display_on_main', '0', true) !!} Нет</label>
</div>
    {!! $errors->first('display_on_main', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
