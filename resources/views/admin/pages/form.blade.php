<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('content') ? 'has-error' : ''}}">
    {!! Form::label('content', 'Content', ['class' => 'control-label']) !!}
    {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => 25]) !!}
    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>

<div class="mb-3">
    Переменные:
    <br><b>[[name]]</b> - Название салона
    <br><b>[[phone]]</b> - Телефон
    <br><b>[[credit_percent]]</b> - Процентная ставка
    <br><b>[[city]]</b> - Город
    <br><b>[[credit_max_month]]</b> - Максимальный срок (мес.)
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
