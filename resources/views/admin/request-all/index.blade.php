@extends('layouts.backend')

@section('content')

    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">


    <script type="text/javascript">
		var minDate, maxDate;

		$.fn.dataTable.ext.search.push(
				function( settings, data, dataIndex ) {
					var min = minDate.val();
					var max = maxDate.val();
					var date = new Date( data[4] );

					if (
							( min === null && max === null ) ||
							( min === null && date <= max ) ||
							( min <= date   && max === null ) ||
							( min <= date   && date <= max )
					) {
						return true;
					}
					return false;
				}
		);

		$(document).ready( function () {

			minDate = new DateTime($('#min'), {
				format: 'MMMM Do YYYY'
			});
			maxDate = new DateTime($('#max'), {
				format: 'MMMM Do YYYY'
			});

			var table = $('#myTable').DataTable({
				"columnDefs": [
					{ "searchable": false, "targets": 3 }
				]
            });

			$('#min, #max').on('change', function () {
				table.draw();
			});
		} );
    </script>

    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Все заявки</div>
                    <div class="card-body">

                        <div class="filters">
                            <input type="text" id="min" name="min" placeholder="Дата от">
                            <input type="text" id="max" name="max" placeholder="Дата до">
                        </div>

                        <hr>

                        <div class="table-responsive">
                            <table class="table table-borderless" id="myTable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Тип</th>
                                    <th>Имя</th>
                                    <th>Телефон</th>
                                    <th>Дата создания</th>
                                    <th>Просмотр</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $type => $requests_in_type)
                                    @foreach($requests_in_type as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $type }}</td>
                                        <td>{{ substr($item->name, 0, 50) }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ date('Y/m/d', strtotime($item->created_at)) }}</td>
                                        <td>
                                        @switch($type)
                                            @case('tradein')
                                            <a href="{{ url('/admin/request-on-trade-in/' . $item->id) }}" title="View TradeIn"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @break

                                            @case('promo')
                                                <a href="{{ url('/admin/request-on-promo/' . $item->id) }}" title="View RequestOnPromo"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @break

                                            @case('credit')
                                                <a href="{{ url('/admin/request-on-credit/' . $item->id) }}" title="View RequestOnCredit"><button class="btn btn-info btn-sm"><i class="fa fa-eye"aria-hidden="true"></i></button></a>
                                            @break

                                            @case('buy')
                                                <a href="{{ url('/admin/request-on-buy/' . $item->id) }}" title="View RequestOnBuy"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @break

                                            @case('contact')
                                                <a href="{{ url('/admin/request-in-contacts/' . $item->id) }}" title="View RequestInContact"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @break
                                            @case('exchange')
                                                <a href="{{ url('/admin/request-on-exchange/' . $item->id) }}" title="View RequestOnExchange"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            @break
                                        @endswitch
                                        </td>
                                    </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
