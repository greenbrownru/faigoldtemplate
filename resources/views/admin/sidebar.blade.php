<div class="col-md-3">
    @foreach($laravelAdminMenus->menus as $section)
        @if($section->items)
            <div class="card">
                <div class="card-header">
                    {{ $section->section }}
                </div>
                <div class="card-body">
                    <ul class="nav flex-column" role="tablist">
                        @foreach($section->items as $menu)
                            <li class="nav-item" role="presentation" @if(str_contains(url()->current(), $menu->url)) style="background-color: rgba(0,0,0,0.09)" @endif>
                                <a class="nav-link" href="{{ url($menu->url) }}">
                                    {{ __($menu->title) }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <br/>
        @endif
    @endforeach
</div>
