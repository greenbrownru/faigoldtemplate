<div class="form-group{{ $errors->has('fio') ? 'has-error' : ''}}">
    {!! Form::label('fio', 'ФИО', ['class' => 'control-label']) !!}
    {!! Form::text('fio', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('fio', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('brand_id') ? 'has-error' : ''}}">
    {!! Form::label('brand_id', 'Марка', ['class' => 'control-label']) !!}
    {!! Form::select('brand_id', \App\Models\Brand::orderBy('name')->get()->pluck('name', 'id')->toArray(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('model_id') ? 'has-error' : ''}}">
    {!! Form::label('model_id', 'Модель', ['class' => 'control-label']) !!}
    {!! Form::select('model_id', !empty($review->model_id) ? \App\Models\AModel::whereBrandId($review->brand_id)->orderBy('name')->pluck('name', 'id')->toArray() : ['' => '--'], null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('model_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('rate') ? 'has-error' : ''}}">
    {!! Form::label('rate', 'Рейтинг', ['class' => 'control-label']) !!}
    {!! Form::select('rate', ['' => '', 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5],null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('review') ? 'has-error' : ''}}">
    {!! Form::label('review', 'Отзыв', ['class' => 'control-label']) !!}
    {!! Form::textarea('review' ,null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('review', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('client_photo_name') ? 'has-error' : ''}}">
    {!! Form::label('client_photo_name', 'Фото клиента', ['class' => 'control-label']) !!}
    {!! Form::file('client_photo_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('client_photo_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('auto_photo_name') ? 'has-error' : ''}}">
    {!! Form::label('auto_photo_name', 'Фото отзыва', ['class' => 'control-label']) !!}
    {!! Form::file('auto_photo_name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('auto_photo_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('video_link') ? 'has-error' : ''}}">
    {!! Form::label('video_link', 'Ссылка на видео', ['class' => 'control-label']) !!}
    {!! Form::text('video_link', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('video_link', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Обновить' : 'Создать', ['class' => 'btn btn-primary']) !!}
</div>
