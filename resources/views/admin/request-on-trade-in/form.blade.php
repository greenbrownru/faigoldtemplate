<div class="form-group{{ $errors->has('advert_id') ? 'has-error' : ''}}">
    {!! Form::label('advert_id', 'Объявление', ['class' => 'control-label']) !!}
    {!! Form::select('advert_id', \App\Models\Advert::query()->active()->get()->pluck('creditname', 'id'),null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('advert_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('client_brand') ? 'has-error' : ''}}">
    {!! Form::label('client_brand', 'Марка клиента', ['class' => 'control-label']) !!}
    {!! Form::select('client_brand', \App\Models\Brand::query()->where('brands.id', 'not like', '777%')->orderBy('name')->get()->pluck('name', 'id'),null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('client_brand', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('client_model') ? 'has-error' : ''}}">
    {!! Form::label('client_model', 'Модель клиента', ['class' => 'control-label']) !!}
    {!! Form::select('client_model', !empty($tradein->client_model) ? \App\Models\AModel::query()->where(['brand_id' => $tradein->client_brand])->orderBy('name')->get()->pluck('name', 'id'): ['' => '--'],null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('client_model', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Имя', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Телефон', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Обновить' : 'Создать', ['class' => 'btn btn-primary']) !!}
</div>
