@extends('layouts.backend')

@push('css')
    <style>
        .popover_img {
            width: 100%;
            max-width: 300px;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Настройки</div>
                    <div class="card-body">
                        <ul class="nav nav-pills mb-3">
                            @foreach($groups as $group)
                                <li class="nav-item">
                                    <a class="nav-link {{ $loop->first ? 'active' : '' }}" href="#" data-toggle="tab" data-target="#{{ $group['name'] }}" aria-selected="{{ $loop->first ? 'true' : 'false' }}">{{ $group['label'] }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <form action="{{ route('settings.update') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="tab-content">
                                @foreach($groups as $groupIndex => $group)
                                    <div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="{{ $group['name'] }}">
                                        @if($group['name'] === 'feed')
                                            <h5>Яндекс</h5>
                                            <hr>

                                            @php
                                                $yandexFeed = array_replace(['all' => ['date' => null]], setting('yandex__feed') ?: []);
                                            @endphp

                                            @foreach($yandexFeed as $name => $v)
                                                <div class="form-group row">
                                                    <label class="col-sm-6 col-form-label">
                                                        {{ $name === 'all' ? 'Все' : $name }}
                                                        <span class="badge badge-primary ml-2">{{ empty($v['date']) ? 'Не запущен' : \Carbon\Carbon::createFromTimestamp($v['date'])->calendar() }}</span>
                                                        <span class="badge bg-secondary text-white" style="cursor: pointer;" data-toggle="popover" data-placement="top" data-content="{{ asset($name === 'all' ? 'yandex' : $name) }}.xml"><i class="fa fa-link"></i></span>
                                                    </label>
                                                    <div class="col-sm-6">
                                                        <button type="button" class="btn btn-success btn-sm btn-yandex-feed" data-yandex-feed-submit="{{ $name }}">Обновить</button>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <hr class="my-5">
                                            <div class="form-group row mb-5">
                                                <label class="col-sm-6 col-form-label">Марка</label>
                                                <div class="col-sm-6 d-flex">
                                                    <select class="form-control" data-yandex-feed-brand>
                                                        <option value="">Выбрать...</option>
                                                        @foreach($brands as $id => $brandName)
                                                            <option value="{{ \Illuminate\Support\Str::lower($brandName) }}">{{ $brandName }}</option>
                                                        @endforeach
                                                    </select>

                                                    <button type="button" class="btn btn-outline-success btn-yandex-feed" data-yandex-feed-brand-submit>Запустить</button>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @foreach($group['items'] as $itemIndex => $item)
                                            @php
                                                $type = $item['type'] ?? null;
                                                $fieldType = $item['field'] ?? 'text';
                                            @endphp

                                            @switch($type)
                                                @case('header')
                                                    <h5 class="mt-5">
                                                        {{ $item['label'] }}
                                                        @isset($item['labelTitle'])
                                                            <span class="badge badge-pill bg-secondary text-white" style="cursor: pointer;" data-toggle="popover" data-placement="top" data-popover-content="#setting_header_{{ $groupIndex }}_{{ $itemIndex }}_popover" data-html="true"><i class="fa fa-info"></i></span>
                                                            <div class="d-none" id="setting_header_{{ $groupIndex }}_{{ $itemIndex }}_popover">
                                                                {!! $item['labelTitle'] !!}
                                                            </div>
                                                        @endisset
                                                    </h5>
                                                    <hr>
                                                @break

                                                @default
                                                    <div class="form-group row">
                                                        <label for="setting_field_{{ $item['name'] }}" class="col-sm-3 col-form-label">
                                                            {{ $item['label'] }}
                                                            @isset($item['labelTitle'])
                                                                <span class="badge badge-pill bg-secondary text-white" style="cursor: pointer;" data-toggle="popover" data-placement="top" data-popover-content="#setting_field_{{ $item['name'] }}_popover" data-html="true"><i class="fa fa-info"></i></span>
                                                                <div class="d-none" id="setting_field_{{ $item['name'] }}_popover">
                                                                    {!! $item['labelTitle'] !!}
                                                                </div>
                                                            @endisset
                                                            @if($fieldType === 'file')
                                                                <span class="badge badge-pill bg-{{ setting($item['name']) ? 'success' : 'danger' }} text-white" data-toggle="popover" data-placement="top" data-popover-content="#setting_field_{{ $item['name'] }}_img_popover" data-html="true" data-trigger="hover"><i class="fa fa-info"></i></span>
                                                                <div class="d-none" id="setting_field_{{ $item['name'] }}_img_popover">
                                                                    <img src="{{ setting($item['name']) }}" class="popover_img" alt="Фото отсутствует">
                                                                </div>
                                                            @endif
                                                        </label>
                                                        <div class="col-sm-9">
                                                            @switch($fieldType)
                                                                @case('text')
                                                                @case('number')
                                                                @case('file')
                                                                <div class="input-group">
                                                                    @isset($item['_prependCopyBtn'])
                                                                        <div class="input-group-prepend">
                                                                            <button type="button" class="btn btn-sm btn-info" data-copy-text="{{ $item['_prependCopyBtn'] }}" title="Скопировать">
                                                                                <i class="fa fa-copy"></i>
                                                                            </button>
                                                                        </div>
                                                                    @endisset
                                                                    <input
                                                                            type="{{ $fieldType }}"
                                                                            name="{{ $item['name'] }}"
                                                                            class="form-control"
                                                                            id="setting_field_{{ $item['name'] }}"
                                                                            value="{{ old($item['name'], setting($item['name'])) }}"
                                                                            @isset($item['step'])step="{{ $item['step'] }}"@endisset
                                                                            @isset($item['min'])min="{{ $item['min'] }}"@endisset
                                                                            @isset($item['max'])max="{{ $item['max'] }}"@endisset
                                                                            @isset($item['accept'])accept="{{ $item['accept'] }}"@endisset
                                                                    />
                                                                    @isset($item['_generate'])
                                                                        <div class="input-group-append">
                                                                            <button type="button" class="btn btn-sm btn-success" data-generate-random-string="32|{{ $item['name'] }}" title="Сгенерировать новый">
                                                                                <i class="fa fa-refresh"></i>
                                                                            </button>
                                                                        </div>
                                                                    @endisset
                                                                </div>
                                                                @break

                                                                @case('checkbox')
                                                                <div class="d-flex align-items-center h-100">
                                                                    <input
                                                                            type="checkbox"
                                                                            name="{{ $item['name'] }}"
                                                                            class="form-check-switcher"
                                                                            id="setting_field_{{ $item['name'] }}"
                                                                            value="1"
                                                                            {{ old($item['name'], setting($item['name'])) ? 'checked' : '' }}
                                                                    />
                                                                </div>
                                                                @break

                                                                @case('textarea')
                                                                <textarea
                                                                        name="{{ $item['name'] }}"
                                                                        id="setting_field_{{ $item['name'] }}"
                                                                        class="form-control"
                                                                        rows="{{ $item['rows'] ?? 5 }}"
                                                                >{{ old($item['name'], setting($item['name'])) }}</textarea>
                                                                @break

                                                                @case('select')
                                                                <select class="form-control" id="setting_field_{{ $item['name'] }}" name="{{ $item['name'] }}">
                                                                    @isset($item['optionDefault'])<option value="">{{ $item['optionDefault'] }}</option>@endisset
                                                                    @foreach($item['options'] as $optionName => $optionLabel)
                                                                        <option value="{{ $optionName }}" {{ old($item['name'], setting($item['name'])) == $optionName ? 'selected' : '' }}>{{ $optionLabel }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @break
                                                            @endswitch
                                                        </div>
                                                    </div>
                                            @endswitch

                                        @endforeach
                                    </div>
                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('[data-toggle="popover"]').popover({
                container: 'body',
                content: function() {
                    var content = $(this).attr('data-popover-content');
                    return content ? $(content).html() : undefined;
                },
            })

            function randomString(length) {
                let result = '';
                const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
                const charactersLength = characters.length;
                let counter = 0;
                while (counter < length) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                    counter += 1;
                }
                return result;
            }
            function copyToClipboard(val) {
                let $temp = $('<textarea />');
                $('body').append($temp);
                $temp.val(val).select();
                document.execCommand('copy');
                $temp.remove();
            }


            $('[data-generate-random-string]').click(function(e) {
                e.preventDefault();
                var $this = $(this);
                var opts = $this.data('generate-random-string').split('|');

                var length = opts[0];
                var fieldName = opts[1];

                $('[name="'+fieldName+'"]').val(randomString(length));
            });


            function runYandexFeed(name) {
                if (! confirm('Запустить фид?')) {
                    return;
                }

                window.onbeforeunload = function confirmExit() {
                    return 'Закрыть страницу? Фид не будет завершен.'
                }

                alert('Фид запушен.\nНе переходите на другую станицу и не сохраняйте и не обновляйтесь, пока не завершится. \nМы сообщим, когда завершится.');
                var $btn = $('.btn-yandex-feed').attr('disabled', true);
                $.ajax({
                    method: 'post',
                    url: '{{ route('settings.yandex_feed', '') }}/' + name,
                    success: function() {
                        $btn.attr('disabled', false);
                        alert('Фид завершен!');
                        window.onbeforeunload = null;
                    },
                    error: function(e) {
                        $btn.attr('disabled', false);
                        console.log(e, [e])
                        alert('Фид завершился с ОШИБКОЙ!');
                        window.onbeforeunload = null;
                    }
                })
            }
            $('[data-yandex-feed-submit]').click(function(e) {
                e.preventDefault();
                runYandexFeed($(this).data('yandex-feed-submit'))
            });
            $('[data-yandex-feed-brand-submit]').click(function(e) {
                e.preventDefault();
                var name = $('[data-yandex-feed-brand]').val();

                if (! name) {
                    return alert('Выберите марку');
                }

                runYandexFeed(name);
            });
            $('[data-copy-text]').click(function(e) {
                e.preventDefault();
                copyToClipboard($(this).data('copy-text'));
            });
        });
    </script>
@endsection
