<div class="form-group{{ $errors->has('left_side') ? 'has-error' : ''}}">
    {!! Form::label('left_side', 'Левая сторона', ['class' => 'control-label']) !!}
    {!! Form::textarea('left_side', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('left_side', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('right_side') ? 'has-error' : ''}}">
    {!! Form::label('right_side', 'Правая сторона', ['class' => 'control-label']) !!}
    {!! Form::textarea('right_side', null, ('' == 'required') ? ['class' => 'crud-richtext', 'required' => 'required'] : ['class' => 'crud-richtext']) !!}
    {!! $errors->first('right_side', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
