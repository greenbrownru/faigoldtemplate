@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Объявление {{ $advert->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/adverts') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/adverts/' . $advert->id . '/edit') }}" title="Edit Advert">
                            <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/adverts', $advert->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-sm',
                                'title' => 'Delete Advert',
                                'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $advert->id }}</td>
                                </tr>
                                <tr>
                                    <th> Марка</th>
                                    <td> {{ $advert->brand->name }} </td>
                                </tr>
                                <tr>
                                    <th> Модель</th>
                                    <td> {{ $advert->amodel->name }} </td>
                                </tr>

                                <tr>
                                    <th> Пробег</th>
                                    <td> {{ $advert->mileage }} </td>
                                </tr>
                                <tr>
                                    <th> Кузов</th>
                                    <td> {{ $advert->carbodylabel }} </td>
                                </tr>
                                <tr>
                                    <th> Коробка</th>
                                    <td> {{ $advert->transmissionlabel }} </td>
                                </tr>
                                <tr>
                                    <th> Двигатель</th>
                                    <td> {{ $advert->enginelabel }} </td>
                                </tr>
                                <tr>
                                    <th> Руль</th>
                                    <td> {{ $advert->steeringwheellabel }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
