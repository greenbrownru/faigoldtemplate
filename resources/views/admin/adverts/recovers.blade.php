@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Откаты для восстановления</div>
                    <div class="card-body">
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>Всего/Загружено</th>
                                    <th>ID импорта</th>
                                    <th>Актуальный импорт</th>
                                    <th>Дата импорта</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td><a href="{{url('/admin/recovers/show', [$item->import_id])}}">{{ $item->cnt }}/{{ $item->cnt }}</a></td>
                                        <td>{{ $item->import_id }}</td>
                                        <td>@if($item->is_current == 1) <span class="alert alert-success">Да</span> @else <span class="alert alert-light">Нет</span> @endif</td>
                                        <td>{{ $date = DateTime::createFromFormat('YmdHi', $item->import_id)->format('d.m.Y H:i') }}</td>
                                        <td>@if($item->is_current == 0)
                                                {!! Form::open(['url' => '/admin/adverts/set-current', 'class' => 'form-horizontal', 'files' => true]) !!}
                                                {!! Form::hidden('import_id', $item->import_id, []) !!}
                                                <input type="submit" class="btn btn-primary" value="Актуализировать">
                                                {!! Form::close() !!}
                                            @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
