@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Импортированные авто</div>
                    <div class="card-body">
                        <a href="/admin/recovers" class="btn btn-sm btn-info">Назад к списку</a>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Импорт от {{ $date = DateTime::createFromFormat('YmdHi', $items->first()->import_id)->format('d.m.Y H:i') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$item->brand->name}} - {{$item->aModel->name}} - {{$item->price}} ₽ - {{$item->year_of_issue}} год - {{$item->mileage}} км пробег</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
