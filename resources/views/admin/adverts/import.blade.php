@extends('layouts.backend')
@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Импорт объявлений</div>
                    <div class="card-body">
                        {!! Form::open(['url' => '/admin/adverts/import', 'class' => 'form-horizontal', 'files' => true]) !!}
                        <div class="form-group{{ $errors->has('filename') ? 'has-error' : ''}}">
                            {!! Form::label('filename', 'Выбрать xml файл', ['class' => 'control-label']) !!}
                            <br>
                            {!! Form::file('filename', null,  ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('filename', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Импортировать', ['class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
