
<div class="form-group{{ $errors->has('brand_id') ? 'has-error' : ''}}">
    {!! Form::label('brand_id', 'Марка', ['class' => 'control-label']) !!}
    {!! Form::select('brand_id', \App\Models\Brand::orderBy('name')->get()->pluck('name', 'id')->toArray(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('model_id') ? 'has-error' : ''}}">
    {!! Form::label('model_id', 'Модель', ['class' => 'control-label']) !!}
    {!! Form::select('model_id', !empty($advert->model_id) ? \App\Models\AModel::whereBrandId($advert->brand_id)->orderBy('name')->pluck('name', 'id')->toArray() : ['' => '--'], null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('model_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('year_of_issue') ? 'has-error' : ''}}">
    {!! Form::label('year_of_issue', 'Год выпуска', ['class' => 'control-label']) !!}
    {!! Form::number('year_of_issue', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('year_of_issue', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mileage') ? 'has-error' : ''}}">
    {!! Form::label('mileage', 'Пробег', ['class' => 'control-label']) !!}
    {!! Form::number('mileage', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mileage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('car_body') ? 'has-error' : ''}}">
    {!! Form::label('car_body', 'Кузов', ['class' => 'control-label']) !!}
    {!! Form::select('car_body', ( new \App\Models\Advert)->carBodies(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('car_body', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('transmission') ? 'has-error' : ''}}">
    {!! Form::label('transmission', 'Коробка', ['class' => 'control-label']) !!}
    {!! Form::select('transmission', (new \App\Models\Advert)->transmissions(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('transmission', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('engine') ? 'has-error' : ''}}">
    {!! Form::label('engine', 'Двигатель', ['class' => 'control-label']) !!}
    {!! Form::select('engine', (new \App\Models\Advert)->engines(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('engine', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('steering_wheel') ? 'has-error' : ''}}">
    {!! Form::label('steering_wheel', 'Руль', ['class' => 'control-label']) !!}
    {!! Form::select('steering_wheel', (new \App\Models\Advert)->steeringWheels(), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('steering_wheel', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('color') ? 'has-error' : ''}}">
    {!! Form::label('color', 'Цвет', ['class' => 'control-label']) !!}
    {!! Form::text('color', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('color', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('car_drive') ? 'has-error' : ''}}">
    {!! Form::label('car_drive', 'Привод', ['class' => 'control-label']) !!}
    {!! Form::select('car_drive',  (new \App\Models\Advert)->carDrives(),  null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('car_drive', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('power') ? 'has-error' : ''}}">
    {!! Form::label('power', 'Мощность', ['class' => 'control-label']) !!}
    {!! Form::number('power', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('power', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('volume') ? 'has-error' : ''}}">
    {!! Form::label('volume', 'Объем', ['class' => 'control-label']) !!}
    {!! Form::number('volume', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('volume', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number_of_owners') ? 'has-error' : ''}}">
    {!! Form::label('number_of_owners', 'Владельцев', ['class' => 'control-label']) !!}
    {!! Form::text('number_of_owners', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('number_of_owners', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('vin') ? 'has-error' : ''}}">
    {!! Form::label('vin', 'ВИН', ['class' => 'control-label']) !!}
    {!! Form::text('vin', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('vin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price') ? 'has-error' : ''}}">
    {!! Form::label('price', 'Цена', ['class' => 'control-label']) !!}
    {!! Form::number('price', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price_by_action') ? 'has-error' : ''}}">
    {!! Form::label('price_by_action', 'Цена по акции', ['class' => 'control-label']) !!}
    {!! Form::number('price_by_action', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price_by_action', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('serv_ae') ? 'has-error' : ''}}">
    {!! Form::checkbox('vin_verified', null) !!}
    {!! Form::label('vin_verified', 'ВИН подтвержден', ['class' => 'control-label']) !!}
    {!! $errors->first('vin_verified', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('kasko_in_gift') ? 'has-error' : ''}}">
    {!! Form::checkbox('kasko_in_gift', null) !!}
    {!! Form::label('kasko_in_gift', 'КАСКО в подарок', ['class' => 'control-label']) !!}
    {!! $errors->first('kasko_in_gift', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('rubber_kit') ? 'has-error' : ''}}">
    {!! Form::checkbox('rubber_kit', null) !!}
    {!! Form::label('rubber_kit', 'Комплект резины', ['class' => 'control-label']) !!}
    {!! $errors->first('rubber_kit', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('is_active') ? 'has-error' : ''}}">
    {!! Form::checkbox('is_active', null, true) !!}
    {!! Form::label('is_active', 'Активно', ['class' => 'control-label']) !!}
    {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('is_vip') ? 'has-error' : ''}}">
    {!! Form::checkbox('is_vip', null) !!}
    {!! Form::label('is_vip', 'VIP-объявление', ['class' => 'control-label']) !!}
    {!! $errors->first('is_vip', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('is_spec') ? 'has-error' : ''}}">
    {!! Form::checkbox('is_spec', null) !!}
    {!! Form::label('is_spec', 'Спецпредложение', ['class' => 'control-label']) !!}
    {!! $errors->first('is_spec', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo[]', 'Загрузите фото', ['class' => 'control-label']) !!}
    {!! Form::file('photo[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control',]) !!}
    {!! $errors->first('photo.0', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo[]', 'Загрузите фото', ['class' => 'control-label']) !!}
    {!! Form::file('photo[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control',]) !!}
    {!! $errors->first('photo.1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo[]', 'Загрузите фото', ['class' => 'control-label']) !!}
    {!! Form::file('photo[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control',]) !!}
    {!! $errors->first('photo.2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo[]', 'Загрузите фото', ['class' => 'control-label']) !!}
    {!! Form::file('photo[]', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control',]) !!}
    {!! $errors->first('photo.3', '<p class="help-block">:message</p>') !!}
</div>
<div>
    <h4>Опции</h4>
    @foreach($equipmentGroups as $item)
        <p><b>{{$item->name}}</b>
            <br>
        @foreach($item->equipments as $equipment)
            <span><input type="checkbox" name="equipment[]" @if(isset($advertEquipments) && array_key_exists($equipment->id, $advertEquipments)) checked="checked" @endif value="{{$equipment->id}}"> {{$equipment->name}}</span>
            <br>
        @endforeach
        </p>
    @endforeach
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
