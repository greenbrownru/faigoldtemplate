@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Объявления</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/adverts/create') }}" class="btn btn-success btn-sm"
                           title="Add New Advert">
                            <i class="fa fa-plus" aria-hidden="true"></i> Добавить
                        </a>
                        <a href="{{ url('/admin/adverts/import') }}" class="btn btn-info btn-sm">
                            Импорт
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/adverts', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Поиск..."
                                   value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@sortablelink('brand.name', 'Марка')</th>
                                    <th>@sortablelink('amodel.name', 'Модель')</th>
                                    <th>@sortablelink('year_of_issue', 'Год выпуска')</th>
                                    <th>@sortablelink('mileage', 'Пробег')</th>
                                    <th>@sortablelink('transmission', 'Коробка')</th>
                                    <th>@sortablelink('engine', 'Двигатель')</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($adverts as $item)
                                    <tr>
                                        <td>{{ $item->id}}</td>
                                        <td style="min-width: 240px"> @if($item->photos()->count() > 0) <img
                                                src="{{asset('/storage/photos/'.$item->photos->first()->filename ?? '')}}"
                                                style="width: 100px"> @endif {{ $item->brand->name }}</td>
                                        <td>{{ $item->amodel->name }}</td>
                                        <td>{{ $item->year_of_issue }}</td>

                                        <td>{{ $item->mileage }}</td>
                                        <td>{{ $item->transmissionLabel }}</td>
                                        <td>{{ $item->engineLabel }}</td>
                                        <td>
                                            <a href="{{ url('/admin/adverts/' . $item->id) }}" title="View Advert">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i></button>
                                            </a>
                                            <a href="{{ url('/admin/adverts/' . $item->id . '/edit') }}"
                                               title="Edit Advert">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/adverts', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-sm',
                                                    'title' => 'Delete Advert',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $adverts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
