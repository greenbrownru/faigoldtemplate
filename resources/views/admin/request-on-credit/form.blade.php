<div class="form-group{{ $errors->has('advert_id') ? 'has-error' : ''}}">
    {!! Form::label('advert_id', 'Объявление', ['class' => 'control-label']) !!}
    {!! Form::select('advert_id', \App\Models\Advert::query()->active()->get()->pluck('creditname', 'id'),null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('advert_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('credit_term') ? 'has-error' : ''}}">
    {!! Form::label('credit_term', 'Срок кредита (месяцев)', ['class' => 'control-label']) !!}
    {!! Form::number('credit_term', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('credit_term', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('down_payment') ? 'has-error' : ''}}">
    {!! Form::label('down_payment', 'Первоначальный взнос (%)', ['class' => 'control-label']) !!}
    {!! Form::number('down_payment', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('down_payment', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('monthly_payment') ? 'has-error' : ''}}">
    {!! Form::label('monthly_payment', 'Ежемесячный платеж (руб)', ['class' => 'control-label']) !!}
    {!! Form::number('monthly_payment', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('monthly_payment', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Имя', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Телефон', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Обновить' : 'Создать', ['class' => 'btn btn-primary']) !!}
</div>
