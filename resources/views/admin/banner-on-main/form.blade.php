<div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Заголовок', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('options') ? 'has-error' : ''}}">
    {!! Form::label('options', 'Опции: <b>[[credit_percent]]</b> - Процентная ставка', ['class' => 'control-label'], false) !!}
    <div>
        <div id="optionsWrap" class="mb-3">
            @if($formMode === 'edit')
                @foreach($banneronmain->options ?: [] as $v)
                    <input type="text" name="options[]" class="form-control" value="{{ $v }}">
                @endforeach
            @endif
        </div>
        <button type="button" class="btn btn-sm btn-success btn-add-option">+</button>
        <button type="button" class="btn btn-sm btn-danger btn-remove-option">x</button>
    </div>


    {!! $errors->first('options', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('link') ? 'has-error' : ''}}">
    {!! Form::label('link', 'Link', ['class' => 'control-label']) !!}
    {!! Form::text('link', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('is_button') ? 'has-error' : ''}}">
    {!! Form::label('is_button', 'Показывать кнопку', ['class' => 'control-label']) !!}
    {!! Form::checkbox('is_button', null, null, ['class' => 'form-check']) !!}
    {!! $errors->first('is_button', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('button_name') ? 'has-error' : ''}}">
    {!! Form::label('button_name', 'Название кнопки', ['class' => 'control-label']) !!}
    {!! Form::text('button_name', $formMode === 'edit' ? null : 'Подробнее', ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('button_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('button_action') ? 'has-error' : ''}}">
    {!! Form::label('button_action', 'Действие кнопки', ['class' => 'control-label']) !!}
    {!! Form::select('button_action', ['link' => 'Открывать ссылку', 'popup_form_contact' => 'Всплывающее окно с формой'], null, ['class' => 'form-control']) !!}
    {!! $errors->first('button_action', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('sort') ? 'has-error' : ''}}">
    {!! Form::label('sort', 'Сортировка', ['class' => 'control-label']) !!}
    {!! Form::number('sort', 0, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('sort', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('img_url') ? 'has-error' : ''}} mt-5">
    {!! Form::label('img_url', 'Фото url', ['class' => 'control-label']) !!}
    {!! Form::file('img_url', ['class' => 'form-control', 'accept' => 'image/*']) !!}
    {!! $errors->first('img_url', '<p class="help-block">:message</p>') !!}
</div>
@if($formMode === 'edit')
    <h6>Уже загруженная картинка</h6>
    <a href="{{ $banneronmain->img_url }}" target="_blank">
        <img src="{{ $banneronmain->img_url }}" alt="" style="max-width: 300px">
    </a>
@endif
<div class="form-group{{ $errors->has('img_mobile_url') ? 'has-error' : ''}} mt-5">
    {!! Form::label('img_mobile_url', 'Фото моб. url', ['class' => 'control-label']) !!}
    {!! Form::file('img_mobile_url', ['class' => 'form-control', 'accept' => 'image/*']) !!}
    {!! $errors->first('img_mobile_url', '<p class="help-block">:message</p>') !!}
</div>
@if($formMode === 'edit')
    <h6>Уже загруженная картинка</h6>
    <a href="{{ $banneronmain->img_mobile_url }}" target="_blank">
        <img src="{{ $banneronmain->img_mobile_url }}" alt="" style="max-width: 300px">
    </a>
@endif

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>


@section('scripts')
<script>
    $(function() {
        var $elOpts = $('#optionsWrap');
        $('.btn-add-option').click(function(e) {
            e.preventDefault();

            $elOpts.append($('<input >', {
                name: 'options[]',
                class: 'form-control'
            }));
        });

        $('.btn-remove-option').click(function(e) {
            e.preventDefault();

            $elOpts.children().last().remove();
        });
    });
</script>
@endsection