@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">BannerOnMain {{ $banneronmain->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/banner-on-main') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/banner-on-main/' . $banneronmain->id . '/edit') }}" title="Edit BannerOnMain"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/banneronmain', $banneronmain->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete BannerOnMain',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th>ID</th><td>{{ $banneronmain->id }}</td></tr>
                                    <tr>
                                        <th>Photo</th>
                                        <td>
                                            <a href="{{ $banneronmain->img_url }}">
                                                <img src="{{ $banneronmain->img_url }}" alt="" style="max-width: 300px">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Photo mobile</th>
                                        <td>
                                            <a href="{{ $banneronmain->img_mobile_url }}">
                                                <img src="{{ $banneronmain->img_mobile_url }}" alt="" style="max-width: 300px">
                                            </a>
                                        </td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $banneronmain->title }} </td></tr>
                                    <tr><th> Options </th><td> {{ implode(',', $banneronmain->options ?: []) }} </td></tr>
                                    <tr><th> Link </th><td> {{ $banneronmain->link }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
