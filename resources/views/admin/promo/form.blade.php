<div class="form-group{{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Фото', ['class' => 'control-label']) !!}
    {!! Form::file('photo', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Название акции', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('short_description') ? 'has-error' : ''}}">
    {!! Form::label('short_description', 'Короткое описание', ['class' => 'control-label']) !!}
    {!! Form::text('short_description', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('full_description') ? 'has-error' : ''}}">
    {!! Form::label('full_description', 'Полное описание', ['class' => 'control-label']) !!}
    {!! Form::textarea('full_description', null, ['class' => 'form-control']) !!}
    {!! $errors->first('full_description', '<p class="help-block">:message</p>') !!}
</div>
<div class="mb-3">
    Переменные:
    <br><b>[[name]]</b> - Название салона
    <br><b>[[credit_percent]]</b> - Процентная ставка
    <br><b>[[city]]</b> - Город
</div>
<div class="form-group{{ $errors->has('is_active') ? 'has-error' : ''}}">
    {!! Form::label('is_active', 'Активна', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('is_active', '1') !!} Да</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('is_active', '0', true) !!} Нет</label>
</div>
    {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('with_form') ? 'has-error' : ''}}">
    {!! Form::label('with_form', 'С формой', ['class' => 'control-label']) !!}
    {!! Form::checkbox('with_form', null) !!}
    {!! $errors->first('with_form', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Обновить' : 'Создать', ['class' => 'btn btn-primary']) !!}
</div>
