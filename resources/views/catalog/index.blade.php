@extends('layouts.auto')
@section('content')
    @php /** @var \Illuminate\Pagination\LengthAwarePaginator $items */ @endphp
    <style>
        .select {
            height: 34px;
        }
    </style>
    <!-- catalog-->
    <section class="catalog" id="js-catalog">
        <div class="container">
            @if(\App\Services\Region::isAutodromium())
                @if(request()->has('brand'))
                    <h1 class="section-title">Автомобили {{\App\Models\Brand::find(request()->brand)->name ?? ''}} с
                        пробегом<span class="grey"><span class="blue"></span></span></h1>
                @else
                    <h1 class="section-title">Продажа авто с пробегом<span class="grey"><span
                                class="blue"></span></span></h1>
                @endif
            @else
                <h1 class="section-title">
                    Продажа @if(isset(request()->brand))
                        {{\App\Models\Brand::find(request()->brand)->name ?? ''}}
                        @if(isset(request()->model))
                            {{\App\Models\AModel::find(request()->model)->name ?? '' }}
                        @endif
                    @else
                        авто
                    @endif с пробегом<span class="grey"><span class="blue"></span></span></h1>
            @endif

            <div class="catalog__row">
                <div class="catalog__left">
                    <div class="filter-btn"><span>Параметры</span>
                        <img src="/img/icons/filter.svg">
                    </div>
                    <!-- filter-->
                    <div class="filter">
                        <div class="filter__header">
                            <div class="filter__back">
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <form id="catalog_form" action="/catalog" method="get">
                            <div class="filter__row">
                                <div class="filter__col">
                                    <div class="select js-search" id="js-brand">
                                        <div
                                            class="select__label">@if(!request()->has('brand') || !request()->filled('brand'))
                                                Марка
                                            @endif</div>
                                        <div class="select__input"
                                             @if(request()->has('brand')) style="box-shadow: 0 0 5px 0 rgba(53,123,181,.6)" @endif> {{ $brand_name }} </div>
                                        <input type="hidden" name="brand" value="{!! request()->brand !!}">
                                        <ul class="select__list">
                                            <li class="li-brand" data-value=""
                                                data-target="#js-brand0">Все марки
                                            </li>
                                            <li class="select__header">
                                                <span class="select__close">Отмена</span>Марка
                                            </li>

                                            @foreach($brandMenu as $brand)
                                                <li class="li-brand" data-value="{{$brand->id}}"
                                                    data-target="#js-{!! md5($brand->name) !!}">{{$brand->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="filter__col">
                                    <div class="select js-search" id="js-model">
                                        <div
                                            class="select__label">@if(!request()->has('model') || !request()->filled('model'))
                                                Модели
                                            @endif</div>
                                        <div class="select__input"
                                             @if(request()->has('model')) style="box-shadow: 0 0 5px 0 rgba(53,123,181,.6)" @endif> {{ $model_name }} </div>
                                        <input type="hidden" name="model" value="{!! request()->model !!}">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Модель
                                            </li>
                                            <li class="li-model" data-value="">Все модели</li>
                                            @if($models)
                                                @foreach($models as $model)
                                                    <li class="li-model"
                                                        data-value="{{$model->id}}">{{$model->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select js-search" id="js-generation">
                                        <div
                                            class="select__label">@if(!request()->has('generation')  || !request()->filled('generation'))
                                                Поколение
                                            @endif</div>
                                        <div class="select__input"
                                             @if(request()->has('generation')) style="box-shadow: 0 0 5px 0 rgba(53,123,181,.6)" @endif> {{ $generation_name }} </div>
                                        <input type="hidden" name="generation" value="{!! request()->generation !!}">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Поколение
                                            </li>
                                            <li class="li-generation" data-value="">Все поколения</li>
                                            @if(isset($generations))
                                                @foreach($generations as $generation)
                                                    <li class="li-generation"
                                                        data-value="{{$generation->id}}">{{$generation->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select select--dual">
                                        {!! Form::text('price_from', request()->price_from, ['class' => 'select select__input input-filter', 'placeholder' => 'Цена от, ₽', 'autocomplete' => 'off']) !!}
                                    </div>
                                    <div class="select select--dual">
                                        {!! Form::text('price_to', request()->price_to, ['class' => 'select select__input input-filter', 'placeholder' => 'До', 'autocomplete' => 'off']) !!}
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select select--dual">
                                        <div class="select__label">@if(!request()->filled('year_from'))
                                                Год,
                                                от
                                            @endif</div>
                                        <div
                                            class="select__input">@if(request()->filled('year_from'))
                                                {{request()->year_from }}
                                            @endif </div>
                                        <input type="hidden" name="year_from"
                                               value="{{ request()->input('year_from') }}">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Год, от
                                            </li>
                                            @foreach($years as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="select select--dual">
                                        <div class="select__label">@if(!request()->filled('year_to'))
                                                Год,
                                                до
                                            @endif</div>
                                        <div
                                            class="select__input">@if(request()->filled('year_to'))
                                                {{request()->year_to }}
                                            @endif </div>
                                        <input type="hidden" name="year_to" value="{{ request()->input('year_to') }}">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Год, от
                                            </li>
                                            @foreach($years as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select select--dual">
                                        <input type="text" value="{{request()->input('mileage_from')}}" autocomplete="off"
                                               name="mileage_from"
                                               placeholder="Пробег от, км" class="select select__input input-filter">
                                    </div>
                                    <div class="select select--dual">
                                        <input type="text" value="{{request()->input('mileage_to')}}" autocomplete="off"
                                               name="mileage_to"
                                               placeholder="До" class="select select__input input-filter">
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select select--half">
                                        <div class="select__label">@if(!request()->filled('transmission'))
                                                Коробка
                                            @endif</div>
                                        <div
                                            class="select__input"> @if(request()->filled('transmission'))
                                                    <?= (new \App\Models\Advert)->transmissions()[request()->transmission] ?>
                                            @endif  </div>
                                        <input type="hidden" name="transmission"
                                               value="{!! request()->transmission !!}">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Коробка
                                            </li>
                                            <li data-value="">Коробка</li>
                                            @foreach((new \App\Models\Advert)->transmissions() as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="select select--half">
                                        <div class="select__label">@if(!request()->filled('car_drive'))
                                                Привод
                                            @endif</div>
                                        <div
                                            class="select__input">@if(request()->filled('car_drive'))
                                                    <?= (new \App\Models\Advert)->carDrives()[request()->car_drive] ?>
                                            @endif </div>
                                        <input type="hidden" name="car_drive" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Привод
                                            </li>
                                            @foreach((new \App\Models\Advert)->carDrives() as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select">
                                        <div class="select__label">Типы кузова</div>
                                        <div class="select__input"></div>
                                        <input type="hidden" name="car_body" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Кузов
                                            </li>
                                            <li data-value="value">Все кузова</li>
                                            @foreach((new \App\Models\Advert)->carBodies() as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter__col">
                                    <div class="select">
                                        <div class="select__label">Типы двигателей</div>
                                        <div class="select__input"></div>
                                        <input type="hidden" name="engine" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Двигатель
                                            </li>
                                            <li data-value="value">Все двигатели</li>
                                            @foreach((new \App\Models\Advert)->engines() as $key => $label)
                                                <li data-value="{{$key}}">{{$label}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="filter__col">
                                    <a href="/catalog" class="filter__reset">Сбросить фильтры</a>
                                </div>
                                <div class="filter__col">
                                    <?php $last_digit = substr($items->total(), -1) ?>
                                    <button class="filter__btn" @if($items->total() == 0) style="background-color: grey"
                                            disabled="disabled" @endif type="submit">@if($items->total() == 0)
                                            Нет
                                            предложений
                                        @else
                                            Показать {{$items->total()}}
                                            предложени@if($last_digit == 0 || ($last_digit > 4) || ($items->total() > 10 && $items->total() < 20 && $last_digit >0))й
                                            @elseif($last_digit==1 && ($items->total() > 20 || $items->total() < 10 ))е
                                            @elseif($last_digit > 1 && ($items->total() < 10 || $items->total() > 20))я
                                            @endif
                                        @endif</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end filter-->
                    <!-- models-->
                    <div class="models">
                        @foreach($modelLinks as $key => $links)
                            <div class="models__section" id="js-{!! md5(trim($key)) !!}"
                                 @if(md5(trim($key)) === md5(trim($brand_name)) && !request()->filled('model')) style="display: block" @endif>
                                <ul class="models__list" @if(request()->filled('model')) style="display:none" @endif>
                                    @foreach($links as $link)
                                        <li>
                                            @if((\App\Services\Region::isAutodromium() || in_array(env('APP_SITE_REGION'), [18]) || env('IS_ROLF_IMPORT'))  && request()->filled('brand'))
                                                <a href="{{route('main.brand_model', ['brand' => mb_strtolower($key), 'model' => mb_strtolower($link['name'])])}}?{{ http_build_query(request()->except(['brand', 'model'])) }}">{{$link['name']}}
                                                    <span>({{$link['cnt']}})</span></a>
                                            @else
                                                <a href="{{request()->fullUrlWithQuery(
                                                            ['brand' => \App\Models\Brand::find(\App\Models\AModel::find($link['model_id'])->brand_id)->id ?? null,  'model' => $link['model_id']])}}">
                                                    {{$link['name']}}
                                                    <span>({{$link['cnt']}})</span>
                                                </a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                        @if(!request()->has('brand'))
                            <div class="brands__section" id="js-{!! uniqid() !!}" style="display: block">
                                <ul class="models__list">
                                    @foreach($brandMenu as $brandLink)
                                        <li>
                                            @if(env('APP_SITE_REGION') == '72' || env('APP_SITE_REGION') == '' || env('APP_SITE_REGION') == '18' || env('IS_ROLF_IMPORT'))
                                                <a href="{{url('/'.strtolower($brandLink->name))}}">{{$brandLink->name}}
                                                    <span>({{$brandLink->cnt}})</span></a>
                                            @else
                                                <a href="{{request()->fullUrlWithQuery(['brand' => $brandLink->id])}}">{{$brandLink->name}}
                                                    <span>({{$brandLink->cnt}})</span></a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <!-- end models-->
                    <!-- catalog-list-->
                    <section class="catalog-list">
                        <!-- sort-->
                        <div class="sort">
                            <div class="sort__left"><span class="sort__label"> Сортировка по </span>
                                <ul class="sort__list" id="js-sort">
                                    <li @if(request()->query('sort') == 'price_up' || !request()->has('sort') || (request()->query('sort') !== 'price_down'
                                            && request()->query('sort') !== 'year_down' && request()->query('sort') !== 'year_up')) class="current" @endif>
                                        <a href="{{request()->fullUrlWithQuery(['sort' => 'price_up'])}}"> Возрастанию
                                            цены</a></li>
                                    <li @if(request()->query('sort') == 'price_down') class="current" @endif><a
                                            href="{{request()->fullUrlWithQuery(['sort' => 'price_down'])}}">Убыванию
                                            цены</a></li>
                                    <li @if(request()->query('sort') == 'year_up') class="current" @endif><a
                                            href="{{request()->fullUrlWithQuery(['sort' => 'year_up'])}}">Году:
                                            старше</a></li>
                                    <li @if(request()->query('sort') == 'year_down') class="current" @endif><a
                                            href="{{request()->fullUrlWithQuery(['sort' => 'year_down'])}}">Году:
                                            младше</a></li>
                                </ul>
                            </div>
                            <div class="sort__right"><span class="sort__label"> Вывод </span>
                                <ul class="sort__list">
                                    <li @if(request()->query('display') == 'list' || !request()->has('display') || request()->query('display') !== 'grid') class="current" @endif>
                                        <a href="{{request()->fullUrlWithQuery(['display' => 'list'])}}">
                                            <span>Списком</span>
                                            <img src="{{asset('/img/icons/list.svg')}}" alt=""></a>
                                    </li>
                                    <li @if(request()->query('display') == 'grid') class="current" @endif>
                                        <a href="{{request()->fullUrlWithQuery(['display' => 'grid'])}}">
                                            <span>Плиткой</span>
                                            <img src="{{asset('/img/icons/grid.svg')}}" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end sort-->
                        @if(request()->query('display') == 'list' || !request()->has('display') || request()->query('display') !== 'grid')
                            @include('catalog.parts._list_view', ['items' => $items])
                        @elseif(request()->query('display') == 'grid')
                            @include('catalog.parts._grid_view', ['items' => $items])
                        @endif

                        @if(setting('page__catalog_autoload'))
                            <div class="catalog__autoload" id="catalog__autoload">
                                <div class="catalog__autoload__loading">
                                    <img src="/img/round_arrow.png" alt=""> Загрузка...
                                </div>
                            </div>
                        @else
                            <div class="pagination-wrapper"> {!! $items->appends(request()->all())->links() !!} </div>
                        @endif

                        <div class="seo-brand-text article__text">
                            @if(request()->filled('brand') && !request()->filled('model') && \App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->exists())
                                {!! preg_replace('/(\+7) \(\d+\) \d+-\d+-\d+/', showPhone(), \App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->first()->content) ?? null !!}
                            @elseif(request()->filled('brand') && request()->filled('model'))
                                @if(\App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->where('model_name', 'ilike', \App\Models\AModel::find(request()->model)->name)->exists())
                                    {!! preg_replace('/(\+7) \(\d+\) \d+-\d+-\d+/', showPhone(), \App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->where('model_name', 'ilike', \App\Models\AModel::find(request()->model)->name)->first()->content) ?? null !!}
                                @else
                                    @if(\App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->exists())
                                        {!! preg_replace('/(\+7) \(\d+\) \d+-\d+-\d+/', showPhone(), \App\Models\SeoBrandText::where('brand_name', 'ilike', \App\Models\Brand::find(request()->brand)->name)->first()->content) ?? null !!}
                                    @endif
                                @endif
                            @endif

                        </div>
                    </section>
                    <!-- end catalog-list-->
                </div>
                <div class="catalog__right">
                    @include('_banners')
                </div>
            </div>
        </div>
        @if($spec->count() > 0)
            @include('parts.similar', ['items' => $spec])
        @endif
    </section>
    <!-- end catalog-->
@endsection
@section('javascript')
    <script>
        (function ($) {
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            };
        }(jQuery));

        $(".input-filter").inputFilter(function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 90000000);
        });

        if (screen.width < 576) {
            $('.filter_catalog').hide();
        } else {
            $('.filter_catalog').show();
        }

        $(function () {
            @if(setting('page__catalog_autoload'))
            var catalog__autoload = {
                firstRequest: true,
                uri: '',
                page: 1,
                loading: false,
                full: false
            };

            new IntersectionObserver(function (entries) {
                var is = entries[0].isIntersecting, uri;

                if (!is || catalog__autoload.loading || catalog__autoload.full) {
                    return;
                }

                catalog__autoload.loading = true;
                $('.catalog__autoload__loading').addClass('show');

                if (catalog__autoload.firstRequest) {
                    uri = location.pathname;

                    var pageRegex = /(\?|&)page=(\d*)&?/
                    var query = location.search;

                    query === '?' && (query = '');

                    var thisPage = query.match(pageRegex);
                    var isPageInQuery = !!thisPage;
                    thisPage = thisPage ? thisPage[2] : 1;

                    if (isPageInQuery) {
                        query = query.replace(pageRegex, '$1');
                    }

                    query += (query ? '&' : '?') + '_ajaxAutoLoad=1&page='

                    catalog__autoload.page = thisPage;
                    catalog__autoload.uri = uri + query;
                    catalog__autoload.firstRequest = false;
                }

                catalog__autoload.page++;

                uri = catalog__autoload.uri + catalog__autoload.page;

                var $catalogContainer = $('.catalog_items_container');
                var isList = $catalogContainer.hasClass('catalog-list__row');
                var isMobile = isList && window.innerWidth <= 575;
                var containerClassName = isMobile ? '.catalog_items_container_mobile' : '.catalog_items_container';

                $.get(uri, function (d) {
                    var $itemsContainer = $(containerClassName, d);
                    catalog__autoload.loading = false;
                    $('.catalog__autoload__loading').removeClass('show');

                    if (!$itemsContainer.find(isMobile ? '.catalog-slider ' : '.catalog_item').length) {
                        catalog__autoload.full = true;
                        return;
                    }

                    $(containerClassName).append($itemsContainer.html());
                });
            }, {threshold: [0]}).observe(document.querySelector('#catalog__autoload'));
            @endif
        });
    </script>
@endsection

@php
$itemsAllPrices = $items->map(fn($v) => $v->priceByAction())->toArray();
$itemsAllYears = $items->pluck('year_of_issue')->toArray();
$_catalogTemplateVars = [
    'title',
    'credit_percent',
    'name',
    'city',
    'count' => $items->total(),
    'min_price' => $itemsAllPrices ? min($itemsAllPrices) : '',
    'max_price' => $itemsAllPrices ? max($itemsAllPrices) : '',
    'min_year' => $itemsAllYears ? min($itemsAllYears) : '',
    'max_year' => $itemsAllYears ? max($itemsAllYears) : '',
];
$_catalogNameTitle = 'page__catalog_title';
$_catalogNameDescription = 'page__catalog_description';
if (isset(request()->brand)) {
    $_catalogNameTitle = 'page__catalog_brand_title';
    $_catalogNameDescription = 'page__catalog_brand_description';
    $_catalogTemplateVars['brand'] = \App\Models\Brand::find(request()->brand)->name ?? null;
    if (isset(request()->model)) {
        $_catalogNameTitle = 'page__catalog_brand_model_title';
        $_catalogNameDescription = 'page__catalog_brand_model_description';
        $_catalogTemplateVars['model'] = \App\Models\AModel::find(request()->model)->name ?? $model_name;
    }
}
@endphp

@section('title'){{ setTemplateVars($_catalogTemplateVars, setting($_catalogNameTitle) ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars($_catalogTemplateVars, setting($_catalogNameDescription) ?: setting('page__description')) }}@endsection