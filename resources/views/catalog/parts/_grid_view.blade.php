<div class="catalog-grid__row catalog_items_container">
@if($items->count() > 0)
    @foreach($items as $item)
        <div class="catalog-grid__item catalog-grid__item--third catalog_item {{ $item->is_current ? '' : 'auto__sold' }}">
            <div class="card-grid">
                <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}"
                   class="card-link"></a>
                <div class="card-grid__labels">
                    @if($item->vin_verified)<span class="green">VIN проверен</span> @endif
                    @if($item->kasko_in_gift)<span class="navy-blue">КАСКО в подарок</span> @endif
                    @if($item->rubber_kit)<span class="light-blue">Комплект резины</span> @endif
                </div>
                <!-- card-gallery-->
                <a class="card-gallery"
                   href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">
                    @if(env('APP_SITE_REGION') == '')
                        <span class="card-gallery-city">@if($item->region_id == 66 && $item->is_current == true)
                                Екатеринбург @elseif($item->region_id == 63 && $item->is_current === true)
                                Самара @elseif($item->region_id == 52 && $item->is_current === true)
                                Нижний Новгород
                            @endif</span>
                    @endif
                    @if(\App\Services\Region::isAutodromium())
                        @if($item->is_current == false) <span
                            class="card-gallery-city">Автомобиль продан</span>@endif
                    @endif
                    <div class="card-gallery__wrap">
                        @if($item->photos->count() > 0)
                            <?php $i = 0 ?>
                            @foreach($item->photos as $photo)
                                @if($i == 4)

                                    <div class="card-gallery__item">
                                        <div class="img">
                                            <img src="{{$photo->imagepathResized ?? ''}}" alt=""></div>
                                        @if($item->photos->count() > 5)
                                            <div class="card-gallery__more">
                                                <img src="img/icons/camera.svg"
                                                     alt=""><span>Еще <?= $item->photos->count() - 5 ?> фото</span>
                                            </div>
                                        @endif
                                    </div>
                                    @break
                                @else

                                    <div class="card-gallery__item @if($i == 0) hover @endif">
                                        <div class="img">
                                            <img src="{{$photo->imagepathResized ?? ''}}" alt="">
                                        </div>
                                    </div>
                                @endif
                                <?php $i++ ?>
                            @endforeach
                        @endif
                    </div>
                </a>
                <!-- end card-gallery-->
                <div class="card-grid__desc">
                    @if(\App\Services\Region::isAutodromium())
                        <b class="card-grid__name">
                            <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                        </b>
                    @else
                        <h3 class="card-grid__name">
                            <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                        </h3>
                    @endif

                    <p class="card-grid__year">{{$item->year_of_issue}}</p>
                    <div class="card-grid__price">
                        @if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                        <span class="old" @if(intval($item->priceByAction()) == $item->price) style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                    </div>
                    <div class="card-grid__info">
                        <p>{{$item->transmissionLabel}}, {{$item->engineLabel}}</p>
                        <p>{{$item->volume}} см3/{{$item->power}} л.с.</p>
                        <p>{{number_format($item->mileage, 0, ',', ' ')}} км,
                            владельцев {{$item->number_of_owners}}</p>
                    </div>
                    @if($item->is_current)
                    <div class="card-grid__footer">
                        <a class="card-grid__btn" data-fancybox data-type="ajax" data-src="{{url('/credit-modal', ['item' => $item->id])}}" href="javascript:;">
                            @if(in_array(env('SITE_SUFFIX'), ['probeg_spb']))
                                Узнать подробнее
                            @else
                                Купить в кредит
                            @endif
                        </a>
                        <div class="card-grid__credit">
                            <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                            <p>от {{\App\Services\Region::getCreditPercent()}}% годовых</p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@endif
</div>
