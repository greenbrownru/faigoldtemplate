<div class="catalog-list__row catalog_items_container">
    @if($items->count() > 0)
        @foreach($items as $item)
            <div class="catalog-list__col catalog_item {{ $item->is_current ? '' : 'auto__sold' }}">
                <div class="card-list">
                    <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}"
                       class="card-link"></a>
                    <!-- card-gallery-->
                    <a class="card-gallery card-img"
                       href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">
                        @if(env('APP_SITE_REGION') == '')
                            <span class="card-gallery-city">@if($item->region_id == 66 && $item->is_current == true)
                                    Екатеринбург @elseif($item->region_id == 63 && $item->is_current === true)
                                    Самара @elseif($item->region_id == 52 && $item->is_current === true)
                                    Нижний Новгород @endif</span>
                        @endif
                        @if(\App\Services\Region::isAutodromium())
                            @if($item->is_current == false) <span
                                class="card-gallery-city">Автомобиль продан</span>@endif
                        @endif
                        <div class="card-gallery__wrap">
                            @if($item->photos->count() > 0)
                                <?php $i = 0 ?>
                                @foreach($item->photos as $photo)
                                    @if($i == 4)
                                        <div class="card-gallery__item">
                                            <div class="img">
                                                <img src="{{$photo->imagepathresized ?? ''}}" alt="">
                                            </div>
                                            @if($item->photos->count() > 5)
                                                <div class="card-gallery__more">
                                                    <img src="/img/icons/camera.svg"
                                                         alt=""><span>Еще <?= $item->photos->count() - 5 ?> фото</span>
                                                </div>
                                            @endif
                                        </div>
                                        @break
                                    @else

                                        <div class="card-gallery__item @if($i == 0) hover @endif">
                                            <div class="img">
                                                <img src="{{$photo->imagepathresized ?? ''}}" alt="">
                                            </div>
                                        </div>
                                    @endif
                                    <?php $i++ ?>
                                @endforeach
                            @endif
                        </div>
                    </a>
                    <!-- end card-gallery-->
                    <div class="card-list__desc">
                        <div class="card-list__header">
                            @if(\App\Services\Region::isAutodromium() || in_array(env('APP_SITE_REGION'), [123]))
                                <b class="card-list__name">
                                    <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                                    @if(Auth::check()) <a data-fancybox="" data-type="iframe"
                                                          href="{{url('/adverts/'.$item->id.'/edit')}}"
                                                          data-title="Редактировать"
                                                          style="z-index: 2; position: relative"
                                                          class="edit-btn my-modal" title="Редактировать"><i
                                            aria-hidden="true" class="fa fa-pencil-square-o"></i></a> @endif
                                </b>
                            @else
                                <h3 class="card-list__name">
                                    <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                                    @if(Auth::check()) <a data-fancybox="" data-type="iframe"
                                                          href="{{url('/adverts/'.$item->id.'/edit')}}"
                                                          data-title="Редактировать"
                                                          style="z-index: 2; position: relative"
                                                          class="edit-btn my-modal" title="Редактировать"><i
                                            aria-hidden="true" class="fa fa-pencil-square-o"></i></a> @endif
                                </h3>
                            @endif

                            @if($item->is_current)
                                <div class="card-list__price">
                                    @if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                    <span class="old"
                                          @if(intval($item->priceByAction()) == $item->price) style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                                </div>
                            @endif
                        </div>
                        <div class="card-list__info">
                            <div class="card-list__col">
                                <p>{{number_format($item->mileage, 0, ',', ' ')}} км</p>
                                <p>{{$item->year_of_issue}} г.</p>
                                <p>{{$item->transmissionLabel}}</p>
                            </div>
                            <div class="card-list__col">
                                <p>{{$item->cardriveLabel}}</p>
                                <p>{{$item->volume}} см3 / {{$item->power}} л.с.</p>
                                <p>владельцев {{$item->number_of_owners}}</p>
                            </div>
                            @if($item->is_current)
                                <div class="card-list__col">
                                    <p>Кредит от <span>{{$item->CreditPayByMonth}} &#8381; </span>в месяц</p>
                                    <p>Первоначальный платеж <span>{{ setting('initial_fee') }}%</span></p>
                                    <p>Низкий отказ в выдаче кредита</p>
                                </div>
                            @endif
                        </div>
                        <div class="card-list__labels">
                            @include('advert._card_greed_labels', ['item' => $item, 'display_block' =>true])

                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>

<div class="catalog_items_container_mobile">
@if($items->count() > 0)
    @foreach($items as $item)
        <!-- catalog-slider-->
        <div class="catalog-slider mobile">
            <div class="card-slider">
                <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}"
                   class="card-link"></a>
                @if(\App\Services\Region::isAutodromium())
                    <b class="card-slider__name"><a
                            href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                    </b>
                @else
                    <h3 class="card-slider__name"><a
                            href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                    </h3>
                @endif

                @if($item->is_current)
                    <div class="card-slider__price">@if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                        <span class="old" @if(intval($item->priceByAction()) == $item->price) style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                    </div>
                @else
                    <div class="card-slider__price">
                        <span class="old">Автомобиль продан</span>
                    </div>
                @endif
                <div class="card-slider__carousel">
                    @if($item->photos->count() > 0)
                        @foreach($item->photos as $photo)
                            <div class="item">
                                <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->id])}}">
                                    <img src="{{$photo->imagepathresized ?? ''}}" alt="">
                                </a>
                            </div>
                        @endforeach
                    @endif
                    <div class="item">
                        <a class="mobile-gallery-call" @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onclick="ym(85871410,'reachGoal','clicktophone');return true;" @endif
                           href="tel:{{showPhone()}}">
                            {{--<img src="/img/catalog_green_btn.webp">--}}
                        </a>
                    </div>
                </div>
                <div class="card-slider__info">
                    <div class="card-slider__col">
                        <p>{{number_format($item->mileage, 0, ',', ' ')}} км</p>
                        <p>{{$item->year_of_issue}} г.</p>
                        <p>{{$item->transmissionLabel}}</p>
                    </div>
                    <div class="card-slider__col">
                        <p>{{$item->cardriveLabel}}</p>
                        <p>{{$item->volume}} см3 / {{$item->power}} л.с.</p>
                        <p>владельцев {{$item->number_of_owners}}</p>
                    </div>
                </div>
                @if($item->is_current)
                    <div class="card-slider__footer">
                        <div style="display: flex;">
                            @if(in_array(env('SITE_SUFFIX'), ['probeg_v_spb']))
                                <a class="card-grid__btn" data-fancybox="" data-type="ajax" data-src="/credit-modal/{{$item->id}}?inOneClick=1"
                                   href="javascript:;"
                                >Купить в 1 клик</a>
                            @endif
                            <a class="card-slider__btn" data-fancybox="" data-type="ajax"
                               data-src="{{url('/credit-modal', ['advert' => $item->id])}}" href="javascript:;">
                                @if(in_array(env('SITE_SUFFIX'), ['probeg_spb']))
                                    Узнать подробнее
                                @else
                                    Купить в кредит
                                @endif
                            </a>
                        </div>
                        <div class="card-slider__credit">
                            <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                            <p>от {{\App\Services\Region::getCreditPercent()}}% годовых</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!-- end catalog-slider-->
    @endforeach
@endif
</div>
