<!-- catalog-grid-->
<section class="catalog-grid" id="js-similar">
    <div class="container">
        <h2 class="section-title">Похожие предложения<span class="grey"><span class="blue"></span></span></h2>
        <div class="catalog-grid__row carousel">
            @foreach($items as $item)
                <div class="catalog-grid__item">
                    <div class="card-grid">
                        @if(env('APP_SITE_REGION') == '')
                            <span class="card-gallery-city">
                                @if($item->region_id == 66 && $item->is_current == true)
                                    Екатеринбург @elseif($item->region_id == 63 && $item->is_current === true)
                                    Самара @elseif($item->region_id == 52 && $item->is_current === true)
                                    Нижний Новгород
                                @endif</span>
                        @endif
                        @if(\App\Services\Region::isAutodromium())
                            @if($item->is_current == false) <span class="card-gallery-city">Автомобиль продан</span>@endif
                        @endif
                        <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : '/adverts', ['advert' => $item->client_id])}}" class="card-link"></a>
                        <div class="card-grid__labels">
                            @include('advert._card_greed_labels', ['item' => $item])
                        </div>
                        <!-- card-img-->
                        <div class="card-img">
                            <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : '/adverts', ['advert' => $item->client_id])}}">
                                <img src="{{$item->photos->first()->imagepathresized ?? ''}}" alt="">
                            </a>
                            <div class="card-img__qnt">
                                <img src="/img/icons/camera-black.svg" alt=""><span>{{$item->photos->count()}}</span>
                            </div>
                        </div>
                        <!-- end card-img-->
                        <div class="card-grid__desc">
                            <h3 class="card-grid__name">
                                <a href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : '/adverts', ['advert' => $item->client_id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                            </h3>
                            <p class="card-grid__year">{{$item->year_of_issue}}</p>
                            <div class="card-grid__price">
                                @if(intval($item->priceByAction()) < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                                <span class="old" @if(intval($item->priceByAction()) == $item->price) style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                            </div>
                            <div class="card-grid__info">
                                <p>{{$item->transmissionLabel}}, {{$item->engineLabel}}</p>
                                <p>{{$item->volume}} см3/{{$item->power}} л.с.</p>
                                <p>{{number_format($item->mileage, 0, ',', ' ')}} км, владельцев {{$item->number_of_owners}}</p>
                            </div>
                            <div class="card-grid__footer">
                                <a class="card-grid__btn" data-fancybox data-type="ajax" data-src="{{url('/credit-modal', ['item' => $item->id])}}" href="javascript:;">Купить в
                                    кредит</a>
                                <div class="card-grid__credit">
                                    <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                                    <p>под {{\App\Services\Region::getCreditPercent()}}% годовых</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- end catalog-grid-->
