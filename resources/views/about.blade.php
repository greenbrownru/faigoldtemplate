@extends('layouts.auto')
@section('content')

@if(!empty($images))
@section('javascript')
<script type="text/javascript" src="/js/jquery-3.5.1.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
@endsection
@endif

@section('styles')
<link rel="stylesheet" type="text/css" href="/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="/css/slick-theme.css"/>
@endsection

<section class="page" id="js-page">
    <div class="container" id="page_about">

        <h1 class="section-title">О компании<span class="grey"><span class="blue"></span></span></h1>

        <div class="page_content">
            <div class="left">

                @if(!empty($images))
                <div id="about_slider">
                    @foreach($images as $filename)
                        <div class="about_slider_item">
                            <img src="/img/about/{{env('APP_SITE_REGION')}}/{{$filename}}">
                        </div>

                    @endforeach
                </div>
                @endif

                    <div class="about-text">
                        {!! html_entity_decode($about_text) !!}
                    </div>

            </div>
            <div class="side">
                {!! html_entity_decode(\App\Models\Page::whereTitle('about_side')->first()->content ?? '') !!}

            </div>
        </div>

    </div>
</section>
@endsection
