@extends('layouts.auto')
@section('content')
    <!-- page-->
    <section class="page" id="js-page">
        <div class="container">
            <h1 class="section-title">Trade-in<span class="grey"><span class="blue"></span></span></h1>
            <div class="page__row">
                <div class="page__left">

                    <!-- article-->
                    <article class="article" style="margin-bottom: 12px">
                        {!! html_entity_decode(\App\Models\Page::whereTitle('tradeIn')->first()->content ?? '') !!}
                    </article>
                    <!-- end article-->
                </div>
                <div class="page__right">
                    <!-- feedback-->
                    <section class="feedback">
                        <h2 class="feedback__title">Форма заявки</h2>
                        <form action="/trade-in/request-add" method="post" id="tradein-form"
                              @if(\App\Services\Region::isAutodromium()) onsubmit="ym(61299526,'reachGoal','tradein'); ga('send', 'event', 'tradein', 'Submit');" @endif
                              @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','tradein');return true;" @endif
                        >
                            @csrf
                            <div class="feedback__section">
                                @if(\App\Services\Region::isAutodromium())
                                    <b class="feedback__subtitle">Выберите желаемый автомобиль  </b>
                                @elseif(env('APP_SITE_REGION') == 123)
                                    <h3 class="feedback__subtitle">Выберите желаемый автомобиль  </h3>
                                @else
                                    <h4 class="feedback__subtitle">Выберите желаемый автомобиль  </h4>
                                @endif

                                <div class="form-group">
                                    <div class="select" id="select_brand">
                                        <div class="select__label"> Марка</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="credit_brand" value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Марка</li>
                                            @if($brands)
                                                @foreach($brands as $brand)
                                                    <li class="li-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_model">
                                        <div class="select__label" > Модель</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="credit_model" required value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Модель</li>
                                            @if($models)
                                                @foreach($models as $model)
                                                    <option class="li-model" data-value="{{$model->id}}">{{$model->name}}</option>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_advert">
                                        <div class="select__label"> Объявление</div>
                                        <div class="select__input" style="height: 43px"> </div>
                                        <input type="hidden" name="credit_advert" required value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Объявление</li>
                                            @if($adverts)
                                                @foreach($adverts as $advert)
                                                    <li class="li-advert" data-value="{{$advert->id}}">{!! $advert->creditName !!}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="feedback__img" style="display: none">
                                </div>
                            </div>
                            <div class="feedback__section">
                                @if(\App\Services\Region::isAutodromium())
                                    <b class="feedback__subtitle">Укажите свой автомобиль</b>
                                @else
                                    <h4 class="feedback__subtitle">Укажите свой автомобиль</h4>
                                @endif

                                <div class="form-group">
                                    <div class="select" id="select_client_brand">
                                        <div class="select__label"> Марка</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="client_brand" required value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Марка</li>
                                            @if($brandsClient)
                                                @foreach($brandsClient as $brand)
                                                    @if($item && ($brand->id == $item->brand_id))
                                                        <li class="li-client-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                                    @else
                                                        <li class="li-client-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_client_model">
                                        <div class="select__label"> Модель</div>
                                        <div class="select__input"> </div>
                                        <input type="hidden" name="client_model" required value="">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Модель</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="feedback__section">
                                @if(\App\Services\Region::isAutodromium())
                                    <b class="feedback__subtitle">Укажите свои данные</b>
                                @else
                                    <h4 class="feedback__subtitle">Укажите свои данные</h4>
                                @endif

                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input class="phone" type="tel" name="phone" required autocomplete="off">
                                </div>
                                <button class="feedback__btn" type="submit">Отправить</button>
                                <div class="mt-2">
                                    <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                                </div>
                            </div>
                        </form>
                    </section>
                    <!-- end feedback-->
                </div>
            </div>
        </div>
    </section>
    <!-- end page-->
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.li-brand', function (e) {
                let elementModel = $('input[name="credit_model"]').parent().find('ul');
                $(elementModel).html('<li data-value="">Модель</li>');
                $('.feedback__img').html('');
                $('input[name="credit_advert"]').parent().find('ul').html('<li data-value="">Объявления</li>');
                let brand = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-models',
                    type: 'post',
                    data: {"brand_id": brand},
                    dataType: 'json',
                    success: function (data) {
                        let content = '<li data-value="">Модель</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-model" data-value="'+value.id+'">'+value.name+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });

            $(document).on('click', '.li-model', function (e) {
                let elementModel = $('input[name="credit_advert"]').parent().find('ul');
                $(elementModel).html('<li data-value="">Объявления</li>');
                $('.feedback__img').html('');
                let model = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-adverts',
                    type: 'post',
                    data: {"model_id": model},
                    dataType: 'json',
                    success: function (data) {
                        let content = '<li data-value="">Объявления</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-advert" data-value="'+value.id+'">'+value.creditname+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });

            $(document).on('click', '.li-advert', function (e) {
                $('.feedback__img').html('');
                let advert = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-advert-image',
                    type: 'post',
                    data: {"advert_id": advert},
                    dataType: 'json',
                    success: function (data) {
                        $('.feedback__img').show().html('<img src="'+data.imagepath+'" >')
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });


            $(document).on('click', '.li-client-brand', function (e) {
                let elementModel = $('input[name="client_model"]').parent().find('ul');
                $(elementModel).html('<li data-value="">Модель</li>');
                let brand = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/trade-in/ajax-client-model',
                    type: 'post',
                    data: {"brand_id": brand},
                    dataType: 'json',
                    success: function (data) {

                        let content = '<li class="li-client-model" data-value="">Модель</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-client-model" data-value="'+value.id+'">'+value.name+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });
        });

        $(document).on('submit', '#tradein-form', function (e) {
            var form = $(this);
            var formData = new FormData(this);
            $.ajax({
                beforeSend: function (xhr, opts) {
                    beforeSendMakes(form, xhr);
                    if($('input[name="client_brand"]').val() === '' || $('input[name="client_model"]').val() === '') {
                        alert('Заполните все данные!');
                        $('.feedback__btn').removeClass('progress-bar-striped');
                        $('.feedback__btn').removeClass('progress-bar-animated');
                        return false;
                    }

                },
                complete: function () {
                    $('.feedback__btn').removeClass('progress-bar-striped');
                    $('.feedback__btn').removeClass('progress-bar-animated');
                    $(form).trigger("reset");
                },
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData,
                dataType: 'html',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {

					if(data == 0){
						$('form#tradein-form input.phone').css({border: '1px solid red'})
					}else{
						$('.fancybox-close-small').click();
						$('[data-fancybox-close]').click();
						$('body').addClass('thanksPopupBody');
						$('.thanks_modal').addClass('active');
					}


                }
            });
            //return false;
            e.preventDefault();

        });
    </script>
@endsection
@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__trade_in_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__trade_in_description') ?: setting('page__description')) }}@endsection
