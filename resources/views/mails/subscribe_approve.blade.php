@include('mails.header')

<div style="width: 100%; height: 0; clear: both"></div>
<h4>Здравствуйте, {{$subscribe->name}}! Вы подписались на рассылку по заданным параметрам:</h4>
<p>Марка: {{$subscribe->brandItem->name}}</p>
<p>Год от/до: {{$subscribe->year_from}} / {{$subscribe->year_to}}</p>
<p>Цена (рублей) от/до: {{$subscribe->price_from}} / {{$subscribe->price_to}}</p>
<p><a href="{{url('/subscribes/approve?token='.$subscribe->token)}}">Подтвердить подписку</a></p>
</body>
</html>


