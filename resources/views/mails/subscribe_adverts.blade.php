@include('mails.header')

<div style="width: 100%; height: 0; clear: both"></div>
<h4>Здравствуйте, {{$subscribe->name}}! На сайте появились новые предложения:</h4>
@foreach($adverts as $advert)
    <div>
        <table>
            <tr>
                <td><a href="{{env('APP_URL')}}/adverts/{{$advert->id}}?utm_source={{$subscribe->email}}"><img src="{{env('APP_URL')}}/storage/photos/{{$advert->photos->first()->filename}}" width="80px" /></a></td>
                <td><a href="{{env('APP_URL')}}/adverts/{{$advert->id}}?utm_source={{$subscribe->email}}"><span style="margin-top: -30px">{!! $advert->creditName !!}</span></a></td>
            </tr>
        </table>
    </div>

@endforeach
</body>
</html>


