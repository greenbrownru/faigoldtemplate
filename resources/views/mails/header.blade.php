<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        .btn {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            color: #fff;
            background-color: #17a2b8;
            border-color: #17a2b8;
            text-decoration: none;
        }

    </style>
</head>
<body>
<div style="width: 40%; float: left; height: 100px">
    <img src="{{env('APP_URL')}}/img/logo.png" style="width: 200px"/>
</div>
<div style="width: 40%; float: right; height: 100px">
    <p style="margin-bottom: 0">г. Екатеринбург, Базовый переулок, 38</p>
    <p style="margin-top: 0">+7 (343) 226-06-65</p>
</div>
