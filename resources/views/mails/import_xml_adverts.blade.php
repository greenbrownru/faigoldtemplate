@include('mails.header')

<div style="width: 100%; height: 0; clear: both"></div>
<h4>Здравствуйте!</h4>
<p>{{ env('APP_URL') }}</p>
@if($success)
    <p style="color: green;">Прошло обновление каталога</p>
@else
    <p style="color: red;">Не прошло обновление каталога</p>
@endif
</body>
</html>


