@if($item->vin_verified)<span class="green">VIN проверен</span> @endif
@if(isset($display_block))
    @if($item->is_current)
        <style>
            .card-grid__footer,
            .card-slider__footer {
                align-items: flex-start;
            }
        </style>
        <div class="card-grid__footer">
            <div>
                <a class="card-grid__btn" data-fancybox="" data-type="ajax" data-src="/credit-modal/{{$item->id}}"
                   href="javascript:;">Купить в
                    кредит</a>

                @if(in_array(env('SITE_SUFFIX'), ['probeg_v_spb']))
                    <a class="card-grid__btn" data-fancybox="" data-type="ajax" data-src="/credit-modal/{{$item->id}}?inOneClick=1"
                       href="javascript:;"
                       style="margin-top: 5px;"
                    >Купить в 1 клик</a>
                @endif
            </div>

            <div class="card-grid__credit">
                <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                <p>от {{\App\Services\Region::getCreditPercent()}}% годовых</p>
            </div>
        </div>
    @else
        <div class="card-grid__footer">
            <button class="card-grid__btn" style="background-color: grey; width: auto">Автомобиль продан</button>
        </div>
    @endif
@endif
@if($item->kasko_in_gift) <span class="navy-blue">КАСКО в подарок</span> @endif
@if($item->rubber_kit) <span class="light-blue">Комплект резины</span> @endif
