<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ссылка на видео</title>
    <link rel="stylesheet" href="/css/styles.min.css">
    <style>
        .wrap-index {
            max-width: 1200px;
            margin: 0 auto;
            padding: 40px 15px;
        }

        img {
            width: 100%;
            max-width: 800px;
            height: auto;
        }

        h1 {
            margin-bottom: 45px;
            font-family: sans-serif;
            font-size: 32px;
        }

        ol {
            font-family: sans-serif;
        }

        ol li {
            font-size: 20px;
            margin-bottom: 20px;
        }

        ol li a {
            color: #000;
            text-decoration: none;
            transition: color .3s;
        }

        ol li a:hover {
            color: red;
        }

        @media (max-width: 576px) {
            h1 {
                font-size: 24px;
            }
        }
    </style>
</head>
<body>
<div>
    <iframe width="100%" height="315" src="{{str_replace('watch?v=', 'embed/', $item->video_link)}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<script src="/js/libs.min.js"></script>
<script src="/js/scripts.js"></script>
<script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script>

</script>
</body>
</html>
