<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@hasSection('title')@yield('title')@else{{ setting('title') }}@endif</title>
    @hasSection('meta_description')
    <meta name="description" content="@yield('meta_description')">
    @endif

    @if(setting('favicon'))
        <link rel="shortcut icon" href="{{ asset(setting('favicon')) }}">
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        .progress-bar-striped {
            background-image: linear-gradient(
                    45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
            background-size: 1rem 1rem;
        }

        .progress-bar-animated {
            -webkit-animation: progress-bar-stripes 1s linear infinite;
            animation: progress-bar-stripes 1s linear infinite;
        }

        @-webkit-keyframes progress-bar-stripes {
            from {
                background-position: 1rem 0
            }
            to {
                background-position: 0 0
            }
        }

        @keyframes progress-bar-stripes {
            from {
                background-position: 1rem 0
            }
            to {
                background-position: 0 0
            }
        }

        .current_page {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            height: 34px;
            padding: 0 15px;
            background-color: #dae4eb;
            font-size: 14px;
            text-decoration: none;
        }

        .disabled_page {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            height: 34px;
            padding: 0 15px;
            background-color: #dae4eb;
            font-size: 14px;
            text-decoration: none;
        }


        .edit-btn {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            max-width: 340px;
            color: #fff !important;
            font-size: 14px;
            text-decoration: none;
            border-radius: 3px;
            padding-bottom: 4px;
            padding-top: 4px;
            padding-left: 6px;
            padding-right: 6px;
            background-color: #3490dc;
        }
    </style>
    <link rel="stylesheet" href="/css/styles.min{{env('APP_SITE_REGION')}}.css?v=1.00">
    <link rel="stylesheet" href="/css/styles-common.min.css?v=1.03">
    <link rel="stylesheet" href="/css/jquery.fancybox-3.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/css/selectize/selectize.css">
    <link rel="stylesheet" href="/css/selectize/selectize.default.css">
    @yield('styles')

    <style>

        .linear-phone-gradient {
            -webkit-mask: linear-gradient(to left,rgba(255,0,0,0), 78%, rgba(255,0,0,1));
            mask: linear-gradient(to left,rgba(255,0,0,0), 78%, rgba(255,0,0,1));
        }

        .alert {
            position: relative;
            padding: .75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: .25rem;
        }

        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
        }

        button.close {
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
        }

        .close:not(:disabled):not(.disabled) {
            cursor: pointer;
            margin-left: 94%;
            font-size: 20px;
        }

        .close {
            position: relative;
            top: -2px;
            right: -21px;
            color: inherit
        }

       .input-filter::placeholder {
            color: black;
        }

        .mt-2, .my-2 {
            margin-top: 0.5rem !important;
        }

        .text--black {
            color: black;
        }
    </style>

    <script>
        window._SETTINGS_ = {
            ym: {
                id: {!! (setting('yandex__use') == 1 ? setting('yandex__id') : setting('yandex__id2')) ?: '""' !!},
                all: '{{ setting('yandex__goals__all') }}',
                credit: '{{ setting('yandex__goals__credit') }}',
                creditModal: '{{ setting('yandex__goals__credit_modal') }}',
                tradeIn: '{{ setting('yandex__goals__trade_in') }}',
                tradeInExchange: '{{ setting('yandex__goals__trade_in_exchange') }}',
                buy: '{{ setting('yandex__goals__buy') }}',
                promo: '{{ setting('yandex__goals__promo') }}',
                contact: '{{ setting('yandex__goals__contact') }}',
                subscribe: '{{ setting('yandex__goals__subscribe') }}',
                phoneClick: '{{ setting('yandex__goals__phone_click') }}',
            },
            gm: {
                id: '{{ setting('google__id') }}',
                all: '{{ setting('google__goals__all') }}',
                credit: '{{ setting('google__goals__credit') }}',
                creditModal: '{{ setting('google__goals__credit_modal') }}',
                tradeIn: '{{ setting('google__goals__trade_in') }}',
                tradeInExchange: '{{ setting('google__goals__trade_in_exchange') }}',
                buy: '{{ setting('google__goals__buy') }}',
                promo: '{{ setting('google__goals__promo') }}',
                contact: '{{ setting('google__goals__contact') }}',
                subscribe: '{{ setting('google__goals__subscribe') }}',
                phoneClick: '{{ setting('google__goals__phone_click') }}',
            },
            mailRuM: {
                id: '{{ setting('mail__id') }}',
                all: '{{ setting('mail__goals__all') }}',
            }
        }
    </script>


@if(setting('yandex__id'))
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            }
            m[i].l = 1 * new Date()
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })(window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym')

        ym(@setting('yandex__id'), 'init', {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        })
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/@setting('yandex__id')" style="position:absolute; left:-9999px;" alt=""/></div></noscript>
    @endif

@if(setting('yandex__id2'))
    <script type="text/javascript">
        @if(! setting('yandex__id'))
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            }
            m[i].l = 1 * new Date()
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })(window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym')
        @endif

        ym(@setting('yandex__id2'), 'init', {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        })
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/@setting('yandex__id2')" style="position:absolute; left:-9999px;" alt=""/></div></noscript>
@endif

@if(setting('google__id'))
    <script async src="https://www.googletagmanager.com/gtag/js?id=@setting('google__id')"></script>
    <script>
        window.dataLayer = window.dataLayer || []

        function gtag() {
            dataLayer.push(arguments)
        }

        gtag('js', new Date())

        gtag('config', '{{ setting('google__id') }}')
    </script>

    <script>
        function gtag_report_conversion(url) {
            var callback = function() {
                if (typeof (url) != 'undefined') {
                    window.location = url
                }
            }
            gtag('event', 'conversion', {
                'send_to': '{{ setting('google__id') }}/CYAxCLG8qfsCEM_M1Zco',
                'event_callback': callback
            })
            return false
        }
    </script>

    <script>
        gtag('event', 'page_view', {
            'send_to': '{{ setting('google__id') }}',
            'value': 'replace with value',
            'items': [{
                'id': 'replace with value',
                'location_id': 'replace with value',
                'google_business_vertical': 'custom'
            }]
        })
    </script>
@endif

@if(setting('mail__id'))
    <!-- Rating Mail.ru counter -->
    <script type="text/javascript">
        var _tmr = window._tmr || (window._tmr = []);
        _tmr.push({id: "{{ setting('mail__id') }}", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
        (function (d, w, id) {
            if (d.getElementById(id)) return;
            var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
            ts.src = "https://top-fwz1.mail.ru/js/code.js";
            var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
        })(document, window, "topmailru-code");
    </script>
    <noscript><div><img src="https://top-fwz1.mail.ru/counter?id={{ setting('mail__id') }};js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" /></div></noscript>
    <!-- //Rating Mail.ru counter -->
@endif

@if(setting('marquizru__id'))
    <script>
        (function(w, d, s, o){
            var j = d.createElement(s); j.async = true; j.src = '//script.marquiz.ru/v2.js';j.onload = function() {
                if (document.readyState !== 'loading') Marquiz.init(o);
                else document.addEventListener("DOMContentLoaded", function() {
                    Marquiz.init(o);
                });
            };
            d.head.insertBefore(j, d.head.firstElementChild);
        })(window, document, 'script', {
                host: '//quiz.marquiz.ru',
                region: 'eu',
                id: '{{ setting('marquiz_ru__id') }}',
                autoOpen: false,
                autoOpenFreq: 'once',
                openOnExit: false,
                disableOnMobile: false
            }
        );
    </script>
@endif

@if(setting('pixel__enable'))
    <!-- Pixel -->
    <script type="text/javascript">
        (function (d, w) {
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script");
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://victorycorp.ru/index.php?ref="+d.referrer+"&page=" + encodeURIComponent(w.location.href);
            n.parentNode.insertBefore(s, n);
        })(document, window);
    </script>
    <!-- /Pixel -->
@endif


@if(setting('calltouch__id'))
    <!-- calltouch -->
    <script type="text/javascript">
        (function (w, d, n, c) {
            w.CalltouchDataObject = n;
            w[n] = function () {
                w[n]["callbacks"].push(arguments)
            };
            if (!w[n]["callbacks"]) {
                w[n]["callbacks"] = []
            }
            w[n]["loaded"] = false;
            if (typeof c !== "object") {
                c = [c]
            }
            w[n]["counters"] = c;
            for (var i = 0; i < c.length; i += 1) {
                p(c[i])
            }

            function p(cId) {
                var a = d.getElementsByTagName("script")[0], s = d.createElement("script"), i = function () {
                    a.parentNode.insertBefore(s, a)
                }, m = typeof Array.prototype.find === 'function', n = m ? "init-min.js" : "init.js";
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mod.calltouch.ru/" + n + "?id=" + cId;
                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", i, false)
                } else {
                    i()
                }
            }
        })(window, document, "ct", '{{ setting('calltouch__id') }}');
    </script>
    <!-- calltouch -->
@endif

{!! setting('head_scripts') !!}
</head>
<body>

<div class="wrap">
    <x-auth_top_bar></x-auth_top_bar>

    <header class="header">
        <div class="container">
            <div class="header__row">
                <div class="header__toggle">
                    <!-- menu-toggle-->
                    <div class="menu-toggle"><span></span><span></span><span></span></div>
                    <!-- end menu-toggle-->
                </div>
                <div class="header__left">
                    <!-- logo-->
                    <a class="logo" href="/">
                        <img src="{{ setting('logo__head') }}" width="200px">
                    </a>
                    <!-- end logo-->
                </div>
                <div class="header__center">
                    <!-- menu-->
                    <ul class="menu">
                        <li><a href="/catalog">Автомобили</a></li>
                        @if(setting('page__credit_enable'))
                            <li><a href="/credit">Автокредит</a></li>
                        @endif
                        @if(setting('page__trade_in_enable'))
                            <li><a href="/trade-in">Trade-in</a></li>
                        @endif
                        @if(setting('page__buy_enable'))
                            <li><a href="/buy">Выкуп</a></li>
                        @endif
                        @if(setting('page__promo_enable'))
                            <li><a href="/promo">Акции</a></li>
                        @endif
                        @if(setting('page__carservice_enable'))
                            <li><a href="/car_service">Автосервис</a></li>
                        @endif
                        <li><a href="/contacts">Контакты</a></li>
                        @if(setting('page__about_enable'))
                            <li><a href="/about">О компании</a></li>
                        @endif
                    </ul>
                    <!-- end menu-->
                </div>
                <div class="header__right">
                    @if(showPhone())
                    <div
                        class="phones_wrap">
                        <a class="phones" href="tel:{{showPhone()}}"><img src="/img/icons/tel.svg" alt="">
                            <div class="phone_test_wrap">
                                <span data-phone="1">{{showPhone()}}</span>
                                @if(setting('phone__bottom_text'))
                                    <span>{{ setting('phone__bottom_text') }}</span>
                                @endif
                            </div>
                        </a>
                    </div>
                    @endif
                </div>
                <div class="header__toggle">
                    <!-- brands-toggle-->
                    <div class="brands-toggle"></div>
                    <!-- end brands-toggle-->
                </div>
            </div>
            <!-- dropdown-->
            <div class="dropdown"
                 @if(\App\Services\Region::isAutodromium() || env('APP_SITE_REGION') == 18) onmouseleave="$(this).stop().fadeOut()"
                 onmouseover="$(this).stop().fadeIn()" @endif>
                @if(env('APP_SITE_REGION') != 18)
                    <div class="dropdown__close"><span></span><span></span></div>@endif
                @include('components.main_blocks.brand_links')
            </div>
            <!-- end dropdown-->
        </div>
    </header>
    <!-- end header-->
    <!-- mobile-menu-->
    <div class="mobile-menu" id="js-menu">
        <div class="mobile-menu__close">
            <div class="mobile-menu__back"><span></span><span></span></div>
            <span>Меню</span>
        </div>
        <ul class="mobile-menu__list">
            <li @if(request()->is('catalog*')) class="current" @endif><a href="/catalog">Каталог</a></li>
            @if(setting('page__credit_enable'))
                <li @if(request()->is('credit*')) class="current" @endif><a href="/credit">Автокредит</a></li>
            @endif
            @if(setting('page__trade_in_enable'))
                <li @if(request()->is('trade-in*')) class="current" @endif><a href="/trade-in">Trade-in</a></li>
            @endif
            @if(setting('page__buy_enable'))
                <li @if(request()->is('buy*')) class="current" @endif><a href="/buy">Выкуп</a></li>
            @endif
            @if(setting('page__promo_enable'))
                <li @if(request()->is('promo*')) class="current" @endif><a href="/promo">Акции</a></li>
            @endif
            <li @if(request()->is('contacts*')) class="current" @endif><a href="/contacts">Контакты</a></li>
        </ul>

        @if(setting('phone__show_mobile_in_sidebar'))
            <div>
                <a class="card-grid__btn" href="tel:{{ showPhone() }}" style="width: auto; margin: 1.5em;height: 50px;">
                    <span data-phone="4">{{ showPhone() }}</span>
                </a>
            </div>
        @endif
    </div>
    <!-- end mobile-menu-->
    <!-- mobile-menu-->
    <div class="mobile-menu" id="js-brands">
        <div class="mobile-menu__close">
            <div class="mobile-menu__back"><span></span><span></span></div>
            <span>Поиск марки</span>
        </div>
        <ul class="mobile-menu__list">
            @if($brandMenu)
                @foreach($brandMenu as $menuList)
                    <li><a href="/catalog?brand={{$menuList->id}}">{{$menuList->name}}</a></li>
                @endforeach
            @endif
        </ul>
    </div>
    <!-- end mobile-menu-->
