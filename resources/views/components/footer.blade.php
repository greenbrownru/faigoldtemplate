<!-- footer-->
<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__left">
                <!-- logo-->
                <a class="logo" href="/">
                    <img src="{{ setting('logo__foot') }}" width="200px" alt="">
                </a>
            </div>
            <div class="footer__right">
                <div class="footer__row">
                    @if(env('APP_SITE_REGION') != 63 || (env('APP_SITE_REGION') == 63 && !request()->is('buy')))
                        <div class="footer__menu">
                            <ul class="menu">
                                <li><a href="/catalog">Автомобили</a></li>
                                @if(setting('page__credit_enable'))
                                    <li><a href="/credit">Автокредит</a></li>
                                @endif
                                @if(setting('page__trade_in_enable'))
                                    <li><a href="/trade-in">Trade-in</a></li>
                                @endif
                                @if(setting('page__buy_enable'))
                                    <li><a href="/buy">Выкуп</a></li>
                                @endif
                                @if(setting('page__promo_enable'))
                                    <li><a href="/promo">Акции</a></li>
                                @endif
                                @if(setting('page__carservice_enable'))
                                    <li><a href="/car_service">Автосервис</a></li>
                                @endif
                                <li><a href="/contacts">Контакты</a></li>
                                @if(setting('page__about_enable'))
                                    <li><a href="/about">О компании</a></li>
                                @endif
                            </ul>
                        </div>

                        <div class="footer__phones">
                            <!-- phones-->
                            @if(env('APP_SITE_REGION') != '')
                                <a class="phones"
                                   href="tel:{{showPhone()}}">
                                    <div class="phone_text_wrap">
                                        <span data-phone="2">{{showPhone()}}</span>
                                    </div>
                                </a>
                        @endif
                        <!-- end phones-->
                        </div>
                    @endif

                    <div class="footer__toggle">
                        <!-- menu-toggle-->
                        <div class="menu-toggle"><span></span><span></span><span></span></div>
                        <!-- end menu-toggle-->
                    </div>

                    <div class="footer__text">
                        <p>
                            {!! setTemplateVars(['phone' => showPhone()], setting('footer__text')) !!}
                            <a href="{{env('APP_URL')}}/about/personal_data" class="footer__credit_conditions" target="_blank">Политика конфиденциальности.</a>
                        </p>

                        <div class="footer__credit_conditions__container">
                            <a href="javascript:void(0);" class="footer__credit_conditions" onclick="$(this).next().show().end().remove();">* Условия по кредитованию</a>
                            <div class="footer__credit_conditions__text">
                                @php
$_footerAdvertText = '';
if (request()->route()) {
    if(request()->route()->named(['advert']) && isset($item)) {
        $_footerAdvertText = "{$item->brand->name} {$item->year_of_issue} г.в";
    } else if((request()->route()->named(['main.brand', 'main.brand_model']) || request()->route()->uri === 'catalog') && isset($items)) {
        $_footerCatalogYear = implode('-', [$items->min('year_of_issue'), $items->max('year_of_issue')]);
        $_footerAdvertText = (\App\Models\Brand::find(request()->brand)->name ?? '') . " {$_footerCatalogYear} г.в";
    }
}
$_footerTemplateVars = [
    'name',
    'phone' => showPhone(),
    'credit_percent',
    'credit_max_month',
    'credit_amount_from' => \App\Services\CreditService::getAmountFromFormatted(),
    'initial_fee',
    'date_start' => (\Carbon\Carbon::now())->startOfMonth()->format('d.m.Y'),
    'date_end' => (\Carbon\Carbon::now())->endOfMonth()->format('d.m.Y'),
    'advert' => $_footerAdvertText,
];
                                @endphp
                                {!! setTemplateVars($_footerTemplateVars, setting('footer__text_hide')) !!}
                            </div>
                        </div>
                        <span style="color: #b3bcc2; font-size: 14px">{{ setting('footer__contact_text') }}</span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- end footer-->

<div class="thanks_modal">
    <a href="#" class="close_thanks_modal">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path
                d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"></path>
        </svg>
    </a>
    <img src="/img/icons/icon-check.svg" class="thanks_check">
    <p>
        Спасибо, ваша заявка принята
    </p>
</div>
<script src="/js/libs.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="/js/selectize.min.js"></script>
<script type="text/javascript" src="/js/jquery.ui.touch-punch.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/scripts.js?V=1.03"></script>
<script src="/js/custom.min.js"></script>
<script src="/js/core.min.js"></script>
<script src="/js/md5.js"></script>
@if(setting('phone__hiding_part'))
<script>
    if(!localStorage.hasOwnProperty('phone1_clicked')) {
        $('.phone_test_wrap span').addClass('linear-phone-gradient');
    }
    if(!localStorage.hasOwnProperty('phone2_clicked')) {
        $('.phone_text_wrap span').addClass('linear-phone-gradient');
    }
    if(!localStorage.hasOwnProperty('phone3_clicked')) {
        $('.page__right .contacts').find('span').addClass('linear-phone-gradient');
    }
    if(!localStorage.hasOwnProperty('phone4_clicked')) {
        $('#js-menu .card-grid__btn').find('span').addClass('linear-phone-gradient');
    }

    $(document).on('click', '.phone_test_wrap span, .phone_text_wrap span, .contacts span, #js-menu .card-grid__btn span', function () {
        let dataPhone = $(this).data('phone');
        $(this).removeClass('linear-phone-gradient');
        localStorage.setItem('phone'+dataPhone+'_clicked', dataPhone);
    });
</script>
@endif

{!! setting('footer_scripts') !!}

@yield('javascript')
@stack('js')
<script>
    $('.product_carousel').owlCarousel({
        items: 1,
        margin: 0,
        stagePadding: 0,
        nav: true,
        smartSpeed: 450
    });

    $('.close_thanks_modal').click(function (e) {
        e.preventDefault();
        $('body').removeClass('thanksPopupBody');
        $('.thanks_modal').removeClass('active');
    });


    $(document).on('click', '.feedback__btn', function () {
        var form = $(this).closest('form');
        $(form).find(':input[required]').each(function (index, value) {
            if ($(value).val() === '') {
                $(value).css('border', '1px solid red');
            }
        });

        $(form).find('input[type="hidden"]').each(function (index, value) {
            if ($(value).val() === '') {
                $(value).closest('.select').find('.select__input').css('border', '1px solid red');
            }
        });

    });

    function beforeSendMakes(form, xhr) {
        var tel = $(form).find('input[type="tel"]')[0];
        if ($(tel).val().length < 15) {
            $(tel).css({border: '1px solid red'});
            xhr.abort();
        } else {
            $('.feedback__btn').addClass('progress-bar-striped');
            $('.feedback__btn').addClass('progress-bar-animated');
            $(form).find('input[type="text"], input[type="tel"], select').each(function (index, value) {
                $(value).removeAttr('style');
            });

            $(form).find('input[type="hidden"]').each(function (index, value) {
                $(value).closest('.select').find('.select__input').removeAttr('style');
            });
        }
    }

    $(document).on('submit', '#credit-modal', function (e) {
        var form = $(this);

        $.ajax({
            beforeSend: function (xhr, opts) {
                beforeSendMakes(form, xhr);
            },
            complete: function () {
                $('.feedback__btn').removeClass('progress-bar-striped');
                $('.feedback__btn').removeClass('progress-bar-animated');
            },
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            success: function (data) {

                if (data == 0) {
                    $('form#credit-modal input.phone').css({border: '1px solid red'})
                } else {
                    $('[data-fancybox-close]').click();
                    $('body').addClass('thanksPopupBody');
                    $('.thanks_modal').addClass('active');
                }
            },
            error: function (error) {
                //console.log(error);
                $('[data-fancybox-close]').click();
                $('body').addClass('thanksPopupBody');
                $('.thanks_modal').addClass('active');
            }
        });
        e.preventDefault();
    });

    $(document).on('submit', '#subscribe-form', function (e) {
        var form = $(this);
        $.ajax({
            beforeSend: function (xhr, opts) {
                beforeSendMakes(form, xhr);
            },
            complete: function () {
                $('.feedback__btn').removeClass('progress-bar-striped');
                $('.feedback__btn').removeClass('progress-bar-animated');
            },
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            dataType: 'json',
            success: function (data) {
                if (data.status == 'success') {
                    $('[data-fancybox-close]').click();
                    $('body').addClass('thanksPopupBody');
                    //$('.thanks_modal').find('p').html('Спасибо, подписка оформлена!');
                    $('.thanks_modal').addClass('active');
                    $(form).trigger("reset");
                } else {
                    alert(data.message);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });
</script>

<script type="text/javascript">
    $(document).on('submit', 'form', function () {
        var _this = $(this);
        var _ymData = _SETTINGS_.ym;
        var _gmData = _SETTINGS_.gm;
        var isCreditModal = /^advert-\d+/.test(_this.attr('id'));

        _SETTINGS_.mailRuM.id && _tmr.push({ id: _SETTINGS_.mailRuM.id, type: "reachGoal", goal: _SETTINGS_.mailRuM.all });

        if (window.gtag && _gmData.all) {
            gtag('event', 'Submit', {'event_category': _gmData.all});
        }

        if (_ymData.id && _ymData.all) {
            ym(_ymData.id, 'reachGoal', _ymData.all);
        }

        switch (true) {
            case _this.is('#credit-modal') || isCreditModal:
                if (window.gtag && _gmData.creditModal) {
                    gtag('event', 'Submit', {'event_category': _gmData.creditModal});
                }

                if (_ymData.id && _ymData.creditModal) {
                    ym(_ymData.id, 'reachGoal', _ymData.creditModal);
                }
                break;
            case _this.is('#tradein-exchange-form'):
                if (window.gtag && _gmData.tradeInExchange) {
                    gtag('event', 'Submit', {'event_category': _gmData.tradeInExchange});
                }

                if (_ymData.id && _ymData.tradeInExchange) {
                    ym(_ymData.id, 'reachGoal', _ymData.tradeInExchange);
                }
                break;
            case _this.is('#credit-form'):
                if (window.gtag && _gmData.credit) {
                    gtag('event', 'Submit', {'event_category': _gmData.credit});
                }

                if (_ymData.id && _ymData.credit) {
                    ym(_ymData.id, 'reachGoal', _ymData.credit);
                }
                break;
            case _this.is('#tradein-form'):
                if (window.gtag && _gmData.tradeIn) {
                    gtag('event', 'Submit', {'event_category': _gmData.tradeIn});
                }

                if (_ymData.id && _ymData.tradeIn) {
                    ym(_ymData.id, 'reachGoal', _ymData.tradeIn);
                }
                break;
            case _this.is('#buy-form'):
                if (window.gtag && _gmData.buy) {
                    gtag('event', 'Submit', {'event_category': _gmData.buy});
                }

                if (_ymData.id && _ymData.buy) {
                    ym(_ymData.id, 'reachGoal', _ymData.buy);
                }
                break;
            case _this.is('#promo-modal'):
                if (window.gtag && _gmData.promo) {
                    gtag('event', 'Submit', {'event_category': _gmData.promo});
                }

                if (_ymData.id && _ymData.promo) {
                    ym(_ymData.id, 'reachGoal', _ymData.promo);
                }
                break;
            case _this.is('#contact-form'):
                if (window.gtag && _gmData.contact) {
                    gtag('event', 'Submit', {'event_category': _gmData.contact});
                }

                if (_ymData.id && _ymData.contact) {
                    ym(_ymData.id, 'reachGoal', _ymData.contact);
                }
                break;
            case _this.is('#subscribe-form'):
                if (window.gtag && _gmData.subscribe) {
                    gtag('event', 'Submit', {'event_category': _gmData.subscribe});
                }

                if (_ymData.id && _ymData.subscribe) {
                    ym(_ymData.id, 'reachGoal', _ymData.subscribe);
                }
                break;
        }

    });

    $("a[href^='tel']").on("click", function () {
        if (window.gtag && _SETTINGS_.gm.phoneClick) {
            gtag('event', 'Submit', {'event_category': _SETTINGS_.gm.phoneClick});
        }

        if (_SETTINGS_.ym.id && _SETTINGS_.ym.phoneClick) {
            ym(_SETTINGS_.ym.id, 'reachGoal', _SETTINGS_.ym.phoneClick);
        }
    });

</script>
</body>
</html>
