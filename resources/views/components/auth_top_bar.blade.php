@if(\Illuminate\Support\Facades\Auth::check())
    <style>
        .header {
            position: fixed;
            z-index: 7000;
            top: 3%;
            left: 0;
            width: 100%;
            height: 90px;
            padding: 20px 0;
            background-color: #fff;
            -webkit-transition: .1s linear;
            transition: .1s linear;
        }
    </style>
    <div class="pre-header">
        <div class="container">
            <div class="header__row">
                <div class="pre-header__center">
                    <ul class="pre-menu">

                        <li><a href="/admin/settings">Настройки</a></li>
                        <li><a href="/admin/adverts">Каталог авто</a></li>
                        <li><a href="/admin/promo">Акции</a></li>
                        <li><a href="/admin/banners">Баннеры</a></li>
                        <li><a href="/admin/reviews">Отзывы</a></li>
                        <li><a class="second_link-dropdown" href="#">Заявки</a>
                            <div class="third__dropdown">
                            </div>
                        </li>
                        <li><a class="" href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Выход
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif