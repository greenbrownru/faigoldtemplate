<section class="short_form_wrapper">
    <div class="short_main_width">
        <div class="short_form">
            <div class="h1">Проверенные автомобили в кредит из салонов по всей России</div>
            <p>Ассортимент нашего автосалона включает более сотни АВТОМОБИЛЕЙ С ПРОБЕГОМ. Все они прошли юридическую и техническую экспертизу. В каталоге «УдмуртАвтоПробег» представлены АВТОМОБИЛИ разных брендов, моделей, комплектаций. </p>
            <p>Можно купить автомобиль наличными или взять в кредит на выгодных условиях, сроком от 1-го года до 7-ми лет. Первоначальный взнос не обязателен.</p>
            <p>На рассмотрение заявки уйдет не больше одного дня. Одобрение дают клиентам даже с плохой кредитной историей. Всем покупателям полагаются подарки — зимние шины, коврики и прочее.</p>
            <div class="h2">Нужна помощь в подборе авто?</div>
            <form justwe-form="" method="POST" action="https://u-autoprobeg.ru/form/2" justwe-metrica="yaCounter68612251.reachGoal('allform');yaCounter68612251.reachGoal('formavnizy');" novalidate="">
                <input type="hidden" name="redirect_to" value="https://u-autoprobeg.ru/?success=form">
                <input type="hidden" name="_token" value="nvAsbQlAQdhvmlq2iv1CCmJbbvzJb10UIwb87s2U">    <input name="" value="" autocomplete="off" style="display: none">
                <input type="hidden" name="title" value="Заявка с формы помощи подбора авто">

                <div class="short_container short_container_3">
                    <div><input justwe-form-required="" justwe-form-only-letter="" name="name" type="text" class="form_control" placeholder="Ваше имя" autocomplete="off"></div>
                    <div><input type="tel" name="telephone" pattern="\d*" class="form_control" placeholder="+7 (___) ___-__-__" autocomplete="off" justwe-form-required=""></div>
                    <div><button type="sybmit" class="short_btn"><span>Перезвоните мне</span></button></div>
                </div>
            </form>
        </div>
    </div>
</section>
