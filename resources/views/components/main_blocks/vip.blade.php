@props(['items'])

<!-- catalog-grid-->
<section class="catalog-grid" id="js-vip">
    <div class="container">
        <h2 class="section-title">VIP-объявления<span class="grey"><span class="blue"></span></span></h2>
        <div class="catalog-grid__row carousel">
            @foreach($items as $item)
                <div class="catalog-grid__item catalog-grid__item--half">
                    <div class="card-grid card-grid--half">
                        <!-- card-gallery-->
                        <a class="card-gallery" href="{{url( env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->client_id])}}">
                            <div class="card-grid__labels">
                                @include('advert._card_greed_labels', ['item' => $item])
                            </div>
                            <div class="card-gallery__wrap">
                                @if($item->photos->count() > 0)
                                    <?php $i = 0 ?>
                                    @foreach($item->photos as $photo)
                                        <div class="card-gallery__item @if($i == 0) hover @endif">
                                            <div class="img">
                                                <img src="/storage/photos/{{$photo->filename}}" alt=""></div>
                                        </div>
                                        @if($i > 3)
                                            <div class="card-gallery__item">
                                                <div class="img">
                                                    <img src="/storage/photos/{{$photo->filename}}" alt=""></div>
                                                <div class="card-gallery__more">
                                                    @if($item->photos->count() > 4)
                                                        <img src="img/icons/camera.svg" alt="">
                                                        <span>Еще <?= $item->photos->count() - 4 ?> фото</span></div>
                                                @endif
                                            </div>
                                        @endif
                                        <?php $i++ ?>
                                    @endforeach
                                @endif
                            </div>
                        </a>
                        <!-- end card-gallery-->
                        <div class="card-grid__desc">
                            @if(\App\Services\Region::isAutodromium())
                                <b class="card-grid__name"><a
                                        href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->client_id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                                </b>
                            @else
                                <h3 class="card-grid__name"><a
                                        href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->client_id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                                </h3>
                            @endif
                            <p class="card-grid__year">{{$item->year_of_issue}}</p>
                            <div class="card-grid__price">
                                @if(!empty($item->priceByAction()))<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span>
                                @endif
                                <span @if(!empty($item->priceByAction())) class="old" @else style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                            </div>
                            <div class="card-grid__info">
                                <p>{{$item->transmissionLabel}}, {{$item->engineLabel}}</p>
                                <p>{{$item->volume}} см3/{{$item->power}} л.с.</p>
                                <p>{{number_format($item->mileage, 0, ',', ' ')}} км, {{$item->number_of_owners}} владельца</p>
                            </div>
                            <div class="card-grid__footer">
                                <a class="card-grid__btn" data-fancybox data-type="ajax" data-src="{{url('/credit-modal', ['item' => $item->id])}}" href="javascript:;">Купить в
                                    кредит</a>
                                <div class="card-grid__credit">
                                    <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                                    <p>под {{\App\Services\Region::getCreditPercent()}}% годовых </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- end catalog-grid-->
