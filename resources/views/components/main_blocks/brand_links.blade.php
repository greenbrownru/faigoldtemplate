@props(['is_part'])
<!-- brand-links-->
<section class="brand-links">
    <div class="container">
        <div class="brand-links__row">
            @if($brandMenu->count() > 0)
                <?php $i = 1 ?>
                @foreach($brandMenu as $brandLink)
                    <a class="brand-links__item @if($i > 20 && isset($is_part))hidden @endif" @if(env('APP_SITE_REGION') == '72' || env('APP_SITE_REGION') == '18' || \App\Services\Region::isAutodromium() || env('IS_ROLF_IMPORT')) href="/{{strtolower($brandLink->name)}}" @else href="/catalog?brand={{$brandLink->id}}" @endif>
                        <img src="@if(file_exists(public_path()."/img/brands/".strtolower($brandLink->name).".webp"))/img/brands/{{strtolower($brandLink->name)}}.webp @else/img/brands/{{strtolower($brandLink->name)}}.svg @endif" alt="">
                        <div>
                            <p>{{$brandLink->name}}</p><span>{{$brandLink->cnt}} авто</span>
                        </div>
                    </a>
                    <?php $i++ ?>
                @endforeach
                    @if(isset($is_part))
                        @if($brandMenu->count() > 20)
                            <a class="brand-links__more" href="#">и еще {{$brandMenu->count() - 19}} марок</a>
                        @endif
                    @endif
            @endif
        </div>
    </div>
</section>
<!-- end brand-links-->
