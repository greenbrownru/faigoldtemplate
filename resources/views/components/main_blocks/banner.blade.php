<section class="banner">
    <div class="main_slider owl-carousel">
        @foreach(\App\Models\BannerOnMain::query()->oldest('sort')->get() as $item)
            <div class="item">
                <a href="{{ $item->link ?: 'javascript:void 0' }}">
                    <img src="{{ $item->img_url }}" alt="">
                </a>
                <a href="{{ $item->link ?: 'javascript:void 0' }}">
                    <img src="{{ $item->img_mobile_url ?: $item->img_url }}" alt="" class="slider_mobile_image">
                </a>

                @if($item->is_button)
                    <div class="banner__btn_wrapper banner__btn_wrapper_{{ $loop->iteration % 2 !== 0 ? 'left' : 'right' }}">
                        @switch($item->button_action)
                            @case('popup_form_contact')
                            <a class="card-grid__btn" data-fancybox="" data-type="ajax" data-src="/contacts" href="javascript:;">{{ $item->button_name }}</a>
                            @break

                            @default
                            <a class="card-grid__btn"  href="{{ $item->link ?: 'javascript:void 0' }}">{{ $item->button_name }}</a>
                        @endswitch
                    </div>
                @endif
            </div>
        @endforeach
    </div>
</section>
<style>
    .banner {
        position: relative;
    }

    .banner__btn_wrapper {
        position: absolute;
        bottom: 1px;
        left: 50%;
        transform: translateX(-50%);
        z-index: 1;
    }

    .banner__btn_wrapper a {
        height: 40px;
    }

    .banner__btn_wrapper_left {
        left: 75%;
        bottom: 75px;
    }

    .banner__btn_wrapper_right {
        left: 25%;
        bottom: 75px;
    }

    @media (max-width: 767px) {
        .banner__btn_wrapper_left,
        .banner__btn_wrapper_right {
            bottom: 35px;
        }
    }
</style>
<!-- end banner-->
