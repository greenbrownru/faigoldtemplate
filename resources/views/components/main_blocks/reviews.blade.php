@props(['items'])

<!-- reviews-->
<section class="reviews" id="js-reviews">
    <div class="container">
        <h2 class="section-title">Отзывы<span class="grey"><span class="blue"></span></span></h2>
        <div class="reviews__row owl-carousel">
            @foreach($items as $item)
                @if(!empty($item->video_link))<a data-fancybox="" class="reviews__item__link" data-type="iframe" href="{{url('/reviews/video', ['item' => $item->id])}}">@endif
                <div class="reviews__item">
                    <div class="review">
                        <div class="review__header">
                            <div class="review__img"><img src="/storage/photos/{{$item->client_photo_name}}" alt=""></div>
                            <div class="review__meta">
                                <p class="review__name">{{$item->fio}}</p>
                                <p class="review__car">Купил: {{$item->brand->name}} {{$item->amodel->name}}</p>
                                <ul class="review__rating">
                                    <?php $i = 0; ?>
                                    @while($i < $item->rate)
                                        <li><img src="img/icons/star.svg" style="height: 20px" alt=""></li>
                                        <?php $i++; ?>
                                    @endwhile
                                </ul>
                            </div>
                        </div>
                        <p class="review__text">{{$item->review}}</p>
                        <div class="review__footer">
                            @if(empty($item->video_link) && !empty($item->auto_photo_name))<a class="review__btn" href="#">ФОТО</a>@endif
                            @if(!empty($item->video_link))<span class="review__btn">ВИДЕО</span>@endif
                        </div>
                    </div>
                </div>
                    @if(!empty($item->video_link))</a>@endif
            @endforeach
        </div>
    </div>
</section>
<!-- end reviews-->
