@props(['items'])

<!-- catalog-grid-->
<section class="catalog-grid" id="js-new">
    <div class="container">
        <h2 class="section-title">Новые поступления<span class="grey"><span class="blue"></span></span></h2>
        <div class="catalog-grid__row carousel">
            @foreach($items as $item)
                <div class="catalog-grid__item">
                    <div class="card-grid">
                        <!-- card-gallery-->
                        <a class="card-gallery" href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->client_id])}}">
                            <div class="card-grid__labels">
                                @include('advert._card_greed_labels', ['item' => $item])
                            </div>
                            <div class="card-gallery__wrap">
                                @if($item->photos->count() > 0)
                                    <?php $i = 0 ?>
                                    @foreach($item->photos as $photo)
                                        @if($i == 4)
                                            <div class="card-gallery__item">
                                                <div class="img">
                                                    <img src="{{$photo->imagepath}}" alt=""></div>
                                                @if($item->photos->count() > 5)
                                                    <div class="card-gallery__more">
                                                        <img src="img/icons/camera.svg"
                                                             alt=""><span>Еще <?= $item->photos->count() - 5 ?> фото</span>
                                                    </div>
                                                @endif
                                            </div>
                                            @break
                                        @else

                                            <div class="card-gallery__item @if($i == 0) hover @endif">
                                                <div class="img">
                                                    <img src="{{$photo->imagepathresized}}" alt="">
                                                </div>
                                            </div>
                                        @endif
                                        <?php $i++ ?>
                                    @endforeach
                                @endif
                            </div>
                        </a>
                        <!-- end card-gallery-->
                        <div class="card-grid__desc">
                                <b class="card-grid__name"><a
                                        href="{{url(env('APP_SITE_REGION') == 72 ? '/catalog' : $item->PathWithDomain, ['advert' => $item->client_id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                                </b>
                            <p class="card-grid__year">{{$item->year_of_issue}}</p>
                            <div class="card-grid__price">
                                @if(!empty($item->priceByAction()) && $item->priceByAction() < $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span>
                                @endif
                                <span @if(!empty($item->priceByAction()) && $item->priceByAction() < $item->price) class="old" @else style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                            </div>
                            <div class="card-grid__info">
                                <p>{{$item->transmissionLabel}}, {{$item->engineLabel}}</p>
                                <p>{{$item->volume}} см3/{{$item->power}} л.с.</p>
                                <p>{{number_format($item->mileage, 0, ',', ' ')}} км, {{$item->number_of_owners}}
                                    владельца</p>
                            </div>
                            <div class="card-grid__footer">
                                <a class="card-grid__btn" data-fancybox data-type="ajax"
                                   data-src="{{url('/credit-modal', ['item' => $item->id])}}" href="javascript:;">
                                    @if(in_array(env('SITE_SUFFIX'), ['probeg_spb']))
                                        Узнать подробнее
                                    @else
                                        Купить в кредит
                                    @endif
                                </a>
                                <div class="card-grid__credit">
                                    <p>от <span>{{$item->CreditPayByMonth}} р/мес.</span></p>
                                    <p>под {{\App\Services\Region::getCreditPercent()}}% годовых</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- end catalog-grid-->
