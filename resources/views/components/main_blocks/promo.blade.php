<!-- m-promo-->
<div class="m-promo">
    <div class="container">
        <div class="m-promo__grid">
            @if(\App\Models\Banner::query()->where(['display_on_main' => true, 'is_big' => true])->exists())
                <?php $big = \App\Models\Banner::query()->where(['display_on_main' => true, 'is_big' => true])->first(); ?>
            <a class="m-promo__item" href="{{$big->link}}"><img src="{{asset('/storage/photos/'.$big->filename)}}" alt="">
                <div class="m-promo__text">
                    @if(env('APP_SITE_REGION') != 72)<p>{{$big->shortTextFormat}}</p>@endif
                </div></a>
            @endif
            @if(\App\Models\Banner::query()->where(['display_on_main' => true, 'is_big' => false])->exists())
                    <?php $banners = \App\Models\Banner::query()->where(['display_on_main' => true, 'is_big' => false])->get(); ?>
                @foreach($banners as $banner)
            <a class="m-promo__item" href="{{$banner->link}}"><img src="{{asset('/storage/photos/'.$banner->filename)}}" alt="">
                <div class="m-promo__text">
                    @if(env('APP_SITE_REGION') != 72)<p>{{$banner->shortTextFormat}}</p>@endif
                </div>
            </a>
                        @endforeach
                @endif

        </div>
    </div>
</div>
<!-- end m-promo-->
