@extends('layouts.auto')
@section('content')
    <x-main_blocks.banner />
    <x-main_blocks.brand_links is_part/>
    @if($advertsVip->count() > 0)
        <x-main_blocks.vip :items="$advertsVip"/>
    @endif
    @if($advertsSpec->count() > 0)
        <x-main_blocks.spec :items="$advertsSpec"/>
    @endif
    @if($advertsNew->count() > 0)
        <x-main_blocks.new :items="$advertsNew"/>
    @endif
    @if(env('APP_SITE_REGION') == 18)
        <x-main_blocks.short />
    @endif
    <x-main_blocks.promo />
    @if($reviews->count() > 0)
        <x-main_blocks.reviews :items="$reviews"/>
    @endif
@endsection

@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__main_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__main_description') ?: setting('page__description')) }}@endsection