@extends('layouts.auto')
@section('content')
<section class="page car_service" id="js-page">
    <div class="container" id="page_about">
        <h1 class="section-title">Автосервис<span class="grey"><span class="blue"></span></span></h1>

        <div class="page_content">
            <div class="left">
                <div class="car_service-text">
                    {!! html_entity_decode(\App\Models\Page::whereTitle('car_service')->first()->content ?? '') !!}
                </div>
            </div>
            <div class="side">
                <section class="feedback">
                    <h2 class="feedback__title">Форма заявки</h2>
                    <form action="/car_service/request-add" method="post" id="car_service-form">
                        @csrf

                        @foreach ($errors->all() as $error)
                            <p style="color: red;">{{ $error }}</p>
                        @endforeach

                        <div class="feedback__section">
                            <h4 class="feedback__subtitle">Укажите свой автомобиль</h4>

                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" name="brand_name" placeholder="Марка" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" name="model_name" placeholder="Модель" required autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="number" name="mileage" placeholder="Пробег" required autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="feedback__section">
                            <h4 class="feedback__subtitle">Укажите свои данные</h4>

                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше ФИО" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email" autocomplete="email">
                            </div>
                            <div class="form-group">
                                <input class="phone" type="tel" name="phone" required autocomplete="off">
                            </div>
                            <button class="feedback__btn" type="submit">Отправить</button>
                            <div class="mt-2">
                                <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                            </div>
                        </div>
                    </form>
                </section>

                <div style="margin-top: 50px;">
                    <div class="item">
                        <i class="fa fa-users fa-fw" aria-hidden="true"></i> Работа у нас
                        <div class="hint">Человек должен отправлять резюме на адрес эл. почты info@armavir-expert.ru<br><a href="javascript:void(0)" class="copy_email" data-email="info@armavir-expert.ru">Скопировать email</a></div>
                    </div>
                    <div class="item">
                        <i class="fa fa-thumbs-o-up fa-fw" aria-hidden="true"></i> Пожелания по качеству работ
                        <div class="hint">так же на эл. почту info@armavir-expert.ru<br><a href="javascript:void(0)" data-email="info@armavir-expert.ru" class="copy_email">Скопировать email</a></div>
                    </div>
                    <div class="item">
                        <i class="fa fa-address-book fa-fw" aria-hidden="true"></i> Контакты
                        <div class="hint">тел. +7(861)376-36-30 info@armavir-expert.ru<br><a href="javascript:void(0)" data-email="info@armavir-expert.ru" class="copy_email">Скопировать email</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
    <script>
        $(document).on('submit', '#car_service-form', function (e) {
            var form = $(this);
            $.ajax({
                beforeSend: function (xhr, opts) {
                    beforeSendMakes(form, xhr);
                },
                complete: function () {
                    $('.feedback__btn').removeClass('progress-bar-striped');
                    $('.feedback__btn').removeClass('progress-bar-animated');
                },
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.serialize(),
                success: function (data) {
                    if(data == 0) {
                        $(':input[required]', form).css({border: '1px solid red'})
                    }else{
                        $('[data-fancybox-close]').click();
                        $('body').addClass('thanksPopupBody');
                        $('.thanks_modal').addClass('active');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
            e.preventDefault();
        });
    </script>
@endsection


