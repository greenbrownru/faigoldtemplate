<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ setting('name') ?: 'Авто' }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        svg {
            width: 30px;
        }
    </style>
    <style>
        .current_page {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            height: 34px;
            padding: 0 15px;
            background-color: #dae4eb;
            color: black;
            font-size: 14px;
            text-decoration: none;
        }

        .disabled_page {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            height: 34px;
            padding: 0 15px;
            background-color: #dae4eb;
            color: black;
            font-size: 14px;
            text-decoration: none;
        }

        .sort__list {
            margin: 0;
            padding: 0;
            list-style: none;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
        }

        .form-check-switcher {
            position: relative;

            width: 50px;
            height: 25px;
            margin: 0;

            vertical-align: top;

            background: #eee;
            border: 1px solid #bbc1e1;
            border-radius: 30px;
            outline: none;
            cursor: pointer;

            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;

            transition: all 0.3s cubic-bezier(0.2, 0.85, 0.32, 1.2);
        }
        .form-check-switcher::after {
            content: "";

            position: absolute;
            left: 3px;
            top: 1.5px;

            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;

            transform: translateX(0);

            transition: all 0.3s cubic-bezier(0.2, 0.85, 0.32, 1.2);
        }
        .form-check-switcher:checked::after {
            transform: translateX(calc(100% + 3px));
        }
        .form-check-switcher:checked {
            background-color: #1ac11f;
        }

    </style>

    @stack('css')
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ setting('name') ?: 'Авто' }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a href="{{ url('/admin') }}" class="nav-link">Dashboard <span class="sr-only">(current)</span></a></li>
                    @if(\App\Services\Region::isAutodromium())
                        <li class="nav-item"><a href="{{ url('/home') }}?refresh_feed=1" class="nav-link" onclick="return confirm('Обновить фид?');">Обновить фид</a></li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
                        <li><a class="nav-link" href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @if (Session::has('flash_message'))
            <div class="container">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_message') }}
                </div>
            </div>
        @endif

        @yield('content')
    </main>

    <hr/>

    <div class="container">
        &copy; {{ date('Y') }}. Created by <a href="http://www.appzcoder.com">AppzCoder</a>
        <br/>
    </div>

</div>

<!-- Scripts -->

@if(!\Illuminate\Support\Facades\Request::is('admin/request-all'))
<script src="{{ asset('js/app.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@endif

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.8.1/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '.crud-richtext'
    });
</script>
<script type="text/javascript">
    $(function () {
        // Navigation active
        $('ul.navbar-nav a[href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"]').closest('li').addClass('active');
    });
</script>

@yield('scripts')
<script>
		$('#brand_id, #model_id, #client_brand, #client_model').select2();

		$(document).on('change', '#brand_id, #client_brand', function () {
			let brand = $(this).val();

			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				beforeSend: function () {
					//$('.preloader').show();
				},
				complete: function () {
					//$('.preloader').hide();
				},
				url: '/home/get-models',
				type: 'post',
				data: {"brand": brand},
				dataType: 'json',
				success: function (data) {
					var options = '';
					$.each(data, function (index, val) {
						console.log(index);
						options += "<option value='" + index + "'>" + val + "</option>"
					});

					$('#model_id').html(options);
					$('#client_model').html(options);
				},
				error: function (error) {
					console.log(error);
				}
			});
		}).on('ajaxComplete', function (e) {
		});

</script>
</body>
</html>
