<?php $banners = \App\Models\Banner::whereType(\App\Models\Banner::BANNER_TYPE_ADDITIONAL)
    ->where(['display_on_main' => false])
    ->get() ?>
@if($banners)
    @foreach($banners as $banner)
        <div style="margin-bottom: 30px;border-radius: 5px;background-color: #f2f5f7;box-shadow: 0px 0px 0px 0px rgba(136,146,153,0.3);">
            <a href="{{$banner->link}}"><img src="{{ asset('/storage/photos/'.$banner->filename) }}" style="border-radius: 4%"></a>
        </div>
    @endforeach
@endif
