@extends('layouts.auto')
@section('content')
    <!-- page-->
    <section class="page" id="js-page">
        <div class="container">
            <h1 class="section-title">Автокредит<span class="grey"><span class="blue"></span></span></h1>
            <div class="page__row">
                <div class="page__left">
                    <!-- article-->
                    <article class="article">
                       {!! html_entity_decode(\App\Models\Page::whereTitle('credit')->first()->contentFormat ?? '') !!}
                    </article>
                    <!-- end article-->
                </div>
                <div class="page__right">
                    <!-- feedback-->
                    <section class="feedback">
                        <h2 class="feedback__title">Форма заявки</h2>
                        <form action="/credit/request-add" method="post" id="credit-form"
                              @if(\App\Services\Region::isAutodromium()) onsubmit="ym(61299526,'reachGoal','credit'); ga('send', 'event', 'credit', 'Submit');" @endif
                              @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','credit');return true;" @endif
                        >
                            @csrf
                            <div class="feedback__section">
                                @if(\App\Services\Region::isAutodromium())
                                    <b class="feedback__subtitle">Выберите желаемый автомобиль </b>
                                @else
                                    <h4 class="feedback__subtitle">Выберите желаемый автомобиль </h4>
                                @endif

                                <div class="form-group">
                                    <div class="select" id="select_brand">
                                        <div class="select__label">@if(!$item) Марка @endif</div>
                                        <div class="select__input" > @if($item) {{ $item->brand->name }} @endif</div>
                                        <input type="hidden" name="credit_brand" value="@if($item) {{ $item->brand_id }} @endif">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Марка</li>
                                            @if($brands)
                                                @foreach($brands as $brand)
                                                    <li class="li-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_model">
                                        <div class="select__label"> @if(!$item) Модель @endif</div>
                                        <div class="select__input" > @if($item) {{ $item->amodel->name }} @endif</div>
                                        <input type="hidden" name="credit_model" value="@if($item) {{ $item->model_id }} @endif">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Модель</li>
                                            @if($models)
                                                @foreach($models as $model)
                                                    <li class="li-model" data-value="{{$model->id}}">{{$model->name}}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="select" id="select_advert">
                                        <div class="select__label"> @if(!$item) @endif</div>
                                        <div class="select__input" style="height: 43px" > @if($item) {{ $item->creditName }} @endif </div>
                                        <input type="hidden" name="credit_advert" value="@if($item) {{ $item->id }} @endif">
                                        <ul class="select__list">
                                            <li class="select__header"><span class="select__close">Отмена</span>Хар-ки авто</li>
                                            @if($adverts)
                                                @foreach($adverts as $advert)
                                                    <li class="li-advert" data-value="{{$advert->id}}">{!! $advert->creditName !!}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="feedback__img" @if(!$item) style="display: none" @endif>
                                    @if($item)
                                    <img src="/storage/photos/{{$item->photos->first()->filename}}" alt="">
                                    @endif
                                </div>
                            </div>
                            <div class="feedback__section" id="credit__section" @if(!$item) style="display: none" @endif>
                                <b class="feedback__subtitle">Кредитные условия </b>
                                <div class="form-slider">
                                    <input type="hidden" value="60" id="loan-terms-val">
                                    <p>
                                        <label>Срок кредитования</label>
                                        <input id="loan-terms" type="text" name="credit_term" value="60" readonly>
                                    </p>
                                    <div id="slider-loan-terms"></div>
                                </div>
                                <div class="form-slider">
                                    <p>
                                        <label>Первоначальный взнос</label>
                                        <input id="initial-fee" type="text" value="10" name="down_payment" readonly>
                                    </p>
                                    <div id="slider-initial-fee"></div>
                                </div>
                                <div style="display: none" id="hidden_price">@if($item) {{ $item->price }} @else 0 @endif</div>
                                <div class="feedback__calculated">
                                    <span>Ежемесячный платеж: </span><span id="credit_pay"> 0 </span></div>
                                <div class="feedback__calculated"><span>Годовая ставка:</span><span>{{\App\Services\Region::getCreditPercent()}}%</span></div>
                            </div>
                            <div class="feedback__section">
                                @if(\App\Services\Region::isAutodromium())
                                    <b class="feedback__subtitle">Укажите свои данные</b>
                                @else
                                    <h4 class="feedback__subtitle">Укажите свои данные</h4>
                                @endif

                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input class="phone" type="tel" name="phone" required autocomplete="off">
                                </div>
                                <button class="feedback__btn" type="submit">Отправить</button>
                                <div class="mt-2">
                                    <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                                </div>
                            </div>
                        </form>
                    </section>
                    <!-- end feedback-->
                </div>
                <div class="page__full">
                    <!-- partners-->
                    <section class="partners">
                        <h3 class="partners__title">Банки-партнеры</h3>
                        <div class="partners__row">
                            @if(env('APP_SITE_REGION') == 52)
                                <div class="partners__item" style="height: auto"><img src="/img/credit/2.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/4.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/12.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/6.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/9.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/10.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/13.jpg" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/14.jpg" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/15.png" alt=""></div>
                            @else
                                <div class="partners__item" style="height: auto"><img src="/img/credit/1.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/2.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/{{ \App\Services\Region::isAutodromium() ? 12 : 3 }}.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/4.png" alt=""></div>
                                @if(!\App\Services\Region::isAutodromium() && env('SITE_SUFFIX') !== 'autofull' && !in_array(env('SITE_SUFFIX'), ['armavir', 'autoclub', 'adb']))
                                    <div class="partners__item" style="height: auto"><img src="/img/credit/5.png" alt=""></div>
                                @endif
                                <div class="partners__item" style="height: auto"><img src="/img/credit/6.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/7.png" alt=""></div>
                                @if(!\App\Services\Region::isAutodromium())
                                    <div class="partners__item" style="height: auto"><img src="/img/credit/8.png" alt=""></div>
                                @endif
                                <div class="partners__item" style="height: auto"><img src="/img/credit/9.png" alt=""></div>
                                <div class="partners__item" style="height: auto"><img src="/img/credit/10.png" alt=""></div>
                                @if(!in_array(env('SITE_SUFFIX'), ['armavir', 'autoclub', 'adb']) && env('APP_SITE_REGION') != 18) <div class="partners__item" style="height: auto"><img src="/img/credit/11.png" alt=""></div> @endif
                            @endif
                        </div>
                    </section>
                    <!-- end partners-->
                </div>
            </div>
        </div>
    </section>
    <!-- end page-->

@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.li-brand', function (e) {
                let elementModel = $('input[name="credit_model"]').parent().find('ul');
                $('#select_model').find('.select__input').html('Модель');
                $('#select_advert').find('.select__input').html('Объявления');
                $(elementModel).html('<li data-value="">Модель</li>');
                $('.feedback__img').html('');
                $('input[name="credit_advert"]').parent().find('ul').html('<li data-value="">Объявления</li>');
                let brand = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-models',
                    type: 'post',
                    data: {"brand_id": brand},
                    dataType: 'json',
                    success: function (data) {
                        let content = '<li data-value="">Модель</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-model" data-value="'+value.id+'">'+value.name+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });

            $(document).on('click', '.li-model', function (e) {
                let elementModel = $('input[name="credit_advert"]').parent().find('ul');
                $(elementModel).html('<li data-value="">Объявления</li>');
                $('#select_advert').find('.select__input').html('Объявления');
                $('.feedback__img').html('');
                let model = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-adverts',
                    type: 'post',
                    data: {"model_id": model},
                    dataType: 'json',
                    success: function (data) {
                        let content = '<li data-value="">Объявления</li>'
                        $(data).each(function (index, value) {
                            content += '<li class="li-advert" data-value="'+value.id+'">'+value.creditname+'</li>'
                        });
                        $(elementModel).html(content);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });

            $(document).on('click', '.li-advert', function (e) {
                $('.feedback__img').html('');
                let advert = $(this).attr('data-value');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-advert-image',
                    type: 'post',
                    data: {"advert_id": advert},
                    dataType: 'json',
                    success: function (data) {
                        $('.feedback__img').show().html('<img src="'+data.imagepath+'" >');
                        $('#hidden_price').text(data.price);

                        $("#initial-fee").val($("#slider-initial-fee").slider("value") + '%');

                        var pay = ((($('#hidden_price').text() - (($("#slider-initial-fee").slider("value") / 100) * $('#hidden_price').text())).toFixed(0) / $("#loan-terms-val").val()));
                        var pay_end = (pay + (6.4 / 100) * pay).toFixed(0);
                        $("#credit_pay").text(formatPrice(pay_end));
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                e.preventDefault();
            }).on('ajaxComplete', function (e) {
            });
        });

        $(document).on('submit', '#credit-form', function (e) {
            var form = $(this);
            $.ajax({
                beforeSend: function (xhr, opts) {
                    beforeSendMakes(form, xhr);

                },
                complete: function () {
                    $('.feedback__btn').removeClass('progress-bar-striped');
                    $('.feedback__btn').removeClass('progress-bar-animated');
                    $(form).trigger("reset");
                },
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.serialize(),
                success: function (data) {

					if(data == 0){
						$('form#credit-form input.phone').css({border: '1px solid red'})
					}else{
						$('[data-fancybox-close]').click();
						$('body').addClass('thanksPopupBody');
						$('.thanks_modal').addClass('active');
					}


                },
                error: function (error) {
                    console.log(error);
                }
            });
            e.preventDefault();
        });
    </script>
@endsection
@section('title'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__credit_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'name', 'city'], setting('page__credit_description') ?: setting('page__description')) }}@endsection
