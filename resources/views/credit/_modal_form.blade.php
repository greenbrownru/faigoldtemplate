@if(request()->filled('inOneClick'))
    <style>
        .fancybox-container .modal-flex .card-grid,
        .fancybox-container .feedback__section_credit_rules {
            display: none !important;
        }
    </style>
@endif

<div id="advert-{{$item->id}}" data-class="advert-block" style="display: none; overflow-y: hidden">
    <input type="hidden" id="credit-percent" value="{{\App\Services\Region::getCreditPercent()}}">
    <div class="modal-flex">
        <div class="card-grid" style="height: auto">
            <div class="card-grid__labels">
                @include('advert._card_greed_labels', ['item' => $item])
            </div>
            <!-- card-gallery-->
            <div class="card-gallery">
                <div class="card-gallery__wrap">
                    @if($item->photos->count() > 0)
                        <?php $i = 0 ?>
                        @foreach($item->photos as $photo)
                            @if($i == 4)

                                <div class="card-gallery__item">
                                    <div class="img">
                                        <img src="{{$photo->imagepath}}" alt=""></div>
                                    @if($item->photos->count() > 5)
                                        <div class="card-gallery__more">
                                            <img src="/img/icons/camera.svg" alt=""><span>Еще <?= $item->photos->count() - 5 ?> фото</span>
                                        </div>
                                    @endif
                                </div>
                                @break
                            @else

                                <div class="card-gallery__item @if($i == 0) hover @endif">
                                    <div class="img">
                                        <img src="{{$photo->imagepathresized}}" alt="">
                                    </div>
                                </div>
                            @endif
                            <?php $i++ ?>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- end card-gallery-->
            <div class="card-grid__desc">
                <h3 class="card-grid__name"><a
                        href="{{url('/adverts', ['advert' => $item->id])}}">{{$item->brand->name}} {{$item->amodel->name}}</a>
                </h3>
                <p class="card-grid__year">{{$item->year_of_issue}}</p>
                <div class="card-grid__price">
                    @if(intval($item->priceByAction()) > 0 && $item->priceByAction() != $item->price)<span>{{number_format($item->priceByAction(), 0, ',', ' ')}} &#8381;</span> @endif
                    <span class="old" @if(intval($item->priceByAction()) == 0 || $item->priceByAction() == $item->price) style="color: #5e5e5e" @endif>{{number_format($item->price, 0, ',', ' ')}} &#8381;</span>
                </div>
                <div class="card-grid__info">
                    <p>{{$item->transmissionLabel}}, {{$item->engineLabel}}</p>
                    <p>{{$item->volume}} л./{{$item->power}} л.с.</p>
                    <p>{{number_format($item->mileage, 0, ',', ' ')}} км, владельцев {{$item->number_of_owners}}</p>
                </div>
            </div>
        </div>

        <form action="/credit/request-add" method="post" id="credit-modal"
              @if(\App\Services\Region::isAutodromium()) onsubmit="ym(61299526,'reachGoal','creditmodel'); ga('send', 'event', 'creditmodel', 'Submit');" @endif
              @if(env('IS_ROLF_IMPORT') && !env('SITE_SUFFIX')) onsubmit="ym(85871410,'reachGoal','creditmodel');return true;" @endif
        >
            @csrf

            <input type="hidden" name="credit_brand" value="{{ $item->brand_id }}">
            <input type="hidden" name="credit_model" value="{{ $item->model_id }}">
            <input type="hidden" name="credit_advert" value="{{ $item->id }}">
            <input type="hidden" name="car_cost" value="{{ $item->priceByAction() ?: $item->price }}">
            <div class="feedback__section feedback__section_credit_rules">
                <h4 class="feedback__subtitle">Кредитные условия </h4>
                <div class="form-slider">
                    <input type="hidden" value="@if(in_array(env('SITE_SUFFIX'), ['orenburg_expert', 'bu_in_orenburg'])) 96 @else 84 @endif" id="loan-terms-val">
                    <p>
                        <label>Срок кредитования</label>
                        <input id="loan-terms" type="text" name="credit_term" readonly>
                    </p>
                    <div id="slider-loan-terms"></div>
                </div>
                <div class="form-slider">
                    <p>
                        <label>Первоначальный взнос</label>
                        <input id="initial-fee" type="text"  name="down_payment" readonly>
                    </p>
                    <div id="slider-initial-fee"></div>
                </div>
                <div style="display: none" id="hidden_price">@if($item) {{ $item->priceByAction() > 0 ? $item->priceByAction() : $item->price }} @else 0 @endif</div>
                <input type="number" step="0.01" style="display: none" id="hidden_percent" value="{{str_replace(',', '.', \App\Services\Region::getCreditPercent())}}"/>
                <div class="feedback__calculated"><span>Ежемесячный платеж: </span><span
                            id="credit_pay"> </span><span style="color: #d40c0c; font-weight: bold">  &#8381;</span>
                </div>
                <div class="feedback__calculated"><span>Годовая ставка:</span><span>{{\App\Services\Region::getCreditPercent()}}%</span></div>
            </div>
            <div class="feedback__section">
                <h4 class="feedback__subtitle">Укажите свои данные</h4>
                <div class="form-group">
                    <input class="name" type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="phone" type="tel" name="phone" autocomplete="off" required>
                </div>
                <button class="feedback__btn" type="submit">Отправить</button>
                <div class="mt-2">
                    <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    var creditPercent = $("#hidden_percent").val();

    /*
        Имеем:

           i= 48%/12 месяцев = 4% или 0,04
           n = 4 года* 12 месяцев = 48 (месяцев)
           S = 2 000
           Рассчитываем К:
           К=(0,04*〖(1+0,04)〗^48)/(〖(1+0,04)〗^48-1) = 0,0472
           подставим полученное значение в формулу ежемесячного платежа:
           А = 0,0472 * 2 000 = 94,4 рублей.

       0,01075×((1+0,01075)^96)÷((1+0,01075)^96−1)

        */
    var i= (creditPercent/12)/100;

    $("#slider-loan-terms").slider({
        value: {{ setting('credit_max_month') }},
        min: 1,
        max: {{ setting('credit_max_month') }},
        range: "min",
        slide: function (event, ui) {

            multiplicator = Math.floor(ui.value / 12);
            $("#loan-terms").val(ui.value + ' мес');
            $("#loan-terms-val").val(ui.value);
            var k = i*(Math.pow((1+i),ui.value))/(Math.pow((1+i), ui.value)-1);
            var pay = ((($('#hidden_price').text() - (($("#slider-initial-fee").slider("value") / 100) * $('#hidden_price').text()))));
            var pay_end = (Math.floor(k * pay)).toFixed(0);
            $("#credit_pay").text(formatPrice(pay_end));
        }
    });
    var k = i*(Math.pow((1+i),$('#slider-loan-terms').slider('value')))/(Math.pow((1+i), $('#slider-loan-terms').slider('value'))-1);
    {{--Подбираем множитель--}}
    var multiplicator = Math.floor($("#slider-loan-terms").slider('value') / 12);

    $("#loan-terms").val($("#slider-loan-terms").slider("value") + ' мес');

    $("#slider-initial-fee").slider({
        value: {{ setting('initial_fee') }},
        min: 0,
        max: 100,
        range: "min",
        slide: function (event, ui) {
            $("#initial-fee").val(ui.value + '%');
            var k = i*(Math.pow((1+i),$('#slider-loan-terms').slider('value')))/(Math.pow((1+i), $('#slider-loan-terms').slider('value'))-1);
            var pay = ((($('#hidden_price').text() - ((ui.value / 100) * $('#hidden_price').text())).toFixed(0)));
            var pay_end = (Math.floor(k * pay)).toFixed(0);
            $("#credit_pay").text(formatPrice(pay_end));
        }
    });
    $("#initial-fee").val($("#slider-initial-fee").slider("value") + '%');

    var pay = ((($('#hidden_price').text() - (($("#slider-initial-fee").slider("value") / 100) * $('#hidden_price').text())).toFixed(0)));

    {{--т.к. в слайдере 84 месяца (7 лет) => 6.4 * 7--}}
    //var pay_end = (pay + ((creditPercent * multiplicator) / 100) * pay).toFixed(0);
    var pay_end = (Math.floor(k * pay)).toFixed(0);
    $("#credit_pay").text(formatPrice(pay_end));


    var phoneFocusTrigger = true;
    $('.phone')
        //======fix for autocomplete
        .attr('readonly',true)
        .on('click focus', function() {
            $(this).attr('readonly', false);
            if (phoneFocusTrigger) {
                phoneFocusTrigger = false;
                document.querySelector('.name').focus();
                this.focus();
            }
        })
        //======./fix for autocomplete

        .intlTelInput({
            defaultCountry: 'ru',
            preferredCountries: ['ru'],
            separateDialCode: true,
        });

    $(document).on('keyup', '.name', function(e){
        $(this).val($(this).val().replace(/[^\+а-яА-Яa-zA-Z ]/g, ''));
    });

    $('.card-gallery__item').hover(
        function () {
            $(this).addClass('hover').siblings('.card-gallery__item').removeClass('hover');
        },
        function () {
            $(this).removeClass('hover');
            $('.card-gallery__item:first-child').addClass('hover');
        });

</script>
