@extends('layouts.auto')
@section('content')
    <!-- page-->
    <section class="page" id="js-page">
        <div class="container">
            <section class="feedback">
                <h3 class="feedback__title">Отписаться от рассылки</h3>
                <form action="/subscribes/unsubscribe" method="post">
                    @csrf
                    <div class="feedback__section">
                        <div class="form-group">
                            <input placeholder="Ваш Email" type="email" name="email" required>
                        </div>
                        <button class="feedback__btn" type="submit">Отправить</button>
                    </div>
                </form>
            </section>
        </div>
    </section>
@endsection
@section('javascript')
    <script>

        (function ($) {
            $.fn.inputFilter = function (inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            };
        }(jQuery));

        $(".input-filter").inputFilter(function (value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 90000000);
        });
    </script>
@endsection
