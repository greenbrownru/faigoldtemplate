{{--@extends('layouts.auto')
@section('content')
    <!-- page-->
    <section class="page" id="js-page">
        <div class="container">--}}
            <section class="feedback">
                <h3 class="feedback__title">Форма подписки</h3>
                <form action="/subscribes/subscribe" id="subscribe-form" method="post">
                    @csrf
                    <div class="feedback__section">
                        <h4 class="feedback__subtitle">Выберите параметры авто </h4>
                        <div class="filter__row">
                            <div class="filter__col">
                                <div class="select" id="select_brand">
                                    <div class="select__label">Марка</div>
                                    <div class="select__input"></div>
                                    <input type="hidden" name="brand" value="">
                                    <ul class="select__list">
                                        <li class="select__header"><span class="select__close">Отмена</span>Марка</li>
                                        @if($brands)
                                            @foreach($brands as $brand)
                                                <li class="li-brand" data-value="{{$brand->id}}">{{$brand->name}}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="filter__col">
                                <div class="select select--dual">
                                    <input class="select select__input input-filter" placeholder="Цена от, ₽" name="price_from" type="text">
                                </div>
                                <div class="select select--dual">
                                    <input class="select select__input input-filter" placeholder="До" name="price_to" type="text">
                                </div>
                            </div>
                            <div class="filter__col">
                                <div class="select select--dual">
                                    <div class="select__label"> Год,
                                        от </div>
                                    <div class="select__input"> </div>
                                    <input type="hidden" name="year_from" value="">
                                    <ul class="select__list">
                                        <li class="select__header"><span class="select__close">Отмена</span>Год, от
                                        </li>
                                        <li data-value="2020">2020</li>
                                        <li data-value="2019">2019</li>
                                        <li data-value="2018">2018</li>
                                        <li data-value="2017">2017</li>
                                        <li data-value="2016">2016</li>
                                        <li data-value="2015">2015</li>
                                        <li data-value="2014">2014</li>
                                        <li data-value="2013">2013</li>
                                        <li data-value="2012">2012</li>
                                        <li data-value="2011">2011</li>
                                        <li data-value="2010">2010</li>
                                        <li data-value="2009">2009</li>
                                        <li data-value="2008">2008</li>
                                        <li data-value="2007">2007</li>
                                        <li data-value="2006">2006</li>
                                        <li data-value="2005">2005</li>
                                        <li data-value="2004">2004</li>
                                        <li data-value="2003">2003</li>
                                        <li data-value="2002">2002</li>
                                        <li data-value="2001">2001</li>
                                        <li data-value="2000">2000</li>
                                        <li data-value="1999">1999</li>
                                        <li data-value="1998">1998</li>
                                        <li data-value="1997">1997</li>
                                        <li data-value="1996">1996</li>
                                        <li data-value="1995">1995</li>
                                        <li data-value="1994">1994</li>
                                        <li data-value="1993">1993</li>
                                        <li data-value="1992">1992</li>
                                        <li data-value="1991">1991</li>
                                        <li data-value="1990">1990</li>
                                        <li data-value="1989">1989</li>
                                        <li data-value="1988">1988</li>
                                        <li data-value="1987">1987</li>
                                        <li data-value="1986">1986</li>
                                        <li data-value="1985">1985</li>
                                        <li data-value="1984">1984</li>
                                        <li data-value="1983">1983</li>
                                        <li data-value="1982">1982</li>
                                        <li data-value="1981">1981</li>
                                        <li data-value="1980">1980</li>
                                        <li data-value="1979">1979</li>
                                        <li data-value="1978">1978</li>
                                        <li data-value="1977">1977</li>
                                        <li data-value="1976">1976</li>
                                        <li data-value="1975">1975</li>
                                        <li data-value="1974">1974</li>
                                        <li data-value="1973">1973</li>
                                        <li data-value="1972">1972</li>
                                        <li data-value="1971">1971</li>
                                        <li data-value="1970">1970</li>
                                        <li data-value="1969">1969</li>
                                        <li data-value="1968">1968</li>
                                        <li data-value="1967">1967</li>
                                        <li data-value="1966">1966</li>
                                        <li data-value="1965">1965</li>
                                        <li data-value="1964">1964</li>
                                        <li data-value="1963">1963</li>
                                        <li data-value="1962">1962</li>
                                        <li data-value="1961">1961</li>
                                    </ul>
                                </div>
                                <div class="select select--dual">
                                    <div class="select__label"> Год,
                                        до </div>
                                    <div class="select__input"> </div>
                                    <input type="hidden" name="year_to" value="">
                                    <ul class="select__list">
                                        <li class="select__header"><span class="select__close">Отмена</span>Год, от</li>
                                        <li data-value="2020">2020</li>
                                        <li data-value="2019">2019</li>
                                        <li data-value="2018">2018</li>
                                        <li data-value="2017">2017</li>
                                        <li data-value="2016">2016</li>
                                        <li data-value="2015">2015</li>
                                        <li data-value="2014">2014</li>
                                        <li data-value="2013">2013</li>
                                        <li data-value="2012">2012</li>
                                        <li data-value="2011">2011</li>
                                        <li data-value="2010">2010</li>
                                        <li data-value="2009">2009</li>
                                        <li data-value="2008">2008</li>
                                        <li data-value="2007">2007</li>
                                        <li data-value="2006">2006</li>
                                        <li data-value="2005">2005</li>
                                        <li data-value="2004">2004</li>
                                        <li data-value="2003">2003</li>
                                        <li data-value="2002">2002</li>
                                        <li data-value="2001">2001</li>
                                        <li data-value="2000">2000</li>
                                        <li data-value="1999">1999</li>
                                        <li data-value="1998">1998</li>
                                        <li data-value="1997">1997</li>
                                        <li data-value="1996">1996</li>
                                        <li data-value="1995">1995</li>
                                        <li data-value="1994">1994</li>
                                        <li data-value="1993">1993</li>
                                        <li data-value="1992">1992</li>
                                        <li data-value="1991">1991</li>
                                        <li data-value="1990">1990</li>
                                        <li data-value="1989">1989</li>
                                        <li data-value="1988">1988</li>
                                        <li data-value="1987">1987</li>
                                        <li data-value="1986">1986</li>
                                        <li data-value="1985">1985</li>
                                        <li data-value="1984">1984</li>
                                        <li data-value="1983">1983</li>
                                        <li data-value="1982">1982</li>
                                        <li data-value="1981">1981</li>
                                        <li data-value="1980">1980</li>
                                        <li data-value="1979">1979</li>
                                        <li data-value="1978">1978</li>
                                        <li data-value="1977">1977</li>
                                        <li data-value="1976">1976</li>
                                        <li data-value="1975">1975</li>
                                        <li data-value="1974">1974</li>
                                        <li data-value="1973">1973</li>
                                        <li data-value="1972">1972</li>
                                        <li data-value="1971">1971</li>
                                        <li data-value="1970">1970</li>
                                        <li data-value="1969">1969</li>
                                        <li data-value="1968">1968</li>
                                        <li data-value="1967">1967</li>
                                        <li data-value="1966">1966</li>
                                        <li data-value="1965">1965</li>
                                        <li data-value="1964">1964</li>
                                        <li data-value="1963">1963</li>
                                        <li data-value="1962">1962</li>
                                        <li data-value="1961">1961</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feedback__section">
                        <h4 class="feedback__subtitle">Укажите свои данные</h4>
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Ваше имя" required>
                        </div>
                        <div class="form-group">
                            <input placeholder="Ваш Email" type="email" name="email" required>
                        </div>
                        <button class="feedback__btn" @if(in_array(env('APP_SITE_REGION'), ['', 63, 66])) onsubmit="ym(61299526,'reachGoal','podpiska'); ga('send', 'event', 'podpiska', 'Submit');" @endif type="submit">Отправить</button>
                    </div>
                    <p>Вы всегда можете <a href="/subscribes/unsubscribe">отписаться</a> от рассылки</p>
                </form>
            </section>
{{--       </div>
  </section>
@endsection
@section('javascript')
  <script>

      (function ($) {
          $.fn.inputFilter = function (inputFilter) {
              return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                  if (inputFilter(this.value)) {
                      this.oldValue = this.value;
                      this.oldSelectionStart = this.selectionStart;
                      this.oldSelectionEnd = this.selectionEnd;
                  } else if (this.hasOwnProperty("oldValue")) {
                      this.value = this.oldValue;
                      this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                  } else {
                      this.value = "";
                  }
              });
          };
      }(jQuery));

      $(".input-filter").inputFilter(function (value) {
          return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 90000000);
      });
  </script>
@endsection
@section('title') @if(env('APP_SITE_REGION') == '')Подписка на поступление автомобилей в автосалон Автодромиум@endif
@if(env('APP_SITE_REGION') == 66)Подписка на поступление автомобилей в автосалон Автодромиум в Екатеринбурге @endif
@if(env('APP_SITE_REGION') == 63)Узнай о поступлении автомобилей с пробегом в автосалон Автодромиум в Самаре @endif
@if(env('APP_SITE_REGION') == 52)Узнай о поступлении автомобилей с пробегом в автосалон Автодромиум в Нижнем Новгороде @endif
@endsection

@section('meta_description')
  @if(env('APP_SITE_REGION') == 63)
      <meta name="description"
            content="Узнай первым о новых поступлениях в автосалон Автодромиум в Самаре заполнив форму подписки выбрав параметры автомобиля. Автомобили всех видов от бюджетного до премиум класса.">
  @endif
@endsection--}}
