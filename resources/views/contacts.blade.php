@extends('layouts.auto')
@section('_formContacts')
    @if(env('APP_SITE_REGION') != '')
        <!-- feedback-->
        <section class="feedback">
            <h3 class="feedback__title">Форма обратной связи</h3>
            <form action="/contacts/request-add" method="post" id="contact-form"
                @csrf
                <div class="form-group">
                    <input type="text" name="name" placeholder="Ваше имя" required autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="phone" type="tel" name="phone" required autocomplete="off">
                </div>
                <button class="feedback__btn" type="submit">Отправить</button>
                <div class="mt-2">
                    <small>Нажимая на кнопку, вы принимаете <a href="{{ route('privacy') }}" target="_blank" class="text--black">Согласие</a> на обработку персональных данных</small>
                </div>
            </form>
        </section>
        <!-- end feedback-->
    @endif
@endsection

@section('content')
<!-- page-->
<section class="page" id="js-page">
    <div class="container">
        <h2 class="section-title">Контакты<span class="grey"><span class="blue"></span></span></h2>


        <div class="page__row contact__row">
            <div class="page__left">
                <!-- map-->
                <div class="map">
                    @if(setting('yandex__map'))
                        <div style="position:relative;overflow:hidden;height: 100%;">
                            <iframe src="{{ str_replace('https://yandex.ru/maps/', 'https://yandex.ru/map-widget/v1/', setting('yandex__map')) }}"
                                    frameborder="0"
                                    allowfullscreen="true"
                                    style="position:relative; width: 100%; height: 100%;"
                            ></iframe>
                        </div>
                    @endif
                </div>
                <!-- end map-->
            </div>
            <div class="page__right">
                <!-- address-->
                <section class="address">
                    <h3 class="address__title">Адрес автоцентра:</h3>
                    <address><span style="font-family: Arial; font-size: 14.6667px; white-space: pre-wrap;">@setting('address')</span></address>
                    <p class="address__time">@setting('schedule')</p>
                </section>
                <!-- end address-->
                <!-- contacts-->
                <section class="contacts">
                    <h3 class="contacts__title">Наши контакты:</h3>
                    <p>Телефон:&nbsp;{{ showPhone() }}</p>
                    <p>E-mail:&nbsp;<span style="background-color: transparent; font-size: 11pt; white-space: pre-wrap; font-family: Arial;">{{ setting('email') }}</span></p>
                </section>
                <!-- end contacts-->

                <div style="margin-top: 20px;">
                    @yield('_formContacts')
                </div>
            </div>
        </div>


        @if(setting('page__contact_subscribe_show'))
            @include('subscribes.index', [
                'brands' => $brands,
            ])
        @endif
    </div>
</section>
<!-- end page-->
@endsection

@section('title'){{ setTemplateVars(['title', 'credit_percent', 'phone' => showPhone(), 'email', 'address', 'schedule'], setting('page__contact_title') ?: setting('title')) }}@endsection
@section('meta_description'){{ setTemplateVars(['title', 'credit_percent', 'phone' => showPhone(), 'email', 'address', 'schedule'], setting('page__contact_description') ?: setting('page__description')) }}@endsection

@section('javascript')
<script>
    $(document).on('submit', '#contact-form', function (e) {
        var form = $(this);

        $.ajax({
            beforeSend: function (xhr, opts) {
                beforeSendMakes(form, xhr);
            },
            complete: function () {
                $('.feedback__btn').removeClass('progress-bar-striped');
                $('.feedback__btn').removeClass('progress-bar-animated');
                $(form).trigger("reset");
            },
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            success: function (data) {

				if(data == 0){
					$('form#contact-form input.phone').css({border: '1px solid red'})
				}else{
					$('[data-fancybox-close]').click();
					$('body').addClass('thanksPopupBody');
					$('.thanks_modal').addClass('active');
				}


            },
            error: function (error) {
                console.log(error);
            }
        });
        e.preventDefault();
    });

    (function ($) {
        $.fn.inputFilter = function (inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    $(".input-filter").inputFilter(function (value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 90000000);
    });
</script>
@endsection
