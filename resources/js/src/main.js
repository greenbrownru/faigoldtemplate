var filter = new Filter();

$(document).ready(function(){

	if($('div').is('#main_filter')){
		filter.init();
	}

	if($('div').is('#page_about')){
		$("#about_slider").slick({
			// normal options...
			infinite: true,
			arrows: true,
			dots: true,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			autoplay: true
		});

		$('a.copy_email').click(function(){
			navigator.clipboard.writeText($(this).data('email'))
			.then(() => {
				alert('Наш email в буфере обмена');
			});
		})
	}
})
