function Filter()
{
	this.selects = null;
	this.get_models_url = '/credit/ajax-models-filter';
	this.headers = {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')};

	this.show_select_dropdown = function(select){
		$(select).addClass('showed');
		$(select).find('input').focus().attr('placeholder', '');
	}

	this.hide_select_dropdown = function(select){
		$(select).removeClass('showed');
		let input = $(select).find('.current_value input');
		input.blur().attr('placeholder', input.attr('data-placeholder'));
	}

	this.element_disable = function(element){
		$(element).addClass('disabled');
		$(element).find('.current_value input').attr('disabled', true);
		$(element).unbind('click');
		$(element).find('input').attr('disabled', true);
	}

	this.element_enable = function(element){
		var _class = this;
		$(element).removeClass('disabled');
		$(element).find('input').removeAttr('disabled');
		$(element).bind( "click", function() {
			_class.show_select_dropdown(this);
		});
		$(element).find('input').removeAttr('disabled');
	}

	this.select_search = function(select, str){
		var list = $(select).find('.variants li').not('.group_name, .divider');
		list.each(function(i, item){
			$(item).show();
			if($(item).text().toLowerCase().indexOf(str.toLowerCase()) === -1){
				$(item).hide();
			}
		})
	};

	this.select_variant = function(select, variant){
		$(select).find('.variants li').removeClass('checked');
		$(select).find(variant).addClass('checked');
		$(select).find('.current_value input').val(variant.text());
	}

	this.select_get_models = function(brand_id){
		return new Promise((resolve, reject) => {
			var _class = this;
			$.ajax({
				url: _class.get_models_url,
				method: 'POST',
				data: {
					brand_id: brand_id
				},
				headers: _class.headers,
				success: function(data){
					resolve(JSON.parse(data));
				}
			})
		})
	}

	this.fill_select = function(select, data){
		$(select).find('input').val('');

		let variants_container = $(select).find('.variants ul');
		let variant_blank = $(variants_container).find('li.blank');
		var _class = this;

		$(variants_container).find('li:not(.blank)').remove();

		$(data).each(function(i, item){
			let cloned = $(variant_blank).clone()
			cloned.removeClass('blank').attr('data-id', item.id).text(item.name).appendTo($(variants_container));
			cloned.bind('click', function(){
				_class.select_variant(select, cloned);
				_class.hide_select_dropdown(select);
			})

		})
	}

	this.init = function()
	{
		var _class = this;

		this.selects = $('#main_filter').find('.select');
		this.selects.each(function(i, select){
			if(!$(select).is('.disabled')){
				$(select).click(function(){
					_class.show_select_dropdown(this);
				})

				$(select).find('.current_value input').keyup(function(e){
					_class.select_search(select, $(this).val());
				})

				$(select).find('.variants li').on('click', async function(e){
					_class.select_variant(select, $(this));
					_class.hide_select_dropdown(select);

					switch ($(select).attr('id')){
						case 'brands':
							let models = await _class.select_get_models($(this).data('id'));
							let models_select = $('.select#models');
							_class.fill_select(models_select, models);
							_class.element_enable(models_select);
							break;
					}
				})
			}else{
				$(select).find('input').attr('disabled', true);
			}
		})

		$(document).mouseup(function (e) {
			_class.selects.each(function(i, select){
				if ($(select).has(e.target).length === 0){
					_class.hide_select_dropdown(select);
				}
			})
		});
	}
}