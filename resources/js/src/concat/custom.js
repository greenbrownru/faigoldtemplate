function Filter()
{
	this.selects = null;
	this.get_models_url = '/credit/ajax-models-filter';
	this.headers = {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')};

	this.show_select_dropdown = function(select){
		$(select).addClass('showed');
		$(select).find('input').focus().attr('placeholder', '');
	}

	this.hide_select_dropdown = function(select){
		$(select).removeClass('showed');
		let input = $(select).find('.current_value input');
		input.blur().attr('placeholder', input.attr('data-placeholder'));
	}

	this.element_disable = function(element){
		$(element).addClass('disabled');
		$(element).find('.current_value input').attr('disabled', true);
		$(element).unbind('click');
		$(element).find('input').attr('disabled', true);
	}

	this.element_enable = function(element){
		var _class = this;
		$(element).removeClass('disabled');
		$(element).find('input').removeAttr('disabled');
		$(element).bind( "click", function() {
			_class.show_select_dropdown(this);
		});
		$(element).find('input').removeAttr('disabled');
	}

	this.select_search = function(select, str){
		var list = $(select).find('.variants li').not('.group_name, .divider');
		list.each(function(i, item){
			$(item).show();
			if($(item).text().toLowerCase().indexOf(str.toLowerCase()) === -1){
				$(item).hide();
			}
		})
	};

	this.select_variant = function(select, variant){
		$(select).find('.variants li').removeClass('checked');
		$(select).find(variant).addClass('checked');
		$(select).find('.current_value input').val(variant.text());
	}

	this.select_get_models = function(brand_id){
		return new Promise((resolve, reject) => {
			var _class = this;
			$.ajax({
				url: _class.get_models_url,
				method: 'POST',
				data: {
					brand_id: brand_id
				},
				headers: _class.headers,
				success: function(data){
					resolve(JSON.parse(data));
				}
			})
		})
	}

	this.fill_select = function(select, data){
		$(select).find('input').val('');

		let variants_container = $(select).find('.variants ul');
		let variant_blank = $(variants_container).find('li.blank');
		var _class = this;

		$(variants_container).find('li:not(.blank)').remove();

		$(data).each(function(i, item){
			let cloned = $(variant_blank).clone()
			cloned.removeClass('blank').attr('data-id', item.id).text(item.name).appendTo($(variants_container));
			cloned.bind('click', function(){
				_class.select_variant(select, cloned);
				_class.hide_select_dropdown(select);
			})

		})
	}

	this.init = function()
	{
		var _class = this;

		this.selects = $('#main_filter').find('.select');
		this.selects.each(function(i, select){
			if(!$(select).is('.disabled')){
				$(select).click(function(){
					_class.show_select_dropdown(this);
				})

				$(select).find('.current_value input').keyup(function(e){
					_class.select_search(select, $(this).val());
				})

				$(select).find('.variants li').on('click', async function(e){
					_class.select_variant(select, $(this));
					_class.hide_select_dropdown(select);

					switch ($(select).attr('id')){
						case 'brands':
							let models = await _class.select_get_models($(this).data('id'));
							let models_select = $('.select#models');
							_class.fill_select(models_select, models);
							_class.element_enable(models_select);
							break;
					}
				})
			}else{
				$(select).find('input').attr('disabled', true);
			}
		})

		$(document).mouseup(function (e) {
			_class.selects.each(function(i, select){
				if ($(select).has(e.target).length === 0){
					_class.hide_select_dropdown(select);
				}
			})
		});
	}
}
var filter = new Filter();

$(document).ready(function(){

	if($('div').is('#main_filter')){
		filter.init();
	}

	if($('div').is('#page_about')){
		$("#about_slider").slick({
			// normal options...
			infinite: true,
			arrows: true,
			dots: true,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true,
			autoplay: true
		});

		$('a.copy_email').click(function(){
			navigator.clipboard.writeText($(this).data('email'))
			.then(() => {
				alert('Наш email в буфере обмена');
			});
		})
	}
})

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlci5qcyIsIm1haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoicmVzb3VyY2VzL2pzL3NyYy9jb25jYXQvY3VzdG9tLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gRmlsdGVyKClcclxue1xyXG5cdHRoaXMuc2VsZWN0cyA9IG51bGw7XHJcblx0dGhpcy5nZXRfbW9kZWxzX3VybCA9ICcvY3JlZGl0L2FqYXgtbW9kZWxzLWZpbHRlcic7XHJcblx0dGhpcy5oZWFkZXJzID0geydYLUNTUkYtVE9LRU4nOiAkKCdtZXRhW25hbWU9XCJjc3JmLXRva2VuXCJdJykuYXR0cignY29udGVudCcpfTtcclxuXHJcblx0dGhpcy5zaG93X3NlbGVjdF9kcm9wZG93biA9IGZ1bmN0aW9uKHNlbGVjdCl7XHJcblx0XHQkKHNlbGVjdCkuYWRkQ2xhc3MoJ3Nob3dlZCcpO1xyXG5cdFx0JChzZWxlY3QpLmZpbmQoJ2lucHV0JykuZm9jdXMoKS5hdHRyKCdwbGFjZWhvbGRlcicsICcnKTtcclxuXHR9XHJcblxyXG5cdHRoaXMuaGlkZV9zZWxlY3RfZHJvcGRvd24gPSBmdW5jdGlvbihzZWxlY3Qpe1xyXG5cdFx0JChzZWxlY3QpLnJlbW92ZUNsYXNzKCdzaG93ZWQnKTtcclxuXHRcdGxldCBpbnB1dCA9ICQoc2VsZWN0KS5maW5kKCcuY3VycmVudF92YWx1ZSBpbnB1dCcpO1xyXG5cdFx0aW5wdXQuYmx1cigpLmF0dHIoJ3BsYWNlaG9sZGVyJywgaW5wdXQuYXR0cignZGF0YS1wbGFjZWhvbGRlcicpKTtcclxuXHR9XHJcblxyXG5cdHRoaXMuZWxlbWVudF9kaXNhYmxlID0gZnVuY3Rpb24oZWxlbWVudCl7XHJcblx0XHQkKGVsZW1lbnQpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xyXG5cdFx0JChlbGVtZW50KS5maW5kKCcuY3VycmVudF92YWx1ZSBpbnB1dCcpLmF0dHIoJ2Rpc2FibGVkJywgdHJ1ZSk7XHJcblx0XHQkKGVsZW1lbnQpLnVuYmluZCgnY2xpY2snKTtcclxuXHRcdCQoZWxlbWVudCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0dGhpcy5lbGVtZW50X2VuYWJsZSA9IGZ1bmN0aW9uKGVsZW1lbnQpe1xyXG5cdFx0dmFyIF9jbGFzcyA9IHRoaXM7XHJcblx0XHQkKGVsZW1lbnQpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xyXG5cdFx0JChlbGVtZW50KS5maW5kKCdpbnB1dCcpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XHJcblx0XHQkKGVsZW1lbnQpLmJpbmQoIFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XHJcblx0XHRcdF9jbGFzcy5zaG93X3NlbGVjdF9kcm9wZG93bih0aGlzKTtcclxuXHRcdH0pO1xyXG5cdFx0JChlbGVtZW50KS5maW5kKCdpbnB1dCcpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XHJcblx0fVxyXG5cclxuXHR0aGlzLnNlbGVjdF9zZWFyY2ggPSBmdW5jdGlvbihzZWxlY3QsIHN0cil7XHJcblx0XHR2YXIgbGlzdCA9ICQoc2VsZWN0KS5maW5kKCcudmFyaWFudHMgbGknKS5ub3QoJy5ncm91cF9uYW1lLCAuZGl2aWRlcicpO1xyXG5cdFx0bGlzdC5lYWNoKGZ1bmN0aW9uKGksIGl0ZW0pe1xyXG5cdFx0XHQkKGl0ZW0pLnNob3coKTtcclxuXHRcdFx0aWYoJChpdGVtKS50ZXh0KCkudG9Mb3dlckNhc2UoKS5pbmRleE9mKHN0ci50b0xvd2VyQ2FzZSgpKSA9PT0gLTEpe1xyXG5cdFx0XHRcdCQoaXRlbSkuaGlkZSgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KVxyXG5cdH07XHJcblxyXG5cdHRoaXMuc2VsZWN0X3ZhcmlhbnQgPSBmdW5jdGlvbihzZWxlY3QsIHZhcmlhbnQpe1xyXG5cdFx0JChzZWxlY3QpLmZpbmQoJy52YXJpYW50cyBsaScpLnJlbW92ZUNsYXNzKCdjaGVja2VkJyk7XHJcblx0XHQkKHNlbGVjdCkuZmluZCh2YXJpYW50KS5hZGRDbGFzcygnY2hlY2tlZCcpO1xyXG5cdFx0JChzZWxlY3QpLmZpbmQoJy5jdXJyZW50X3ZhbHVlIGlucHV0JykudmFsKHZhcmlhbnQudGV4dCgpKTtcclxuXHR9XHJcblxyXG5cdHRoaXMuc2VsZWN0X2dldF9tb2RlbHMgPSBmdW5jdGlvbihicmFuZF9pZCl7XHJcblx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cdFx0XHR2YXIgX2NsYXNzID0gdGhpcztcclxuXHRcdFx0JC5hamF4KHtcclxuXHRcdFx0XHR1cmw6IF9jbGFzcy5nZXRfbW9kZWxzX3VybCxcclxuXHRcdFx0XHRtZXRob2Q6ICdQT1NUJyxcclxuXHRcdFx0XHRkYXRhOiB7XHJcblx0XHRcdFx0XHRicmFuZF9pZDogYnJhbmRfaWRcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdGhlYWRlcnM6IF9jbGFzcy5oZWFkZXJzLFxyXG5cdFx0XHRcdHN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpe1xyXG5cdFx0XHRcdFx0cmVzb2x2ZShKU09OLnBhcnNlKGRhdGEpKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0dGhpcy5maWxsX3NlbGVjdCA9IGZ1bmN0aW9uKHNlbGVjdCwgZGF0YSl7XHJcblx0XHQkKHNlbGVjdCkuZmluZCgnaW5wdXQnKS52YWwoJycpO1xyXG5cclxuXHRcdGxldCB2YXJpYW50c19jb250YWluZXIgPSAkKHNlbGVjdCkuZmluZCgnLnZhcmlhbnRzIHVsJyk7XHJcblx0XHRsZXQgdmFyaWFudF9ibGFuayA9ICQodmFyaWFudHNfY29udGFpbmVyKS5maW5kKCdsaS5ibGFuaycpO1xyXG5cdFx0dmFyIF9jbGFzcyA9IHRoaXM7XHJcblxyXG5cdFx0JCh2YXJpYW50c19jb250YWluZXIpLmZpbmQoJ2xpOm5vdCguYmxhbmspJykucmVtb3ZlKCk7XHJcblxyXG5cdFx0JChkYXRhKS5lYWNoKGZ1bmN0aW9uKGksIGl0ZW0pe1xyXG5cdFx0XHRsZXQgY2xvbmVkID0gJCh2YXJpYW50X2JsYW5rKS5jbG9uZSgpXHJcblx0XHRcdGNsb25lZC5yZW1vdmVDbGFzcygnYmxhbmsnKS5hdHRyKCdkYXRhLWlkJywgaXRlbS5pZCkudGV4dChpdGVtLm5hbWUpLmFwcGVuZFRvKCQodmFyaWFudHNfY29udGFpbmVyKSk7XHJcblx0XHRcdGNsb25lZC5iaW5kKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcblx0XHRcdFx0X2NsYXNzLnNlbGVjdF92YXJpYW50KHNlbGVjdCwgY2xvbmVkKTtcclxuXHRcdFx0XHRfY2xhc3MuaGlkZV9zZWxlY3RfZHJvcGRvd24oc2VsZWN0KTtcclxuXHRcdFx0fSlcclxuXHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0dGhpcy5pbml0ID0gZnVuY3Rpb24oKVxyXG5cdHtcclxuXHRcdHZhciBfY2xhc3MgPSB0aGlzO1xyXG5cclxuXHRcdHRoaXMuc2VsZWN0cyA9ICQoJyNtYWluX2ZpbHRlcicpLmZpbmQoJy5zZWxlY3QnKTtcclxuXHRcdHRoaXMuc2VsZWN0cy5lYWNoKGZ1bmN0aW9uKGksIHNlbGVjdCl7XHJcblx0XHRcdGlmKCEkKHNlbGVjdCkuaXMoJy5kaXNhYmxlZCcpKXtcclxuXHRcdFx0XHQkKHNlbGVjdCkuY2xpY2soZnVuY3Rpb24oKXtcclxuXHRcdFx0XHRcdF9jbGFzcy5zaG93X3NlbGVjdF9kcm9wZG93bih0aGlzKTtcclxuXHRcdFx0XHR9KVxyXG5cclxuXHRcdFx0XHQkKHNlbGVjdCkuZmluZCgnLmN1cnJlbnRfdmFsdWUgaW5wdXQnKS5rZXl1cChmdW5jdGlvbihlKXtcclxuXHRcdFx0XHRcdF9jbGFzcy5zZWxlY3Rfc2VhcmNoKHNlbGVjdCwgJCh0aGlzKS52YWwoKSk7XHJcblx0XHRcdFx0fSlcclxuXHJcblx0XHRcdFx0JChzZWxlY3QpLmZpbmQoJy52YXJpYW50cyBsaScpLm9uKCdjbGljaycsIGFzeW5jIGZ1bmN0aW9uKGUpe1xyXG5cdFx0XHRcdFx0X2NsYXNzLnNlbGVjdF92YXJpYW50KHNlbGVjdCwgJCh0aGlzKSk7XHJcblx0XHRcdFx0XHRfY2xhc3MuaGlkZV9zZWxlY3RfZHJvcGRvd24oc2VsZWN0KTtcclxuXHJcblx0XHRcdFx0XHRzd2l0Y2ggKCQoc2VsZWN0KS5hdHRyKCdpZCcpKXtcclxuXHRcdFx0XHRcdFx0Y2FzZSAnYnJhbmRzJzpcclxuXHRcdFx0XHRcdFx0XHRsZXQgbW9kZWxzID0gYXdhaXQgX2NsYXNzLnNlbGVjdF9nZXRfbW9kZWxzKCQodGhpcykuZGF0YSgnaWQnKSk7XHJcblx0XHRcdFx0XHRcdFx0bGV0IG1vZGVsc19zZWxlY3QgPSAkKCcuc2VsZWN0I21vZGVscycpO1xyXG5cdFx0XHRcdFx0XHRcdF9jbGFzcy5maWxsX3NlbGVjdChtb2RlbHNfc2VsZWN0LCBtb2RlbHMpO1xyXG5cdFx0XHRcdFx0XHRcdF9jbGFzcy5lbGVtZW50X2VuYWJsZShtb2RlbHNfc2VsZWN0KTtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9ZWxzZXtcclxuXHRcdFx0XHQkKHNlbGVjdCkuZmluZCgnaW5wdXQnKS5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xyXG5cdFx0XHR9XHJcblx0XHR9KVxyXG5cclxuXHRcdCQoZG9jdW1lbnQpLm1vdXNldXAoZnVuY3Rpb24gKGUpIHtcclxuXHRcdFx0X2NsYXNzLnNlbGVjdHMuZWFjaChmdW5jdGlvbihpLCBzZWxlY3Qpe1xyXG5cdFx0XHRcdGlmICgkKHNlbGVjdCkuaGFzKGUudGFyZ2V0KS5sZW5ndGggPT09IDApe1xyXG5cdFx0XHRcdFx0X2NsYXNzLmhpZGVfc2VsZWN0X2Ryb3Bkb3duKHNlbGVjdCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0fSk7XHJcblx0fVxyXG59IiwidmFyIGZpbHRlciA9IG5ldyBGaWx0ZXIoKTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcblxuXHRpZigkKCdkaXYnKS5pcygnI21haW5fZmlsdGVyJykpe1xuXHRcdGZpbHRlci5pbml0KCk7XG5cdH1cblxuXHRpZigkKCdkaXYnKS5pcygnI3BhZ2VfYWJvdXQnKSl7XG5cdFx0JChcIiNhYm91dF9zbGlkZXJcIikuc2xpY2soe1xuXHRcdFx0Ly8gbm9ybWFsIG9wdGlvbnMuLi5cblx0XHRcdGluZmluaXRlOiB0cnVlLFxuXHRcdFx0YXJyb3dzOiB0cnVlLFxuXHRcdFx0ZG90czogdHJ1ZSxcblx0XHRcdHNsaWRlc1RvU2hvdzogMSxcblx0XHRcdGNlbnRlck1vZGU6IHRydWUsXG5cdFx0XHR2YXJpYWJsZVdpZHRoOiB0cnVlLFxuXHRcdFx0YXV0b3BsYXk6IHRydWVcblx0XHR9KTtcblxuXHRcdCQoJ2EuY29weV9lbWFpbCcpLmNsaWNrKGZ1bmN0aW9uKCl7XG5cdFx0XHRuYXZpZ2F0b3IuY2xpcGJvYXJkLndyaXRlVGV4dCgkKHRoaXMpLmRhdGEoJ2VtYWlsJykpXG5cdFx0XHQudGhlbigoKSA9PiB7XG5cdFx0XHRcdGFsZXJ0KCfQndCw0YggZW1haWwg0LIg0LHRg9GE0LXRgNC1INC+0LHQvNC10L3QsCcpO1xuXHRcdFx0fSk7XG5cdFx0fSlcblx0fVxufSlcbiJdfQ==
