<?php


namespace App\Imports;


use App\Jobs\ImportPhoto;
use App\Mail\AdvertsImportXml as MailAdvertsImportXml;
use App\Models\Advert;
use App\Models\AdvertEquipments;
use App\Models\AdvertPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Matrix\Exception;
use XMLReader;

class AdvertsImportXml
{
    /**
     * @var $request Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function store()
    {
        $mailSuccess = false;

        $file = $this->request->filename;
        $xml = new XMLReader();
        $xml->open($file);

        $specs = Advert::query()->select('client_id')->spec()->current()->get();
        $vips = Advert::query()->select('client_id')->vip()->current()->get();
        $importId = date('YmdHi');

        $importsCount = Advert::query()->selectRaw('count(distinct import_id) as cnt')->first()->cnt;

        if($importsCount >= 5) {
            $minImport = Advert::query()->selectRaw('min(import_id) as min_import')->first()->min_import;
            $minAdverts = Advert::query()->where(['import_id' => $minImport])->get();
            foreach ($minAdverts as $minAdvert) {
                $minPhotos = $minAdvert->photos;
                if($minPhotos) {
                    foreach ($minPhotos as $minPhoto) {
                        $filepath = public_path().'/storage/photos/'.$minPhoto->filename;
                        $fileResizePath = public_path().'/storage/photos/resize/'.$minPhoto->filename;
                        if(file_exists($filepath)) {
                            unlink($filepath);
                        }
                        if(file_exists($fileResizePath)) {
                            unlink($fileResizePath);
                        }
                    }
                }
                $minAdvert->photos()->delete();
                $minAdvert->equipments()->delete();
            }
            Advert::query()->where(['import_id' => $minImport])->delete();
        }

        Advert::query()->where(['is_current' => true])->update(['is_current' => false, 'is_active' => false]);

        while ($xml->read() && ($xml->name != 'vehicle')) {
            ;
        }

        $i = 0;

        while ($xml->name == 'vehicle') {
            $item = new \SimpleXMLElement($xml->readOuterXML());
            $equipments = [];
            $photos = [];

            $data = [
                'client_id' => (integer) $item->id,
                'brand_id' => (integer)$item->brand->attributes()->id,
                'model_id' => (integer)$item->model->attributes()->id,
                'generation_id' => (integer)$item->generation->attributes()->id,
                'modification_id' => (integer)$item->modification->attributes()->id,
                'modification_name' => (string)$item->modification,
                'year_of_issue' => (integer)$item->year,
                'mileage' => (string)$item->mileage,
                'car_body' => (string)$item->bodyType,
                'transmission' => (string)$item->gearboxType,
                'engine' => (string)$item->engineType,
                'steering_wheel' => (string)$item->steeringWheel,
                'color' => (string)$item->bodyColor,
                'car_drive' => (string)$item->driveType,
                'power' => (integer)$item->enginePower,
                'volume' => (float)$item->engineVolume,
                'description' => (string)$item->description,
                'number_of_owners' => (integer)$item->ownersCount,
                'vin' => (string)$item->vin,
                'price' => (double)$item->price,
                'price_by_action' => doubleval($item->price) - doubleval($item->tradeinDiscount) - doubleval($item->creditDiscount) - doubleval($item->insuranceDiscount),
                'tradein_discount' => doubleval($item->tradeinDiscount),
                'credit_discount' => doubleval($item->creditDiscount),
                'insurance_discount' => doubleval($item->insuranceDiscount),
                'vin_verified' => true,
                'import_id' => $importId,
                'is_current' => true,
                'region_id' => env('APP_SITE_REGION') !== '' ? intval(env('APP_SITE_REGION')) : null,
            ];

            /**
             * @var $advert Advert
             */
            $advert = Advert::where($data)->first() ?: Advert::create($data);
            if ($advert) {
                if (is_object($item->equipment)) {
                    foreach ($item->equipment as $key => $val) {
                        foreach ($val->group as $eq) {
                            foreach ($eq->element as $el) {
                                $equipments[] = ['equipment_id' => (integer) $el->attributes()->id, 'region_id' => (env('APP_SITE_REGION') ?: null), 'advert_id' => $advert->id];
                            }
                        }
                    }
                }

                if (! empty($equipments)) {
                    try {
                        $advert->equipments()->insert($equipments);
                    } catch (\Exception $exception) {
                        dd($exception->getMessage());
                    }
                }

                $order_photo = 1;
                if (is_object($item->photos)) {
                    foreach ($item->photos as $photoObj) {
                        foreach ((array) $photoObj->photo as $photo) {
                            ImportPhoto::dispatch($advert, $photo, $order_photo)->onQueue('import' . env('APP_SITE_REGION'));
                            $order_photo++;
                        }
                    }
                }

                $mailSuccess = true;
            }


            $i++;
            unset($item);
            $xml->next($xml->name);
        }

        if($vips) {
            foreach ($vips as $vip) {
                if($vip->client_id != null) {
                    Advert::query()->where(['client_id' => $vip->client_id])->current()->update(['is_vip' => true]);
                }
            }
        }

        if($specs) {
            foreach ($specs as $spec) {
                if($spec->client_id != null) {
                    Advert::query()->where(['client_id' => $spec->client_id])->current()->update(['is_spec' => true]);
                }
            }
        }



        if(env('SITE_SUFFIX') == 'armavir') {
            Mail::to('info@greenbrown.ru')->send(new MailAdvertsImportXml($mailSuccess));
        }

        Cache::flush();
    }
}
