<?php


namespace App\Imports;


use App\Jobs\ImportPhoto;
use App\Models\Advert;
use App\Models\AdvertEquipments;
use App\Models\AdvertPhoto;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\Equipment;
use App\Models\EquipmentGroup;
use App\Models\Generation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Concerns\FromCollection;
use Matrix\Exception;
use XMLReader;

class AdvertsImportRolfCsV
{
    /**
     * @var $request Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function store()
    {
        $file = $this->request->file('filename');

        $i = 0;

        $import_id = strtotime('Now');
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($item = fgetcsv($handle, 10000, ",")) !== FALSE) {

                if($item[0] == 'ŠKODA') {
                    $item[0] = 'Skoda';
                    $item[1] = str_replace('ŠKODA', 'Skoda', $item[1]);
                }

                if(!isset($item[4]) || !isset($item[0]) || !isset($item[1])) {
                    continue;
                }

                if(isset($item[5])) {
                    $opts = explode(';', $item[5]);

                    $n = 0;
                    $opts_group =[];
                    $opts_props = [];
                    foreach ($opts as $opt) {
                        $opt_raw = explode('|', $opt);
                        $opts_group[$n] = trim($opt_raw[0]);
                        if(isset($opt_raw[1])) {
                            $opts_props[$n] = explode("\n", $opt_raw[1]);
                        }

                        $n++;
                    }

                    $m = 0;
                    foreach ($opts_group as $key => $value) {
                        $eg = EquipmentGroup::firstOrCreate(
                            ['name' =>  $value],
                            ['name' => $value, 'sort_order' => $m]
                        );

                        if(isset($opts_props[$key])) {
                            foreach ($opts_props[$key] as $opts_prop) {
                                Equipment::firstOrCreate(
                                    ['name' =>  $opts_prop, 'group_id' => $eg->id],
                                    ['name' => $opts_prop, 'group_id' => $eg->id]
                                );
                            }
                        }

                        $m++;
                    }
                }

                $photos = [];

                if(! empty($item[3])) {
                    $photos = explode(';', $item[3]);
                    if(! $photos[0]) {
                        $photos = [];
                    }
                }

                $props = preg_replace('/Гарантия\|до (\d+.\d+);/', '', $item[4]);
                $props = explode(';', $props);
                $brand = Brand::where('name', 'ilike', trim($item[0]))->first();
                if(!$brand) {
                    continue;
                }

                if($item[1] == 'Toyota RAV 4') {
                    $item[1] = 'Toyota RAV4';
                }

                $model = AModel::where('name', 'ilike', trim(str_replace($item[0], '', $item[1])))
                    ->where(['brand_id' => $brand->id])
                    ->first();

                if(!$model) {
                    continue;
                }

                if(!isset($props[1]) || !isset($props[2]) || !isset($props[3]) || !isset($props[4]) || !isset($props[5]) || !isset($props[7]) || !isset($props[8])) {
                    continue;
                }
                $props1 = explode('|', $props[1]);
                $year_of_issue = intval(str_replace(' ', '', $props1[1] ?? ''));
                $mileage = intval(str_replace(' ', '', explode('|', $props[2])[1]));
                $number_of_owners = intval(str_replace(' ', '', explode('|', $props[0])[1]));
                $color = isset($props[8]) ? explode('|', $props[8]) : '';
                $transmission = explode('|', $props[4]);
                $car_body = isset($props[7]) ? explode('|', $props[7]) : '';
                $car_drive = explode('|', $props[5]);
                $volume = floatval(explode('|', explode('/', $props[3])[0])[1]);
                $power = explode('/', $props[3]);
                $power = isset($power[1]) ? intval($power[1]) : 0;
                $steering_wheel = trim(explode('|', $props[6])[1]);
                $engine = explode('/', $props[3]);
                $engine = isset($engine[2]) ? trim($engine[2]) : 0;
                $price = doubleval(str_replace(' ', '', $item[2]));
                $volume = intval($volume*1000);

                // Параметры для тестового сервера
                if ($year_of_issue < 2006 || !$photos || $price < 300000) {
                    continue;
                }
                // Конец параметры

                $generation = Generation::query()
                    ->where(['brand_id' => $brand->id, 'model_id' => $model->id])
                    ->whereRaw("{$year_of_issue} between year_from and year_to")
                    ->first();

                $data = [
                    'client_id' => strtotime('now').$i,
                    'brand_id' => $brand->id,
                    'model_id' => $model->id,
                    'generation_id' => $generation ? $generation->id : null,
                    'modification_id' => null,
                    'modification_name' => null,
                    'year_of_issue' => $year_of_issue,
                    'mileage' => $mileage,
                    'car_body' => isset($car_body[1]) ? $car_body[1] : '',
                    'transmission' => isset($transmission[1]) ? $transmission[1] : '',
                    'engine' => $engine,
                    'steering_wheel' => $steering_wheel,
                    'color' => isset($color[1]) ? $color[1] : '',
                    'car_drive' => isset($car_drive[1]) ? $car_drive[1] : '',
                    'power' => $power,
                    'volume' => $volume,
                    'description' => null,
                    'number_of_owners' => $number_of_owners,
                    'vin' => null,
                    'price' => $price,
                    'price_by_action' => $price,
                    'tradein_discount' => 0,
                    'credit_discount' => 0,
                    'insurance_discount' => 0,
                    'vin_verified' => true,
                    'import_id' => $import_id,
                    'is_current' => true,
                    'region_id' => env('APP_SITE_REGION') !== '' ? intval(env('APP_SITE_REGION')) : null,
                ];

                /**
                 * @var $advert Advert
                 */
                $advert = Advert::create($data);

                if ($advert) {
                    $order_photo = 1;
                    if (!empty($photos)) {
                        //$photos = array_slice($photos, 0, 2);
                        foreach ($photos as $photo) {
                            ImportPhoto::dispatch($advert, $photo, $order_photo)->onQueue('import' . env('APP_SITE_REGION'));
                            $order_photo++;
                        }
                    }
                }

                if(!empty($opts_props)) {
                    foreach ($opts_props as $opts) {
                        foreach ($opts as $opt) {
                            $equipment =  Equipment::query()->where('name', 'ilike', $opt)->first();
                            if($equipment) {
                                AdvertEquipments::query()->create(['equipment_id' => $equipment->id, 'advert_id' => $advert->id, 'text' => $opt, 'is_autoru' => false]);
                            }
                        }
                    }
                }
                $i++;
            }
            Cache::flush();
        }
    }
}
