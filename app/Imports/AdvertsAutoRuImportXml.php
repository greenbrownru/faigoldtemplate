<?php


namespace App\Imports;


use App\Jobs\ImportPhoto;
use App\Models\Advert;
use App\Models\AdvertEquipments;
use App\Models\AdvertPhoto;
use App\Models\AModel;
use App\Models\Generation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Matrix\Exception;
use XMLReader;
use function GuzzleHttp\Psr7\str;

class AdvertsAutoRuImportXml
{

    /**
     * @var $request Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function store()
    {
        $file = $this->request->filename;
        $xml = new XMLReader();
        $xml->open($file);

        $brands = DB::table('brands')
            ->select(['id', 'name'])
            ->get()
            ->toArray();

        $models = DB::table('models')
            ->select(['brand_id', 'name', 'id'])
            ->get()
            ->toArray();

        $generations = DB::table('generations')
            ->select(['id', 'name', 'model_id'])
            ->get()
            ->toArray();

        $brands_array = [];
        $brands_for_insert = [];

        $models_array = [];
        $models_for_insert = [];

        $generations_array = [];
        $generations_for_insert = [];

        foreach ($brands as $brand) {
            $brands_array[$brand->name] = $brand->id;
        }

        foreach ($models as $model) {
            $models_array[$model->name] = $model->id;
        }

        $specs = Advert::query()->select('client_id')->spec()->get();
        $vips = Advert::query()->select('client_id')->vip()->get();

        Advert::query()->delete();
        AdvertPhoto::query()->delete();
        AdvertEquipments::query()->delete();

        while ($xml->read() && ($xml->name != 'offer')) {
            ;
        }

        $i = 0;

        while ($xml->name == 'offer') {
            $item = new \SimpleXMLElement($xml->readOuterXML());
            $equipments = [];

            $mark = (string)$item->mark;

            if($mark == 'LADA (ВАЗ)') {
                $mark = 'LADA';
            }

            //dd(substr((string)$item->model, 0, strpos((string)$item->model, ',')));
            $model = AModel::query()
                ->where(
                    ['brand_id' =>  $brands_array[$mark],
                        'name' => substr((string)$item->model, 0, strpos((string)$item->model, ','))])
                ->first();

            if($model) {
                $generation = Generation::query()->where(['model_name' => $model->name, 'name' => (string)$item->generation])->first();
                if(!$generation) {
                    $generation = Generation::query()->where(['model_name' => $model->name])->where('name', 'ilike',  (string)$item->generation.'%')->first();
                }

                try {
                    $data = [
                        'client_id' => (integer) $item->id,
                        'brand_id' => $brands_array[$mark] ?? '',
                        'model_id' => $model->id,
                        'generation_id' => $generation->id,
                        'year_of_issue' => (integer)$item->year,
                        'mileage' => (string)$item->run,
                        'car_body' => (string)$item->{"body-type"},
                        'transmission' => (string)$item->transmission,
                        'engine' => (string)$item->{"engine-type"},
                        'steering_wheel' => (string)$item->{"steering-wheel"},
                        'color' => (string)$item->color,
                        'car_drive' => (string)$item->{"gear-type"},
                        'power' => (integer)$item->{"horse-power"},
                        'volume' => (float)$item->displacement,
                        'number_of_owners' => (integer)$item->owners,
                        'vin' => (string)$item->vin,
                        'price' => (double)$item->price,
                        'price_by_action' =>  doubleval($item->price) - 100000,
                        'tradein_discount' => 25000,
                        'credit_discount' => 25000,
                        'insurance_discount' => 50000,
                        'vin_verified' => true,
                        'is_current' => true,
                        'region_id' => env('APP_SITE_REGION') !== '' ? env('APP_SITE_REGION') : null,
                    ];

                } catch (\Exception $exception) {
                    Log::error($exception->getMessage());
                    continue;
                }

                /**
                 * @var $advert Advert
                 */
                $advert = Advert::create($data);
                if ($advert) {

                    if(is_object($item->equipment)) {

                        foreach ($item->equipment as $equipment) {
                            $equipments[] = ['equipment_id' => 1, 'advert_id' => $advert->id, 'text' => $equipment, 'is_autoru' =>true];
                        }
                    }

                    if (!empty($equipments)) {
                        try {
                            $advert->equipments()->insert($equipments);
                        } catch (\Exception $exception) {
                            dd($exception->getMessage());
                        }
                    }

                    $order_photo = 1;
                    $images = (array) $item->image;
                    if(!empty($images)) {
                        foreach ($images as $photo) {
                            ImportPhoto::dispatch($advert, $photo, $order_photo);
                            $order_photo++;
                        }
                    }
                }
            }

            $i++;
            unset($item);
            $xml->next($xml->name);
        }

        if($vips) {
            foreach ($vips as $vip) {
                if($vip->client_id != null) {
                    Advert::query()->where(['client_id' => $vip->client_id])->update(['is_vip' => true]);
                }
            }
        }

        if($specs) {
            foreach ($specs as $spec) {
                if($spec->client_id != null) {
                    Advert::query()->where(['client_id' => $spec->client_id])->update(['is_spec' => true]);
                }
            }
        }

    }
}
