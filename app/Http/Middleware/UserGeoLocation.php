<?php

namespace App\Http\Middleware;

use App\Services\Region;
use Closure;
use GeoIp2\Database\Reader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;


class UserGeoLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Region::isAutodromium()) {
            $cookieValue = null;
            $time = time() + 60 * 60 * 24;
            if(!$request->hasCookie('userLocation') && !$request->has('region_iso_code')) {

                $local_ips = [
                    '127.0.0.1', '192.168.1.20'
                ];

                if(in_array($this->getIp(), $local_ips)){
                    $ip = '103.136.43.0';
                }else{
                    $ip = $this->getIp();
                }

                //$ip = $this->getIp() === '127.0.0.1' ? '103.136.43.0' : $this->getIp();

                $reader = new Reader(storage_path().'/GeoLite2-City.mmdb');
                $result = $reader->city($ip);
                $cookieValue =  $result->city->name;
                Cookie::queue('userLocation', $cookieValue, $time);
                $response = $next($request);
                $response->withCookie(cookie('userLocation', $cookieValue));
                if($cookieValue == 'Samara' && env('APP_SITE_REGION') != 63) {
                    return redirect('https://samara.autodromium.ru');
                }
                if($cookieValue == 'Yekaterinburg' && env('APP_SITE_REGION') != 66) {
                    return redirect('https://ekb.autodromium.ru');
                }
                if($cookieValue == 'NN' && env('APP_SITE_REGION') != 52) {
                    return redirect('https://nn.autodromium.ru');
                }

                return $response;
            }

            if($request->has('region_iso_code')) {
                Cookie::forget('userLocation');
                $cookieValue = $request->region_iso_code;
                Cookie::queue('userLocation', $cookieValue, $time);
                $response = $next($request);
                $response->withCookie(cookie('userLocation', $cookieValue));
                return $response;
            }
        }

        return $next($request);
    }

    /**
     * @return mixed
     */
    private function getIp()
    {
        $ip = $_SERVER["REMOTE_ADDR"];

        if (!empty($_SERVER["HTTP_CLIENT_IP"]))
        {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }

        return $ip;
    }
}
