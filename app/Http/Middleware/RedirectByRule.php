<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RedirectByRule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $host = $request->header('host');
        $fullUrl = $request->fullUrl();

        if (substr($host, 0, 4) == 'www.') {
            $request->headers->set('host', substr($host, 4));

            return Redirect::to(preg_replace('~(https?://)www\.(.+)~', '$1$2', $fullUrl));
        }

        if ($request->getBaseUrl() === '/index.php') {
            return Redirect::to(str_replace('/index.php', '', $fullUrl));
        }

        // Редирект со страниц со "/" на конце URL на соответствующие URL без "/"
        $regex = '~/(\?)|/$~';
        preg_match($regex, $_SERVER['REQUEST_URI'], $match);
        if (! preg_match('~^/(\?.*)?$~', $_SERVER['REQUEST_URI']) && $match) {
            return Redirect::to(preg_replace($regex, '$1', $fullUrl));
        }

        return $next($request);
    }
}
