<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\AdvertEquipments;
use App\Models\AdvertPhoto;
use App\Models\Equipment;
use App\Models\EquipmentGroup;
use App\Models\Review;
use App\Repository\AdvertsRepository;
use Illuminate\Http\Request;

class AdvertsController extends Controller
{

    private $repository;

    public function __construct(AdvertsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @param int $advert
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function advert(Request $request, int $advert)
    {
        $row = Advert::where('client_id', $advert);

        if($request->has('sold')) {
            $row->where('is_current', false)->orderByDesc('id');
        } else {
            $row->where('is_current', true);
        }

        $advert = $row->firstOrFail();

        if (env('APP_SITE_REGION') == 23) {
            abort_if($advert->photos->isEmpty(), 404);
        }

        /*
        if (!$advert->is_current) {
            $ids = Advert::where('is_current', '=', false)
                ->selectRaw('distinct on(client_id) *')
                ->whereNotIn('client_id', Advert::query()
                    ->selectRaw('client_id')
                    ->where(['is_current' => true])
                    ->pluck('client_id')
                    ->toArray()
                )->pluck('id')->toArray();
            if(!in_array($advert->id, $ids)) {
                abort(404);
            }

        }
        */

        $advertsSpec = $this->repository->getSpec($advert);
        $selectedEquipments = $advert->selectedEquipments();
        $equipments = Equipment::selectRaw('group_id')->whereIn('id', $selectedEquipments)
            ->groupBy('group_id')
            ->get()->pluck('group_id');
        $equipmentGroups = EquipmentGroup::whereIn('equipment_groups.id', $equipments)->get();



        $data = [
            'item' => $advert,
            'similar' => $advertsSpec,
            'equipmentGroups' => $equipmentGroups,
            'selectedEquipments' => $selectedEquipments,
        ];
        return view('advert', $data);
    }

    /**
     * @param Request $request
     * @param Advert $advert
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editAjax(Request $request, Advert $advert)
    {
        if ($request->method() == 'PATCH') {
            if (!$request->has('is_vip')) {
                $request->request->add(['is_vip' => false]);
            }

            if (!$request->has('is_spec')) {
                $request->request->add(['is_spec' => false]);
            }

            $requestData = $request->all();
            $advert->update($requestData);

            if ($request->hasFile('photo')) {
                foreach ($request->file('photo') as $file) {
                    $filename = uniqid() . '.' . $file->clientExtension();
                    $file->storeAs('photos', $filename, ['disk' => 'public']);

                    $file_data = [
                        'advert_id' => $advert->id,
                        'filename' => $filename,
                    ];
                    AdvertPhoto::create($file_data);
                }
            }

            if ($request->has('equipment')) {
                $advert->equipments()->delete();
                foreach ($request->equipment as $equipment) {
                    $equipments = [
                        'equipment_id' => $equipment,
                        'advert_id' => $advert->id,
                    ];
                    AdvertEquipments::create($equipments);
                }
            }
        }

        $equipmentGroups = EquipmentGroup::all();
        $advertEquipments = $advert->equipments()->get();
        if ($advertEquipments) {
            $advertEquipments = $advertEquipments->pluck('equipment_id')->toArray();
            $advertEquipments = array_flip($advertEquipments);
        }

        return view('advert.edit-ajax', compact('advert', 'equipmentGroups', 'advertEquipments'));
    }

    /**
     * @param Request $request
     * @param Review $item
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function reviewVideo(Request $request, Review $item)
    {
        return view('advert.review_video_modal', ['item' => $item]);
    }
}
