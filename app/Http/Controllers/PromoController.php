<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\Promo;
use App\Models\RequestOnCredit;
use App\Models\RequestOnPromo;
use App\Rules\CreditPhone;
use App\Services\RequestFormService;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PromoController extends Controller
{
    /**
     * @param Request $request
     * @param Advert|null $advert
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, Advert $advert = null)
    {
        abort_unless(setting('page__promo_enable'), 404);

        $promos = Promo::query()->where(['is_active' => true])->get();
        $data = [
            'items' => $promos,
        ];
        return view('promos.index', $data);
    }

    /**
     * @param Request $request
     * @param Promo $promo
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, Promo $promo)
    {
        abort_unless(setting('page__promo_enable'), 404);

        $otherPromos = Promo::query()->where('id', '!=', $promo->id)->where(['is_active' => true])->get();

        $data = [
            'item' => $promo,
            'otherPromos' => $otherPromos,
        ];

        return view('promos.show', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestAdd(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'promo_id' => 'nullable|exists:promos,id',
        ]);

        $requestOnPromo = new RequestOnPromo();

        $requestForm = new RequestFormService($requestOnPromo, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestOnPromo->fill($requestForm->getData());
        $requestOnPromo->save();
        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param Promo $item
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modal(Request $request, Promo $item)
    {
        return view('promos._modal_form', ['item' => $item]);
    }


}
