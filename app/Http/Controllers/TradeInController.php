<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\RequestInContact;
use App\Models\RequestOnCredit;
use App\Models\RequestOnExchange;
use App\Models\RequestOnTradeIn;
use App\Rules\CreditPhone;
use App\Services\RequestFormService;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TradeInController extends Controller
{
    /**
     * @param Request $request
     * @param Advert|null $advert
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, Advert $advert = null)
    {
        abort_unless(setting('page__trade_in_enable'), 404);

        $brandsClient = Brand::selectRaw('brands.id, brands.name')
            ->leftjoin('models', 'models.brand_id', '=', 'brands.id')
            ->distinct('brands.name')
            ->whereNotNull('models.id')
            ->orderBy('name')
            ->get();

        $brands = Advert::getBrandsByExistedAdverts();

        if(!$advert) {
            $adverts = null;
            $models = null;

        } else {
            $adverts = Advert::where(['brand_id' => $advert->brand_id, 'model_id' => $advert->model_id])->current()->get();
            $models = Advert::getModelsByBrandAndExistingAdverts($advert->brand_id);
        }

        $data = [
            'item' => $advert,
            'adverts' => $adverts,
            'brands' => $brands,
            'models' => $models,
            'brandsClient' => $brandsClient,
        ];
        return view('trade-in.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestAdd(Request $request)
    {
        $request->request->add([
            'brand_id' => $request->credit_brand,
            'model_id' => $request->credit_model,
            'advert_id' => $request->credit_advert,
            ]
        );

        $data = $this->validate($request, [
            'brand_id' => 'required',
            'model_id' => 'required',
            'name' => 'required',
            'advert_id' => 'required',
            'phone' => 'required',
            'client_brand' => 'required',
            'client_model' => 'required',
            'credit_advert' => 'required|numeric|exists:adverts,id',
        ]);

        $phone_validator = Validator::make($request->all(), [
            'phone' => new CreditPhone()
        ]);

        if ($phone_validator->fails()) {
            return 0;
        }

        $requestOnTradeIn = new RequestOnTradeIn();

        $requestForm = new RequestFormService($requestOnTradeIn, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestOnTradeIn->fill($requestForm->getData());
        $requestOnTradeIn->save();
        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestAddExchange(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
            'advert_id' => 'required|numeric|exists:adverts,id',
            'phone' => ['required'],
        ]);

        $requestModel = new RequestOnExchange();

        $requestForm = new RequestFormService($requestModel, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestModel->fill($requestForm->getData());
        $requestModel->save();

        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxClientModel(Request $request)
    {
        $models = AModel::selectRaw('models.id, models.name')
            ->where(['models.brand_id' => $request->brand_id])
            ->orderBy('name')->get();

        return $models->toJson();
    }


}
