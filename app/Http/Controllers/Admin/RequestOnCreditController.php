<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Advert;
use App\Models\RequestOnCredit;
use Illuminate\Http\Request;

class RequestOnCreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $requestoncredit = RequestOnCredit::where('brand_id', 'LIKE', "%$keyword%")
                ->orWhere('model_id', 'LIKE', "%$keyword%")
                ->orWhere('advert_id', 'LIKE', "%$keyword%")
                ->orWhere('credit_term', 'LIKE', "%$keyword%")
                ->orWhere('down_payment', 'LIKE', "%$keyword%")
                ->orWhere('monthly_payment', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $requestoncredit = RequestOnCredit::latest()->paginate($perPage);
        }

        return view('admin.request-on-credit.index', compact('requestoncredit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.request-on-credit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $advert = null;
        if($request->has('advert_id')) {
            $advert = Advert::find($request->advert_id);
            $request->request->add(['model_id' => $advert->model_id, 'brand_id' => $advert->brand_id]);
        }

        $this->validate($request, [
            'brand_id' => 'required',
            'model_id' => 'required',
            'advert_id' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ]);
        $requestData = $request->all();

        RequestOnCredit::create($requestData);

        return redirect('admin/request-on-credit')->with('flash_message', 'RequestOnCredit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $requestoncredit = RequestOnCredit::findOrFail($id);

        return view('admin.request-on-credit.show', compact('requestoncredit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $requestoncredit = RequestOnCredit::findOrFail($id);

        return view('admin.request-on-credit.edit', compact('requestoncredit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $advert = null;
        if($request->has('advert_id')) {
            $advert = Advert::find($request->advert_id);
            $request->request->add(['model_id' => $advert->model_id, 'brand_id' => $advert->brand_id]);
        }

        $this->validate($request, [
			'brand_id' => 'required',
			'model_id' => 'required',
			'advert_id' => 'required',
			'name' => 'required',
			'phone' => 'required',
		]);

        $requestData = $request->all();

        $requestoncredit = RequestOnCredit::findOrFail($id);
        $requestoncredit->update($requestData);

        return redirect('admin/request-on-credit')->with('flash_message', 'RequestOnCredit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        RequestOnCredit::destroy($id);

        return redirect('admin/request-on-credit')->with('flash_message', 'RequestOnCredit deleted!');
    }
}
