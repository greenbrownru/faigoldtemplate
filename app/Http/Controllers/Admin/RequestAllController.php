<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\RequestInContact;
use App\Models\RequestOnBuy;
use App\Models\RequestOnCredit;
use App\Models\RequestOnExchange;
use App\Models\RequestOnPromo;
use App\Models\RequestOnTradeIn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RequestAllController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tradein = RequestOnTradeIn::get();
        $promos = RequestOnPromo::get();
        $credits = RequestOnCredit::get();
        $buys = RequestOnBuy::get();
        $contacts = RequestInContact::get();
        $exchanges = RequestOnExchange::get();

        $all_requests['tradein'] = $tradein;
        $all_requests['promo'] = $promos;
        $all_requests['credit'] = $credits;
        $all_requests['buy'] = $buys;
        $all_requests['contact'] = $contacts;
        $all_requests['exchange'] = $exchanges;

        return view('admin.request-all.index', ['requests' => $all_requests]);
    }


}
