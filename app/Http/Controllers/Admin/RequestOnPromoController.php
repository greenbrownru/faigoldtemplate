<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\RequestOnPromo;
use Illuminate\Http\Request;

class RequestOnPromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $requestonpromo = RequestOnPromo::where('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('promo_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $requestonpromo = RequestOnPromo::latest()->paginate($perPage);
        }

        return view('admin.request-on-promo.index', compact('requestonpromo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.request-on-promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        RequestOnPromo::create($requestData);

        return redirect('admin/request-on-promo')->with('flash_message', 'RequestOnPromo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $requestonpromo = RequestOnPromo::findOrFail($id);

        return view('admin.request-on-promo.show', compact('requestonpromo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $requestonpromo = RequestOnPromo::findOrFail($id);

        return view('admin.request-on-promo.edit', compact('requestonpromo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $requestonpromo = RequestOnPromo::findOrFail($id);
        $requestonpromo->update($requestData);

        return redirect('admin/request-on-promo')->with('flash_message', 'RequestOnPromo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        RequestOnPromo::destroy($id);

        return redirect('admin/request-on-promo')->with('flash_message', 'RequestOnPromo deleted!');
    }
}
