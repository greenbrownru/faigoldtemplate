<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Brand;
use App\Models\Setting;
use App\Services\Admin\SettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class SettingsController extends Controller
{
    protected function getSettings() {
        return SettingService::make()->get();
    }

    public function index(Request $request) {
        $groups = $this->getSettings();
        $brands = Brand::query()
            ->has('adverts')
            ->pluck('name', 'id');

        return view('admin.settings.index', compact('groups', 'brands'));
    }

    public function update(Request $request) {
        $data = $request->except('_token');

        $groups = collect($this->getSettings());
        $onlyFields = $groups
            ->flatMap(fn($v) => $v['items'])
            ->filter(fn($v) => ! isset($v['type']) || $v['type'] === 'field')
            ->values();
        $onlyFieldsGroup = $onlyFields->groupBy(fn($v) => $v['field'] ?? 'text')->map(fn($v) => $v->pluck('name')->all());

        $this->updateFiles(Arr::only($data, $onlyFieldsGroup['file']));
        $data = Arr::except($data, $onlyFieldsGroup['file']);

        foreach ($onlyFieldsGroup['checkbox'] as $name) {
            $data[$name] = $data[$name] ?? 0;
        }

        foreach ($onlyFieldsGroup['text'] as $name) {
            if (array_key_exists($name, $data) && is_null($data[$name])) {
                $data[$name] = '';
            }
        }

        setting($data)->save();
        return back()->with('flash_message', 'Настройки успешно обновлены!');
    }

    public function runYandexFeed(Request $request, $name) {
        Artisan::call('yandex:set_feed', $name === 'all' ? [] : [
            '--brand_name' => $name
        ]);

        return response('', 204);
    }

    protected function updateFiles($data) {
        foreach ($data as $name => $val) {
            /** @var \Illuminate\Http\UploadedFile $val */
            $oldVal = setting($name);

            if ($oldVal) {
                $path = public_path($oldVal);
                if (File::exists($path)) {
                    File::delete($path);
                }
            }

            $file = $val->store('different_photos', 'public');

            setting([
                $name => '/storage/' . $file
            ]);
        }
    }
}
