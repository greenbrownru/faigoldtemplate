<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\RequestOnBuy;
use Illuminate\Http\Request;

class RequestOnBuyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $requestonbuy = RequestOnBuy::where('client_brand', 'LIKE', "%$keyword%")
                ->orWhere('client_model', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $requestonbuy = RequestOnBuy::latest()->paginate($perPage);
        }

        return view('admin.request-on-buy.index', compact('requestonbuy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.request-on-buy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'client_brand' => 'required',
			'client_model' => 'required',
			'name' => 'required',
			'phone' => 'required'
		]);
        $requestData = $request->all();
        
        RequestOnBuy::create($requestData);

        return redirect('admin/request-on-buy')->with('flash_message', 'RequestOnBuy added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $requestonbuy = RequestOnBuy::findOrFail($id);

        return view('admin.request-on-buy.show', compact('requestonbuy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $requestonbuy = RequestOnBuy::findOrFail($id);

        return view('admin.request-on-buy.edit', compact('requestonbuy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'client_brand' => 'required',
			'client_model' => 'required',
			'name' => 'required',
			'phone' => 'required'
		]);
        $requestData = $request->all();
        
        $requestonbuy = RequestOnBuy::findOrFail($id);
        $requestonbuy->update($requestData);

        return redirect('admin/request-on-buy')->with('flash_message', 'RequestOnBuy updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        RequestOnBuy::destroy($id);

        return redirect('admin/request-on-buy')->with('flash_message', 'RequestOnBuy deleted!');
    }
}
