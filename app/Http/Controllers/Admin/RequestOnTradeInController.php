<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\RequestOnTradeIn;
use Illuminate\Http\Request;

class RequestOnTradeInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tradein = RequestOnTradeIn::where('brand_id', 'LIKE', "%$keyword%")
                ->orWhere('model_id', 'LIKE', "%$keyword%")
                ->orWhere('advert_id', 'LIKE', "%$keyword%")
                ->orWhere('trade_brand_id', 'LIKE', "%$keyword%")
                ->orWhere('trade_model_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tradein = RequestOnTradeIn::latest()->paginate($perPage);
        }

        return view('admin.request-on-trade-in.index', compact('tradein'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.request-on-trade-in.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'brand_id' => 'required',
			'model_id' => 'required',
			'advert_id' => 'required',
			'trade_brand_id' => 'required',
			'trade_model_id' => 'required',
			'name' => 'required',
			'phone' => 'required'
		]);
        $requestData = $request->all();

        RequestOnTradeIn::create($requestData);

        return redirect('admin/request-on-trade-in')->with('flash_message', 'TradeIn added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tradein = RequestOnTradeIn::findOrFail($id);

        return view('admin.request-on-trade-in.show', compact('tradein'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tradein = RequestOnTradeIn::findOrFail($id);

        return view('admin.request-on-trade-in.edit', compact('tradein'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'brand_id' => 'required',
			'model_id' => 'required',
			'advert_id' => 'required',
			'trade_brand_id' => 'required',
			'trade_model_id' => 'required',
			'name' => 'required',
			'phone' => 'required'
		]);
        $requestData = $request->all();

        $tradein = RequestOnTradeIn::findOrFail($id);
        $tradein->update($requestData);

        return redirect('admin/request-on-trade-in')->with('flash_message', 'TradeIn updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        RequestOnTradeIn::destroy($id);

        return redirect('admin/request-on-trade-in')->with('flash_message', 'TradeIn deleted!');
    }
}
