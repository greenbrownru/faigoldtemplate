<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Review;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $reviews = Review::where('fio', 'LIKE', "%$keyword%")
                ->orWhere('brand_id', 'LIKE', "%$keyword%")
                ->orWhere('model_id', 'LIKE', "%$keyword%")
                ->orWhere('rate', 'LIKE', "%$keyword%")
                ->orWhere('client_photo_name', 'LIKE', "%$keyword%")
                ->orWhere('auto_photo_name', 'LIKE', "%$keyword%")
                ->orWhere('video_link', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $reviews = Review::latest()->paginate($perPage);
        }

        return view('admin.reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'fio' => 'required',
			'brand_id' => 'required',
			'model_id' => 'required',
            'review' => 'required',
            'client_photo_name' => 'mimes:jpeg,jpg,png|max:3000',
            'auto_photo_name' => 'mimes:jpeg,jpg,png|max:3000',
		]);

        $requestData = $request->all();

        if ($request->hasFile('client_photo_name')) {
                $filename = uniqid() . '.' . $request->file('client_photo_name')->clientExtension();
                $request->file('client_photo_name')->storeAs('photos', $filename, ['disk' => 'public']);
                $requestData['client_photo_name'] = $filename;
        }

        if ($request->hasFile('auto_photo_name')) {
            $filename = uniqid() . '.' . $request->file('auto_photo_name')->clientExtension();
            $request->file('auto_photo_name')->storeAs('photos', $filename, ['disk' => 'public']);
            $requestData['auto_photo_name'] = $filename;
        }

        Review::create($requestData);

        return redirect('admin/reviews')->with('flash_message', 'Отзыв добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $review = Review::findOrFail($id);

        return view('admin.reviews.show', compact('review'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $review = Review::findOrFail($id);

        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'fio' => 'required',
			'brand_id' => 'required',
			'model_id' => 'required',
            'review' => 'required',
            'client_photo_name' => 'mimes:jpeg,jpg,png|max:3000',
            'auto_photo_name' => 'mimes:jpeg,jpg,png|max:3000',
		]);

        $requestData = $request->all();

        if ($request->hasFile('client_photo_name')) {
            $filename = uniqid() . '.' . $request->file('client_photo_name')->clientExtension();
            $request->file('client_photo_name')->storeAs('photos', $filename, ['disk' => 'public']);
            $requestData['client_photo_name'] = $filename;
        }

        if ($request->hasFile('auto_photo_name')) {
            $filename = uniqid() . '.' . $request->file('auto_photo_name')->clientExtension();
            $request->file('auto_photo_name')->storeAs('photos', $filename, ['disk' => 'public']);
            $requestData['auto_photo_name'] = $filename;
        }

        $review = Review::findOrFail($id);
        $review->update($requestData);

        return redirect('admin/reviews')->with('flash_message', 'Review updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Review::destroy($id);

        return redirect('admin/reviews')->with('flash_message', 'Review deleted!');
    }
}
