<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\BannerOnMain;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class BannerOnMainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $banneronmain = BannerOnMain::query()->when(! empty($keyword), function($q) use ($keyword) {
            $q->where('title', 'LIKE', "%$keyword%")
                ->orWhere('options', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->orWhere('button_name', 'LIKE', "%$keyword%")
                ->orWhere('img_url', 'LIKE', "%$keyword%")
                ->orWhere('sort', 'LIKE', "%$keyword%");
        })->oldest('sort')->paginate($perPage);


        return view('admin.banner-on-main.index', compact('banneronmain'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.banner-on-main.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'img_url' => 'required|file'
		]);
        $data = $request->except('_token');
        $data['is_button'] = ! empty($data['is_button']);

        $data['img_url'] = $this->loadFile($data['img_url']);
        empty($data['img_mobile_url']) || $data['img_mobile_url'] = $this->loadFile($data['img_mobile_url']);

        BannerOnMain::create($data);

        return redirect('admin/banner-on-main')->with('flash_message', 'BannerOnMain added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $banneronmain = BannerOnMain::findOrFail($id);

        return view('admin.banner-on-main.show', compact('banneronmain'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $banneronmain = BannerOnMain::findOrFail($id);

        return view('admin.banner-on-main.edit', compact('banneronmain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'img_url' => 'file'
		]);
        $data = $request->except('_token', '_method');
        $data['is_button'] = ! empty($data['is_button']);
        
        $banneronmain = BannerOnMain::findOrFail($id);

        empty($data['img_url']) || $data['img_url'] = $this->loadFile($data['img_url'], $banneronmain->img_url);
        empty($data['img_mobile_url']) || $data['img_mobile_url'] = $this->loadFile($data['img_mobile_url'], $banneronmain->img_mobile_url);

        $banneronmain->update($data);

        return redirect('admin/banner-on-main')->with('flash_message', 'BannerOnMain updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $banneronmain = BannerOnMain::findOrFail($id);
        $this->deleteFile($banneronmain->img_url);
        $this->deleteFile($banneronmain->img_mobile_url);
        BannerOnMain::destroy($id);

        return redirect('admin/banner-on-main')->with('flash_message', 'BannerOnMain deleted!');
    }

    protected function deleteFile($oldFilePath) {
        if ($oldFilePath && File::exists($oldFilePath = public_path($oldFilePath))) {
            File::delete($oldFilePath);
        }
    }

    protected function loadFile(UploadedFile $file, $oldFilePath = null) {
        $this->deleteFile($oldFilePath);

        return '/storage/' . $file->store('different_photos', 'public');
    }
}
