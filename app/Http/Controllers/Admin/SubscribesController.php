<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Subscribe;
use Illuminate\Http\Request;

class SubscribesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $subscribes = Subscribe::where('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('brand', 'LIKE', "%$keyword%")
                ->orWhere('year_from', 'LIKE', "%$keyword%")
                ->orWhere('year_to', 'LIKE', "%$keyword%")
                ->orWhere('price_from', 'LIKE', "%$keyword%")
                ->orWhere('price_to', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $subscribes = Subscribe::latest()->paginate($perPage);
        }

        return view('admin.subscribes.index', compact('subscribes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.subscribes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Subscribe::create($requestData);

        return redirect('admin/subscribes')->with('flash_message', 'Subscribe added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subscribe = Subscribe::findOrFail($id);

        return view('admin.subscribes.show', compact('subscribe'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $subscribe = Subscribe::findOrFail($id);

        return view('admin.subscribes.edit', compact('subscribe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $subscribe = Subscribe::findOrFail($id);
        $subscribe->update($requestData);

        return redirect('admin/subscribes')->with('flash_message', 'Subscribe updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Subscribe::destroy($id);

        return redirect('admin/subscribes')->with('flash_message', 'Subscribe deleted!');
    }
}
