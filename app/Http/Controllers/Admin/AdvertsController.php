<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ConverterExportXml;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Imports\AdvertsAutoRuImportXml;
use App\Imports\AdvertsImportRolfCsV;
use App\Imports\AdvertsImportXml;
use App\Models\AdvertEquipments;
use App\Models\EquipmentGroup;
use Illuminate\Http\Request;
use App\Models\Advert;
use App\Models\AdvertPhoto;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class AdvertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $advertEntity = (new Advert());
        $transmissions = $advertEntity->transmissions();
        $carDrives = $advertEntity->carDrives();
        $carBodies = $advertEntity->carBodies();
        $engines = $advertEntity->engines();

        $params = array_merge($transmissions, $carBodies, $carDrives, $engines);
        $params = array_flip($params);

        $adverts = Advert::selectRaw('adverts.*, b.name as brand_name, m.name as model_name')
            ->where('is_current', '=', true)
            ->join('brands as b', 'b.id', '=', 'adverts.brand_id')
            ->join('models as m', 'm.id', '=', 'adverts.model_id');
        if (!empty($keyword)) {
            if(isset($params[$keyword])) {
                $keyword = $params[$keyword];
            }
            $adverts = $adverts->where('adverts.id', 'ILIKE', "%$keyword%")
                ->current()
                ->orWhere('b.name', 'ILIKE', "%$keyword%")
                ->orWhere('m.name', 'ILIKE', "%$keyword%")
                ->orWhere('year_of_issue', 'ILIKE', "%$keyword%")
                ->orWhere('mileage', 'ILIKE', "%$keyword%")
                ->orWhere('car_body', 'ILIKE', "%$keyword%")
                ->orWhere('transmission', 'ILIKE', "%$keyword%")
                ->orWhere('engine', 'ILIKE', "%$keyword%")
                ->orWhere('steering_wheel', 'ILIKE', "%$keyword%")
                ->orWhere('color', 'ILIKE', "%$keyword%")
                ->orWhere('car_drive', 'ILIKE', "%$keyword%")
                ->orWhere('power', 'ILIKE', "%$keyword%")
                ->orWhere('volume', 'ILIKE', "%$keyword%")
                ->orWhere('number_of_owners', 'ILIKE', "%$keyword%")
                ->orWhere('vin', 'ILIKE', "%$keyword%")
                ->orWhere('price', 'ILIKE', "%$keyword%")
                ->orWhere('price_by_action', 'ILIKE', "%$keyword%")
                ->sortable()->paginate($perPage);
        } else {
            $adverts = $adverts->sortable()->paginate($perPage);
        }

        return view('admin.adverts.index', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $equipmentGroups = EquipmentGroup::all();

        return view('admin.adverts.create', ['equipmentGroups' => $equipmentGroups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'brand_id' => 'required',
			'model_id' => 'required',
			'year_of_issue' => 'required',
			'mileage' => 'required',
			'car_body' => 'required',
			'transmission' => 'required',
			'engine' => 'required',
			'steering_wheel' => 'required',
			'color' => 'required',
			'car_drive' => 'required',
			'power' => 'required',
			'volume' => 'required',
			'number_of_owners' => 'required',
			'price' => 'required',
            'photo.*' => 'mimes:jpeg,jpg,png|max:3000',
		]);
        $requestData = $request->all();
        $advert = Advert::create($requestData);

        if ($request->hasFile('photo')) {
            foreach ($request->file('photo') as $file) {
                $filename = uniqid() . '.' . $file->clientExtension();
                $file->storeAs('/photos/', $filename, ['disk' => 'public']);

                $file_data = [
                    'advert_id' => $advert->id,
                    'filename' => $filename,
                ];
                AdvertPhoto::create($file_data);
            }
        }

        if($request->has('equipment')) {
            $equipments = [];
            $advert->equipments()->delete();
            foreach ($request->equipment as $equipment) {
                $equipments = [
                    'equipment_id' => $equipment,
                    'advert_id' => $advert->id,
                ];
                AdvertEquipments::create($equipments);
            }
        }

        return redirect('admin/adverts')->with('flash_message', 'Advert added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $advert = Advert::findOrFail($id);

        return view('admin.adverts.show', compact('advert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $advert = Advert::findOrFail($id);
        $equipmentGroups = EquipmentGroup::all();
        $advertEquipments = $advert->equipments()->get();
        if($advertEquipments) {
            $advertEquipments = $advertEquipments->pluck('equipment_id')->toArray();
            $advertEquipments = array_flip($advertEquipments);
        }

        return view('admin.adverts.edit', compact('advert', 'equipmentGroups', 'advertEquipments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'brand_id' => 'required',
			'model_id' => 'required',
			'year_of_issue' => 'required',
			'mileage' => 'required',
			'car_body' => 'required',
			'transmission' => 'required',
			'engine' => 'required',
			'steering_wheel' => 'required',
			'color' => 'required',
			'car_drive' => 'required',
			'power' => 'required',
			'volume' => 'required',
			'number_of_owners' => 'required',
			'price' => 'required',
            'photo.*' => 'mimes:jpeg,jpg,png|max:3000',

		]);

        if(!$request->has('is_vip')) {
            $request->request->add(['is_vip' => false]);
        }

        if(!$request->has('is_spec')) {
            $request->request->add(['is_spec' => false]);
        }

        $requestData = $request->all();

        /**
         * @var $advert Advert
         */
        $advert = Advert::findOrFail($id);
        $advert->update($requestData);

        if ($request->hasFile('photo')) {
            foreach ($request->file('photo') as $file) {
                $filename = uniqid() . '.' . $file->clientExtension();
                $res = $file->storeAs('photos', $filename, ['disk' => 'public']);

                $file_data = [
                    'advert_id' => $advert->id,
                    'filename' => $filename,
                ];
                AdvertPhoto::create($file_data);
            }
        }

        if($request->has('equipment')) {
            $advert->equipments()->delete();
            foreach ($request->equipment as $equipment) {
                $equipments = [
                    'equipment_id' => $equipment,
                     'advert_id' => $advert->id,
                ];
                AdvertEquipments::create($equipments);
            }
        }

        return redirect('admin/adverts')->with('flash_message', 'Advert updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Advert::destroy($id);

        return redirect('admin/adverts')->with('flash_message', 'Advert deleted!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function import(Request $request)
    {
        ini_set('max_execution_time', 20*600);
        //ini_set('post_max_size', '36M');
        //ini_set('upload_max_filesize', '36M');
        set_time_limit(20*600);
        if($request->method() == 'POST') {
            $this->validate($request, [
                'filename' => 'required|file',
            ]);


            if(env('IS_ROLF_IMPORT')) {
                //dd('test');
                (new AdvertsImportRolfCsV($request))->store();
            } elseif(env('APP_SITE_CITY_LOCATION') && env('APP_SITE_CITY_LOCATION') == 'chel') {
               // (new AdvertsAutoRuImportXml($request))->store();
            } else {
              //  (new AdvertsImportXml($request))->store();
            }

            Session::flash('message', 'Объявления успешно загружены!');
            return redirect()->back();
        }

        return view('admin.adverts.import');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function recovers(Request $request)
    {
        $items = Advert::query()->selectRaw('count(id) as cnt, import_id, is_current')
            ->groupBy('import_id', 'is_current')
            ->orderByDesc('import_id')
            ->get();
        return view('admin.adverts.recovers', compact('items'));
    }

    /**
     * @param Request $request
     * @param $importId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function recoversShow(Request $request, $importId)
    {
        $items = Advert::query()->where(['import_id' => $importId])->orderBy('id')->get();
        return view('admin.adverts.recovers_show', compact('items'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setCurrent(Request $request)
    {
        Advert::query()->update(['is_current' => false]);
        Advert::query()->where(['import_id' => $request->import_id])->update(['is_current' => true]);
        return redirect()->back()->with('flash_message', 'Данные актуализированы!');
    }
}
