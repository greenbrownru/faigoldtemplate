<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\EquipmentGroup;
use Illuminate\Http\Request;

class EquipmentGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $equipmentgroups = EquipmentGroup::where('group_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('sort_order', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $equipmentgroups = EquipmentGroup::latest()->paginate($perPage);
        }

        return view('admin.equipment-groups.index', compact('equipmentgroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.equipment-groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'group_id' => 'required',
			'name' => 'required',
			'sort_order' => 'required'
		]);
        $requestData = $request->all();

        EquipmentGroup::create($requestData);

        return redirect('admin/equipment-groups')->with('flash_message', 'equipmentGroup added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $equipmentgroup = EquipmentGroup::findOrFail($id);

        return view('admin.equipment-groups.show', compact('equipmentgroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $equipmentgroup = EquipmentGroup::findOrFail($id);

        return view('admin.equipment-groups.edit', compact('equipmentgroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'group_id' => 'required',
			'name' => 'required',
			'sort_order' => 'required'
		]);
        $requestData = $request->all();

        $equipmentgroup = EquipmentGroup::findOrFail($id);
        $equipmentgroup->update($requestData);

        return redirect('admin/equipment-groups')->with('flash_message', 'equipmentGroup updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EquipmentGroup::destroy($id);

        return redirect('admin/equipment-groups')->with('flash_message', 'equipmentGroup deleted!');
    }
}
