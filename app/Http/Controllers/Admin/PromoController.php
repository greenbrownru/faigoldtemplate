<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $promo = Promo::where('photo', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('short_description', 'LIKE', "%$keyword%")
                ->orWhere('full_description', 'LIKE', "%$keyword%")
                ->orWhere('is_active', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $promo = Promo::latest()->paginate($perPage);
        }

        return view('admin.promo.index', compact('promo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpeg,jpg,png|max:3000',
			'name' => 'required',
			'short_description' => 'required',
			'full_description' => 'required',
			'is_active' => 'required'
		]);

        $requestData = $request->all();

        if ($request->hasFile('photo')) {
            $filename = uniqid() . '.' . $request->file('photo')->clientExtension();
            $request->file('photo')->storeAs('photos', $filename, ['disk' => 'public']);
            $requestData['photo'] = $filename;
        }

        Promo::create($requestData);
        return redirect('admin/promo')->with('flash_message', 'Акция добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $promo = Promo::findOrFail($id);
        return view('admin.promo.show', compact('promo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $promo = Promo::findOrFail($id);
        return view('admin.promo.edit', compact('promo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if(!$request->has('with_form')) {
            $request->request->add(['with_form' => false]);
        }
        $this->validate($request, [
            'photo' => 'required|mimes:jpeg,jpg,png|max:3000',
			'name' => 'required',
			'short_description' => 'required',
			'full_description' => 'required',
			'is_active' => 'required'
		]);
        $requestData = $request->all();

        if ($request->hasFile('photo')) {
            $filename = uniqid() . '.' . $request->file('photo')->clientExtension();
            $request->file('photo')->storeAs('photos', $filename, ['disk' => 'public']);
            $requestData['photo'] = $filename;
        }

        $promo = Promo::findOrFail($id);
        $promo->update($requestData);
        return redirect('admin/promo')->with('flash_message', 'Акция обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Promo::destroy($id);

        return redirect('admin/promo')->with('flash_message', 'Акция удалена!');
    }
}
