<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class AboutController extends Controller
{
    public function index(){
        abort_unless(setting('page__about_enable'), 404);

        $files = [];
        if(File::exists($_SERVER['DOCUMENT_ROOT'] . '/img/about/' . env('APP_SITE_REGION'))){
            $rsFiles = File::files($_SERVER['DOCUMENT_ROOT'] . '/img/about/' . env('APP_SITE_REGION'));
            foreach($rsFiles as $file){
                $files[] = $file->getFilename();
            }

            sort($files, SORT_NATURAL);
        }


        $pageRow = Page::whereTitle('about_text')->first();
        $about_text = '';

        if ($pageRow) {
            $about_text = str_replace('[PHONE_NUMBER]', showPhone(), $pageRow->content);
        }

        $data['images'] = $files;
        $data['about_text'] = $about_text;

        return view('about', $data);
    }

    public function personalData(Request $request)
    {
        return view('about.personal_data');
    }
}
