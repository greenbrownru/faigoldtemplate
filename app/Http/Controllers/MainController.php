<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\Promo;
use App\Models\RequestInContact;
use App\Models\RequestOnCarService;
use App\Models\Review;
use App\Rules\CreditPhone;
use App\Services\RequestFormService;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        if (in_array(env('APP_SITE_REGION'), [63, 66])) {
            if (request()->display || request()->model) {
                return response()
                    ->redirectTo(url()->current() . '?' . http_build_query(request()->except(request()->display ? 'display' : 'model')));
            }
        }

        $seconds = 3600;
        $advertsVip = Cache::remember('advertsVip'.env('APP_SITE_REGION'), $seconds, function () {
            return Advert::query()->active()->current()->vip()->get();
        });

        $advertsSpec = Cache::remember('advertsSpec'.env('APP_SITE_REGION'), $seconds, function () {
            return Advert::query()->active()->current()->spec()->get();
        });


        $advertsNew = Cache::remember('advertsNew'.env('APP_SITE_REGION'), 3600 * 24, function () {
            $items = Advert::query()
                ->when(
                    env('APP_SITE_REGION') == 23,
                    function($q) { $q->inRandomOrder(); },
                    function($q) { $q->latest(); }
                )
                ->current()
                ->limit(4)
                ->get();

            return $items;
        });

        $reviews = Cache::remember('reviews'.env('APP_SITE_REGION'), $seconds, function () {
            return Review::all();
        });

        $data = [
            'advertsVip' => $advertsVip,
            'advertsSpec' => $advertsSpec,
            'advertsNew' => $advertsNew,
            'reviews' => $reviews,
        ];
        return view('index', $data);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function contacts(Request $request)
    {
        if ($request->filled('fancybox')) {
            return view('contacts._modal_form');
        }

        $brands = Advert::getBrandsByExistedAdverts();

        $data = [
            'brands' => $brands,
        ];

        return view('contacts', $data);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function promo()
    {
        $promos = Promo::query()->where(['is_active' => true])->get();
        $data = [
            'items' => $promos,
        ];
        return view('promo', $data);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function tradein()
    {
        return view('tradein');
    }

    public function credit()
    {
        return view('credit');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function car_service()
    {
        abort_unless(setting('page__carservice_enable'), 404);

        return view('car_service');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function contactsRequestAdd(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
        ]);

        $phone_validator = Validator::make($request->all(), [
            'phone' => new CreditPhone()
        ]);

        if ($phone_validator->fails()) {
            return 0;
        }

        $requestOnTradeIn = new RequestInContact();

        $requestForm = new RequestFormService($requestOnTradeIn, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestOnTradeIn->fill($requestForm->getData());
        $requestOnTradeIn->save();

        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function carServiceRequestAdd(Request $request)
    {
        $data = $request->validate([
            'brand_name' => 'required|string|max:255',
            'model_name' => 'required|string|max:255',
            'mileage' => 'required|numeric|min:0|max:1000000000',
            'name' => 'required|string|max:255',
            'email' => 'nullable|string|email|max:255',
            'phone' => ['required', new CreditPhone()],
        ]);

        $row = new RequestOnCarService();

        $requestForm = new RequestFormService($row, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $row->fill($requestForm->getData());
        $row->save();
        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }
}
