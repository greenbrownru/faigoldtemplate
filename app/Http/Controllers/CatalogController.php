<?php


namespace App\Http\Controllers;


use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\Generation;
use App\Repository\CatalogRepository;
use App\Services\Region;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CatalogController extends Controller
{

    private $repository;

    private $advertsQuery;

    private $modelsLinks;

    private $generations;

    private $model_name;

    private $brand_name;

    private $models;

    private $generation_name;

    private $year_to;

    private $spec;

    public function __construct(CatalogRepository $repository)
    {
        if(request()->filled('brand')) {
            request()->request->set('brand', intval(request()->brand));
        }

        if (request()->filled('model')) {
            request()->request->set('model', intval(request()->model));
        }

        $this->repository = $repository;
        $this->advertsQuery = Advert::query()
            ->select('adverts.*')
            ->current()->active();

        if (env('APP_SITE_REGION') == 23) {
            $this->advertsQuery->photo();
        }

        $this->models = [];
        $this->generations = [];
        $this->years = [];
        $this->brand_name = null;
        $this->model_name = null;
        $this->generation_name = null;
        $this->year = intval(date('Y'));
        $this->year_to = 2005;
        $this->modelsLinks = $this->repository->getModelsLinks();
        $this->spec = Advert::query()->active()->spec()->current()->latest()->get();

        while ($this->year > $this->year_to) {
            $this->years[$this->year] = $this->year;
            $this->year--;
        }
    }

    public function index(Request $request)
    {

        $_redirectExcepts = [];

        if (request()->model && !request()->brand) {
            $_redirectExcepts[] = 'model';
        }

        if (request()->display === 'list') {
            $_redirectExcepts[] = 'display';
        }

        if ($_redirectExcepts) {
            return response()
                ->redirectTo($request->fullUrlWithoutQuery($_redirectExcepts));
        }

        if (in_array(env('APP_SITE_REGION'), ['', 63, 66, 18]) && !$request->has('is_cnt')) {
            if ($request->filled('brand') && !$request->filled('model')) {
                return redirect()->route('main.brand', ['brand' => mb_strtolower(Brand::findOrFail($request->brand)->name)] + $request->except('brand'));
            }
            if ($request->filled('brand') && $request->filled('model')) {
                return redirect()->route('main.brand_model',
                    [
                        'brand' => mb_strtolower(Brand::findOrFail($request->brand)->name),
                        'model' => mb_strtolower(AModel::findOrFail($request->model)->name),
                    ] + $request->except(['brand', 'model'])
                );
            }
        }

        $minAndMaxYear = Advert::selectRaw('min(year_of_issue) as min_y')->first();
        if ($minAndMaxYear) {
            $this->year_to = $minAndMaxYear->min_y;
        }

        $this->filter($request);

        $this->modelsLinks = $this->modelsLinks->get()->toArray();

        $modelsLinksArray = [];
        foreach ($this->modelsLinks as $link) {
            $modelsLinksArray[$link['brand_name']][] = ['name' => $link['name'], 'model_id' => $link['id'], 'cnt' => $link['count']];
        }

        if(!env('IS_ROLF_IMPORT')) {
            if ((env('APP_SITE_REGION') != 23 && !env('SITE_SUFFIX'))) {
               // $this->advertsQuery->withSoldOut($request);
            }
        }

        $this->advertsQuery = $this->sorting($request);

        if ($request->has('is_cnt')) {
            return $this->setButtonQuantityMessage();
        }

        if (Region::isAutodromium()) {
            $this->advertsQuery->orderByDesc('is_current');
            $this->advertsQuery = $this->sorting($request);
            $adverts = $this->advertsQuery->paginate(16);

        } else {
            $adverts = $this->advertsQuery->paginate();
        }

        $data = [
            'items' => $adverts,
            'spec' => $this->spec,
            'models' => $this->models,
            'years' => $this->years,
            'generations' => $this->generations,
            'brand_name' => $this->brand_name,
            'model_name' => $this->model_name,
            'generation_name' => $this->generation_name,
            'modelLinks' => $modelsLinksArray,
        ];

        return view('catalog.index', $data);
    }

    public function brand(Request $request, $brand)
    {
        if (array_key_exists('brand', $request->query())) {
            return redirect()->to($request->fullUrlWithoutQuery('brand'));
        }

        $minAndMaxYear = Advert::selectRaw('min(year_of_issue) as min_y')->first();
        if ($minAndMaxYear) {
            $this->year_to = $minAndMaxYear->min_y;
        }

        $brandItem = Brand::query()->where('name', 'ilike', $brand)->firstOrFail();

        if (in_array(env('APP_SITE_REGION'), ['', 63, 66, 18]) && !$request->has('is_cnt')) {
            if ($request->filled('brand') && $request->filled('model')) {
                return redirect()->route('main.brand_model',
                    [
                        'brand' => mb_strtolower(Brand::findOrFail($request->brand)->name),
                        'model' => mb_strtolower(AModel::findOrFail($request->model)->name),
                    ] + $request->except(['brand', 'model'])
                );
            }
        }

        $request->request->set('brand', $brandItem->id);

        $this->filter($request);

        $this->modelsLinks = $this->modelsLinks->get()->toArray();

        $modelsLinksArray = [];
        foreach ($this->modelsLinks as $link) {
            $modelsLinksArray[$link['brand_name']][] = ['name' => $link['name'], 'model_id' => $link['id'], 'cnt' => $link['count']];
        }

        if ($request->has('is_cnt')) {
            return $this->setButtonQuantityMessage();
        }

        if ((env('APP_SITE_REGION') != 23 && !env('SITE_SUFFIX'))) {
            $this->advertsQuery->withSoldOut($request);
        }

        $this->advertsQuery = $this->sorting($request);

        if (env('APP_SITE_REGION') == '' || env('APP_SITE_REGION') == 63 || env('APP_SITE_REGION') == 66) {
            $this->advertsQuery->orderByDesc('is_current');
            $this->advertsQuery = $this->sorting($request);
            $adverts = $this->advertsQuery->paginate(14);
        } else {
            $adverts = $this->advertsQuery->paginate();
        }

        $data = [
            'items' => $adverts,
            'spec' => $this->spec,
            'models' => $this->models,
            'years' => $this->years,
            'generations' => $this->generations,
            'brand_name' => $this->brand_name,
            'model_name' => $this->model_name,
            'generation_name' => $this->generation_name,
            'modelLinks' => $modelsLinksArray,
        ];
        return view('catalog.index', $data);
    }

    private function sorting(Request $request)
    {
        if ($request->has('sort')) {
            if ($request->sort == 'price_down') {
                $this->advertsQuery->orderByDesc('price_by_action');
            } elseif ($request->sort == 'price_up') {
                $this->advertsQuery->orderBy('price_by_action');
            } elseif ($request->sort == 'year_up') {
                $this->advertsQuery->orderBy('year_of_issue');
            } elseif ($request->sort == 'year_down') {
                $this->advertsQuery->orderByDesc('year_of_issue');
            }
        } else {

            $this->advertsQuery->orderBy('is_current', 'DESC');
            $this->advertsQuery->orderBy('price_by_action', 'ASC');
        }
        return $this->advertsQuery;
    }

    public function brandModel(Request $request, $brand, $model)
    {
        if (array_key_exists('brand', $request->query()) || array_key_exists('model', $request->query())) {
            return redirect()->to($request->fullUrlWithoutQuery(['brand', 'model']));
        }

        $brandItem = Brand::query()->where('name', 'ilike', $brand)->firstOrFail();
        $modelItem = AModel::query()->where(['brand_id' => $brandItem->id])->where('name', 'ilike', $model)->firstOrFail();

        $request->request->set('brand', $brandItem->id);
        $request->request->set('model', $modelItem->id);

        $this->filter($request);

        $this->modelsLinks = $this->modelsLinks->get()->toArray();

        $modelsLinksArray = [];
        foreach ($this->modelsLinks as $link) {
            $modelsLinksArray[$link['brand_name']][] = ['name' => $link['name'], 'model_id' => $link['id'], 'cnt' => $link['count']];
        }

        if ($request->has('is_cnt')) {
            return $this->setButtonQuantityMessage();
        }

        $this->advertsQuery = $this->sorting($request);

        if (in_array(env('APP_SITE_REGION'), ['', 63, 66])) {
            $this->advertsQuery->orderByDesc('is_current');
            $this->advertsQuery = $this->sorting($request);

            $adverts = $this->advertsQuery->paginate(14);

        } else {
            $adverts = $this->advertsQuery->paginate();
        }


        $data = [
            'items' => $adverts,
            'spec' => $this->spec,
            'models' => $this->models,
            'years' => $this->years,
            'generations' => $this->generations,
            'brand_name' => $this->brand_name,
            'model_name' => $this->model_name,
            'generation_name' => $this->generation_name,
            'modelLinks' => $modelsLinksArray,

        ];
        return view('catalog.index', $data);
    }

    /**
     * @param Request $request
     */
    private function filter(Request $request) : void
    {
        $discount = app(Advert::class)->getDiscount();

        if ($request->filled('brand')) {
            $this->advertsQuery->where(['adverts.brand_id' => $request->brand]);
            $this->models = $this->repository->getModelsByBrand($request->brand);
            $this->brand_name = Brand::find($request->brand)->name;
        }

        if ($request->filled('model') && $request->filled('brand')) {
            $this->model_name = AModel::find($request->model)->name;
            $this->advertsQuery->where(['adverts.model_id' => $request->model]);
            $this->generations = $this->repository->getGenerationsByParams($request->brand, $request->model);
        }

        if ($request->filled('generation') && $request->filled('brand') && $request->filled('model')) {
            $this->generation_name = Generation::find($request->generation)->name;
            $this->advertsQuery->where(['generation_id' => $request->generation]);
        }

        if ($request->filled('transmission')) {
            $transmissions[] = $request->transmission;
            $transmissions[] = (new Advert())->transmissions()[$request->transmission];

            $this->advertsQuery->whereIn('transmission', $transmissions);
            $this->modelsLinks->whereIn('transmission', $transmissions);
        }

        if ($request->filled('car_body') && $request->car_body != 'value') {
            $car_bodies[] = $request->car_body;
            $car_bodies[] = (new Advert())->carBodies()[$request->car_body];
            $this->advertsQuery->where(function ($query) use ($car_bodies){
                $query->whereIn('car_body', $car_bodies);
                $query->orWhere('car_body', 'ilike', $car_bodies[1].'%');
            });
            $this->modelsLinks->where(function ($query) use ($car_bodies){
                $query->whereIn('car_body', $car_bodies);
                $query->orWhere('car_body', 'ilike', $car_bodies[1].'%');
            });
        }

        if ($request->filled('engine')) {
            $engines[] = $request->engine;
            $engines[] = (new Advert())->engines()[$request->engine];
            $this->advertsQuery->whereIn('engine', $engines);
            $this->modelsLinks->whereIn('engine', $engines);
        }

        if ($request->filled('car_drive')) {
            $car_drives[] = $request->car_drive;
            $car_drives[] = (new Advert())->carDrives()[$request->car_drive];
            $this->advertsQuery->whereIn('car_drive', $car_drives);
            $this->modelsLinks->whereIn('car_drive', $car_drives);
        }

        if ($request->filled('price_from')) {
            $this->advertsQuery->whereRaw('price *'.$discount .' >= '. intval($request->price_from));
            $this->modelsLinks->whereRaw('price *'.$discount . ' >= '. intval($request->price_from));
        }

        if ($request->filled('price_to')) {
            $this->advertsQuery->whereRaw('price *'.$discount . ' <= '. intval($request->price_to));
            $this->modelsLinks->whereRaw('price *'.$discount . ' <= '. intval($request->price_to));
        }

        if ($request->filled('year_from')) {
            $this->advertsQuery->where('year_of_issue', '>=', intval($request->year_from));
            $this->modelsLinks->where('year_of_issue', '>=', intval($request->year_from));
        }

        if ($request->filled('year_to')) {
            $this->advertsQuery->where('year_of_issue', '<=', intval($request->year_to));
            $this->modelsLinks->where('year_of_issue', '<=', intval($request->year_to));
        }

        if ($request->filled('mileage_from')) {
            $this->advertsQuery->where('mileage', '>=', intval($request->mileage_from));
            $this->modelsLinks->where('mileage', '>=', intval($request->mileage_from));
        }

        if ($request->filled('mileage_to')) {
            $this->advertsQuery->where('mileage', '<=', intval($request->mileage_to));
            $this->modelsLinks->where('mileage', '<=', intval($request->mileage_to));
        }
    }


    private function setButtonQuantityMessage(): string
    {
        $cnt = $this->advertsQuery->get()->count();

        $last_digit = substr($cnt, -1);
        $response = "Показать {$cnt} предложени";
        if ($cnt == 0) {
            return "Нет предложений";
        }
        if ($last_digit == 0 || ($last_digit > 4) || ($cnt > 10 && $cnt < 20 && $last_digit > 0)) {
            $response .= "й";
        } elseif ($last_digit == 1 && ($cnt > 20 || $cnt < 10)) {
            $response .= "е";
        } elseif ($last_digit > 1 && ($cnt < 10 || $cnt > 20)) {
            $response .= "я";
        }
        return $response;
    }


}
