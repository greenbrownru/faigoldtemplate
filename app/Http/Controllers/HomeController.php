<?php

namespace App\Http\Controllers;

use App\Models\AModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (request()->refresh_feed) {
            ini_set('memory_limit', '256M');
            set_time_limit(1800);

            Artisan::call('yandex:set_feed');
            echo '<p style="color: green;">Успешно выполнено!</p><a href="javascript:void(0);" onclick="history.back();">Назад</a>';
            return;
        }

        return view('home');
    }

    public function getModels(Request $request)
    {
        $models = AModel::whereBrandId($request->brand)->orderBy('name')->pluck('name', 'id')->toArray();
        return response()->json($models);
    }
}
