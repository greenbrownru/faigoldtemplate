<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Advert;
use App\Models\RequestInContact;
use App\Models\RequestOnBuy;
use App\Models\RequestOnCarService;
use App\Models\RequestOnCredit;
use App\Models\RequestOnExchange;
use App\Models\RequestOnPromo;
use App\Models\RequestOnTradeIn;
use App\Models\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubscribesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        if (!$request->has('token') || $request->token !== setting('api_token')) {
            return response('Unauthorized.', 401);
        }

        $this->validate($request, [
            'last_request_date' => 'date_format:Y-m-d H:i:s|nullable',
        ]);

        $dataAll = [];

        $dataCredits = RequestOnCredit::query()
            ->join('brands', 'brands.id', '=', 'request_on_credits.brand_id')
            ->join('models', 'models.id', '=', 'request_on_credits.model_id')
            ->join('adverts', 'adverts.id', '=', 'request_on_credits.advert_id')
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_credits.created_at', '>', $request->last_request_date);
                }
            })
            ->get([
                'request_on_credits.name as first_name', 'phone as mobile_tel', 'brands.name as brand', 'models.name as model',
                'user_ip as ip_address', 'additional_params', 'request_on_credits.created_at as created', 'credit_term', 'request_on_credits.down_payment',
                'adverts.price', 'adverts.price_by_action', 'adverts.client_id'
            ])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = "заявка на кредит";
                $row->ad_channel = "";

                $advert = Advert::where(['is_current' => true, 'client_id' => $row->client_id])->first();
                if($advert) {
                    $row->price_by_action = $advert->priceByAction();
                } else {
                    $row->price_by_action = 0;
                }
                $price = ($row->price_by_action > 0) ? $row->price_by_action : $row->price;
                $row->inital_pay = number_format($price * ($row->down_payment / 100), 0, '', ' ') . ' руб';
                $row->car_cost = $price;
                $row->referer = route('advert', [$row->client_id]);

                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                unset($row->down_payment, $row->price, $row->price_by_action, $row->client_id);

                return $row;
            })
            ->toArray();

        $dataTradein = RequestOnTradeIn::query()
            ->join('brands', 'brands.id', '=', 'request_on_trade_in.brand_id')
            ->join('models', 'models.id', '=', 'request_on_trade_in.model_id')
            ->join('brands as client_brands', 'client_brands.id', '=', 'request_on_trade_in.client_brand')
            ->join('models as client_models', 'client_models.id', '=', 'request_on_trade_in.client_model')
            ->join('adverts', 'adverts.id', '=', 'request_on_trade_in.advert_id')
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_trade_in.created_at', '>', $request->last_request_date);
                }
            })
            ->get([
                'request_on_trade_in.name as first_name', 'phone as mobile_tel', 'brands.name as brand', 'models.name as model', 'client_brands.name as client_brand',
                'client_models.name as client_model', 'user_ip as ip_address', 'additional_params', 'request_on_trade_in.created_at as created',
                'adverts.price', 'adverts.price_by_action', 'adverts.client_id', 'adverts.year_of_issue', 'adverts.mileage'
            ])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = "заявка на трейдин {$row->client_brand} {$row->client_model}";
                $row->ad_channel = "";

                $advert = Advert::where(['is_current' => true, 'client_id' => $row->client_id])->first();
                if($advert) {
                    $row->price_by_action = $advert->priceByAction();
                } else {
                    $row->price_by_action = 0;
                }

                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                $price = ($row->price_by_action > 0) ? $row->price_by_action : $row->price;
                $row->car_cost = number_format($price, 0, '', ' ') . ' руб';
                $row->referer = route('advert', [$row->client_id]);

                unset($row->price, $row->price_by_action, $row->client_id);

                return $row;
            })
            ->toArray();

        $dataExchanges = RequestOnExchange::query()
            ->join('adverts', 'adverts.id', 'request_on_exchanges.advert_id')
            ->join('brands', 'brands.id', '=', 'adverts.brand_id')
            ->join('models', 'models.id', '=', 'adverts.model_id')
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_exchanges.created_at', '>', $request->last_request_date);
                }
            })
            ->get([
                'request_on_exchanges.name as first_name', 'phone as mobile_tel', 'brands.name as brand', 'models.name as model',
                'user_ip as ip_address', 'additional_params', 'request_on_exchanges.created_at as created', 'adverts.mileage', 'adverts.client_id'
            ])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = 'заявка на трейдин из объявления';
                $row->ad_channel = '';

                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                $row->referer = route('advert', [$row->client_id]);

                unset($row->client_id);

                return $row;
            })
            ->toArray();

        $dataBuys = RequestOnBuy::query()
            ->join('brands', 'brands.id', '=', 'request_on_buys.client_brand')
            ->join('models', 'models.id', '=', 'request_on_buys.client_brand')
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_buys.created_at', '>', $request->last_request_date);
                }
            })
            ->get(['request_on_buys.name as first_name', 'phone as mobile_tel', 'brands.name as brand', 'models.name as model',
                'user_ip as ip_address', 'additional_params', 'request_on_buys.created_at as created'])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = "выкуп авто";
                $row->ad_channel = "";
                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                return $row;
            })
            ->toArray();

        $dataContacts = RequestInContact::query()
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('created_at', '>', $request->last_request_date);
                }
            })
            ->get(['name as first_name', 'phone as mobile_tel', 'user_ip as ip_address', 'additional_params', 'created_at as created'])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = "форма обратной связи";
                $row->ad_channel = "";
                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                return $row;
            })
            ->toArray();


        $dataPromos = RequestOnPromo::query()
            ->join('promos', 'promos.id', '=', 'promo_id')
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_promos.created_at', '>', $request->last_request_date);
                }
            })
            ->get(['request_on_promos.name as first_name', 'phone as mobile_tel', 'user_ip as ip_address',
                'promos.name as promo_name', 'additional_params', 'request_on_promos.created_at as created'])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = "Промо акция";
                $row->ad_channel = "";
                $row->last_name = "";
                $row->middle_name = "";
                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                return $row;
            })
            ->toArray();

        $dataCarService = RequestOnCarService::query()
            ->where(function ($query) use ($request) {
                if ($request->filled('last_request_date')) {
                    $query->where('request_on_car_services.created_at', '>', $request->last_request_date);
                }
            })
            ->get(['request_on_car_services.name as first_name', 'phone as mobile_tel', 'brand_name as brand', 'model_name as model',
                'user_ip as ip_address', 'additional_params', 'created_at as created', 'mileage', 'email'])
            ->map(function ($row) {
                $row->additional_params = serialize($row->additional_params ?: []);
                $row->comment = 'Автосервис';
                $row->ad_channel = '';
                if (!empty($row->mobile_tel)) {
                    $row->mobile_tel = '7' . preg_replace("/[^0-9.]/", "", $row->mobile_tel);
                }

                return $row;
            })
            ->toArray();

        return response()->json(array_values(array_merge($dataCredits, $dataTradein, $dataBuys, $dataContacts, $dataPromos, $dataCarService, $dataExchanges)));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index2(Request $request)
    {

        $dateFrom = null;
        if (!$request->has('token') || $request->token !== setting('api_token')) {
            return response('Unauthorized.', 401);
        }

        $this->validate($request, [
            //   'from' => 'numeric|nullable',
        ]);

        $dataAll = [];

        if ($request->filled('from')) {
            $dateFrom = Carbon::parse(date('Y-m-d H:i:s', $request->from))->setTimezone('Europe/Moscow')->format('Y-m-d H:i:s');
        }

        $dataCredits = RequestOnCredit::query()
            ->join('brands', 'brands.id', '=', 'request_on_credits.brand_id')
            ->join('models', 'models.id', '=', 'request_on_credits.model_id')
            ->join('adverts', 'adverts.id', '=', 'request_on_credits.advert_id')
            ->where(function ($query) use ($request, $dateFrom) {
                if ($dateFrom) {
                    $query->where('request_on_credits.created_at', '>', $dateFrom);
                }
            })
            ->get([
                'request_on_credits.created_at as created',
                'client_id',
                'phone',
                'request_on_credits.name as first_name',
                'user_ip as ip',
                'credit_term',
                'request_on_credits.down_payment as credit_initial_payment',
                'brands.name as car_brand_name',
                'models.name as car_model_name',
                'modification_name as car_modification_name',
                'year_of_issue as car_year',
                'color as car_color',
                'price as car_cost',
                'additional_params',
            ])
            ->map(function ($row) {
                $avito_brand_id = null;
                $avito_model_id = null;

                $avito = DB::table('avito_brand')
                    ->select('avito_model.*')
                    ->join('avito_model', 'avito_brand.brand_id', '=', 'avito_model.brand_id')
                    ->where('avito_brand.name', 'ilike', $row->car_brand_name)
                    ->where('avito_model.name', 'ilike', $row->car_model_name)
                    ->first();
                if ($avito) {
                    $avito_brand_id = $avito->brand_id;
                    $avito_model_id = $avito->model_id;
                }

                $row->make_id = $avito_brand_id;
                $row->model_id = $avito_model_id;
                $row->last_name = null;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->age = null;
                $row->deal_type = "credit";
                $row->car_complete_name = null;
                $row->car_condition = 'used';
                $row->car_link = route('advert', ['advert' => $row->client_id]);
                $row->registration_fias_id = null;
                $row->is_trade_in = false;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;

                $advert = Advert::where(['is_current' => true, 'client_id' => $row->client_id])->first();
                if($advert) {
                    $row->price_by_action = $advert->priceByAction();
                } else {
                    $row->price_by_action = 0;
                }

                $price = ($row->price_by_action > 0) ? $row->price_by_action : $row->price;
                $row->inital_pay = number_format($price * ($row->down_payment / 100), 0, '', ' ') . ' руб';
                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }

                unset($row->down_payment, $row->client_id, $row->price_by_action, $row->additional_params);

                return $row;
            })->toArray();

        $dataTradein = RequestOnTradeIn::query()
            ->join('brands', 'brands.id', '=', 'request_on_trade_in.brand_id')
            ->join('models', 'models.id', '=', 'request_on_trade_in.model_id')
            ->join('adverts', 'adverts.id', '=', 'request_on_trade_in.advert_id')
            ->where(function ($query) use ($request, $dateFrom) {
                if ($request->filled('from')) {
                    $query->where('request_on_trade_in.created_at', '>', $dateFrom);
                }
            })
            ->get([
                'request_on_trade_in.created_at as created',
                'client_id',
                'phone',
                'request_on_trade_in.name as first_name',
                'user_ip as ip',
                'brands.name as car_brand_name',
                'models.name as car_model_name',
                'modification_name as car_modification_name',
                'year_of_issue as car_year',
                'color as car_color',
                'price as car_cost',
                'additional_params',
            ])
            ->map(function ($row) {

                $avito_brand_id = null;
                $avito_model_id = null;

                $avito = DB::table('avito_brand')
                    ->select('avito_model.*')
                    ->join('avito_model', 'avito_brand.brand_id', '=', 'avito_model.brand_id')
                    ->where('avito_brand.name', 'ilike', $row->car_brand_name)
                    ->where('avito_model.name', 'ilike', $row->car_model_name)
                    ->first();
                if ($avito) {
                    $avito_brand_id = $avito->brand_id;
                    $avito_model_id = $avito->model_id;
                }

                $row->make_id = $avito_brand_id;
                $row->model_id = $avito_model_id;

                $row->last_name = null;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->age = null;
                $row->deal_type = "credit";
                $row->car_complete_name = null;
                $row->car_condition = 'used';
                $row->car_link = route('advert', ['advert' => $row->client_id]);
                $row->registration_fias_id = null;
                $row->is_trade_in = true;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;

                $advert = Advert::where(['is_current' => true, 'client_id' => $row->client_id])->first();
                if($advert) {
                    $row->price_by_action = $advert->priceByAction();
                } else {
                    $row->price_by_action = 0;
                }

                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }

                unset($row->price_by_action, $row->client_id, $row->additional_params);

                return $row;
            })
            ->toArray();


        $dataBuys = RequestOnBuy::query()
            ->join('brands', 'brands.id', '=', 'request_on_buys.client_brand')
            ->join('models', 'models.id', '=', 'request_on_buys.client_model')
            ->where(function ($query) use ($request, $dateFrom) {
                if ($request->filled('from')) {
                    $query->where('request_on_buys.created_at', '>', $dateFrom);
                }
            })
            ->get(['request_on_buys.created_at as created',
                'phone',
                'request_on_buys.name as first_name',
                'user_ip as ip',
                'brands.name as car_brand_name',
                'models.name as car_model_name',
                'additional_params',
            ])
            ->map(function ($row) {

                $avito_brand_id = null;
                $avito_model_id = null;

                $avito = DB::table('avito_brand')
                    ->select('avito_model.*')
                    ->join('avito_model', 'avito_brand.brand_id', '=', 'avito_model.brand_id')
                    ->where('avito_brand.name', 'ilike', $row->car_brand_name)
                    ->where('avito_model.name', 'ilike', $row->car_model_name)
                    ->first();
                if ($avito) {
                    $avito_brand_id = $avito->brand_id;
                    $avito_model_id = $avito->model_id;
                }

                $row->make_id = $avito_brand_id;
                $row->model_id = $avito_model_id;

                $row->last_name = null;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->age = null;
                $row->deal_type = "cash";
                $row->car_condition = 'used';
                $row->car_complete_name = null;
                $row->registration_fias_id = null;
                $row->is_trade_in = false;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;

                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }

                unset($row->client_id);

                return $row;
            })
            ->toArray();

        $dataContacts = RequestInContact::query()
            ->where(function ($query) use ($request, $dateFrom) {
                if ($request->filled('from')) {
                    $query->where('created_at', '>', $dateFrom);
                }
            })
            ->get(['created_at as created',
                'phone',
                'name as first_name',
                'user_ip as ip',
                'additional_params',
            ])
            ->map(function ($row) {
                $row->last_name = null;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->age = null;
                $row->car_condition = 'used';
                $row->registration_fias_id = null;
                $row->is_trade_in = false;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;

                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }
                unset($row->additional_params);
                return $row;
            })
            ->toArray();


        $dataPromos = RequestOnPromo::query()
            ->join('promos', 'promos.id', '=', 'promo_id')
            ->where(function ($query) use ($request, $dateFrom) {
                if ($request->filled('from')) {
                    $query->where('request_on_promos.created_at', '>', $dateFrom);
                }
            })
            ->get(['request_on_promos.created_at as created',
                'phone',
                'request_on_promos.name as first_name',
                'user_ip as ip',
                'additional_params',
            ])
            ->map(function ($row) {
                $row->last_name = null;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->car_condition = 'used';
                $row->age = null;
                $row->registration_fias_id = null;
                $row->is_trade_in = false;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;
                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }
                unset($row->price_by_action, $row->client_id, $row->additional_params);

                return $row;
            })
            ->toArray();

        $dataExchanges = RequestOnExchange::query()
            ->join('adverts', 'adverts.id', 'request_on_exchanges.advert_id')
            ->join('brands', 'brands.id', '=', 'adverts.brand_id')
            ->join('models', 'models.id', '=', 'adverts.model_id')
            ->where(function ($query) use ($request, $dateFrom) {
                if ($request->filled('from')) {
                    $query->where('request_on_exchanges.created_at', '>', $dateFrom);
                }
            })
            ->get([
                'request_on_exchanges.name as first_name',
                'phone',
                'user_ip as ip_address',
                'additional_params',
                'request_on_exchanges.created_at as created',
                'adverts.client_id',

                'brands.name as car_brand_name',
                'models.name as car_model_name',
                'modification_name as car_modification_name',
                'year_of_issue as car_year',
                'color as car_color',
                'price as car_cost',
            ])
            ->map(function ($row) {
                $row->last_name = $row->name;
                $row->middle_name = null;
                $row->birthdate = null;
                $row->deal_type = "cash";
                $row->car_condition = 'used';
                $row->car_complete_name = null;
                $row->age = null;
                $row->registration_fias_id = null;
                $row->is_trade_in = false;
                $row->utm_source = $row->additional_params['utm_source'] ?? null;
                $row->utm_medium = $row->additional_params['utm_medium'] ?? null;
                $row->utm_campaign = $row->additional_params['utm_campaign'] ?? null;
                $row->utm_term = $row->additional_params['utm_term'] ?? null;
                $row->utm_content = $row->additional_params['utm_content'] ?? null;
                $row->referer = $row->additional_params['referer'] ?? null;
                $row->car_link = route('advert', ['advert' => $row->client_id]);
                if (!empty($row->phone)) {
                    $row->phone = '7' . preg_replace("/[^0-9.]/", "", $row->phone);
                }

                unset($row->down_payment, $row->client_id, $row->price_by_action, $row->additional_params);

                return $row;
            })
            ->toArray();

        // return response()->json(array_values($dataCredits));
        return response()->json(array_values(array_merge($dataCredits, $dataTradein, $dataBuys, $dataContacts, $dataPromos, $dataExchanges)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
