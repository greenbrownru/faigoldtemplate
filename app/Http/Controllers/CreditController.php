<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\AdvertPhoto;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\Generation;
use App\Models\RequestOnCredit;
use App\Repository\AdvertsRepository;
use App\Rules\CreditPhone;
use App\Services\RequestFormService;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CreditController extends Controller
{

    private $repository;

    public function __construct(AdvertsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @param Advert|null $advert
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, Advert $advert = null)
    {
        abort_unless(setting('page__credit_enable'), 404);

        $brands = Advert::getBrandsByExistedAdverts();
        if (!$advert) {
            $adverts = null;
            $models = null;

        } else {
            $adverts = Advert::where(['brand_id' => $advert->brand_id, 'model_id' => $advert->model_id])->current()->get();
            $models = Advert::getModelsByBrandAndExistingAdverts($advert->brand_id);
        }

        $data = [
            'item' => $advert,
            'adverts' => $adverts,
            'brands' => $brands,
            'models' => $models,
        ];
        return view('credit.index', $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxModels(Request $request)
    {
        $models = Advert::getModelsByBrandAndExistingAdverts($request->brand_id);
        return $models->toJson();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxModelsFilter(Request $request)
    {
        $models = Advert::getModelsByBrandAndExistingAdverts($request->brand_id);
        return $models->toJson();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxGenerationsFilter(Request $request)
    {

        $gens = Generation::selectRaw("id, name || ' (' || year_from || '-' || year_to || ')' as name")
            ->where(['model_id' => $request->model_id])
            ->whereIn('generations.id', Advert::query()->select('generation_id')
                ->where(['model_id' => $request->model_id])
                ->groupBy('generation_id')
                ->pluck('generation_id')
                ->toArray()
            )->orderBy('name')->get();

        return $gens->toJson();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxAdverts(Request $request)
    {
        $adverts = Advert::where(['model_id' => $request->model_id])
            ->active()
            ->current()
            //->latest()
            ->priceAsc()
            ->when(!empty(env('APP_SITE_REGION')), function($adverts){
                return $adverts->where('adverts.region_id', '=', env('APP_SITE_REGION'));
            })
            ->get('*')->map(function ($row) {
                $advert = Advert::where(['is_current' => true, 'client_id' => $row->client_id])->first();
                if($advert) {
                    $row->price_by_action = $advert->priceByAction();
                }
                return $row;
            });;
        return $adverts->toJson();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ajaxAdvertImage(Request $request)
    {
        $advertPhoto = AdvertPhoto::getFirstByAdvert($request->advert_id);
        return $advertPhoto->toJson();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestAdd(Request $request)
    {
        $request->request->add([
                'brand_id' => $request->credit_brand,
                'model_id' => $request->credit_model,
                'advert_id' => $request->credit_advert,
                'credit_term' => floatval($request->credit_term),
                'down_payment' => ($request->car_cost/100)*floatval($request->down_payment),
            ]
        );

        $this->validate($request, [
            'brand_id' => 'required',
            'model_id' => 'required',
            'name' => 'required',
            'advert_id' => 'required',
            'phone' => 'required',
            'car_cost' => 'nullable|string',
        ]);

        $phone_validator = Validator::make($request->all(), [
            'phone' => new CreditPhone()
        ]);

        if ($phone_validator->fails()) {
            return 0;
        }

        $data = $request->all();
        $requestOnCredit = new RequestOnCredit();

        $requestForm = new RequestFormService($requestOnCredit, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestOnCredit->fill($requestForm->getData());
        $requestOnCredit->save();
        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }

    public function modal(Request $request, Advert $item)
    {
        return view('credit._modal_form', ['item' => $item]);
    }


}
