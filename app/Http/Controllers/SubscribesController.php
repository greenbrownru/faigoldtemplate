<?php

namespace App\Http\Controllers;

use App\Mail\SubscribeApprove;
use App\Mail\Unsubscribe;
use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\Subscribe;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscribesController extends Controller
{

    public function index(Request $request)
    {
        $brands = Advert::getBrandsByExistedAdverts();

        $data = [
            'brands' => $brands,
        ];
        return view('subscribes.index', $data);
    }

    public function subscribe(Request $request)
    {
        $subscribeExists = Subscribe::query()
            ->where(['email' => $request->email])
            ->where('region_id', '=', env('APP_SITE_REGION'))
            ->first();
        if($subscribeExists) {
            return response()->json(['status' => 'error', 'message' => 'Такой email уже есть в базе!']);
        }

        $subscribe = new Subscribe();
        $request->request->add([
            'token' => uniqid(),
            'user_ip' => $_SERVER['REMOTE_ADDR'],
        ]);
        $subscribe->fill($request->all());
        $subscribe->save();

        $mails_to = [];
        array_push($mails_to, $subscribe->email);
        Mail::to($mails_to)
            ->send(new SubscribeApprove($subscribe));

        return response()->json(['status' => 'success', 'message' => '']);

    }

    public function approve(Request $request)
    {
        $subscribe = Subscribe::query()->where(['token' => $request->token, 'approved' => false])->firstOrFail();
        $subscribe->approved = true;
        $subscribe->save();
        return view('subscribes.approve');
    }

    public function unsubscribe(Request $request)
    {
        if($request->method() == 'POST') {
            $subscribe = Subscribe::query()->where(['email' => $request->email, 'approved' => true])->firstOrFail();
            $mails_to = [];
            array_push($mails_to, $request->email);
            Mail::to($mails_to)
                ->send(new Unsubscribe($subscribe));
            return view('subscribes.unsubscribe_check_email');
        }
        return view('subscribes.unsubscribe');
    }

    public function unsubscribeApprove(Request $request)
    {
        $subscribe = Subscribe::query()->where(['token' => $request->token, 'approved' => true])->firstOrFail();
        $subscribe->delete();
        return view('subscribes.unsubscribe_approve');
    }

}
