<?php

namespace App\Http\Controllers;

use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use App\Models\RequestOnCredit;
use App\Models\RequestOnBuy;
use App\Rules\CreditPhone;
use App\Services\RequestFormService;
use App\Services\ToBitrixRequestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BuyController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        abort_unless(setting('page__buy_enable'), 404);

        //$brandsClient = Brand::orderBy('name')->get();
        $brandsClient = Brand::selectRaw('brands.id, brands.name')
            ->leftjoin('models', 'models.brand_id', '=', 'brands.id')
            ->distinct('brands.name')
            ->whereNotNull('models.id')
            ->orderBy('name')
            ->get();

        $data = [
            'brandsClient' => $brandsClient,
        ];
        return view('buy.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function requestAdd(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'client_brand' => 'required',
            'client_model' => 'required',
        ]);

        $phone_validator = Validator::make($request->all(), [
            'phone' => new CreditPhone()
        ]);

        if ($phone_validator->fails()) {
            return 0;
        }

        $requestOnBuy = new RequestOnBuy();

        $requestForm = new RequestFormService($requestOnBuy, $data);
        if (! is_null($requestFormBefore = $requestForm->before())) {
            if ($requestFormBefore === false) {
                return 0;
            }

            return $requestFormBefore;
        }

        $requestOnBuy->fill($requestForm->getData());
        $requestOnBuy->save();
        $requestForm->after();

        Session::flash('message', 'Спасибо, ваша заявка принята');
        return redirect()->back();
    }


}
