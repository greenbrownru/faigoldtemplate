<?php

namespace App\Console\Commands;

use App\Models\Advert;
use App\Models\Brand;
use Illuminate\Console\Command;

class setFeedAdvertsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yandex:set_feed
                            {--brand_name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        if (env('IS_ROLF_IMPORT')) {
            $advertsQuery = Advert::query()->select('adverts.*')->where('price', '>', 0);
        } else {
            if (env('APP_SITE_REGION') == '') {
                $advertsQuery = Advert::query()->select('adverts.*');
            } else {
                $advertsQuery = Advert::query()->select('adverts.*')->where(['region_id' => env('APP_SITE_REGION')]);
            }
        }

        if($this->option('brand_name')) {
            $brand_name = $this->option('brand_name');
            if($brand_name === 'lada') {
                if(!Brand::query()->where('name', 'ilike', $brand_name)->exists()) {
                    $brand_name = 'LADA (ВАЗ)';
                }
            }
            $brand = Brand::query()->where('name', 'ilike', $brand_name)->first();
            if(!$brand) {
                $this->error("brand with name '{$brand_name}' not found");
                exit(0);
            }
            $advertsQuery->where(['brand_id' =>$brand->id]);
        }

        $cars = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><cars>";

        if($this->option('brand_name')) {
            $feed_path =  base_path() .'/public/'.$this->option('brand_name').'.xml';
        } else {
            $feed_path = base_path() .'/public/yandex.xml';
        }

        if(file_exists($feed_path)) {
            unlink($feed_path);
        }

        $feed = fopen($feed_path, 'a+');

        fwrite($feed, $cars);

        $advertsQuery->photo()->current()->active()->chunk(500, function ($adverts) use (&$feed) {
            $car = '';
            foreach ($adverts as $advert) {
                $images = '';
                foreach ($advert->photos as $photo) {
                    $images .= '<image>' . $photo->imagepath . '</image>';
                }

                $address = $advert->region_id == 63 ? 'г. Самара, Южное шоссе, 20' : 'г. Екатеринбург, Базовый переулок, 38';
                if ($advert->region_id == 23) {
                    $address = '';
                }

                if ($address) {
                    $address = "<poi_id>{$address}</poi_id>";
                }


//            $phone = $advert->region_id == 63 ? '+7 (846) 300-26-10' : '+7(343) 226-06-65';
                $phone = showPhone();

                $equipments = [];

                foreach ($advert->equipments as $equipment) {
                    $equipments[] = trim($equipment->equipment->name) ?? '';
                }

                $extras = '';
                $doors = '';
                $car_drive = '';

                if (env('SITE_SUFFIX') == 'autocentermsk') {
                    $doors_res = $this->getDoors($advert);
                    if ($doors_res !== 777) {
                        $doors = $doors_res . ' дв.';
                    }

                    $extras = '<extras>' . implode(', ', $equipments) . '</extras>';
                    $car_drive = '<drive>' . $advert->carDriveLabel . '</drive>';
                }

                $price = "<price>{$advert->priceByAction()}</price>";

                if (!$advert->modification_name) {
                    $advert->modification_name = $this->makeModificationName($advert);
                }

                if (empty($price)) {
                    $oldprice = "<price>{$advert->price}</price>";
                } else {
                    $oldprice = "<oldprice>{$advert->price}</oldprice>";
                }

                // <modification_id>1.4d AT (68 л.с.)</modification_id>

                $car .= "<car>
                    <mark_id>{$advert->brand->name}</mark_id>
                    <folder_id>{$advert->amodel->name}</folder_id>
                    <modification_id>{$advert->modification_name}</modification_id>
                    <url>{$advert->PathWithDomain}</url>
                    <body_type>{$advert->carBodyLabel} {$doors}</body_type>
                    {$car_drive}
                    <wheel>{$advert->steeringWheelLabel}</wheel>
                    <color>" . __($advert->color) . "</color>
                    <metallic>да</metallic>
                    <availability>1</availability>
                    <custom>растаможен</custom>
                    <state>не требует ремонта</state>
                    <owners_number>владельцев {$advert->number_of_owners}</owners_number>
                    <run>{$advert->mileage}</run>
                    <year>{$advert->year_of_issue}</year>
                    {$price}
                    <currency>RUR</currency>
                    <registry_year>{$advert->year_of_issue}</registry_year>
                    <vin>{$advert->vin}</vin>
                    {$extras}
                    <unique_id>{$advert->client_id}</unique_id>
                    <images>
                    {$images}
                    </images>
                    <sale_services>special</sale_services>
                    {$address}
                    <contact_info>
                    <contact>
                    <name>Иван</name>
                    <phone>{$phone}</phone>
                    <time>09:00-21:00</time>
                    </contact>
                    </contact_info>
                    </car>";
            }
            fwrite($feed, $car);
        });
        fwrite($feed,"</cars></data>");
        fclose($feed);

        setting([
            'yandex__feed' => array_replace_recursive(setting('yandex__feed') ?: [], [
                $this->option('brand_name') ?: 'all' => [
                    'date' => now()->timestamp
                ]
            ])
        ])->save();
    }

    private function getDoors(Advert $advert)
    {
        $doors = 4;

        if (preg_match('/\d{1} дв\./', $advert->car_body, $matches) == 1) {
            return 777;
        }

        switch ($advert->car_body) {
            case 'Седан':
            case 'sedan':
                return 4;
            case 'Универсал':
            case 'universal':
            case 'Внедорожник':
            case 'hatchback':
            case 'Хэтчбек':
            case 'Лифтбек':
            case 'liftback':
            case 'Компактвэн':
            case 'suv':
                return 5;
            case 'Купе':
                return 2;

        }
        return $doors;
    }

    private function makeModificationName(Advert $advert): string
    {
        $transmission = '';
        switch ($advert->transmission) {
            case 'manual':
            case 'Механическая':
            case 'Вариатор':
                $transmission = 'MT';
                break;
            case 'Автоматическая':
            case 'automatic':
                $transmission = 'AT';
                break;
            case 'robotized':
            case 'Робот':
                $transmission = 'AMT';
                break;
        }

        return  number_format((float)$advert->volume / 1000, 1, '.', '') . ' ' . $transmission . ' ('.$advert->power.' л.с.)';
    }
}
