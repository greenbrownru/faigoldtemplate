<?php

namespace App\Console\Commands;

use App\Mail\SubscribesAdverts;
use App\Models\Advert;
use App\Models\Brand;
use App\Models\Subscribe;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class SubscribesSendCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscribes:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscribes = Subscribe::query()->where(['approved' => true])->get();

        if(!$subscribes) exit(0);

        foreach ($subscribes as $subscribe) {
            $brand = Brand::find($subscribe->brand);
            if(!$brand) continue;
            $date = date('Y-m-d H:i:00', strtotime('-1 hour'));
            $adverts = Advert::query()->where(['brand_id' => $brand->id])->current()
                ->whereBetween('year_of_issue', [$subscribe->year_from, $subscribe->year_to])
                ->whereBetween('price', [$subscribe->price_from, $subscribe->price_to])
                ->whereRaw("created_at > '{$date}'")
            ->get();
            if(!$adverts) continue;

            $mails_to = [];
            array_push($mails_to, $subscribe->email);
            Mail::to($mails_to)
                ->send(new SubscribesAdverts($adverts, $subscribe));

        }


    }
}
