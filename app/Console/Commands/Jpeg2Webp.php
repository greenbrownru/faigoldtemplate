<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class Jpeg2Webp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:jpeg2webp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Конвертирует все картинки из jpeg в webp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = Storage::disk('public')->files('result');
        $bar = $this->output->createProgressBar(count($files));
        $bar->start();
        foreach($files as $file){
            $pathinfo = pathinfo($file);
            $conv_file = 'result/webp/' . $pathinfo['filename'] . '.webp';

            if(!Storage::disk('public')->exists($conv_file)){
                try
                {
                    $img = Image::make(Storage::disk('public')->path($file));
                    $img->save(Storage::disk('public')->path($conv_file), 100, 'webp');
                }catch (NotReadableException $e){
                    continue;
                }
            }

            $bar->advance();
        }
        $bar->finish();
        return 0;
    }
}
