<?php

namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;

class CatalogPriceDecrease extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catalog:price_decrease';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проставление уменьшенной цены в акцию';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $adverts = Advert::where('price', '>', 0)->get();

        foreach($adverts as $advert){
            $advert->price_by_action = $advert->price * 0.7;
            $advert->save();
        }



        return 0;
    }
}
