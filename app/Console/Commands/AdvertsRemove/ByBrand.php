<?php

namespace App\Console\Commands\AdvertsRemove;

use App\Models\Advert;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ByBrand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adverts:remove_by_brand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаление объявлений по бренду';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $strings = file(__DIR__ . '/Assets/removebybrand.csv');

        foreach($strings as $brand_name) {
            $adverts = DB::table('adverts')
                ->join('brands', 'brands.id', '=', 'adverts.brand_id')
                ->where('brands.name', '=', trim($brand_name))
                ->select('brands.name AS brand_name', 'brands.id AS brand_id', 'adverts.id AS advert_id')
                ->get();

            $this->info('Удаление бренда ' . trim($brand_name));

            if(!empty($adverts)){
                foreach($adverts as $advert)
                {
                    $current = Advert::where('id', $advert->advert_id)->first();
                    $this->info("Работаю с объявлением {$current->id}");

                    //Фотки
                    $photos = DB::table('advert_photos')->where('advert_id', $current->id)->get();
                    foreach($photos as $photo){
                        //Удаляем фотки
                        $photo_path = 'photos/' . $photo->filename;
                        if(Storage::disk('public')->exists($photo_path)){
                            Storage::disk('public')->delete($photo_path);
                            $this->line("Картинка {$photo_path} удалена");
                        }
                        //Удаляем запись о картинках
                        $this->line("Запись о картинке удалена");
                        //$photo->delete();
                        DB::table('advert_photos')->delete($photo->id);

                    }

                    //Оборудование
                    $equipments = DB::table('advert_equipments')->where('advert_id', $current->id)->get();
                    foreach($equipments as $equip){
                        //$equip->delete();
                        DB::table('advert_equipments')->delete($equip->id);
                        $this->line("Запись о оборудовании {$equip->equipment_id} удалена");
                    }

                    //Удаляем объявление
                    $current->delete();
                    $this->line("Объявление удалено");
                }
            }

        }

        return 0;
    }
}
