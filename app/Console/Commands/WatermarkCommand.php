<?php

namespace App\Console\Commands;

use App\Jobs\WatermarkPhoto;
use App\Models\Advert;
use App\Models\AdvertPhoto;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WatermarkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watermark:add_for_all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Advert::query()->select('advert_photos.filename')
                     ->join('advert_photos', 'advert_photos.advert_id', '=', 'adverts.id')
                     ->where(['is_active' => true])
                     ->orderBy('adverts.id')
                     ->get()->toArray() as $item) {

            WatermarkPhoto::dispatch($item)->onQueue('default');
        }
    }
}
