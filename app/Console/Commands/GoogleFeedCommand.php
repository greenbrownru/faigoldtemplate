<?php

namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;

class GoogleFeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google:make_feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        if(env('IS_ROLF_IMPORT')) {
            $advertsQuery = Advert::query()->select('adverts.*')->where('price', '>', 0)->current()->active();
        } else {
            if(env('APP_SITE_REGION') == '') {
                $advertsQuery = Advert::query()->select('adverts.*')->current()->active();
            } else {
                $advertsQuery = Advert::query()->select('adverts.*')->where(['region_id' => env('APP_SITE_REGION')])->current()->active();
            }
        }

        $fp = fopen(public_path().'/google.csv', 'w');

        fputcsv($fp, ['ID', 'Item title', 'Final URL', 'Image URL', 'Item category', 'Price']);

        $advertsQuery->chunk(500, function ($adverts) use (&$fp) {

            foreach ($adverts as $advert) {
                $images = '';
                $imageQuery = $advert->photos()->first();
                if($imageQuery) {
                    $images = $imageQuery->imagepath;
                }

                $price = (intval($advert->priceByAction()) < $advert->price) ? $advert->priceByAction() ." RUB" : " RUB";

                fputcsv($fp, [$advert->client_id, $advert->brand->name .' '. $advert->amodel->name, url()->current().'/adverts/'.$advert->client_id, $images, $advert->amodel->name, $price]);
            }
        });

        fclose($fp);
    }
}
