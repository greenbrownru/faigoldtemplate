<?php

namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;

class CatalogFilter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catalog:filter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Скрывает объявления у которых цена меньше 300к, год выпуска позже 2006';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $adverts = Advert::where('is_active', true)->get();

        foreach($adverts as $advert){
            if($advert->price < 300000 || $advert->year_of_issue <= 2006){
                $advert->is_active = false;
                $advert->save();
            }
        }

        return 0;
    }
}
