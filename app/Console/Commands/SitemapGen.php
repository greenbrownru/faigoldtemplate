<?php

namespace App\Console\Commands;

use App\Models\Advert;
use App\Models\Promo;
use App\Repository\CatalogRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SitemapGen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tools:sitemap_generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация sitemap.xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    private function get_sitemap_header()
    {
$string = <<<STRING
<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
STRING;
        return $string;
    }

    private function get_sitemap_footer()
    {
        return '</urlset>';
    }

    private function get_url_instance($url, $lastmod = '', $changefreq = '', $priority = '')
    {
$instance = <<<INSTANCE
        <url>
            <loc>$url</loc>
            <lastmod>$lastmod</lastmod>
            <changefreq>$changefreq</changefreq>
            <priority>$priority</priority>
        </url>
INSTANCE;

        return $instance;
    }

    private function generate_url($base)
    {
        return env('APP_URL') . '/' . mb_strtolower($base) . '/';
    }

    public function handle()
    {
        $filename = 'sitemap.xml';
        $repository = new CatalogRepository();
        if(Storage::disk('public')->exists($filename)){
            Storage::disk('public')->delete($filename);
        }

        Storage::disk('public')->append($filename, $this->get_sitemap_header());

        //Текстовые страницы
        $text_pages = [
            'catalog',
            'credit',
            'trade-in',
            'buy',
            'promo',
            'contacts'
        ];

        if(in_array(env('APP_SITE_REGION'), [63,66,52])){
            $text_pages = array_merge($text_pages, ['about']);
        }

        Storage::disk('public')->append($filename, $this->get_url_instance(env('APP_URL') . '/'));
        foreach($text_pages as $text){
            $text_url = $this->generate_url($text);
            Storage::disk('public')->append($filename, $this->get_url_instance($text_url));
        }

        //Акции
        $promos = Promo::query()->select('promos.id')->where('region_id', '=', env('APP_SITE_REGION'))->where('is_active', '=', true)->get()->toArray();
        foreach($promos as $promo){
            $promo_url = $this->generate_url('promos/' . $promo['id']);
            Storage::disk('public')->append($filename, $this->get_url_instance($promo_url));
        }

        //Модели и бренды
        $models = $repository->getModelsLinks()->get()->toArray();

        $modelsLinksArray = [];
        foreach ($models as $i => $link) {
            $modelsLinksArray[$link['brand_name']][] = ['name' => $link['name'], 'model_id' => $link['id'], 'cnt' => $link['count']];
        }

        foreach($modelsLinksArray as $brand_name => $brand){
            //Ссылка с брендом
            $brand_url = $this->generate_url($brand_name);

            Storage::disk('public')->append($filename, $this->get_url_instance($brand_url));

            foreach($brand as $model){
                //Ссылка с моделью
                $model_url = $this->generate_url($brand_name . '/' . $model['name']);
                Storage::disk('public')->append($filename, $this->get_url_instance($model_url));
            }
        }

        //Объявления
        $adverts = Advert::query()->select('adverts.client_id')
            ->when(env('APP_SITE_REGION') != 18, function($adverts){
                return $adverts->where('is_active', '=', true)
                    ->where('is_current', '=', true)
                    ->where('region_id', '=', env('APP_SITE_REGION'));
            })
            ->get()
            ->toArray();
        foreach($adverts as $advert){
            $advert_url = $this->generate_url('adverts/' . $advert['client_id']);
            Storage::disk('public')->append($filename, $this->get_url_instance($advert_url));
        }

        Storage::disk('public')->append($filename, $this->get_sitemap_footer());

        File::copy(storage_path() . '/app/public/' . $filename, public_path() . '/' . $filename);


        return 0;
    }


}
