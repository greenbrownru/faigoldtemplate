<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Jpeg2Tar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:jpeg2tar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Разбиваем картинки по папкам';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = Storage::disk('public')->files('jpg');
        $bar = $this->output->createProgressBar(count($files));
        $bar->start();

        $count = 1;
        $part = 1;
        foreach($files as $file){
            Storage::disk('public')->copy($file, 'parts/' . $part . '/' . basename($file));
            $count++;
            if($count == 32001){
                $part++;
                $count = 1;
            }
            $bar->advance();
        }
        $bar->finish();

        return 0;
    }
}
