<?php

namespace App\Console\Commands;

use App\Imports\AdvertsAutoRuImportXml;
use App\Imports\AdvertsImportRolfCsV;
use App\Imports\AdvertsImportXml;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class AdvetsImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adverts:import {--is_cron}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $request = new \Illuminate\Http\Request();

        $request->replace(['filename' => env('ADVERTS_IMPORT_FILE')]);

        if(env('APP_SITE_CITY_LOCATION') == 'chel' || env('APP_SITE_REGION') == 23) {
            if(env('IS_ROLF_IMPORT')) {
                if(!$this->option('is_cron')) {
                    (new AdvertsImportRolfCsV($request))->store();
                }

            } else {
                if(env('SITE_SUFFIX') == 'olimp-trade' || env('SITE_SUFFIX') == 'armavir') {
                    (new AdvertsImportXml($request))->store();
                } else {
                    (new AdvertsAutoRuImportXml($request))->store();
                }

            }

        } else {
            (new AdvertsImportXml($request))->store();
        }
    }
}
