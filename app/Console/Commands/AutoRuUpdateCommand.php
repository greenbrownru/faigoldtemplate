<?php

namespace App\Console\Commands;

use App\Models\Brand;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use XMLReader;

class AutoRuUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoru:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет данные базы авто с сайта auto.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Asia/Novosibirsk');
        $this->info('Ищем скачанный за сегодня');
        $tempfiles = scandir(sys_get_temp_dir());

        foreach ($tempfiles as $file) {
            if(preg_match('/'.date('Ymd').'(.+)_autoru.xml/i', $file)) {
                $tempFile = sys_get_temp_dir().'/'.$file;
                if(is_file($tempFile) && is_readable($tempFile)) {
                    $matches[] = $file;
                }
            };
        }

        if(!empty($matches)) {
            sort($matches);
            $tempFile = sys_get_temp_dir().'/'. $matches[0];
        } else {
            $this->info("Скачиваем Autocatalog.xml");

            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
            $file_path = 'http://api.auto.ru/catalog/cars/';

            $file_name = date('YmdHis') . '_autoru.xml';

            $file = file_get_contents($file_path, false, stream_context_create($arrContextOptions));

            $tempFile = tempnam(sys_get_temp_dir(), $file_name);
            file_put_contents($tempFile, $file);

        }

        if (!is_file($tempFile) || !is_readable($tempFile)) {
            Log::error("file" . $tempFile . " not found or not is readable");
        }

        $this->info("Начинаем парсить");
        $xml = new XMLReader();
        $xml->open($tempFile);

        $brands = DB::table('brands')
            ->select(['id', 'name'])
            ->get()
            ->toArray();

        $models = DB::table('models')
            ->select(['brand_id', 'name', 'id'])
            ->get()
            ->toArray();

        $generations = DB::table('generations')
            ->select(['id', 'name', 'model_id'])
            ->get()
            ->toArray();

        $brands_array = [];
        $brands_for_insert = [];

        $models_array = [];
        $models_for_insert = [];

        $generations_array = [];
        $generations_for_insert = [];

        foreach ($brands as $brand) {
            $brands_array[$brand->id] = $brand->name;
        }

        foreach ($models as $model) {
            $models_array[$model->id] = $model->name;
        }

        $models_name = [];
        $generations_name = [];

        while ($xml->read() && $xml->name != 'mark') {
            ;
        }

        while ($xml->name == 'mark') {

            $mark = new \SimpleXMLElement($xml->readOuterXML());
            $brand = (string)$mark->attributes()['id'];
            $brand_name = (string)$mark->attributes()['name'];

            if (!isset($brands_array[$brand]) && $brand !== "") {
                $brands_for_insert[] = [
                    'name' => $brand_name,
                    'id' => $brand,
                   // 'code' => (string) $mark->code,
                ];
            }

            foreach ($mark->folder as $models) {
                if($brand == "") continue;
                $model = (string) $models->attributes()['id'];
                if($model == "") continue;

                $model_name = (string)$models->attributes()['name'];
                $model_name_array = explode(', ', $model_name);

                $model_name_clean = $model_name_array[0];
                $generation_name = isset($model_name_array[1]) ? trim($model_name_array[1]) : 'I';
                $year_from = 0;
                $year_to = 0;

                if (!isset($models_array[$model])) {
                    if(!isset($models_name[$model_name_clean])) {
                        foreach ($models->modification as $modifications) {
                            $years = explode('-', trim((string)$modifications->years));
                            if ($years[1] == ' по н.в.') {
                                $years[1] = date('Y');
                            }

                            $years[0] = intval($years[0]);
                            $years[1] = intval($years[1]);

                            if($year_from == 0) {
                                $year_from = $years[0];
                            }
                            if($year_to == 0) {
                                $year_to = $years[1];
                            }

                            if($years[0] < $year_from) {
                                $year_from = $years[0];
                            }

                            if($years[1] > $year_to) {
                                $year_to = $years[1];
                            }
                        }

                        $models_for_insert[$model] = [
                            'name' => $model_name_clean,
                            'brand_id' => $brand,
                           // 'id' => $model,
                           // 'model_id' => (string) $models->model,
                            'year_from' => $year_from,
                            'year_to' => $year_to,
                        ];
                        $models_name[$model_name_clean] = $model_name_clean;
                    }

                }

                if(count($models_for_insert) >= 50) {

                    DB::table('models')->insert($models_for_insert);
                    $models_for_insert = [];
                }

                if($generation_name) {
                    if(!isset($generations_name[$generation_name])) {

                        $generations_for_insert[$model_name_clean.$generation_name] = [
                            'name' => $generation_name,
                            'brand_id' => $brand,
                            'model_id' => (string) $model,
                            'autoru_id' => (string) $models->generation,
                            'year_from' => $year_from,
                            'year_to' => $year_to,
                            'is_active' => true,
                            'category_id' => 1,
                            'model_name' => $model_name_clean,
                        ];
                        $generations_name[$model_name_clean.$generation_name] = $model_name_clean.$generation_name;
                    }
                }

                if(count($generations_for_insert) >= 50) {
                    DB::table('generations')->insert($generations_for_insert);
                    $generations_for_insert = [];
                }
            }

            if (count($brands_for_insert) >= 20) {
                DB::beginTransaction();
                try {
                    DB::table('brands')->insert($brands_for_insert);
                    DB::table('models')->insert($models_for_insert);
                    DB::table('generations')->insert($generations_for_insert);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error($e->getMessage());
                }

                $brands_for_insert = [];
                $models_for_insert = [];
                $generations_for_insert = [];
            }
            unset($mark);
            $xml->next('mark');
        }

        DB::beginTransaction();
        try {
            if(count($brands_for_insert) > 0){ DB::table('brands')->insert($brands_for_insert);}
            if(count($models_for_insert) > 0){ DB::table('models')->insert($models_for_insert);}
            if(count($generations_for_insert) > 0){ DB::table('generations')->insert($generations_for_insert);}
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
        }

        $this->comment("memory_get_usage() =" . memory_get_usage() / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_usage(true) =" . memory_get_usage(true) / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_peak_usage() =" . memory_get_peak_usage() / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_peak_usage(true) =" . memory_get_peak_usage(true) / 1024 / 1024 . "Mb\n");
    }
}
