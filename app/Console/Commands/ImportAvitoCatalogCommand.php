<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use XMLReader;

class ImportAvitoCatalogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:avito';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import and update avito-db by cars from avito';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->info('Ищем скачанный за сегодня');
        $tempfiles = scandir(sys_get_temp_dir());

        foreach ($tempfiles as $file) {
            if(preg_match('/'.date('Ymd').'(.+)_autocatalog.xml/i', $file)) {
                $tempFile = sys_get_temp_dir().'/'.$file;
                if(is_file($tempFile) && is_readable($tempFile)) {
                    $matches[] = $file;
                }
            }
        }

        if(!empty($matches)) {
            sort($matches);
            $tempFile = tempnam(sys_get_temp_dir(), last($matches));
        } else {
            $this->info("Скачиваем Autocatalog.xml");
            $file_path = 'http://autoload.avito.ru/format/Autocatalog.xml'; //'autoru_catalog.xml';
            $file_name = date('YmdHis') . '_autocatalog.xml';
            $tempFile = tempnam(sys_get_temp_dir(), $file_name);
            copy($file_path, $tempFile);
        }

        if (!is_file($tempFile) || !is_readable($tempFile)) {
           // dd('noo');
            Log::error("file" . $tempFile . " not found or not is readable");
        }

        //dd(filesize($tempFile), $tempFile);

        $this->info("Начинаем парсить");
        $xml = new XMLReader();
        $xml->open($tempFile);
        //$xml->open('/tmp/20210301_autocatalog.xml');

        $brands = DB::table('avito_brand')
            ->select(['brand_id', 'name'])
            ->get()
            ->toArray();

        $models = DB::table('avito_model')
            ->select(['brand_id', 'name', 'model_id'])
            ->get()
            ->toArray();

        $generations = DB::table('avito_generations')
            ->select(['generation_id', 'name', 'model_id'])
            ->get()
            ->toArray();

        $modifications = DB::table('avito_modifications')
            ->select(['modification_id', 'name', 'generation_id', 'model_id'])
            ->get()
            ->toArray();
        $complectations = DB::table('avito_complectations')
            ->select(['modification_id', 'name', 'complectation_id', 'model_id'])
            ->get()
            ->toArray();

        $brands_array = [];
        $brands_for_insert = [];

        $models_array = [];
        $models_for_insert = [];

        $generations_array = [];
        $generations_for_insert = [];

        $modifications_array = [];
        $modifications_for_insert = [];

        $complectations_array = [];
        $complectations_for_insert = [];

        foreach ($brands as $brand) {
            $brands_array[$brand->brand_id] = $brand->name;
        }

        foreach ($models as $model) {
            $models_array[$model->model_id] = $brand->name;
        }

        foreach ($generations as $generation) {
            $generations_array[$generation->generation_id] = $generation->name;
        }

        foreach ($modifications as $modification) {
            $modifications_array[$modification->modification_id] = $modification->name;
        }
        foreach ($complectations as $complectation) {
            $complectations_array[$complectation->complectation_id] = $complectation->name;
        }

        while ($xml->read() && $xml->name != 'Make') {
            ;
        }

        while ($xml->name == 'Make') {
            $mark = new \SimpleXMLElement($xml->readOuterXML());
            $brand = (string)$mark->attributes()['id'];
            $brand_name = (string)$mark->attributes()['name'];

            if (!isset($brands_array[$brand])) {
                $brands_for_insert[] = [
                    'name' => $brand_name,
                    'brand_id' => $brand
                ];
            }

            foreach ($mark->Model as $models) {
                $model = (string)$models->attributes()['id'];
                $model_name = (string)$models->attributes()['name'];
                if (!isset($models_array[$model])) {
                    $models_for_insert[] = [
                        'name' => $model_name,
                        'brand_id' => $brand,
                        'model_id' => $model
                    ];
                }

                foreach ($models->Generation as $generations) {
                    $generation = (string)$generations->attributes()['id'];
                    $generation_name = (string)$generations->attributes()['name'];
                    if (!isset($generations_array[$generation])) {
                        if (!isset($generations_for_insert[$generation])) {
                            $generations_for_insert[$generation] = [
                                'name' => $generation_name,
                                'generation_id' => $generation,
                                'model_id' => $model
                            ];
                        }
                    }

                    foreach ($generations->Modification as $modifications) {
                        $modification = (string)$modifications->attributes()['id'];
                        $modification_name = stripcslashes((string)$modifications->attributes()['name']);

                        if ($modification_name == "1.6 AT (105 л.с.) ТагАЗ \\") {
                            continue;
                        }
                        if (!isset($modifications_array[$modification])) {
                            if (!isset($modifications_for_insert[$modification])) {
                                $modifications_for_insert[$modification] = [
                                    'name' => $modification_name,
                                    'generation_id' => $generation,
                                    'model_id' => $model,
                                    'modification_id' => $modification
                                ];
                            }
                        }

                        if (isset($modifications->Complectations->Complectation)) {
                            foreach ($modifications->Complectations->Complectation as $complectations) {
                                $complectation = (string)$complectations->attributes()['id'];
                                $complectation_name = (string)$complectations->attributes()['name'];
                                $complectation_name = str_replace('"', "'", $complectation_name);

                                if (!isset($complectations_array[$complectation])) {
                                    if (!isset($complectations_for_insert[$complectation])) {
                                        $complectations_for_insert[$complectation] = [
                                            'name' => $complectation_name,
                                            'complectation_id' => $complectation,
                                            'modification_id' => $modification,
                                            'model_id' => $model
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (count($brands_for_insert) >= 20) {
                DB::beginTransaction();
                try {
                    DB::table('avito_brand')->insert($brands_for_insert);
                    DB::table('avito_model')->insert($models_for_insert);
                    DB::table('avito_generations')->insert($generations_for_insert);
                    DB::table('avito_modifications')->insert($modifications_for_insert);
                    DB::table('avito_complectations')->insert($complectations_for_insert);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error($e->getMessage());
                }

                $brands_for_insert = [];
                $models_for_insert = [];
                $generations_for_insert = [];
                $modifications_for_insert = [];
                $complectations_for_insert = [];
            }

            unset($mark);
            $xml->next('Make');
        }

        if (count($brands_for_insert) > 0) {
            DB::beginTransaction();
            try {
                DB::table('avito_brand')->insert($brands_for_insert);
                DB::table('avito_model')->insert($models_for_insert);
                DB::table('avito_generations')->insert($generations_for_insert);
                DB::table('avito_modifications')->insert($modifications_for_insert);
                DB::table('avito_complectations')->insert($complectations_for_insert);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error($e->getMessage());
            }
        }

        $this->comment("memory_get_usage() =" . memory_get_usage() / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_usage(true) =" . memory_get_usage(true) / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_peak_usage() =" . memory_get_peak_usage() / 1024 / 1024 . "Mb\n");
        $this->comment("memory_get_peak_usage(true) =" . memory_get_peak_usage(true) / 1024 / 1024 . "Mb\n");
        exit(0);
    }
}
