<?php

namespace App\Console\Commands;

use App\Models\Advert;
use Illuminate\Console\Command;

class setVKCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vk_feed:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //dd(public_path('target.xml'));

        $items = Advert::query()
            ->where(['is_current' => true])
            ->orderByDesc('adverts.id')
            //->take(50)
            ->get();

        $xml = '<?xml version="1.0" encoding="utf-8"?>
                    <yml_catalog_date="'.date('Y-m-d H:i').'">
                    <shop>
                        <name>'.env('APP_NAME').'</name>
                        <company>'.env('APP_NAME').'</company>
                        <url>'.env('APP_URL').'</url>
                        <currencies>
                            <currency id="RUR"/>
                        </currencies>
                        <categories>
                            <category id="1" parentId="0">Автомобили</category>
                        </categories>
                        <offers>';
        foreach ($items as $item) {

            if($item->photos()->count() > 0) {
                $imgName = storage_path('app/public/photos/'.$item->photos()->first()->filename);
                $newName = str_replace('.webp', '.jpeg', $imgName);
                if(!file_exists($newName)) {
                    // Create and save
                    $img = imagecreatefromwebp($imgName);
                    imagepalettetotruecolor($img);
                    imagealphablending($img, true);
                    imagesavealpha($img, true);
                    imagejpeg($img, $newName, 100);
                    imagedestroy($img);
                }

                $picture = "<picture>".str_replace('.webp', '.jpeg', $item->photos()->first()->getImagePathAttribute())."</picture>";
            }

            $xml .="<offer id=\"$item->client_id\" available=\"true\">
            <url>".env('APP_URL').'/adverts/'.$item->client_id."</url>
            <price>{$item->priceByAction()}</price>
            <oldprice>$item->price</oldprice>
            <currencyId>RUR</currencyId>
            <categoryId>1</categoryId>
            $picture
            <typePrefix>Автомобили</typePrefix>
            <vendor>{$item->brand->name}</vendor>
            <model>{$item->amodel->name}</model>
            <description>".str_replace(['⚡', '✅'], '', htmlspecialchars($item->description)) ."</description>
        </offer>";
        }

        $xml .= '</offers></shop></torg_price>';

        file_put_contents(public_path('vk.yml'), $xml);

    }
}
