<?php

namespace App\Console\Commands;

use App\Models\Advert;
use App\Models\AdvertPhoto;
use App\Models\Brand;
use App\Models\Equipment;
use App\Models\EquipmentGroup;
use App\Models\Generation;
use App\Models\MainModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdvertsExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adverts:export {brand}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Экспорт объявлений';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brand_name = $this->argument('brand');
        $models_file = 'export/models.data';
        $adverts_file = 'export/adverts.data';
        $equipments_file = 'export/equipments.data';
        $equipments_groups_file = 'export/equipments_groups.data';
        $generations_file = 'export/generations.data';

        //Проверяем есть ли такой бренд в БД
        if(Brand::where('name', $brand_name)->count() == 0){
            $this->error("Бренд {$brand_name} не найден");
            return 0;
        }

        //Удаляем файлы
        if(Storage::disk('public')->exists($models_file)){
            Storage::disk('public')->delete($models_file);
        }
        if(Storage::disk('public')->exists($adverts_file)){
            Storage::disk('public')->delete($adverts_file);
        }
        if(Storage::disk('public')->exists($equipments_file)){
            Storage::disk('public')->delete($equipments_file);
        }
        if(Storage::disk('public')->exists($equipments_groups_file)){
            Storage::disk('public')->delete($equipments_groups_file);
        }
        if(Storage::disk('public')->exists($generations_file)){
            Storage::disk('public')->delete($generations_file);
        }
        if(Storage::disk('public')->exists('export/photos')){
            Storage::disk('public')->deleteDirectory('export/photos');
        }


        $brand = Brand::where('name', $brand_name)->first();

        //Ищем модели связанные с брендом
        $models = DB::table('models')->where('brand_id', $brand->id)->get();
        $models_arr = [];
        foreach($models as $model){
            $model_arr = [
                'id' => $model->id,
                'name' => $model->name,
                'category_id' => $model->category_id,
                'generation_id' => $model->generation_id,
                'year_from' => $model->year_from,
                'year_to' => $model->year_to,
                'is_active' => $model->is_active,
            ];
            $models_arr[] = $model_arr;
        }

        Storage::disk('public')->append($models_file, serialize($models_arr));

        //Ищем объявления по бренду

        $adverts = Advert::where('brand_id', $brand->id)->get()->toArray();
        Storage::disk('public')->put($adverts_file, serialize($adverts));

        //Копируем фотки объявлений

        foreach($adverts as $advert){
            $photos = AdvertPhoto::where('advert_id', $advert['id'])->get();
            foreach($photos as $photo){
                Storage::disk('public')->copy('photos/' . $photo->filename, 'export/photos/' . $photo->filename . '.' . $photo->order);
            }
        }

        //Копируем табличку equipments
        $equipments = Equipment::get()->toArray();
        Storage::disk('public')->put($equipments_file, serialize($equipments));

        //Копируем табличку equipments_groups
        $equipments_groups = EquipmentGroup::get()->toArray();
        Storage::disk('public')->put($equipments_groups_file, serialize($equipments_groups));

        //Копируем табличку generations
        $generations = Generation::where('brand_id', $brand->id)->get()->toArray();
        Storage::disk('public')->put($generations_file, serialize($generations));


        return 0;

    }
}
