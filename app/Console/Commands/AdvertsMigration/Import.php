<?php

namespace App\Console\Commands\AdvertsMigration;

use App\Models\Advert;
use App\Models\AdvertEquipments;
use App\Models\AdvertPhoto;
use App\Models\Brand;
use App\Models\Equipment;
use App\Models\EquipmentGroup;
use App\Models\Generation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adverts:import_from_file {brand}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортирование объявлений из файлов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brand_id = $this->argument('brand');

        //Проверяем есть ли такой бренд в БД
        if(Brand::where('id', $brand_id)->count() == 0){
            $this->error("Бренд {$brand_id} не найден");
            return 0;
        }else{
            $brand = Brand::where('id', $brand_id)->first();
        }

        $models_file = 'export/models.data';
        $adverts_file = 'export/adverts.data';
        $equipments_file = 'export/equipments.data';
        $equipments_groups_file = 'export/equipments_groups.data';
        $advert_equipments_file = 'export/advert_equipments.data';
        $generations_file = 'export/generations.data';

        //Проверяем наличие файлов

        $adverts_arr = unserialize(Storage::disk('public')->get($adverts_file));
        $models_arr = unserialize(Storage::disk('public')->get($models_file));
        $generations_arr = unserialize(Storage::disk('public')->get($generations_file));
        $equipments_arr = unserialize(Storage::disk('public')->get($equipments_file));
        $equipments_groups_arr = unserialize(Storage::disk('public')->get($equipments_groups_file));
        $advert_equipments_arr = unserialize(Storage::disk('public')->get($advert_equipments_file));

        /*
        print_r($equipments_groups_arr);
        return 0;
        */



        foreach($adverts_arr as $advert)
        {
            $this->info("Работа с объявлением {$advert['client_id']}");

            //Применяем новый бренд
            $advert['brand_id'] = $brand->id;

            //Работаем с моделями
            foreach($models_arr as $model)
            {
                //Ищем модель в файле
                if($model['id'] == $advert['model_id']){

                    //Проверяем есть ли модель в БД
                    $count = DB::table('models')->where('brand_id', $brand->id)->where('name', $model['name'])->count();
                    if($count == 0){
                        $this->info("Создаю модель {$model['name']}");
                        unset($model['id']);
                        DB::table('models')->insert($model);
                    }

                    $current_model = DB::table('models')->where('brand_id', $brand->id)->where('name', $model['name'])->first();

                    $this->info("Для объявления найдена модель {$current_model->id} {$current_model->name}");

                    //Применили к объявлению новую модель
                    $advert['model_id'] = $current_model->id;

                    break;
                }
            }

            //Работаем с generations
            foreach($generations_arr as $generation)
            {
                if($generation['id'] == $advert['generation_id']){
                    //Проверяем есть ли generation d БД
                    $count = DB::table('generations')
                        ->where('brand_id', $brand->id)
                        ->where('name', $generation['name'])
                        ->where('model_name', $current_model->name)
                        ->count();
                    if($count == 0){
                        $this->info("Создаю generation {$generation['name']}");

                        unset($generation['id']);
                        $generation['brand_id'] = $brand->id;
                        $generation['model_name'] = $current_model->name;
                        $generation['model_id'] = $current_model->id;

                        Generation::create($generation);
                    }

                    $current_generation = Generation::where('brand_id', $brand->id)->where('name', $generation['name'])->where('model_name', $current_model->name)->first();
                    $this->info("Для объявления найден generation {$current_generation->id} {$current_generation->name}");

                    //Применили к объявлению новый generation
                    $advert['generation_id'] = $current_generation->id;

                    break;
                }
            }

            $new_advert = Advert::create($advert);
            $this->info("Создано объявление {$new_advert->id}");
            $this->line("Наполняю объявление");

            //Фотки
            $photos = Storage::disk('public')->files('export/photos');
            foreach($photos as $photo)
            {
                preg_match('/(\d+)_(.*)\.webp\.(\d+)/', basename($photo), $matches);

                if($matches[1] == $advert['id']){

                    $new_advert_id = $new_advert->id;
                    $new_filename = $new_advert_id . '_' . $matches[2] . '.webp';
                    $new_order = $matches[3];

                    $new_advert_photo = new AdvertPhoto();
                    $new_advert_photo->advert_id = $new_advert_id;
                    $new_advert_photo->filename = $new_filename;
                    $new_advert_photo->order = $new_order;
                    $new_advert_photo->region_id = 23;
                    $new_advert_photo->save();
                    Storage::disk('public')->copy($photo, 'photos/resize/' . $new_filename);
                    Storage::disk('public')->copy($photo, 'photos/' . $new_filename);

                    $this->line("Создана фотография {$new_filename}");
                }
            }

            //Equipments

            //Пробегаем массив equipments
            foreach($advert_equipments_arr as $equip){
                //Нашли equipments для объявления
                if($equip['advert_id'] == $advert['id']){
                    foreach($equipments_arr as $equip_type){
                        if($equip_type['id'] == $equip['equipment_id']){
                            if(Equipment::where('name', $equip_type['name'])->count() > 0){
                                //Значит в БД уже есть equipment c аналогичным названием и соотв. с типом
                                $new_equip_type = Equipment::where('name', $equip_type['name'])->first();
                            }else{
                                //Если нет - то надо создать
                                //Смотрим, что с группой

                                foreach($equipments_groups_arr as $equip_group){
                                    if($equip_group['id'] == $equip_type['group_id']){
                                        if(EquipmentGroup::where('name', $equip_group['name'])->count() > 0){
                                            //Значит такая группа есть
                                            $new_equip_group = EquipmentGroup::where('name', $equip_group['name'])->first();
                                        }else{
                                            //Если нет то надо создать
                                            unset($equip_group['id']);
                                            $new_equip_group = EquipmentGroup::create($equip_group);
                                            $this->info("Создана группа характеристик {$new_equip_group->name}");
                                        }
                                    }
                                }

                                unset($equip_type['id']);
                                $equip_type['group_id'] = $new_equip_group->id;

                                $new_equip_type = Equipment::create($equip_type);
                                $this->info("Создан тип характеристик {$new_equip_type->name}");


                            }
                        }
                    }

                    //После всех манипуляций добавляем equipment для объявления
                    unset($equip['id']);
                    $equip['equipment_id'] = $new_equip_type->id;
                    $equip['advert_id'] = $new_advert_id;

                    $new = AdvertEquipments::create($equip);
                    $this->info("Характеристика добвлена: {$new->id}");

                }
            }
        }

        return 0;
    }
}
