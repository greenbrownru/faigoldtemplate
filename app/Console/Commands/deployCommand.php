<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class deployCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $servers = require 'deploy_servers.php';

        $hosts = Arr::pluck($servers, 'host');

        $chosen_hosts = $this->choice(
            'Куда? (через запятую)',
            $hosts,
            implode(',', array_keys(array_filter($servers, function($v) {return empty($v['_test_']);}))),
            $maxAttempts = null,
            $allowMultipleSelections = true
        );

        $this->line('Выбраны: '.implode(', ', $chosen_hosts));

        $with_git_reset = $this->confirm('использовать - "git reset --hard"?', true);
        $with_git_clean = $this->confirm('использовать - "git clean -f"?', true);
        $with_composer = $this->confirm('использовать - "composer install"?', true);
        $with_migrate = $this->confirm('использовать - "php artisan migrate"?', true);

        $deploy_allow = $this->confirm('деплоим?', true);

        if(!$deploy_allow) {
            $this->comment('========== Деплой отменен ==============');
            exit();
        }

        $get_choosen_hosts_data = [];

        $i =0;
        foreach ($servers as $server) {
            if(array_key_exists($server['host'], array_flip($chosen_hosts))) {
                $get_choosen_hosts_data[] = $servers[$i];
            }
            $i++;
        }

        $commands = [];

        foreach ($get_choosen_hosts_data as $data) {
            $command = "cd ";
            $command .=  Arr::exists($data, 'project_path') && !empty($data['project_path']) ? $data['project_path'] : "/var/www/auto";
            if($with_git_reset) {
                $command .=" && git reset --hard";
            }
            if($with_git_clean) {
                $command .=" && git clean -f";
            }

            $command .= " && git pull";

            if($with_composer) {
                $command .=" && composer install";
            }
            if($with_migrate) {
                $command .=" && php artisan migrate";
            }

            $commands[] = [
                'host' => $data['host'],
                'ip' => $data['ip'],
                'login' => $data['login'],
                'password' => $data['password'],
                'command' => $command,
            ];
        }

        foreach ($commands as $deploy) {
            try {
                $connection = ssh2_connect($deploy['ip'], 22);
                ssh2_auth_password($connection, $deploy['login'], $deploy['password']);
                $this->info("===================== Выполняем для {$deploy['host']} ====================");
                $this->newLine(1);
                $stream = ssh2_exec($connection, $deploy['command']);
                stream_set_blocking($stream, true);
                $output = stream_get_contents($stream);
                echo $output;
                $this->newLine(3);
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
                exit();
            }
        }
    }
}
