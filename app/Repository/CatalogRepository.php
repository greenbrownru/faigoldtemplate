<?php


namespace App\Repository;


use App\Models\Advert;
use App\Models\AModel;
use App\Models\Generation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class CatalogRepository
{

    /**
     * @return mixed
     */
    public function getModelsLinks()
    {

        $model = AModel::selectRaw('models.id, brands.name as brand_name, models.brand_id, models.name, count(models.id)')
            ->join('adverts', 'adverts.model_id', '=', 'models.id')
            ->join('brands', 'adverts.brand_id', '=', 'brands.id')
            ->whereIn('adverts.brand_id', Advert::select('brand_id')->current()->active()->groupBy('brand_id')->get()->pluck('brand_id')->toArray())
            ->where('adverts.is_active', '=', true)
            ->where('adverts.is_current', '=', true)
            ->where('models.is_active', '=', true)
            ->when(env('APP_SITE_REGION') !== '' && env('APP_SITE_REGION') != 18, function($model) {
                $model->where(['adverts.region_id' => intval(env('APP_SITE_REGION'))]);
            })
            ->groupBy('models.id', 'models.brand_id', 'models.name', 'brands.name')
            ->orderBy('name');

        return $model;
    }

    /**
     * @param int $brandId
     * @return mixed
     */
    public function getModelsByBrand(int $brandId)
    {
        $adverts = AModel::selectRaw('models.id, models.brand_id, models.name')
            ->join('adverts', 'adverts.model_id', '=', 'models.id')
            ->where('adverts.brand_id', '=', $brandId)
            ->where('adverts.is_current', '=', true)
            ->where('adverts.is_active', '=', true)
            ->where('models.is_active', '=', true)
            ->groupBy('models.id', 'models.brand_id', 'models.name')
            ->orderBy('name');

        //$adverts->dump()->get();

        return $adverts->get();
    }

    /**
     * @param int $brandId
     * @param int $modelId
     * @return mixed
     */
    public function getGenerationsByParams(int $brandId, int $modelId)
    {
        return Generation::selectRaw('generations.id, generations.name')
            ->where(['generations.brand_id' => $brandId, 'generations.model_id' => $modelId])
            ->whereIn('generations.id', Advert::query()->select('generation_id')
                ->where(['brand_id' => $brandId, 'model_id' => $modelId])
                ->current()
                ->groupBy('generation_id')
                ->pluck('generation_id')
                ->toArray()
            )
            ->join('adverts', 'adverts.generation_id', '=', 'generations.id')
            ->where('adverts.is_active', '=', true)
            ->where('adverts.is_current', '=', true)
            ->groupBy('name', 'generation_id', 'generations.id')
            ->orderBy('name')->get();
    }

}
