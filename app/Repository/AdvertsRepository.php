<?php


namespace App\Repository;


use App\Models\Advert;
use App\Models\Brand;

class AdvertsRepository
{

    public function getSpec(Advert $advert)
    {
        return Advert::query()->current()->active()
            ->where('id', '<>', $advert->id)
            ->where('price', '>=', ($advert->price - 70000))
            ->where('price', '<=', ($advert->price + 70000))
            ->latest()
            ->limit(4)
            ->get();
    }



}
