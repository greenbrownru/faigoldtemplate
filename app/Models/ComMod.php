<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $mod_id
 * @property int $com_id
 * @property string $created_at
 * @property string $updated_at
 */
class ComMod extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'com_mod';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mod_id', 'com_id', 'created_at', 'updated_at'];

}
