<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BannerOnMain extends Model
{
    use LogsActivity;

    protected $table = 'banners_on_main';

    protected $fillable = ['title', 'options', 'link', 'is_button', 'button_name', 'button_action', 'img_url', 'img_mobile_url', 'sort'];

    protected $casts = [
        'options' => 'json'
    ];

    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
