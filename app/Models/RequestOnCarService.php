<?php

namespace App\Models;

use Spatie\Activitylog\Traits\LogsActivity;

class RequestOnCarService extends MainModel
{
    use LogsActivity;

    protected $fillable = ['brand_name', 'model_name', 'mileage', 'name', 'phone', 'email', 'region_id', 'additional_params', 'user_ip'];

    protected $casts = [
        'additional_params' => 'json'
    ];

    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
