<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Subscribe extends MainModel
{
    use LogsActivity;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscribes';


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'brand', 'year_from', 'year_to', 'price_from',
        'price_to', 'approved', 'token', 'region_id', 'user_ip',
    ];

    public function getBrandNameAttribute()
    {
        return $this->belongsTo(Brand::class, 'brand', 'id');
    }

    public function brandItem()
    {
        return $this->belongsTo(Brand::class, 'brand', 'id');
    }

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
