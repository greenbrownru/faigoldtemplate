<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\DB;

class FromPriceScope implements Scope
{
    /**
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model): void
    {
        if(in_array(env('SITE_SUFFIX'), ['orenburg_probeg', 'bu_in_orenburg', 'oren56_probeg',])) {
            $builder->where(DB::raw('price * ' .$model->getDiscount()), '>', 300000);
        }
    }
}
