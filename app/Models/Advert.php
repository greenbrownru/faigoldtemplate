<?php

namespace App\Models;

use App\Models\Scopes\FromPriceScope;
use App\Services\CreditService;
use App\Services\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Kyslik\ColumnSortable\Sortable;

/**
 * @property integer $id
 * @property integer $brand_id
 * @property integer $model_id
 * @property integer $generation_id
 * @property integer $modification_id
 * @property int $year_of_issue
 * @property int $mileage
 * @property integer $car_body
 * @property integer $transmission
 * @property integer $engine
 * @property string $steering_wheel
 * @property string $color
 * @property string $car_drive
 * @property string $modification_name
 * @property string $description
 * @property int $power
 * @property float $volume
 * @property int $number_of_owners
 * @property string $vin
 * @property float $price
 * @property float $price_by_action
 * @property boolean $vin_verified
 * @property boolean $kasko_in_gift
 * @property boolean $rubber_kit
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property int $client_id
 * @property float $tradein_discount
 * @property float $credit_discount
 * @property float $insurance_discount
 * @property int $import_id
 * @property boolean $is_current
 * @property int $region_id
 */
class Advert extends MainModel
{
    use Sortable;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = [
        'brand_id', 'model_id', 'generation_id', 'modification_id', 'modification_name', 'year_of_issue', 'mileage', 'car_body',
        'transmission', 'engine', 'steering_wheel', 'color', 'car_drive',
        'power', 'volume', 'number_of_owners', 'vin', 'price', 'price_by_action',
        'vin_verified', 'kasko_in_gift', 'rubber_kit', 'is_active',
        'is_vip', 'is_spec', 'created_at', 'updated_at', 'client_id',
        'tradein_discount', 'credit_discount', 'insurance_discount', 'import_id', 'is_current',
        'region_id', 'description',
    ];

    /**
     * @var array
     */
    protected array $sortable = [
        'brand_id', 'model_id', 'generation_id', 'modification_id', 'year_of_issue', 'mileage', 'car_body',
        'transmission', 'engine', 'color', 'car_drive',
        'power', 'volume', 'price', 'price_by_action',
    ];


    private array $carDrives = [
        'front',
        'rear',
        'full',
        // 'full_4wd'
    ];

    public function getPathWithDomainAttribute(): string
    {
        $domain = env('APP_URL');

        $sold = '';

        if($this->is_current == false) {
            $sold = '?sold';
        }

        return $domain . '/adverts/' . $this->client_id.$sold;
    }

    public function carDrives()
    {
        $labels = [
            'Передний',
            'Задний',
            'Полный',
            // 'Полный 4wd'
        ];

        return array_combine($this->carDrives, $labels);
    }

    public function getCarDriveLabelAttribute(): string
    {
        if ($this->car_drive == 'full_4wd') {
            $this->car_drive = 'full';
        }
        return $this->carDrives()[$this->car_drive] ?? $this->car_drive;
    }

    private array $steeringWheels = ['left', 'right'];

    public function steeringWheels()
    {
        $labels = ['Левый', 'Правый'];
        return array_combine($this->steeringWheels, $labels);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modification()
    {
        return $this->belongsTo(Modification::class);
    }

    /**
     * @return float|int
     */
    private function setDiscount()
    {
        if(in_array(env('SITE_SUFFIX'), ['adb'])) {
            return 0.85;
        }
        if(in_array(env('SITE_SUFFIX'), ['krasnoyarsk', 'kemerovo'])) {
            return 0.75;
        }
        if(in_array(env('SITE_SUFFIX'), ['autoexpert_rst'])) {
            return 0.8;
        }

        if(in_array(env('SITE_SUFFIX'), ['orenburg_probeg',])) {
            return 0.6;
        }

        if(in_array(env('SITE_SUFFIX'), ['23auto_expert', 'expert_krd', 'rostov', 'voronezh', 'ufa_expert', '52nn_expert', 'autoexpert73'])) {
            return 0.7;
        }
        if(in_array(env('SITE_SUFFIX'), ['by_v_vlg', 'by_vlg', 'probeg_v_spb'])) {
            return 0.65;
        }
        if(in_array(env('SITE_SUFFIX'), ['probeg_98', 'by_izhevsk'])) {
            return 0.55;
        }

        if(in_array(env('SITE_SUFFIX'), [
            'probeg_in_nsk', 'tmna', 'probeg_spb',
            'probeg_v_volgograde', 'autos_probeg98',
            'bu_in_orenburg', 'orenburg_expert', '74avtoexpert',
            'oren56_probeg',
        ])) {
            return 1;
        }

        return 0.9;
    }

    /**
     * @return float|int
     */
    public function getDiscount()
    {
        return $this->setDiscount();
    }

    /**
     * @return float
     */
    public function priceByAction(): float
    {
        $percent = (int) setting('advert__percent');
        return intval($this->price - ($this->price / 100 * $percent));
    }


    public function getSteeringWheelLabelAttribute(): string
    {
        return $this->steeringWheels()[$this->steering_wheel] ?? $this->steering_wheel;
    }

    private array $transmissions = ['automatic', 'robotized', 'variator', 'manual'];

    private array $transmissionLabels = ['Автоматическая', 'Робот', 'Вариатор', 'Механическая'];

    // suv - внедорожник, van - фургон
    private array $carBodies = [
        'sedan', 'hatchback', 'liftback', 'suv', 'coupe',
        'minivan', 'pickup', 'limousine', 'van', 'cabriolet', 'universal', 'compactvan',
        'fullmetal_van'
    ];

    private array $carBodyLabels = [
        'Седан', 'Хэтчбек', 'Лифтбек', 'Внедорожник', 'Купе', 'Минивен', 'Пикап', 'Лимузин',
        'Фургон', 'Кабриолет', 'Универсал', 'Компактвен',
        'Цельнометаллический фургон',
    ];

    private array $engines = ['petrol', 'diesel', 'hybrid', 'electro', 'gas'];

    private array $enginesLabels = ['Бензин', 'Дизель', 'Гибрид', 'Электро', 'Газ'];

    public function engines()
    {
        return array_combine($this->engines, $this->enginesLabels);
    }

    public function getEngineLabelAttribute()
    {
        return $this->engines()[$this->engine] ?? $this->engine;
    }

    public function transmissions()
    {
        return array_combine($this->transmissions, $this->transmissionLabels);
    }

    public function getTransmissionLabelAttribute()
    {
        return $this->transmissions()[$this->transmission] ?? $this->transmission;
    }

    public function carBodies()
    {
        return array_combine($this->carBodies, $this->carBodyLabels);
    }

    public function getCarBodyLabelAttribute()
    {
        return $this->carBodies()[$this->car_body] ?? $this->car_body;
    }

    public function photos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AdvertPhoto::class, 'advert_id', 'id')->orderBy('order');
    }

    public function brand(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    public function aModel(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AModel::class, 'model_id', 'id');
    }

    /**
     * @return string[]
     */
    public function getEngines(): array
    {
        return $this->engines;
    }

    /**
     * @return string[]
     */
    public function getCarBodies(): array
    {
        return $this->carBodies;
    }

    /**
     * @return string[]
     */
    public function getCarDrives(): array
    {
        return $this->carDrives;
    }

    /**
     * @return string[]
     */
    public function getSteeringWheels(): array
    {
        return $this->steeringWheels;
    }

    /**
     * @return string[]
     */
    public function getTransmissions(): array
    {
        return $this->transmissions();
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', '=', true);
    }

    public function scopeVip($query)
    {
        return $query->where('is_vip', '=', true);
    }

    public function scopeSpec($query)
    {
        return $query->where('is_spec', '=', true);
    }

    public function scopeCurrent($query)
    {
        return $query->where('is_current', '=', true);
    }

    public function scopePhoto($query)
    {
        return $query->join('advert_photos', 'advert_photos.advert_id', '=', 'adverts.id')->whereNotNull('advert_photos.id')->groupby("adverts.id");
    }

    public function scopeWithSoldOut($query, Request $request = null)
    {

        /*
         * select id, import_id from adverts where is_current = false
        and region_id = 63
        and client_id not in(select client_id from adverts where region_id = 63 and is_current = true)
         */
        $b = Advert::where('is_current', '=', false)
            ->selectRaw('distinct on(client_id) *')
            ->whereNotIn('client_id', Advert::query()
                ->selectRaw('client_id')
                ->where(['is_current' => true])
                ->pluck('client_id')
                ->toArray()
            );
        if ($request) {
            if ($request->filled('brand')) {
                $b->where(['adverts.brand_id' => $request->brand]);
            }

            if ($request->filled('model')) {
                $b->where(['adverts.model_id' => $request->model]);
            }

            if ($request->filled('generation')) {
                $b->where(['generation_id' => $request->generation]);
            }

            if ($request->filled('transmission')) {
                $b->where(['transmission' => $request->transmission]);
            }
        }
        return $query->union($b);
    }

    public function getCreditNameAttribute()
    {
        return $this->year_of_issue . ', ' . number_format($this->price_by_action, 0, ',', ' ') . ' ₽, ' . $this->transmissionLabel . ', ' . $this->mileage . ' км, владельцев ' . $this->number_of_owners;
    }

    function getBrandCountsByRegion($region = null)
    {
        if(!$region) {
            $result = collect(DB::select('select id, name, sum(cnt) as cnt from (
                            select brands.id, brands.name, count(brands.id) as cnt
                            from "brands"
                            inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                            where is_current = true
                            group by "brands"."id", "brands"."name"
                               union all
                            select id, name, count(client_id) as cnt from (
                                 select distinct on (client_id) client_id, brands.id, brands.name
                                 from "brands"
                                          inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                                 where is_current = false
                                   and client_id not in (select client_id from adverts where is_current = true)
                             ) as tt
                            group by id, name)
                            as mm
                            group by id, name
                            order by name
                            '));
        } else {
           $result = collect(DB::select('select id, name, sum(cnt) as cnt from (
                            select brands.id, brands.name, count(brands.id) as cnt
                            from "brands"
                            inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                            where is_current = true
                            and region_id = :region_1
                            group by "brands"."id", "brands"."name"
                               union all
                            select id, name, count(client_id) as cnt from (
                                 select distinct on (client_id) client_id, brands.id, brands.name
                                 from "brands"
                                          inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                                 where is_current = false
                                   and region_id = :region_2
                                   and client_id not in (select client_id from adverts where is_current = true)
                             ) as tt
                            group by id, name)
                            as mm
                            group by id, name
                            order by name
                            ', ['region_1' => $region, 'region_2' => $region]));
        }

        return $result;

    }

    /**
     * @return void
     */
    protected static function booting()
    {
        parent::booting();
        static::addGlobalScope(new FromPriceScope);
    }

    public function getCreditPayByMonthAttribute()
    {

        /*
         Имеем:

            i= 48%/12 месяцев = 4% или 0,04
            n = 4 года* 12 месяцев = 48 (месяцев)
            S = 2 000
            Рассчитываем К:
            К=(0,04*〖(1+0,04)〗^48)/(〖(1+0,04)〗^48-1) = 0,0472
            подставим полученное значение в формулу ежемесячного платежа:
            А = 0,0472 * 2 000 = 94,4 рублей.

        0,01075×((1+0,01075)^96)÷((1+0,01075)^96−1)
         */

        $i = (Region::getCreditPercent()/12)/100;

        $price = intval($this->priceByAction()) > 0 ? $this->priceByAction() : $this->price;

        $maxPayMonth = setting('credit_max_month');
        $number = $price;
        $resK = $i*(pow((1+$i),$maxPayMonth))/(pow((1+$i), $maxPayMonth)-1);
        $percentage = setting('initial_fee') / 100;
        //$price_prom = (($number - ($number * $percentage)) / $maxPayMonth);
        $price_prom = (($number - ($number * $percentage)));
        $price = $resK*$price_prom;
        //$price = $price_prom + (Region::getCreditPercent()*(($maxPayMonth/12)/ 100) * $price_prom);
        //$price = $price_prom + (($maxPayMonth / 100) * $price_prom);
        return number_format(floor($price), 0, ',', ' ');
        //return 100;
    }

    public function selectedEquipments()
    {
        return $this->equipments()->get()->pluck('equipment_id')->toArray();
    }

    /**
     * @return mixed
     */
    public static function getBrandsByExistedAdverts()
    {
        if (env('APP_SITE_REGION') == 123) {
            $brands = Brand::selectRaw('brands.id, brands.name')
                ->distinct('brands.name')
                ->orderBy('name')
                ->get();
        } else {
            $brands = Brand::selectRaw('brands.id, brands.name')
                ->where('brands.id', 'not like', '777%')
                ->join('adverts', 'adverts.brand_id', '=', 'brands.id')
                ->where('adverts.is_current', '=', true)
                ->where('adverts.is_active', '=', true)
                ->when(!empty(env('APP_SITE_REGION')), function($brands){
                    return $brands->where('adverts.region_id', '=', env('APP_SITE_REGION'));
                })
                ->groupBy('brands.id', 'brands.name')
                ->orderBy('name')->get();
        }

        return $brands;
    }

    /**
     * @param $brandId
     * @return mixed
     */
    public static function getModelsByBrandAndExistingAdverts($brandId)
    {

        /*
        $models = AModel::selectRaw('models.id, models.name')
            ->where(['models.brand_id' => $brandId])
            ->join('adverts', 'adverts.model_id', '=', 'models.id')
            ->where('is_current', '=', true)
            ->groupBy('models.id', 'models.name')
            ->orderBy('name');
        */


        $models = AModel::selectRaw('models.id, models.name')
            ->join('adverts', 'adverts.model_id', '=', 'models.id')
            ->where('adverts.brand_id', '=', $brandId)
            ->where('adverts.is_current', '=', true)
            ->where('adverts.is_active', '=', true)
            ->when(!empty(env('APP_SITE_REGION')), function($models){
                return $models->where('adverts.region_id', '=', env('APP_SITE_REGION'));
            })
            ->groupBy('models.id', 'models.name')
            ->orderBy('name')
            ->get();

        /*
        if(!empty(env('APP_SITE_REGION'))){
            $models->where('adverts.region_id', '=', env('APP_SITE_REGION'));
        }
        */


        //Log::debug($models->dump()->get());
        //return $models->get();
        return $models;

    }

    public function getPhoneByUtm()
    {

    }

    protected $appends = ['creditname', 'creditpaybymonth'];

    protected $casts = [
        'is_vip' => 'boolean',
        'is_spec' => 'boolean',
        'is_active' => 'boolean',
        'vin_verified' => 'boolean',
        'kasko_in_gift' => 'boolean',
        'rubber_kit' => 'boolean',
    ];

    public function equipments()
    {
        return $this->hasMany(AdvertEquipments::class, 'advert_id', 'id');
    }

    public function getStarredVinAttribute()
    {
        $len = strlen($this->vin);
        if($len == 0) {
            return  $this->vin;
        }
        return substr($this->vin, 0, 4) . str_repeat('*', $len - 6) . substr($this->vin, $len - 1);
    }

    public function scopePriceDesc($query)
    {
        return $query->orderBy('price','DESC');
    }

    public function scopePriceAsc($query)
    {
        return $query->orderBy('price','ASC');
    }

    public function kaskoPrice()
    {
        return ($this->price - $this->priceByAction() - $this->tradeinDiscount())*0.4;
    }

    public function creditPrice()
    {
        return ($this->price - $this->priceByAction() - $this->tradeinDiscount())*0.6;
    }

    public function tradeinDiscount()
    {
        $res = $this->price - $this->priceByAction();
        if($res <= 100000) {
            return 50000;
        }
        if($res > 100000 && $res <= 250000) {
            return 100000;
        }
        if($res > 250000) {
            return 150000;
        }
    }

    public function getConnectionName() {
        return env('USE_PGSQL_MAIN') ? 'pgsql_main' : $this->connection;
    }
}
