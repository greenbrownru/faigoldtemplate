<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $created_at
 * @property string $updated_at
 */
class Brand extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'code', 'created_at', 'updated_at'];

    public function models() {
        return $this->hasMany(AModel::class);
    }

    public function adverts() {
        return $this->hasMany(Advert::class);
    }

    public function getConnectionName() {
        return env('USE_PGSQL_MAIN') ? 'pgsql_main' : $this->connection;
    }
}
