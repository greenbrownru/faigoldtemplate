<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Promo extends MainModel
{
    use LogsActivity;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['photo', 'name', 'short_description', 'full_description', 'is_active', 'with_form', 'region_id'];

    public function getNameFormatAttribute() {
        return setTemplateVars(['name', 'credit_percent', 'city'], $this->name);
    }

    public function getShortDescriptionFormatAttribute() {
        return setTemplateVars(['name', 'credit_percent', 'city'], $this->short_description);
    }

    public function getFullDescriptionFormatAttribute() {
        return setTemplateVars(['name', 'credit_percent', 'city'], $this->full_description);
    }

    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
