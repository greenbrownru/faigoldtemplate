<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $brand_id
 * @property integer $category_id
 * @property integer $model_id
 * @property integer $autoru_id
 * @property int $year_from
 * @property int $year_to
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $model_name
 */
class Generation extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['id', 'name', 'brand_id', 'category_id', 'model_id', 'autoru_id', 'year_from', 'year_to', 'is_active', 'created_at', 'updated_at', 'model_name'];

}
