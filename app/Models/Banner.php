<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Banner extends MainModel
{
    use LogsActivity;

    const BANNER_TYPE_MAIN = 'main';

    const BANNER_TYPE_ADDITIONAL = 'additional';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'filename', 'name', 'short_text', 'link', 'is_big', 'display_on_main', 'region_id'];

    public function getNameFormatAttribute() {
        return setTemplateVars(['name', 'credit_percent', 'city'], $this->name);
    }

    public function getShortTextFormatAttribute() {
        return setTemplateVars(['name', 'credit_percent', 'city'], $this->short_text);
    }

    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
