<?php

namespace App\Models;

use App\Helpers\DBHelper;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $advert_id
 * @property string $filename
 * @property string $order
 * @property int $region_id
 * @property string $created_at
 * @property string $updated_at
 */
class AdvertPhoto extends MainModel
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['advert_id', 'filename', 'order', 'region_id', 'created_at', 'updated_at'];

    protected $appends = ['imagepath'];

    public function getImagePathAttribute()
    {
        $domain = env('APP_URL');

        if (DBHelper::isPgsqlMain($this)) {
            return $domain.$this->filename;
        }

        return $domain.(isset($this->__isPlug) ? '/' : '/storage/photos/').$this->filename;
    }

    public function getImagePathResizedAttribute()
    {
        $domain = env('APP_URL');

        if (isset($this->__isPlug)) {
            return "{$domain}/{$this->filename}";
        }

        if (DBHelper::isPgsqlMain($this)) {
            return $domain.$this->filename;
        }

        $filenameWebp = str_replace('.jpg', '.webp', $this->filename);

        if (! \Storage::disk('public')->exists($path = 'photos/resize/'.$filenameWebp)) {
            $path = 'photos/'.$this->filename;
        }

        return "{$domain}/storage/{$path}";
    }

    /**
     * @param int $advertId
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function getFirstByAdvert(int $advertId)
    {
        return AdvertPhoto::query()
            ->orderBy('order', 'ASC')
            ->where(['advert_id' => $advertId])
            ->has('advert')
            ->first();
    }

    public function advert()
    {
        return $this->belongsTo(Advert::class, 'advert_id');
    }

    public static function getPlugPhoto(int $advertId) {
        $plugPhoto = new static();
        $plugPhoto->mergeFillable(['id', '__isPlug']);

        return $plugPhoto->fill([
            'id' => -1,
            'advert_id' => $advertId,
            'filename' => 'advertplug.jpg',
            'order' => 0,
            '__isPlug' => true,
        ]);
    }

    public static function addPlugPhotoIfEmpty($advert) {
        if ($advert->photos->isEmpty()) {
            $advert->photos->push(static::getPlugPhoto($advert->id));
        }
    }

    public function getConnectionName() {
        return env('USE_PGSQL_MAIN') ? 'pgsql_main' : $this->connection;
    }
}
