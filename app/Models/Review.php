<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Review extends MainModel
{
    use LogsActivity;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reviews';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio', 'brand_id', 'model_id', 'rate', 'review', 'client_photo_name',
        'auto_photo_name', 'video_link', 'region_id',
    ];



    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function aModel()
    {
        return $this->belongsTo(AModel::class, 'model_id', 'id');
    }
}
