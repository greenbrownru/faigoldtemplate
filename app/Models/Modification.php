<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $model_id
 * @property string $name
 * @property string $body
 * @property int $year_start
 * @property int $year_end
 * @property integer $configuration_id
 * @property integer $tech_param_id
 * @property string $created_at
 * @property string $updated_at
 */
class Modification extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['model_id', 'name', 'body', 'year_start', 'year_end', 'configuration_id', 'tech_param_id', 'created_at', 'updated_at'];

}
