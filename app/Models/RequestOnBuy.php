<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class RequestOnBuy extends MainModel
{
    use LogsActivity;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'request_on_buys';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['client_brand', 'client_model', 'name', 'phone', 'region_id', 'additional_params', 'user_ip',];

    protected $casts = [
        'additional_params' => 'json'
    ];


    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function clientBrand()
    {
        return $this->belongsTo(Brand::class, 'client_brand', 'id');
    }

    public function clientModel()
    {
        return $this->belongsTo(AModel::class, 'client_model', 'id');
    }
}
