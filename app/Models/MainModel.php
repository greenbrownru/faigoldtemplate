<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MainModel extends Model
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        if (env('APP_SITE_REGION') !== '' && env('APP_SITE_REGION') != 18) {
            static::addGlobalScope('region', function (Builder $builder) {
                $builder->where([$builder->getModel()->getTable() . '.region_id' => intval(env('APP_SITE_REGION'))]);
            });

        }

        if (env('APP_SITE_REGION') == '') {

            static::addGlobalScope('region', function (Builder $builder) {
                if (
                    static::class == 'App\Models\Banner'
                    || static::class == 'App\Models\Contact'
                    || static::class == 'App\Models\Promo'
                    || static::class == 'App\Models\SeoBrandText'
                ) {
                    $builder->whereNull($builder->getModel()->getTable() . '.region_id');
                };
            });

        }

    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->region_id = is_numeric(env('APP_SITE_REGION')) ? env('APP_SITE_REGION') : null;
        });
    }

}
