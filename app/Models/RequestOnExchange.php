<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\Traits\LogsActivity;

class RequestOnExchange extends MainModel
{
    use LogsActivity;

    protected $fillable = ['name', 'phone', 'advert_id', 'region_id', 'additional_params', 'user_ip'];

    protected $casts = [
        'additional_params' => 'json'
    ];

    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
