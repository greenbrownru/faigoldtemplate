<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class RequestOnTradeIn extends MainModel
{
    use LogsActivity;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'request_on_trade_in';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id', 'model_id', 'advert_id', 'client_brand', 'client_model',
        'name', 'phone', 'region_id', 'additional_params', 'user_ip',
    ];

    protected $casts = [
        'additional_params' => 'json'
    ];

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function aModel()
    {
        return $this->belongsTo(AModel::class, 'model_id', 'id');
    }

    public function clientBrand()
    {
        return $this->belongsTo(Brand::class, 'client_brand', 'id');
    }

    public function clientModel()
    {
        return $this->belongsTo(AModel::class, 'client_model', 'id');
    }

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
