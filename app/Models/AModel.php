<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $brand_id
 * @property string $name
 * @property integer category_id
 * @property integer year_from
 * @property integer year_to
 * @property boolean is_active
 * @property string $created_at
 * @property string $updated_at
 */
class AModel extends Model
{

    protected $table = 'models';

    /**
     * @var array
     */
    protected $fillable = ['id', 'brand_id', 'name', 'category_id', 'year_from', 'year_to', 'is_active', 'created_at', 'updated_at'];

    public function adverts() {
        return $this->hasMany(Advert::class, 'model_id');
    }

    public function getConnectionName() {
        return env('USE_PGSQL_MAIN') ? 'pgsql_main' : $this->connection;
    }
}
