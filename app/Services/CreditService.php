<?php

namespace App\Services;

class CreditService
{
    public static function getPercent()
    {
        return setting('credit_percent');
    }

    public static function getAmountFromFormatted(): string
    {
        return number_format(setting('credit_amount_from'), 0, ',', ' ');
    }
}
