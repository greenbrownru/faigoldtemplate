<?php

namespace App\Services;

use App\Models\Advert;
use App\Models\AModel;
use App\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class RequestFormService
{

    /** @var Model */
    protected $model;

    protected $tableName;
    protected $name;

    protected $data;
    protected $utm_names = ['utm_campaign', 'utm_content', 'utm_medium', 'utm_source', 'utm_term'];
    protected $utm = [];

    /**
     * @param Model $model
     * @param array $data
     *
     */
    public function __construct(Model $model, array $data) {
        $this->model = $model;
        $this->data = $data;

        $this->tableName = $model->getTable();
        $this->name = array_flip(static::names())[$this->tableName] ?? $this->tableName;

        $this->setUTM();
    }

    public function before() {
        if (empty($this->data['additional_params']) || !is_array($this->data['additional_params'])) {
            $this->data['additional_params'] = [];
        }

        $this->setUTM($this->data['additional_params']);
        $this->data['additional_params'] = array_merge($this->utm, $this->data['additional_params']);
        $this->data['user_ip'] = request()->ip();
    }

    public function after() {
        $this->executeBitrix();
        $this->executeAutocrm();
    }

    private function executeAutocrm() {
        if (env('APP_SITE_REGION') != 123 || env('SITE_SUFFIX') !== 'olimp-trade') {
            return;
        }

        $crm = new ToAutocrmRequestService();
        $response = $crm->post('leads/request', [
            'type' => 11,
            'salon_id' => 20,
            'request_type_id' => 2,
            'source_id' => 1,
            'source2_id' => 148,

            'phone' => $this->getData('phone'),
            'first_name' => $this->getData('name'),
        ] + $this->utm);

        if ($response && $response['success']) {
            $this->model->additional_params = array_merge($this->model->additional_params, [
                'autocrm_lead_request_id' => $response['result']['id']
            ]);
            $this->model->save();
        }

//        dd('response', $response);
    }

    private function executeBitrix() {
        $toBitrixRequestService = new ToBitrixRequestService();

        $dataBitrix = [
            'telephone' => $this->getData('phone'),
            'name' => $this->getData('name'),
        ];

        $advert = new Advert();
        if (in_array($this->name, ['credits', 'trade_in', 'exchanges'])) {
            $advert = $advert->find($this->getData('credit_advert') ?: $this->getData('advert_id'));

            $dataBitrix += [
                'mark' => $advert->brand->name,
                'model' => $advert->aModel->name,
                'year' => $advert->year_of_issue,
                'price' => $advert->price,
                'credit_period' => $this->getData('credit_term'),
            ];
        }

        switch ($this->name) {
            case 'credits':
                $dataBitrix['initial_payment'] = (string) (($advert->price_by_action > 0 ? $advert->price_by_action : $advert->price) * (floatval($this->getData('down_payment')) / 100));
                break;
            case 'trade_in':
                $dataBitrix += [
                    'initial_payment' => floatval($this->getData('down_payment')),
                    'your_car_mark' => Brand::find($this->getData('client_brand'))->name,
                    'your_car_model' => AModel::find($this->getData('client_model'))->name,
                ];
                break;
            case 'buys':
                $dataBitrix += [
                    'your_car_mark' => Brand::find($this->getData('client_brand'))->name,
                    'your_car_model' => AModel::find($this->getData('client_model'))->name,
                ];
                break;
        }

        $toBitrixRequestService->onFormAppendToDatabase($dataBitrix);
    }

    protected function setUTM($data = []) {
        $utm = [];
        foreach ($this->utm_names as $name) {
            $utm[$name] = $data[$name] ?? request()->cookie($name);
        }

        $this->utm = $utm;
    }

    public function getData($name = null, $default = null) {
        return data_get($this->data, $name, $default);
    }

    public static function names($name = null) {
        $names = [
            'contacts' => 'request_in_contacts',
            'buys' => 'request_on_buys',
            'car_services' => 'request_on_car_services',
            'credits' => 'request_on_credits',
            'promos' => 'request_on_promos',
            'trade_in' => 'request_on_trade_in',
            'exchanges' => 'request_on_exchanges'
        ];

        if ($name === true) {
            return array_values($names);
        }
        if ($name === false) {
            return array_keys($names);
        }

        if (!is_null($name)) {
            return $names[$name] ?? null;
        }

        return $names;
    }
}
