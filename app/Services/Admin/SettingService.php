<?php

namespace App\Services\Admin;


class SettingService
{
    /**
     * Общий
    */
    public function groupCommon() {
        return [
            'label' => 'Общие',
            'name' => 'common',
            'items' => [
                $this->fieldSelect('theme', 'Шаблон', [
                    'classic' => 'Классический'
                ]),
                $this->fieldText('name', 'Название салона'),
                $this->fieldText('email', 'Почта'),
                $this->fieldText('phone', 'Телефон'),
                $this->fieldCheckbox('phone__hiding_part', 'Частичное скрытие номера телефона'),
                $this->fieldCheckbox('phone__show_mobile_in_sidebar', 'Показывать телефон в меню (sidebar) на моб.'),
                $this->fieldText('phone__bottom_text', 'Текст под телефоном'),
                $this->fieldText('city', 'Город'),
                $this->fieldText('address', 'Адрес (Город, Ул., Дом)'),
                $this->fieldText('schedule', 'График работы'),
                $this->fieldNumber('advert__percent', 'Скидка на авто (в процентах)', ['min' => 0, 'max' => 100]),
                $this->fieldNumber('credit_percent', 'Кредитная ставка (в процентах)', ['step' => 0.01, 'min' => 0]),
                $this->fieldNumber('credit_max_month', 'Максимальный срок кредитования (в месяцах)'),
                $this->fieldNumber('credit_amount_from', 'Сумма кредита (от)', ['min' => 0]),
                $this->fieldNumber('initial_fee', 'Первоначальный взнос (в процентах)', ['step' => 0.01, 'min' => 0, 'max' => 100]),
                $this->fieldText('yandex__map', 'Яндекс карта (ссылка)'),
                $this->fieldTextarea('footer__text', 'Юрнабивка', 5, [
                    'labelTitle' => $this->vars(['phone' => 'Телефон'])
                ]),
                $this->fieldTextarea('footer__text_hide', 'Юрнабивка (скрытый)', 10, [
                    'labelTitle' => $this->vars(['name' => 'Название салона', 'phone' => 'Телефон', 'credit_percent' => 'Процентная ставка', 'credit_max_month' => 'Максимальный срок (мес.)', 'initial_fee' => 'Первоначальный взнос', 'credit_amount_from' => 'Сумма кредита (от)', 'date_start' => 'Начало месяца', 'date_end' => 'Конец месяца', 'advert' => 'Объявление'])
                ]),
                $this->fieldText('footer__contact_text', 'Юрнабивка (контакты)'),
            ]
        ];
    }

    /**
     * Метрика
     */
    public function groupMetrics() {
        return [
            'label' => 'Метрика',
            'name' => 'metrics',
            'items' => [
                $this->fieldText('yandex__id', 'Яндекс ID'),
                $this->fieldText('yandex__id2', 'Яндекс ID 2'),
                $this->fieldSelect('yandex__use', 'Использовать Яндекс метрику для целей', [
                    1 => 'Яндекс ID',
                    2 => 'Яндекс ID 2',
                ]),
                $this->fieldText('google__id', 'Google ID'),
                $this->fieldText('mail__id', 'Mail ID'),

                $this->header('Цели для форм Яндекс'),
                $this->fieldText('yandex__goals__all', 'Все'),
                $this->fieldText('yandex__goals__credit', 'Кредит'),
                $this->fieldText('yandex__goals__credit_modal', 'Кредит в модальном окне'),
                $this->fieldText('yandex__goals__trade_in', 'Trade in'),
                $this->fieldText('yandex__goals__trade_in_exchange', 'Trade in обмен авто'),
                $this->fieldText('yandex__goals__buy', 'Выкуп'),
                $this->fieldText('yandex__goals__promo', 'Акции'),
                $this->fieldText('yandex__goals__contact', 'Контакты'),
                $this->fieldText('yandex__goals__subscribe', 'Подписка'),

                $this->header('Цели для форм Google'),
                $this->fieldText('google__goals__all', 'Все'),
                $this->fieldText('google__goals__credit', 'Кредит'),
                $this->fieldText('google__goals__credit_modal', 'Кредит в модальном окне'),
                $this->fieldText('google__goals__trade_in', 'Trade in'),
                $this->fieldText('google__goals__trade_in_exchange', 'Trade in обмен авто'),
                $this->fieldText('google__goals__buy', 'Выкуп'),
                $this->fieldText('google__goals__promo', 'Акции'),
                $this->fieldText('google__goals__contact', 'Контакты'),
                $this->fieldText('google__goals__subscribe', 'Подписка'),

                $this->header('Цели для форм Mail'),
                $this->fieldText('mail__goals__all', 'Все'),

                $this->header('Другие цели Яндекс'),
                $this->fieldText('yandex__goals__phone_click', 'Нажатие на номер телефона'),

                $this->header('Другие цели Google'),
                $this->fieldText('google__goals__phone_click', 'Нажатие на номер телефона'),
            ]
        ];
    }

    /**
     * Картинки
     */
    public function groupPhotos() {
        return [
            'label' => 'Картинки',
            'name' => 'photos',
            'items' => [
                $this->fieldFile('logo__head', 'Логотип (шапка)', ['accept' => 'image/*']),
                $this->fieldFile('logo__foot', 'Логотип (подвал)', ['accept' => 'image/*']),
                $this->fieldFile('favicon', 'Favicon', ['accept' => 'image/*']),
            ]
        ];
    }

    /**
     * Страницы
     */
    public function groupPages() {
        return [
            'label' => 'Страницы',
            'name' => 'pages',
            'items' => [
                $this->fieldCheckbox('page__credit_enable', 'Автокредит'),
                $this->fieldCheckbox('page__trade_in_enable', 'Trade in'),
                $this->fieldCheckbox('page__buy_enable', 'Выкуп'),
                $this->fieldCheckbox('page__promo_enable', 'Акции'),
                $this->fieldCheckbox('page__carservice_enable', 'Автосервис'),
                $this->fieldCheckbox('page__about_enable', 'О компании'),

                $this->header('Каталог'),
                $this->fieldCheckbox('page__catalog_autoload', 'Автозагрузка машин'),

                $this->header('Акции'),
                $this->fieldCheckbox('page__promo_header_text_enable', 'Текст в шапке'),
                $this->fieldTextarea('page__promo_header_text', 'Текст в шапке', 5, [
                    'labelTitle' => $this->vars(['name' => 'Название салона', 'city' => 'Город', 'credit_percent' => 'Процентная ставка'])
                ]),

                $this->header('Контакты'),
                $this->fieldCheckbox('page__contact_subscribe_show', 'Подписка'),
            ]
        ];
    }

    /**
     * Мета
     */
    public function groupMeta() {
        $commonVars = [
            'name' => 'Название салона',
            'title' => 'Общий заголовок',
            'city' => 'Город',
            'credit_percent' => 'Процентная ставка',
        ];

        $commonVarsCatalog = [
            'count' => 'Кол-во объявлении',
            'min_price' => 'Мин. цена',
            'max_price' => 'Макс. цена',
            'min_year' => 'Мин. год',
            'max_year' => 'Макс. год'
        ];

        return [
            'label' => 'SEO',
            'name' => 'meta',
            'items' => [
                $this->fieldText('title', 'Общий заголовок'),
                $this->fieldTextarea('page__description', 'Общее описание', 3),

                $this->header('Главная страница', [
                    'labelTitle' => $this->vars($commonVars)
                ]),
                $this->fieldText('page__main_title', 'Заголовок'),
                $this->fieldTextarea('page__main_description', 'Описание', 3),

                $this->header('Страница "Автокредит"', [
                    'labelTitle' => $this->vars($commonVars)
                ]),
                $this->fieldText('page__credit_title', 'Заголовок'),
                $this->fieldTextarea('page__credit_description', 'Описание', 3),

                $this->header('Страница "Trade-in"', [
                    'labelTitle' => $this->vars($commonVars)
                ]),
                $this->fieldText('page__trade_in_title', 'Заголовок'),
                $this->fieldTextarea('page__trade_in_description', 'Описание', 3),

                $this->header('Страница "Выкуп"', [
                    'labelTitle' => $this->vars($commonVars)
                ]),
                $this->fieldText('page__buy_title', 'Заголовок'),
                $this->fieldTextarea('page__buy_description', 'Описание', 3),

                $this->header('Страница "Акции"', [
                    'labelTitle' => $this->vars($commonVars)
                ]),
                $this->fieldText('page__promo_title', 'Заголовок'),
                $this->fieldTextarea('page__promo_description', 'Описание', 3),

                $this->header('Страница "Акции - Акция"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, ['promo_name' => 'Название акции', 'full_description' => 'Полное описание']))
                ]),
                $this->fieldText('page__promo_item_title', 'Заголовок'),
                $this->fieldTextarea('page__promo_item_description', 'Описание', 3),

                $this->header('Страница "Контакты"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, ['phone' => 'Телефон', 'email' => 'Почта', 'address' => 'Адрес', 'schedule' => 'График работы']))
                ]),
                $this->fieldText('page__contact_title', 'Заголовок'),
                $this->fieldTextarea('page__contact_description', 'Описание', 3),

                $this->header('Страница "Каталог"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, $commonVarsCatalog))
                ]),
                $this->fieldText('page__catalog_title', 'Заголовок'),
                $this->fieldTextarea('page__catalog_description', 'Описание', 3),

                $this->header('Страница "Каталог - Марка"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, $commonVarsCatalog, ['brand' => 'Марка']))
                ]),
                $this->fieldText('page__catalog_brand_title', 'Заголовок'),
                $this->fieldTextarea('page__catalog_brand_description', 'Описание', 3),

                $this->header('Страница "Каталог - Марка - Модель"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, $commonVarsCatalog, ['brand' => 'Марка', 'model' => 'Модель']))
                ]),
                $this->fieldText('page__catalog_brand_model_title', 'Заголовок'),
                $this->fieldTextarea('page__catalog_brand_model_description', 'Описание', 3),

                $this->header('Страница "Объявление"', [
                    'labelTitle' => $this->vars(array_merge($commonVars, ['brand' => 'Марка', 'model' => 'Модель', 'year_of_issue' => 'Год выпуска', 'mileage' => 'Пробег', 'transmission' => 'КПП', 'engine' => 'Двигатель', 'volume' => 'Объем', 'price' => 'Цена', 'price_by_action' => 'Цена по скидке', 'discount' => 'Скидка (руб.)']))
                ]),
                $this->fieldText('page__advert_title', 'Заголовок'),
                $this->fieldTextarea('page__advert_description', 'Описание', 3),
            ]
        ];
    }

    /**
     * Фид
     */
    public function groupFeed() {
        return [
            'label' => 'Фид',
            'name' => 'feed',
            'items' => []
        ];
    }

    /**
     * Прочее
     */
    public function groupOthers() {
        return [
            'label' => 'Прочее',
            'name' => 'others',
            'items' => [
                $this->fieldText('api_token', 'API токен', [
                    '_generate' => true,
                    '_prependCopyBtn' => url('/api/v2/subscribes?token=' . setting('api_token')),
                    'labelTitle' => url('/api/v2/subscribes?token=' . setting('api_token'))
                ]),
                $this->fieldText('marquizru__id', 'Марквиз id (marquiz.ru)'),
                $this->fieldText('calltouch__id', 'Calltouch id (calltouch.ru)'),
                $this->fieldCheckbox('pixel__enable', 'Pixel'),
                $this->fieldTextarea('head_scripts', 'Скрипты HEAD', 10),
                $this->fieldTextarea('footer_scripts', 'Скрипты FOOTER', 10),
            ]
        ];
    }

    protected function header($label, $attrs = []) {
        return  array_merge([
            'label' => $label,
            'type' => 'header'
        ], $attrs);
    }

    protected function field($name, $label, $attrs = []) {
        return array_merge([
            'name' => $name,
            'label' => $label
        ], $attrs);
    }

    protected function vars($data, $prefix = 'Переменные:<br>') {
        $result = [];

        foreach ($data as $name => $label) {
            $result[] = "<b>[[{$name}]]</b> - $label";
        }

        return $prefix . implode('<br>', $result);
    }

    protected function fieldText($name, $label, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'text'], $attrs));
    }

    protected function fieldNumber($name, $label, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'number'], $attrs));
    }

    protected function fieldFile($name, $label, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'file'], $attrs));
    }

    protected function fieldTextarea($name, $label, $rows = 5, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'textarea', 'rows' => $rows], $attrs));
    }

    protected function fieldCheckbox($name, $label, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'checkbox'], $attrs));
    }

    protected function fieldSelect($name, $label, $options, $attrs = []) {
        return $this->field($name, $label, array_merge(['field' => 'select', 'options' => $options], $attrs));
    }

    public function selectOnOff() {
        return [
            '' => 'Выключить',
            1 => 'Включить',
        ];
    }

    public function get() {
        $groupNames = [
            'common',
            'metrics',
            'photos',
            'pages',
            'meta',
            'feed',
            'others',
        ];

        return $this->data($groupNames);
    }

    public function data($groupNames) {
        $result = [];

        foreach ($groupNames as $groupName) {
            if (! method_exists($this, $methodName = 'group' . (ucfirst($groupName)))) {
                continue;
            }

            $result[] = $this->{$methodName}();
        }

        return $result;
    }

    public static function make() {
        return new static();
    }
}
