<?php


namespace App\Services;


class Region
{
    public static function isAutodromium(): bool
    {
        return in_array(env('APP_SITE_REGION'), ['', 66, 63, 52]) ?? false;
    }

    public static function getCreditPercent()
    {
        return CreditService::getPercent();
    }

}
