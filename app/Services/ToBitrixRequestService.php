<?php


namespace App\Services;


use Illuminate\Support\Facades\Log;

class ToBitrixRequestService
{

    private function send($param, $next = 0)
    {
        $appParams = http_build_query(array(
            'halt' => 0,
            'cmd' => $param
        ));
        $appRequestUrl = 'https://autocenter.bitrix24.ru/rest/193/qii5h2xl3znhrxca/batch/';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $appRequestUrl,
            CURLOPT_POSTFIELDS => $appParams
        ));
        $out = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($out, 1);

        //Log::debug($appRequestUrl . $appParams);

        return $result;
    }

    function onFormAppendToDatabase($data)
    {

        if(!Region::isAutodromium()) {
            return [];
        }

        $TITLE = 'Форма обратной связи';   // название формы
        $NAME = isset($data['name']) ? $data['name'] : '';   // имя клиента
        $email = $data['email'] ?? ''; // сюда почту
        $phone = $data['telephone'] ?? ''; // телефон клиента
        $marka = isset($data['mark']) ? $data['mark'] : ''; // марка желаемого авто
        $model = isset($data['model']) ? $data['model'] : ''; // модель желаемого авто
        $reliseyear = isset($data['year']) ? $data['year'] : ''; // Год выпуска желаемого авто
        $firstpaid = isset($data['initial_payment']) ? $data['initial_payment'] : ''; // первый взнос
        $creditterm = isset($data['credit_period']) ? $data['credit_period'] : ''; // срок кредита
        $checkinregion = isset($data['region']) ? $data['region'] : ''; // Регион регистрации
        $ownmarka = isset($data['your_car_mark']) ? $data['your_car_mark'] : ''; // Ваш автомобиль марка
        $ownmodel = isset($data['your_car_model']) ? $data['your_car_model'] : ''; // Ваш автомобиль модель
        $ownreliseyear = isset($data['your_car_year']) ? $data['your_car_year'] : ''; // Ваш автомобиль год выпуска
        $distancekm = $_REQUEST['distancekm'] ?? null; // Ваш автомобиль пробег
        $price = isset($data['price']) ? $data['price'] : ''; // стоимость желаемого авто
        $registration = isset($data['region']) ? $data['region'] : ''; //прописка
        $ipregion = $_REQUEST['ipregion'] ?? null; // Регион по ip

        $lead1 = '';
        $UTM_CAMPAIGN = request()->cookie('utm_campaign');
        $UTM_CONTENT = request()->cookie('utm_content');
        $UTM_MEDIUM = request()->cookie('utm_medium');
        $UTM_SOURCE = request()->cookie('utm_source');
        $UTM_TERM = request()->cookie('utm_term');

        $ASSIGNED_BY_ID = 1; // ответственный за лид сейчас 1 пользователь
//        $SOURCE_ID = env('APP_SITE_REGION') == 72 ? 29 : 31; // источник - вебсайт
        $SOURCE_ID = env('SOURCE_ID');
        /*
        Имя NAME
        Телефон WORK_PHONE
        Марка UF_CRM_1594214457
        Модель UF_CRM_1594214471
        Первый взнос UF_CRM_1594214520
        Срок кредита UF_CRM_1594214547
        Регион регистрации UF_CRM_1594214580
        Ваш автомобиль марка UF_CRM_1594214714
        Ваш автомобиль модель UF_CRM_1594214741
        Ваш автомобиль год выпуска UF_CRM_1594214791
        Ваш автомобиль пробег UF_CRM_1594214815

        Год выпуска UF_CRM_1594214974
        Стоимость OPPORTUNITY
        Прописка UF_CRM_1594215074
        Регион по ip UF_CRM_1594047429
        */
        $Phone = '';
        if ($NAME) {
            if ($phone) {
                $len = strlen($phone);
                for ($i = 0; $i < $len; $i++) {
                    $char = substr($phone, $i, 1);
                    if ($char == 0 || $char == 1 || $char == 2 || $char == 3 || $char == 4 || $char == 5 || $char == 6 || $char == 7 || $char == 8 || $char == 9) {
                        $Phone = $Phone . $char;
                    }
                }

                $param['act'] = "crm.duplicate.findbycomm?"
                    . http_build_query(array(
                        'entity_type' => ["CONTACT", "COMPANY"],
                        'type' => "PHONE",
                        'values' => [$Phone, '+7' . $Phone, '+8' . $Phone, '7' . $Phone, '8' . $Phone, '+' . $Phone]
                    ));
                $res1 = $this->send($param);


                $idc1 = $res1['result']['result']['act']['CONTACT'][0] ?? null;
                $idco1 = $res1['result']['result']['act']['COMPANY'][0] ?? null;


                $param['act'] = "crm.lead.add?"
                    . http_build_query(array(
                        'fields' => ['TITLE' => $TITLE,
                            'NAME' => $NAME,
                            'UF_CRM_1594214457' => $marka,
                            'UF_CRM_1594214471' => $model,
                            'UF_CRM_1594214974' => $reliseyear,
                            'UF_CRM_1594214520' => $firstpaid,
                            'UF_CRM_1594214547' => $creditterm,
                            'UF_CRM_1594214580' => $checkinregion,
                            'UF_CRM_1594214714' => $ownmarka,
                            'UF_CRM_1594214741' => $ownmodel,
                            'UF_CRM_1594214791' => $ownreliseyear,
                            'UF_CRM_1594214815' => $distancekm,
                            'CONTACT_ID' => $idc1,
                            'COMPANY_ID' => $idco1,
                            'OPPORTUNITY' => $price,
                            "SOURCE_ID" => $SOURCE_ID,
                            "UTM_CAMPAIGN" => $UTM_CAMPAIGN,
                            "UTM_CONTENT" => $UTM_CONTENT,
                            "UTM_MEDIUM" => $UTM_MEDIUM,
                            "UTM_SOURCE" => $UTM_SOURCE,
                            "UTM_TERM" => $UTM_TERM,
                            "ASSIGNED_BY_ID" => $ASSIGNED_BY_ID,
                            'UF_CRM_1594215074' => $registration,
                            'UF_CRM_1594047429' => $ipregion,
                            "PHONE" => array(array("VALUE" => $Phone, "VALUE_TYPE" => "WORK")), //PHONE
                        ]
                    ));
                $lead1 = $this->send($param);
            }
            if (empty($Phone)) {
                if ($email) {
                    $param['act'] = "crm.duplicate.findbycomm?"
                        . http_build_query(array(
                            'entity_type' => ["CONTACT", "COMPANY"],
                            'type' => "PHONE",
                            'values' => [$email]
                        ));
                    $res1 = $this->send($param);


                    $idc1 = $res1['result']['result']['act']['CONTACT'][0] ?? null;
                    $idco1 = $res1['result']['result']['act']['COMPANY'][0] ?? null;


                    $param['act'] = "crm.lead.add?"
                        . http_build_query(array(
                            'fields' => ['TITLE' => $TITLE,
                                'NAME' => $NAME,
                                'UF_CRM_1594214457' => $marka,
                                'UF_CRM_1594214471' => $model,
                                'UF_CRM_1594214974' => $reliseyear,
                                'UF_CRM_1594214520' => $firstpaid,
                                'UF_CRM_1594214547' => $creditterm,
                                'UF_CRM_1594214580' => $checkinregion,
                                'UF_CRM_1594214714' => $ownmarka,
                                'UF_CRM_1594214741' => $ownmodel,
                                'UF_CRM_1594214791' => $ownreliseyear,
                                'UF_CRM_1594214815' => $distancekm,
                                'CONTACT_ID' => $idc1,
                                'COMPANY_ID' => $idco1,
                                'OPPORTUNITY' => $price,
                                "SOURCE_ID" => $SOURCE_ID,
                                "UTM_CAMPAIGN" => $UTM_CAMPAIGN,
                                "UTM_CONTENT" => $UTM_CONTENT,
                                "UTM_MEDIUM" => $UTM_MEDIUM,
                                "UTM_SOURCE" => $UTM_SOURCE,
                                "UTM_TERM" => $UTM_TERM,
                                "ASSIGNED_BY_ID" => $ASSIGNED_BY_ID,
                                'UF_CRM_1594215074' => $registration,
                                'UF_CRM_1594047429' => $ipregion,
                                "EMAIL" => array(array("VALUE" => $email, "VALUE_TYPE" => "WORK")), //PHONE
                            ]
                        ));
                    $lead1 = $this->send($param);

                }
                if (empty($email)) {
                    $param['act'] = "crm.lead.add?"
                        . http_build_query(array(
                            'fields' => [
                                'TITLE' => $TITLE,
                                'NAME' => $NAME,
                                'UF_CRM_1594214457' => $marka,
                                'UF_CRM_1594214471' => $model,
                                'UF_CRM_1594214974' => $reliseyear,
                                'UF_CRM_1594214520' => $firstpaid,
                                'UF_CRM_1594214547' => $creditterm,
                                'UF_CRM_1594214580' => $checkinregion,
                                'UF_CRM_1594214714' => $ownmarka,
                                'UF_CRM_1594214741' => $ownmodel,
                                'UF_CRM_1594214791' => $ownreliseyear,
                                'UF_CRM_1594214815' => $distancekm,
                                'OPPORTUNITY' => $price,
                                'CONTACT_ID' => $idc1,
                                'COMPANY_ID' => $idco1,
                                "SOURCE_ID" => $SOURCE_ID,
                                "UTM_CAMPAIGN" => $UTM_CAMPAIGN,
                                "UTM_CONTENT" => $UTM_CONTENT,
                                "UTM_MEDIUM" => $UTM_MEDIUM,
                                "UTM_SOURCE" => $UTM_SOURCE,
                                "UTM_TERM" => $UTM_TERM,
                                "ASSIGNED_BY_ID" => $ASSIGNED_BY_ID,
                                'UF_CRM_1594215074' => $registration,
                                'UF_CRM_1594047429' => $ipregion,
                            ]
                        ));
                    $lead1 = $this->send($param);
                }
            }
        }

        //$param1['act'] = "crm.lead.fields";
        /*
        $param1['act'] = "crm.lead.list?"
            . http_build_query(array(
                'order' => [
                    'DATE_CREATE' => 'DESC',
                ],
                'select' => ["NAME", "UF_CRM_1594214520" ]
            ));
        */

        //$an = $this->send($param1);


        //Log::debug($firstpaid);
        //Log::debug($an);
        //Log::info($param);
        //Log::info($SOURCE_ID);
    }

}
