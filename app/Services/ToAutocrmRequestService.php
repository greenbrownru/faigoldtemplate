<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

/**
 * @method Http delete(string $url, array $data = [])
 * @method Http post(string $url, array $data = [])
 * @method Http get(string $url, array|string|null $query = null)
 * @method Http head(string $url, array|string|null $query = null)
 * @method Http patch(string $url, array $data = [])
 * @method Http put(string $url, array $data = [])
 */
class ToAutocrmRequestService
{
    public $http;
    protected $url;
    protected $response = null;
    protected $dd_on = false;

    public function __construct() {
        $this->url = 'https://'.env('AUTOCRM_NAME').'.autocrm.ru/yii/api/';
        $login_password = explode(':', env('AUTOCRM_LOGIN_PASSWORD', ':'));

        $this->http = Http::withBasicAuth($login_password[0], $login_password[1]);
    }

    public function send($method = 'get', $endpoint = '', $options = []) {
        if ($this->dd_on) {
            $this->http->dd();
        }

        $url = $this->url . $endpoint;

        if (in_array($method, ['post', 'get', 'delete', 'put', 'patch', 'head'])) {
            $this->response = $this->http->{$method}($url, $options);
        } else {
            $this->response = $this->http->send($method, $url, $options);
        }

        return $this->response->json();
    }

    public function getResponse() {
        return $this->response;
    }

    public function dd($bool = true) {
        $this->dd_on = $bool;

        return $this;
    }

    public function __call($name, $arguments) {
        if (in_array($name, ['post', 'get', 'delete', 'put', 'patch', 'head'])) {
            return $this->send($name, ...$arguments);
        }
    }
}
