<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class WatermarkPhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $item;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $item)
    {
        $this->item = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        //dd($this->item);
        $image = new \Imagick(storage_path().'/app/public/photos/'.$this->item['filename']);
        //$this->comment($this->item['filename']);

        $watermark = new \Imagick();
        $watermark_file = storage_path().'/watermarks/'.env('SITE_SUFFIX').'.png';

        if(!is_file($watermark_file)) {
            trigger_error("Файл {$watermark_file} не найден", E_USER_ERROR);
            //$this->fail();
        }

        $svg = file_get_contents($watermark_file);
        $watermark->readImageBlob($svg);
        $watermark->setBackgroundColor('transparent');
        $watermark->setImageFormat("png24");
        $watermark->setImageCompressionQuality(100);
        $watermarkResizeFactor = 3;

        $watermark_Width = $watermark->getImageWidth();
        $watermark_Height = $watermark->getImageHeight();

        $watermark->scaleImage($watermark_Width / $watermarkResizeFactor, $watermark_Height / $watermarkResizeFactor);

        for ($w = -150; $w < $image->getImageWidth(); $w += ($image->getImageWidth() / 100) * 45) {
            for ($h = 0; $h < $image->getImageHeight(); $h += ($image->getImageHeight() / 100) * 45) {
                try {
                    $image->compositeImage($watermark, \Imagick::COMPOSITE_OVER, $w, $h);
                } catch (\Exception $exception) {
                    Log::debug($this->item['filename']);
                }

            }
        }

        for ($w = 150; $w < $image->getImageWidth(); $w += ($image->getImageWidth() / 100) * 50) {
            for ($h = 180; $h < $image->getImageHeight(); $h += ($image->getImageHeight() / 100) * 45) {
                try {
                    $image->compositeImage($watermark, \Imagick::COMPOSITE_OVER, $w, $h);
                } catch (\Exception $exception) {
                    Log::debug($this->item['filename']);
                }
            }
        }

        $image->writeImage(storage_path().'/app/public/photos/'.$this->item['filename']);

    }
}
