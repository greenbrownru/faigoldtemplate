<?php

namespace App\Jobs;

use App\Models\Advert;
use App\Models\AdvertPhoto;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ImportPhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $advert;

    private $url_photo;

    private $file_name;

    private $order;

    /**
     * Create a new job instance.
     *
     * @param Advert $advert
     * @param string $url_photo
     * @param int $order
     */
    public function __construct(Advert $advert, string $url_photo, int $order)
    {
        $this->advert = $advert;
        $this->url_photo = $url_photo;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        try {
            $url_photo = str_replace(' ', '', $this->url_photo);
            $filename = $this->advert->id .'_'.uniqid().rand(100000, 999999). '.jpg';
            if($this->get_http_response_code($url_photo) == 200) {
                $img = public_path() . '/storage/photos/' . $filename;
                file_put_contents($img, file_get_contents($url_photo));
                imagewebp(imagecreatefromjpeg($img),str_replace('.jpg', '.webp', $img));

                $photo_resize = \Intervention\Image\ImageManagerStatic::make(Storage::disk('public')->get("/photos/" . $filename));
                $photo_resize->resize(500, null, function($constraint){
                    $constraint->aspectRatio();
                });
                $photo_resize->save(public_path('storage/photos/resize/' . $filename), 100);
                imagewebp(imagecreatefromjpeg(public_path('storage/photos/resize/' . $filename)), public_path('storage/photos/resize/' . str_replace('.jpg', '.webp', $filename)));
                @unlink(public_path('storage/photos/resize/' . $filename));
                @unlink(public_path('storage/photos/' . $filename));
                AdvertPhoto::create([
                    'advert_id' => $this->advert->id,
                    'filename' => str_replace('.jpg', '.webp', $filename),
                    'order' => $this->order,
                ]);
            }
        } catch (\Exception $e) {
            \Log::error('ImportPhoto', (array) $e);
        }
    }

    /**
     * @return array
     */
    public function tags()
    {
        return ['import', 'photo:'.$this->order];
    }

    /**
     * @param $url
     * @return false|string
     */
    function get_http_response_code($url)
    {
        $headers = @get_headers($url);
        return substr($headers[0], 9, 3);
    }
}
