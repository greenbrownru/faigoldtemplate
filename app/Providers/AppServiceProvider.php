<?php

namespace App\Providers;

use App\Models\Advert;
use App\Models\Brand;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        DB::listen(function ($query) { dump([$query->sql, $query->bindings, $query->time]); });
        Carbon::setLocale('ru');

        $seconds = 3600;
        Paginator::useBootstrap();

        view()->composer('*', function ($view) use($seconds) {
            if (env('APP_SITE_REGION') !== '') {
                $view->with('brandMenu', collect(
                        Cache::remember('brandMenu' . env('APP_SITE_REGION'), $seconds, function() {
                            return DB::select('select id, name, sum(cnt) as cnt from (
                            select brands.id, brands.name, count(brands.id) as cnt
                            from "brands"
                            inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                            where is_current = true and is_active = true
                            and region_id = :region_1
                            group by "brands"."id", "brands"."name"
                               union all
                            select id, name, count(client_id) as cnt from (
                                 select distinct on (client_id) client_id, brands.id, brands.name
                                 from "brands"
                                          inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                                 where is_current = false
                                   and region_id = :region_2
                                   and client_id not in (select client_id from adverts where is_current = true)
                             ) as tt
                            group by id, name)
                            as mm
                            group by id, name
                            order by name
                            ', ['region_1' => env('APP_SITE_REGION'), 'region_2' => env('APP_SITE_REGION')]);
                        })
                    )
                );
            } else {
                $view->with('brandMenu', collect(
                        Cache::remember('brandMenu' . env('APP_SITE_REGION'), $seconds, function() {
                            return DB::select('select id, name, sum(cnt) as cnt from (
                            select brands.id, brands.name, count(brands.id) as cnt
                            from "brands"
                            inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                            where is_current = true
                            group by "brands"."id", "brands"."name"
                               union all
                            select id, name, count(client_id) as cnt from (
                                 select distinct on (client_id) client_id, brands.id, brands.name
                                 from "brands"
                                          inner join "adverts" on "adverts"."brand_id" = "brands"."id"
                                 where is_current = false
                                   and client_id not in (select client_id from adverts where is_current = true)
                             ) as tt
                            group by id, name)
                            as mm
                            group by id, name
                            order by name
                        ');
                        })
                    )
                );
            }
        });

//        view()->composer('*', function ($view) use($seconds) {
//            $view->with('brandMenu', collect(
//                    Cache::remember('brandMenu'.env('APP_SITE_REGION'), $seconds, function () {
//                        return DB::query()
//                            ->selectRaw('id, name, sum(id) as cnt')
//                            ->from(function($q) {
//                                $q
//                                    ->selectRaw('brands.id, brands.name, count(brands.id) as cnt')
//                                    ->from('brands')
//                                    ->join('adverts', 'adverts.brand_id', '=', 'brands.id')
//                                    ->where('is_current', '=', true)
//                                    ->when(env('APP_SITE_REGION') !== '', function($q) {
//                                        $q->where('is_active', '=', true)
//                                            ->where('region_id', '=', env('APP_SITE_REGION'));
//                                    })
//                                    ->groupBy('brands.id', 'brands.name')
//                                    ->unionAll(function($q) {
//                                        $q->selectRaw('id, name, count(client_id) as cnt')
//                                            ->from(function($q) {
//                                                $q
//                                                    ->selectRaw('distinct on (client_id) client_id, brands.id, brands.name')
//                                                    ->from('brands')
//                                                    ->join('adverts', 'adverts.brand_id', '=', 'brands.id')
//                                                    ->where('is_current', '=', false)
//                                                    ->when(env('APP_SITE_REGION') !== '', function($q) {
//                                                        $q->where('region_id', '=', env('APP_SITE_REGION'));
//                                                    })
//                                                    ->whereNotIn('client_id', function($q) {
//                                                        $q->select('client_id')
//                                                            ->from('adverts')
//                                                            ->where('is_current', '=', true);
//                                                    });
//                                            }, 'tt')
//                                            ->groupBy('id', 'name');;
//                                    });
//                            }, 'mm')
//                            ->groupBy('id', 'name')
//                            ->orderBy('name')
//                            ->get();
//                    })
//                )
//            );
//        });
    }
}
