<?php

if (! function_exists('showPhone')) {
    function showPhone() {
        return setting('phone');
    }
}

if (! function_exists('theme')) {
    function theme() {
        return setting('theme');
    }
}

if (! function_exists('setTemplateVars')) {
    function setTemplateVars($vars, $str) {
        $names = [];
        $replace = [];

        foreach ($vars as $name => $val) {
            if (is_numeric($name)) {
                $name = $val;
                $val = setting($name);
            }

            $names[] = "[[{$name}]]";
            $replace[] = $val;
        }

        return str_replace($names, $replace, $str);
    }
}
