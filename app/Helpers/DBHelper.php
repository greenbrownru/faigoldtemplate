<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class DBHelper
{
    public static function isPgsqlMain(Model $model) {
        return $model->getConnectionName() === 'pgsql_main';
    }

    public static function isNotPgsqlMain(Model $model) {
        return ! static::isPgsqlMain($model);
    }
}
