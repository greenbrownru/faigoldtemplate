<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdvertsImportXml extends Mailable
{
    use Queueable, SerializesModels;

    private $success;

    /**
     * Create a new message instance.
     *
     * @param bool $success
     */
    public function __construct($success = false)
    {
        $this->success = $success;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.import_xml_adverts', ['success' => $this->success])->subject(($this->success ? 'Да' : 'Нет').'. Обновление каталога.');
    }
}
