<?php

namespace App\Mail;

use App\Models\Subscribe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscribeApprove extends Mailable
{
    use Queueable, SerializesModels;

    private $subscribe;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscribe $subscribe)
    {
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.subscribe_approve', ['subscribe' => $this->subscribe])->subject('Подтверждение подписки');
    }
}
