<?php

namespace App\Mail;

use App\Models\Advert;
use App\Models\Subscribe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SubscribesAdverts extends Mailable
{
    use Queueable, SerializesModels;

    private $adverts;

    private $subscribe;

    /**
     * Create a new message instance.
     *
     * @param Collection $adverts
     * @param Subscribe $subscribe
     */
    public function __construct(Collection $adverts, Subscribe $subscribe)
    {
        $this->adverts = $adverts;
        $this->subscribe = $subscribe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.subscribe_adverts', ['adverts' => $this->adverts, 'subscribe' => $this->subscribe])->subject('Новые объявления');
    }
}
