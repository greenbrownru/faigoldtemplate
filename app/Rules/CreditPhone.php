<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CreditPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return true;

        /*
        $arr = str_split($value);

        if($arr[1] == 8){
            return false;
        }

        if(in_array($arr[2], [1,2,5,6,7])){
            return false;
        }

        return true;
        */
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Phone err.';
    }
}
