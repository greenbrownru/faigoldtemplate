$(function () {

    let screenWidth = screen.width;
    let screenHeight = screen.height;
    let halfScreenHeight = screenHeight / 2;

    /*
     *  Header fixed
     */

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.header').addClass('scroll');
        } else {
            $('.header').removeClass('scroll');
        }
    });

    /*
     *  Dropdown menu
     */

    /*
    $('.card-grid').click(function (e) {
        e.preventDefault;
        $('.dropdown__second').fadeIn();
        $('.wrap').addClass('active');
        $('body').addClass('active');
        $('.banner').addClass('active');
        $('.header').addClass('active');
        $('.banner__btn').addClass('active');
        $('.banner__inner').addClass('active');
        $('.brand-links__more').addClass('active');
        $('.section-title').addClass('active');
        $('.card-grid').addClass('active');
        $('.review').addClass('active');
    });
    */

    $('.dropdown__close__second').click(function (e) {
        e.preventDefault;
        $('.dropdown__second').fadeOut();
        $('.wrap').removeClass('active');
        $('body').removeClass('active');
        $('.banner').removeClass('active');
        $('.header').removeClass('active');
        $('.banner__btn').removeClass('active');
        $('.banner__inner').removeClass('active');
        $('.brand-links__more').removeClass('active');
        $('.section-title').removeClass('active');
        $('.card-grid').removeClass('active');
        $('.review').removeClass('active');
    });

    $('.link-dropdown').click(function (e) {
        e.preventDefault;
        $('.dropdown').fadeIn();
    });

    $('.dropdown__close').click(function (e) {
        e.preventDefault;
        $('.dropdown').fadeOut();
    });

    $('.second_link-dropdown').hover(
        function () {
            $('.third__dropdown').fadeIn();
        },
        function (e) {
            e.preventDefault;
            $('.third__dropdown').fadeOut();
        },
        200);

    /*
     *  Section title animation
     */

    function animationSectionTitle(section) {
        if ($(section).length) {

            let offsetTopReviews = $(section).offset().top;
            let scrollReviews = offsetTopReviews - halfScreenHeight;

            if (offsetTopReviews < halfScreenHeight) {
                $(section).find('.section-title .grey').addClass('scroll');
                setTimeout(function () {
                    $(section).find('.section-title .blue').addClass('scroll');
                }, 1000);
            }

            $(window).scroll(function () {
                if ($(this).scrollTop() > scrollReviews) {
                    $(section).find('.section-title .grey').addClass('scroll');
                    setTimeout(function () {
                        $(section).find('.section-title .blue').addClass('scroll');
                    }, 1000);
                }
            });
        }
    }

    animationSectionTitle('#js-vip');
    animationSectionTitle('#js-spec');
    animationSectionTitle('#js-new');
    animationSectionTitle('#js-reviews');
    animationSectionTitle('#js-catalog');
    animationSectionTitle('#js-similar');
    animationSectionTitle('#js-desc');
    animationSectionTitle('#js-promo');
    animationSectionTitle('#js-page');

    /*
     *  Card gallery
     */

    $('.card-gallery__item').hover(
            function () {
                $(this).addClass('hover').siblings('.card-gallery__item').removeClass('hover');
            },
            function () {
                $(this).removeClass('hover');
                $('.card-gallery__item:first-child').addClass('hover');
            });

    /*
     * Mobile menu
     */

    $('.menu-toggle').click(function () {
        $('#js-menu').addClass('open');
        $('body').addClass('noscroll');
    });

    $('.brands-toggle').click(function () {
        $('#js-brands').addClass('open');
        $('body').addClass('noscroll');
    });

    $('.mobile-menu__close').click(function () {
        $(this).parent('.mobile-menu').removeClass('open');
        $('body').removeClass('noscroll');
    });


    /*
     *  Carousels
     */


    if (screenWidth < 576) {
        $('.carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            autoplay: false,
            speed: 800,
            prevArrow: '',
            nextArrow: '<button class="next" type="button"></button>'
        });
    }

    if (screenWidth < 767) {
        $('.advant__row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            autoplay: false,
            speed: 800,
            prevArrow: '<button class="prev" type="button"></button>',
            nextArrow: '<button class="next" type="button"></button>'
        });
    }

    if (screenWidth < 767) {
        $('.steps__row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            autoplay: false,
            speed: 800,
            prevArrow: '<button class="prev" type="button"></button>',
            nextArrow: '<button class="next" type="button"></button>'
        });
    }

    if (screenWidth < 767) {
        $('.partners__row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            autoplay: false,
            speed: 800,
            prevArrow: '<button class="prev" type="button"></button>',
            nextArrow: '<button class="next" type="button"></button>'
        });
    }

    /*
     *  Select
     */

    function delay(fn, ms) {
        let timer = 0
        return function(...args) {
            clearTimeout(timer)
            timer = setTimeout(fn.bind(this, ...args), ms || 0)
        }
    }
    if (screenWidth > 575) {
        $('.catalog-list__col').css({"display": "block"});
        $('.select').click(function () {
            if($(this).attr('id') == 'select_model') {
                if($('input[name="credit_brand"]').val() === "") {
                    return false;
                }
            }

            if($(this).attr('id') == 'select_advert') {
                if($('input[name="credit_model"]').val() === "") {
                    return false;
                }
            }

            if($(this).attr('id') == 'select_client_model') {
                if($('input[name="client_brand"]').val() === "") {
                    return false;
                }
            }

            if($(this).attr('id') == 'select_buy_model') {
                if($('input[name="client_brand"]').val() === "") {
                    return false;
                }
            }

            var thisEl = $(this);
            thisEl.addClass('active');
            thisEl.find('.select__list').slideToggle();

            $('.select').each(function(){
                var thisEl2 = $(this);

                if(thisEl2.hasClass('active')){
                    thisEl2.removeClass('active');
                } else{
                    thisEl2.find('.select__list').slideUp(300);
                }
            });
        });

        $(document).mouseup(function (e){
            var div = $(".select");
            if(!div.is(e.target) && div.has(e.target).length === 0) {
                div.find('.select__list').slideUp(300);
            }
        });

        $(document).on('blur', '.input-filter', function () {
            if($(this).val() !== '') {
                $(this).css("box-shadow", "0 0 5px 0 rgba(53,123,181,.6)");
            }
        })

        //Событие изменения текстовых полей

        $(document).on('keyup', '.select__input', delay(function (e) {
            console.log('testtt');
            var form = $('#catalog_form');
            var formData = form.serialize();

            $(this).val($(this).val().replace (/\D/, ''));

            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData+'&is_cnt=true',
                dataType: 'html',
                success: function (data) {

                    $('.filter__btn').html(data);
                    let cnt = data.replace(/\D/g, "");
                    if(cnt !== "0" && data !== "Нет предложений") {
                        $('.filter__btn').removeAttr('disabled');
                        $('.filter__btn').removeAttr('style');
                    } else {
                        $('.filter__btn').attr('disabled', 'disabled');
                        $('.filter__btn').css('background-color', 'gray');
                    }
                    if($("input[name='model']").val() !== "") {
                        $('.models__list').hide();
                    } else {
                        $('.models__list').show();
                    }
                },
                error: function (error) {
                }
            });


        }, 500));

        $(document).on('click', '.select__list li', function (e) {

            let clearFilter = "";

            if(!$(this).closest('.select').hasClass('js-search')) {
                clearFilter = "<span class='fa fa-close clear-filter' style='position: absolute; right: 10px; color: grey'></span>";
            }
            $(this).parent().siblings('.select__input').html($(this).html()+clearFilter)
                .css("box-shadow", "0 0 5px 0 rgba(53,123,181,.6)")
                .siblings('.select__label')
                .hide();
            $(this).parent().siblings('input').val($(this).attr('data-value'));
            if($(this).attr('data-value') === "") {
                $(this).parent().siblings('.select__input').css("box-shadow", "");
            }
            $(this).parent().slideUp();

            let filterType = $(this).attr('class');

            let id = $(this).attr('data-value');
            let data = {}
            let action_type = 'models';
            let elementModel = $('#js-model .select__list');
            let elementGeneration = $('#js-generation .select__list');
            let li_class = 'li-model'
            if(filterType === 'li-brand') {
                $('input[name="model"]').val('');
                $('input[name="generation"]').val('');
                 elementModel = $('#js-model .select__list');
                 data = {"brand_id" : id}
                $(elementModel).html('<li data-value="">Все модели</li>');
                $(elementModel).parent().find('.select__input').html(' Модели');
                $(elementGeneration).parent().find('.select__input').html(' Поколение');
                $('#js-generation .select__list').html('<li data-value="">Все Поколения</li>');

            } else if(filterType === 'li-model') {
                $('input[name="generation"]').val('');
                 elementModel = $('#js-generation .select__list');
                 data = {"model_id" : id}
                 action_type = 'generations';
                 li_class = 'li-generation';
                $(elementModel).parent().find('.select__input').html(' Поколение');
            } else if(filterType === 'li-generation') {

            }
            var form = $('#catalog_form');
            var formData = form.serialize();
            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData+'&is_cnt=true',
                dataType: 'html',
                success: function (data) {
                    $('.filter__btn').html(data);
                    let cnt = data.replace(/\D/g, "");
                    if(cnt !== "0" && data !== "Нет предложений") {
                        $('.filter__btn').removeAttr('disabled');
                        $('.filter__btn').removeAttr('style');
                    } else {
                        $('.filter__btn').attr('disabled', 'disabled');
                        $('.filter__btn').css('background-color', 'gray');
                    }
                    if($("input[name='model']").val() !== "") {
                        $('.models__list').hide();
                    } else {
                        $('.models__list').show();
                    }
                },
                error: function (error) {
                }
            });

            if(filterType === 'li-brand' || filterType === 'li-model' || filterType === 'li-generation') {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-'+action_type+'-filter',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (data) {

                        setTimeout(function(){
                            let content = '<li data-value="">--</li>'
                            $(data).each(function (index, value) {
                                content += '<li class="'+li_class+'" data-value="' + value.id + '">' + value.name + '</li>'
                            });

                            $(elementModel).html(content);
                        },500);


                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            e.stopPropagation();
        });

        $(document).on('click', '.clear-filter', function (e) {
            let selectEl = $(this).closest('.select');
            let thisEl = $(this);
            $(selectEl).find('input').val('');
            $(selectEl).find('.select__label').fadeIn();
            $(thisEl).parent().html('');
            var form = $('#catalog_form');
            var formData = form.serialize();
            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData+'&is_cnt=true',
                dataType: 'html',
                success: function (data) {
                    $('.filter__btn').html(data);
                    let cnt = data.replace(/\D/g, "");
                    if(cnt !== "0" && data !== "Нет предложений") {
                        $('.filter__btn').removeAttr('disabled');
                        $('.filter__btn').removeAttr('style');
                    } else {
                        $('.filter__btn').attr('disabled', 'disabled');
                        $('.filter__btn').css('background-color', 'gray');
                    }
                    if($("input[name='model']").val() !== "") {
                        $('.models__list').hide();
                    } else {
                        $('.models__list').show();
                    }
                },
                error: function (error) {
                }
            });
            e.preventDefault();
            e.stopPropagation();
        });
    } else {
        $(document).on('keyup', '.select__input', delay(function (e) {
            var form = $('#catalog_form');
            var formData = form.serialize();

            $(this).val($(this).val().replace (/\D/, ''));

            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData+'&is_cnt=true',
                dataType: 'html',
                success: function (data) {

                    $('.filter__btn').html(data);
                    let cnt = data.replace(/\D/g, "");
                    if(cnt !== "0" && data !== "Нет предложений") {
                        $('.filter__btn').removeAttr('disabled');
                        $('.filter__btn').removeAttr('style');
                    } else {
                        $('.filter__btn').attr('disabled', 'disabled');
                        $('.filter__btn').css('background-color', 'gray');
                    }
                    if($("input[name='model']").val() !== "") {
                        $('.models__list').hide();
                    } else {
                        $('.models__list').show();
                    }
                },
                error: function (error) {
                }
            });


        }, 500));

        $('.select').click(function () {
            $(this).find('.select__list').fadeIn();
        });

        $(document).on('click', '.select__list li:not(.select__header)', function (e) {
            $(this).addClass('selected').siblings('li').removeClass('selected');
            $(this).parent().siblings('.select__input').html($(this).html()).css("box-shadow", "0 0 5px 0 rgba(53,123,181,.6)");
            $(this).parent().siblings('input').val($(this).attr('data-value'));
            if($(this).attr('data-value') === "") {
                $(this).parent().siblings('.select__input').css("box-shadow", "");
            }
            $(this).parent().fadeOut();
            let filterType = $(this).attr('class').substr(0,$(this).attr('class').indexOf(' '));
            let id = $(this).attr('data-value');
            let data = {}
            let action_type = 'models';
            let elementModel = $('#js-model .select__list');
            let li_class = 'li-model'
            if(filterType === 'li-brand') {
                elementModel = $('#js-model .select__list');
                data = {"brand_id" : id}
                $(elementModel).html('<li data-value=""><span class="select__close">Отмена</span> Все модели</li>');
                $(elementModel).parent().find('.select__input').html(' Модели');
                $('#js-generation .select__list').html('<li data-value=""><span class="select__close">Отмена</span> Все Поколения</li>');

            } else if(filterType === 'li-model') {
                elementModel = $('#js-generation .select__list');
                data = {"model_id": id}
                action_type = 'generations';
                li_class = 'li-generation';
            }

            var form = $('#catalog_form');
            var formData = form.serialize();

            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: formData+'&is_cnt=true',
                dataType: 'html',
                async: false,
                success: function (data) {
                    $('.filter__btn').html(data);
                    let cnt = data.replace(/\D/g, "");
                    if(cnt !== "0" && data !== "Нет предложений") {
                        $('.filter__btn').removeAttr('disabled');
                        $('.filter__btn').removeAttr('style');
                    } else {
                        $('.filter__btn').attr('disabled', 'disabled');
                        $('.filter__btn').css('background-color', 'gray');
                    }
                    if($("input[name='model']").val() !== "") {
                        $('.models__list').hide();
                    } else {
                        $('.models__list').show();
                    }
                },
                error: function (error) {
                }
            });

            if(filterType === 'li-brand' || filterType === 'li-model') {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/credit/ajax-'+action_type+'-filter',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        let content = '';
                        if(filterType === 'li-brand') {
                            content +='<li class="select__header"><span class="select__close">Отмена</span>Модель</li><li data-value="">--</li>';

                        } else {
                            content +='<li data-value="">--</li>';
                        }

                        $(data).each(function (index, value) {
                            content += '<li class="'+li_class+'" data-value="' + value.id + '">' + value.name + '</li>'
                        });

                        $(elementModel).html(content);

                        if(filterType === 'li-brand') {
                            if(screenWidth <= 575) {

                                $('.select.js-search').each(function () {
                                    var thisEl = $(this);
                                    var list = thisEl.find('.select__list');
                                    var hdr = list.find('.select__header');
                                    var ttl = thisEl.find('.select__label').html();

                                    thisEl.find('.select__close').next().remove();

                                    if (ttl == ' Марка') {
                                        var ttl = 'Поиск марки';
                                    } else if (ttl == ' Модели') {
                                        // alert(ttl);
                                        var ttl = 'Поиск модели';
                                    }

                                    hdr.append('<div class="form-group"><input class="js-search-input" type="text" placeholder="' + ttl + '" /></div>');
                                });
                            }
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            e.stopPropagation();
        });

        $(document).on('click', '.select__close', function (e) {

            $(this).parents('.select__list').fadeOut();
            var thisEl = $(this);
            if(thisEl.parents('.select:first').hasClass('js-search')){
                var parentEl = thisEl.parents('.js-search');
                parentEl.find('.js-search-input').val('');
                parentEl.find('[data-value]').each(function(){
                    $(this).css('display', 'block');
                });
            } else {
                let selectEl = $(this).closest('.select');
                let thisEl = $(this);
                $(selectEl).find('input').val('');
                $(selectEl).find('.select__input').html('');
                var form = $('#catalog_form');
                var formData = form.serialize();
                $.ajax({
                    url: form.attr("action"),
                    type: form.attr("method"),
                    data: formData+'&is_cnt=true',
                    dataType: 'html',
                    success: function (data) {
                        $('.filter__btn').html(data);
                        let cnt = data.replace(/\D/g, "");
                        if(cnt !== "0" && data !== "Нет предложений") {
                            $('.filter__btn').removeAttr('disabled');
                            $('.filter__btn').removeAttr('style');
                        } else {
                            $('.filter__btn').attr('disabled', 'disabled');
                            $('.filter__btn').css('background-color', 'gray');
                        }
                        if($("input[name='model']").val() !== "") {
                            $('.models__list').hide();
                        } else {
                            $('.models__list').show();
                        }
                    },
                    error: function (error) {
                    }
                });
                e.preventDefault();
                e.stopPropagation();
            }
            e.stopPropagation();
        });

    }

    /*
     * Filter models
     */

    if (screenWidth > 575) {
        $('#js-brand .select__list li').on('click', function () {

            var _this = $(this);

            var link = ($(this).attr('data-target'));
            $('.models__section').hide();
            $('.brands__section').hide();

            $(link === '#js-brand0' ? '.brands__section' : link).fadeIn();

            var links = $(link).find('a');

            /*
            $.each(links, function(i, a){
                var re = /(.*)\/(.*)\/(.*)/;
                var new_href = $(a).attr('href').replace(re, "$1/" + _this.text().toLowerCase() + "/$3");
                $(a).attr('href', new_href);
            })

             */

        });
    }

    /*
     *  Filter reset
     */

    if (screenWidth > 575) {
        $('.filter__reset').click(function () {
            $(this).parents('form').find('.select__input').html('')
                    .siblings('.select__label').fadeIn()
                    .siblings('input').val('');
            $('.models__section').hide();
        });
    } else {

        $('#js-brand .select__label').html('Марка');
        $('#js-model .select__label').html('Модели');

        $('.filter__reset').click(function () {
            $(this).parents('form').find('.select__input').html('')
                    .siblings('.select__label').fadeIn()
                    .siblings('input').val('');
            $('.select__list li:not(.select__header)').removeClass('selected');
        });
    }

    /*
     *  Filter mobile
     */

    $('.filter-btn').click(function () {
        $('.filter').addClass('open');
        $('body').addClass('noscroll');
    });

    $('.filter__back').click(function () {
        $('.filter').removeClass('open');
        $('body').removeClass('noscroll');
    });

    if(screenWidth <= 575) {
        $('.product').css({"background-color": "#ffff"})
        $('.catalog-list__col').css({"display": "none"});

        $('.select.js-search').each(function(){
            var thisEl = $(this);
            var list = thisEl.find('.select__list');
            var hdr = list.find('.select__header');
            var ttl = thisEl.find('.select__label').html();

            thisEl.find('.select__close').next().remove();

            if(ttl == ' Марка'){
                var ttl = 'Поиск марки';
            } else
            if(ttl == ' Модели'){
                var ttl = 'Поиск модели';
            }

            hdr.append('<div class="form-group"><input class="js-search-input" type="text" placeholder="' + ttl + '" /></div>');
        });

        $(document).on('keyup', '.js-search-input', function(){
            var thisEl = $(this);
            var thisVal = thisEl.val().toLowerCase();
            var parentEl = thisEl.parents('.select__list:first');

            parentEl.find('[data-value]').each(function(){
                let thisEl2 = $(this);
                let thisHtml = thisEl2.html().toLowerCase();

                var result = thisHtml.match(thisVal);
                if(result){
                    thisEl2.css('display', 'block');
                } else{
                    thisEl2.css('display', 'none');
                }
            });
        });
    }

    /*
     *  Сортировка в мобилах
     */

    if (screenWidth < 992) {
        $('#js-sort li.current a').click(function (e) {
            e.preventDefault();
            $(this).parent('li').siblings('li').slideToggle();
        });
    }

    /*
     *  Gallery
     */

    const gallery = $('.gallery__img .gallery__item');
    const quantityImages = gallery.length;
    let counter = 0;

    $('.gallery__img .next').click(function () {
        counter++;
        gallery.hide();

        if (counter < quantityImages) {
            gallery[counter].style.display = 'block';
        } else {
            gallery[0].style.display = 'block';
            counter = 0;
        }
    });

    $('.gallery__img .prev').click(function () {
        counter--;
        gallery.hide();

        if (counter >= 0) {
            gallery[counter].style.display = 'block';
        } else {
            gallery[quantityImages - 1].style.display = 'block';
            counter = quantityImages - 1;
        }
    });

    $('.gallery-thumbs__item').hover(
            function () {
                let indexImg = $(this).attr('data-index');
                gallery.hide();
                gallery[indexImg].style.display = 'block';
                counter = indexImg;
            },
            function () {

            },
            200);

    $("form input[name='name']").keypress(function(event){
        var regex = new RegExp("^[a-zA-Zа-яА-Я ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    /*
     *  Телефон в форме
     */

    $('.phone').intlTelInput({
        defaultCountry: 'ru',
        preferredCountries: ['ru'],
        separateDialCode: true,
    });

    $(document).on('paste', '.phone', function (e) {
        var clipboardData = e.originalEvent.clipboardData || window.clipboardData;
        var phone = (clipboardData.getData('Text') || '').replace(/\D/g, '');

        if (/^89/.test(phone)) {
            phone = phone.substr(1);
            $(this).val(phone);
        }
    }).on('input', '.phone', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);
        if(x[1] == 89) {
            x[1] = 9;
        }
        if(x[1] == 1 || x[1] == 2 || x[1] == 5 || x[1] == 6 || x[1] == 7) {
            x[1] = '';
        }
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '')+(x[4] ? '-' + x[4] : '');
    });

    $(document).on('mouseover', '.brand-links__item', function () {
        $(this).find('p').css({"color": "red"});
    });

    $(document).on('mouseout', '.brand-links__item', function () {
          $(this).find('p').css({"color": "#ababab"});
    });

    $(document).on('mouseover', '.models__list li', function () {
        $(this).find('a').css({"color": "red"});
    });

    $(document).on('mouseout', '.models__list li', function () {
        $(this).find('a').css({"color": "#000"});
    });

    /*
     *  Slider
     */

    //alert($('#slider-initial-fee').slider("value"));



    $("#slider-loan-terms").slider({
        value: 60,
        min: 1,
        max: 60,
        range: "min",
        slide: function (event, ui) {
            $("#loan-terms").val(ui.value + ' мес');
            $("#loan-terms-val").val(ui.value);
            var percent = $('#hidden_percent').val();
            var pay = ((($('#hidden_price').text() - (($("#slider-initial-fee").slider("value") / 100) * $('#hidden_price').text())).toFixed(0) / $("#loan-terms-val").val()));
            var pay_end = (pay + (percent / 100) * pay).toFixed(0);
            $("#credit_pay").text(formatPrice(pay_end));
        }
    });

    $("#loan-terms").val($("#slider-loan-terms").slider("value") + ' мес');

    $("#slider-initial-fee").slider({
        value: 10,
        min: 0,
        max: 100,
        range: "min",
        slide: function (event, ui) {
            $("#initial-fee").val(ui.value + '%');
            var percent = $('#hidden_percent').val();
            var pay = ((($('#hidden_price').text() - ((ui.value / 100) * $('#hidden_price').text())).toFixed(0) / $("#loan-terms-val").val()));
            var pay_end = (pay + (percent / 100) * pay).toFixed(0);
            $("#credit_pay").text(formatPrice(pay_end));
        }
    });
    $("#initial-fee").val($("#slider-initial-fee").slider("value") + '%');
    var percent = $('#hidden_percent').val();
    var pay = ((($('#hidden_price').text() - (($("#slider-initial-fee").slider("value") / 100) * $('#hidden_price').text())).toFixed(0) / $("#loan-terms-val").val()));
    var pay_end = (pay + (percent / 100) * pay).toFixed(0);
    $("#credit_pay").text(formatPrice(pay_end));



    $('.brand-links__more').click(function (e) {
        e.preventDefault();
        $('.brand-links__item').removeClass('hidden');
        $(this).hide();
    });

    $('.more_info_btn').click(function (e) {
        e.preventDefault();
        $('.product-desc__col').removeClass('hidden');
        $(this).hide();
    });

    $('.main_slider').owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
    });

    $('.reviews__row').owlCarousel({
        loop: true,
        margin: 30,
        items:3,
        responsiveClass: true,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1,
            },
            620:{
                items:2,
            },
            993:{
                items:3,
            }
        }
    });

    $('.product-card__main-price').click(function (e) {

        e.preventDefault();
        $(this).toggleClass('active');
        $('.product-card__tooltip').stop().slideToggle();
    });


    function refreshCatalogFilterBtnText() {
        var $form = $('#catalog_form');
        var formData = $form.serialize();
        $.ajax({
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: formData+'&is_cnt=true',
            dataType: 'html',
            success: function (data) {
                $('.filter__btn').html(data);
                let cnt = data.replace(/\D/g, "");
                if(cnt !== "0" && data !== "Нет предложений") {
                    $('.filter__btn').removeAttr('disabled');
                    $('.filter__btn').removeAttr('style');
                } else {
                    $('.filter__btn').attr('disabled', 'disabled');
                    $('.filter__btn').css('background-color', 'gray');
                }
                if($("input[name='model']").val() !== "") {
                    $('.models__list').hide();
                } else {
                    $('.models__list').show();
                }
            },
            error: function (error) {
            }
        });
    }

    (function() {
        var $brandWrap = $('#js-brand-select')
        var $modelWrap = $('#js-model-select')
        var $generationWrap = $('#js-generation-select')

        $brandWrap.find('select').selectize({
            plugins: ['remove_button'],
            onInitialize: function() {
                var _this = this;
                setTimeout(function() {
                //     _this.trigger('change', _this.$input.val());
                    var modelSelectize = $modelWrap.find('select').data('selectize');
                    var generationSelectize = $generationWrap.find('select').data('selectize');

                    (!_this.$input.val() || !Object.keys(modelSelectize.options).length) && modelSelectize.disable();
                    (!modelSelectize.$input.val() || !Object.keys(generationSelectize.options).length) && generationSelectize.disable();
                }, 1);
            },
            onChange: function(val) {
                refreshCatalogFilterBtnText();

                var modelSelectize = $modelWrap.find('select').data('selectize');
                var generationSelectize = $generationWrap.find('select').data('selectize');
                var withoutAjax = modelSelectize.$input.data('withoutAjax') !== undefined;

                modelSelectize.disable();
                modelSelectize.clearOptions();
                generationSelectize.disable();
                generationSelectize.clearOptions();

                if (!val) {
                    return;
                }

                // modelSelectize.enable();
                // var models = _BRAND_LINKS_DATA[val] || []


                if (withoutAjax) {
                    modelSelectize.$input.removeAttr('data-without-ajax').removeData('withoutAjax');
                    modelSelectize.trigger('change', modelSelectize.$input.val());
                } else {
                    modelSelectize.load(function(callback) {
                        $.ajax({
                            url: '/credit/ajax-models-filter',
                            type: 'post',
                            data: { brand_id: val },
                            dataType: 'json',
                            success: function(data) {
                                callback(data);

                                if (data.length) {
                                    modelSelectize.enable();
                                    modelSelectize.refreshOptions();
                                }
                            },
                            error: function(error) {
                                callback();
                            }
                        });
                    });
                }

                // modelSelectize.clearOptions();
                // for (var i in models) {
                //     modelSelectize.addOption({
                //         text: models[i].name,
                //         value: models[i].model_id,
                //     });
                // }
                // modelSelectize.refreshOptions()
            }
        });

        $modelWrap.find('select').selectize({
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'name',
            searchField: ['name'],
            onChange: function(val) {
                refreshCatalogFilterBtnText();

                var generationSelectize = $generationWrap.find('select').data('selectize')
                var withoutAjax = generationSelectize.$input.data('withoutAjax') !== undefined;

                generationSelectize.disable();
                generationSelectize.clearOptions();

                if (!val) {
                    return;
                }

                // generationSelectize.enable();

                if (withoutAjax) {
                    generationSelectize.$input.removeAttr('data-without-ajax').removeData('withoutAjax');
                } else {
                    generationSelectize.load(function(callback) {
                        $.ajax({
                            url: '/credit/ajax-generations-filter',
                            type: 'post',
                            data: { model_id: val },
                            dataType: 'json',
                            success: function(data) {
                                callback(data);

                                if (data.length) {
                                    generationSelectize.enable();
                                    generationSelectize.refreshOptions();
                                }
                            },
                            error: function(error) {
                                callback();
                            }
                        });
                    });
                }
            }
        });

        $generationWrap.find('select').selectize({
            plugins: ['remove_button'],
            valueField: 'id',
            labelField: 'name',
            onInitialize: function() {
                this.$control_input.attr('readonly', true);
            },
            onChange: function() {
                refreshCatalogFilterBtnText();
            }
        });

        $('.js-year-select select').selectize({
            plugins: ['remove_button'],
            onChange: function() {
                refreshCatalogFilterBtnText();
            }
        });

        $('#js-transmission-select select, #js-car_drive-select select, #js-car_body-select select, #js-engine-select select').selectize({
            plugins: ['remove_button'],
            onInitialize: function() {
                this.$control_input.attr('readonly', true);
            },
            onChange: function() {
                refreshCatalogFilterBtnText();
            }
        });
    })()
});
function formatPrice(price) {
    return price.reverse().replace(/((?:\d{2})\d)/g, '$1 ').reverse();
}

String.prototype.reverse = function() {
    return this.split('').reverse().join('');
}
