<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestOnCarServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_on_car_services', function (Blueprint $table) {
            $table->id();
            $table->string('brand_name');
            $table->string('model_name');
            $table->unsignedBigInteger('mileage');
            $table->string('name');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->json('additional_params')->nullable();
            $table->string('user_ip')->nullable();
            $table->integer('region_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_on_car_services');
    }
}
