<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestOnTradeInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_on_trade_in', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('brand_id');
            $table->bigInteger('model_id');
            $table->bigInteger('advert_id');
            $table->bigInteger('client_brand');
            $table->bigInteger('client_model');
            $table->string('name');
            $table->string('phone');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_on_trade_in');
    }
}
