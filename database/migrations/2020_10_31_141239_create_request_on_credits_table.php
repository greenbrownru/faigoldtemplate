<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestOnCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_on_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('brand_id');
            $table->bigInteger('model_id');
            $table->bigInteger('advert_id');
            $table->float('credit_term')->nullable();
            $table->float('down_payment')->nullable();
            $table->float('monthly_payment')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_on_credits');
    }
}
