<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAdditionalParams extends Migration
{

    protected $tables = [
        'request_in_contacts',
        'request_on_buys',
        'request_on_credits',
        'request_on_promos',
        'request_on_trade_in'
    ];

    public function up()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $columns = Schema::getColumnListing($tableName);

                in_array('additional_params', $columns) || $table->text('additional_params')->nullable();
                in_array('user_ip', $columns) || $table->string('user_ip')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $columns = Schema::getColumnListing($tableName);

                in_array('additional_params', $columns) && $table->dropColumn('additional_params');
                in_array('user_ip', $columns) && $table->dropColumn('user_ip');
            });
        }
    }
}
