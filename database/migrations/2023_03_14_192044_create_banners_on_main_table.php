<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners_on_main', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->json('options')->nullable();
            $table->string('link')->nullable();
            $table->boolean('is_button')->default(true);
            $table->string('button_name')->nullable();
            $table->string('button_action')->default('link');
            $table->string('img_url');
            $table->string('img_mobile_url')->nullable();
            $table->integer('sort')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banners_on_main');
    }
};
