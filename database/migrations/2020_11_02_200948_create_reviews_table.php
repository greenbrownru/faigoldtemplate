<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fio');
            $table->bigInteger('brand_id');
            $table->bigInteger('model_id');
            $table->integer('rate')->nullable();
            $table->text('review');
            $table->string('client_photo_name')->nullable();
            $table->string('auto_photo_name')->nullable();
            $table->string('video_link')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
