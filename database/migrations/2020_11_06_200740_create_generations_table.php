<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        {"id":2283,"name":"I","brandId":2,"categoryId":1,
         * "modelId":8,"autoruId":"5139648","yearFrom":1997,
         * "yearTo":2000,"isActive":true
         * },
         */
        Schema::create('generations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('brand_id');
            $table->bigInteger('category_id');
            $table->bigInteger('model_id');
            $table->bigInteger('autoru_id')->nullable();
            $table->integer('year_from');
            $table->integer('year_to');
            $table->boolean('is_active');
            $table->string('model_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generations');
    }
}
