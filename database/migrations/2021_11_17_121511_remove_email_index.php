<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveEmailIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $tables = [
        'subscribes'
    ];

    public function up()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $table->dropUnique('subscribes_email_unique');
                $columns = Schema::getColumnListing($tableName);
                in_array('email', $columns) || $table->string('email');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $table->string('email')->unique();
            });
        }
    }
}
