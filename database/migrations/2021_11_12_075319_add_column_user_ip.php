<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUserIp extends Migration
{
    protected $tables = [
        'subscribes'
    ];

    public function up()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $columns = Schema::getColumnListing($tableName);
                in_array('user_ip', $columns) || $table->string('user_ip')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $tableName) {
            Schema::table($tableName, function(Blueprint $table) use ($tableName) {
                $columns = Schema::getColumnListing($tableName);
                in_array('user_ip', $columns) && $table->dropColumn('user_ip');
            });
        }
    }
}
