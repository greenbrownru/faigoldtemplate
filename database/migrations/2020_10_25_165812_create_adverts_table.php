<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('brand_id');
            $table->bigInteger('model_id');
            $table->bigInteger('generation_id')->nullable();
            $table->bigInteger('modification_id')->nullable();
            $table->string('modification_name')->nullable();
            $table->integer('year_of_issue');
            $table->integer('mileage');
            $table->string('car_body');
            $table->string('transmission');
            $table->string('engine');
            $table->string('steering_wheel');
            $table->string('color');
            $table->string('car_drive');
            $table->integer('power');
            $table->float('volume');
            $table->integer('number_of_owners');
            $table->string('vin')->nullable();
            $table->float('price');
            $table->float('price_by_action')->nullable();
            $table->boolean('vin_verified')->default(true);
            $table->boolean('kasko_in_gift')->default(false);
            $table->boolean('rubber_kit')->default(false);
            $table->boolean('is_active')->default(true);
            $table->boolean('is_vip')->nullable()->default(false);
            $table->boolean('is_spec')->nullable()->default(false);
            $table->bigInteger('client_id')->nullable();
            $table->double('tradein_discount')->nullable();
            $table->double('credit_discount')->nullable();
            $table->double('insurance_discount')->nullable();
            $table->bigInteger('import_id')->nullable();
            $table->boolean('is_current')->default(false);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
