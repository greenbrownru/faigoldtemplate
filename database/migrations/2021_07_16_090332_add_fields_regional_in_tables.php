<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsRegionalInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_log', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('advert_equipments', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('advert_photos', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('adverts', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('contacts', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('failed_jobs', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('pages', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('password_resets', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('permission_role', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('promos', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('request_in_contacts', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('request_on_buys', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('request_on_credits', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('request_on_promos', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('request_on_trade_in', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('reviews', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('role_user', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('subscribes', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('region_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }
}
