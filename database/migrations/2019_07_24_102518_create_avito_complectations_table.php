<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvitoComplectationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avito_complectations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("complectation_id")->nullable();
            $table->integer("modification_id")->nullable();
            $table->integer("model_id")->nullable();
            $table->string("name");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avito_complectations');
    }
}
