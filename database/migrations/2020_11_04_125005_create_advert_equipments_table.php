<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_equipments', function (Blueprint $table) {
            $table->id();
            $table->integer('equipment_id');
            $table->bigInteger('advert_id');
            $table->string('text')->nullable();
            $table->boolean('is_autoru')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert_equipments');
    }
}
